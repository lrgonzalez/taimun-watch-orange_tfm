package com.orange.taimun_watch.es;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.model.Estrategia;

/**
 * Created by Yux on 05/06/2016.
 * Modified by Yux on 29/07/2016.
 */

public class StrategySecondTabFragment extends Fragment implements View.OnClickListener{
    private TextView txtTransition, txtTimeLabel, txtTimeEdit, txtTimerLabel, txtTimerValue, txtGlobalTime;
    private ImageButton btnTransition, btnTimeEdit, btnTimer;
    private Switch swithGlobal, switchQuestion, switchRefuerzo;
    private final int TRANSITION = 0;
    private final int TIMER = 1;
    private String[] opciones;
    private boolean isEdition = false;
    private long idEstrategia;
    private Fragment firstTab;
    private DatabaseAdapter db;
    private ImageButton btnTransitionBlurb, btnBlurbTiempo, btnBlurbTimer, btnBlurbGlobal,
                        btnComprobacionBlurb, btnRefuerzoBlurb;

    public StrategySecondTabFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        db = new DatabaseAdapter(getContext());
        firstTab = ((NewStrategyActivity)getActivity()).getFirstTabFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.strategy_second_tab_fragment, container, false);

        /*********************** Gestión de textos *********************/
        txtTransition = (TextView) view.findViewById(R.id.txtTransicion);
        txtTimeLabel = (TextView) view.findViewById(R.id.txtTimeLabel);
        txtTimeEdit = (TextView) view.findViewById(R.id.txtTimeEdit);
        txtTimerLabel = (TextView) view.findViewById(R.id.txtTimerLabel);
        txtTimerValue = (TextView) view.findViewById(R.id.txtTimerValue);
        txtGlobalTime = (TextView) view.findViewById(R.id.txtGlobalTime);

        /*********************** Gestión de textos *********************/
        btnTransition = (ImageButton) view.findViewById(R.id.imgTransition);
        btnTimeEdit = (ImageButton) view.findViewById(R.id.imgEditTime);
        btnTimer = (ImageButton) view.findViewById(R.id.imgTemporizador);
        btnComprobacionBlurb = (ImageButton) view.findViewById(R.id.blurb_comprobacion);
        btnRefuerzoBlurb = (ImageButton) view.findViewById(R.id.blurb_refuerzo);
        btnTransition.setOnClickListener(this);
        btnTimeEdit.setOnClickListener(this);
        btnTimer.setOnClickListener(this);

        btnTransitionBlurb = (ImageButton) view.findViewById(R.id.blurb_transition);
        btnBlurbTiempo = (ImageButton) view.findViewById(R.id.blurb_tiempo);
        btnBlurbTimer = (ImageButton) view.findViewById(R.id.blurb_timer);
        btnBlurbGlobal = (ImageButton) view.findViewById(R.id.blurb_global);

        btnComprobacionBlurb.setOnClickListener(this);
        btnRefuerzoBlurb.setOnClickListener(this);
        btnTransitionBlurb.setOnClickListener(this);
        btnBlurbTiempo.setOnClickListener(this);
        btnBlurbTimer.setOnClickListener(this);
        btnBlurbGlobal.setOnClickListener(this);


        /*********************** Gestión de switches *********************/
        swithGlobal = (Switch) view.findViewById(R.id.switchGlobal);
        switchQuestion = (Switch) view.findViewById(R.id.switchQuestion);
        switchRefuerzo = (Switch) view.findViewById(R.id.switchRefuerzo);

        //Comprobamos si es creación o edición de estrategia
        Intent intent = getActivity().getIntent();
        idEstrategia = intent.getLongExtra("strategyId", -1);

        if (idEstrategia > 0) {
            isEdition = true;
            getStrategySettings(idEstrategia);
        } else {
            disableTimeOptions();
            switchRefuerzo.setChecked(false);
            switchQuestion.setChecked(false);
        }

        return view;
    }

    /**
     * Método que recupera la configuración guardada de una estrategia
     */
    private void getStrategySettings(long idEstrategia) {
        String tipoTemp = "";
        int tempValue;
        db.open();
        Estrategia strategy = db.getStrategyInfo(idEstrategia);

        if (strategy != null) {
            //Configuramos transicion
            if (strategy.getTime() == 0) {
                selectOption(getString(R.string.touch), TRANSITION);

            } else if (strategy.getTime() > 0) {
                selectOption(getString(R.string.time), TRANSITION);
                txtTimeEdit.setText(strategy.getTime() + "s");
                tempValue = strategy.getTimerType();

                if (tempValue == -1) {
                    tipoTemp = getString(R.string.none);
                } else if (tempValue == 0) {
                    tipoTemp = getString(R.string.tempDonut);
                } else if (tempValue == 1) {
                    tipoTemp = getString(R.string.tempVaceado);
                } else if (tempValue == 2) {
                    tipoTemp = getString(R.string.tempLlenado);
                }
                selectOption(tipoTemp, TIMER);
            }

            //Configuramos valores del switch
            swithGlobal.setChecked(strategy.isGlobalTime());
            switchQuestion.setChecked(strategy.getQuestion());
            boolean hasReinforcement = strategy.isReinforcement();
            switchRefuerzo.setChecked(hasReinforcement);
        }

        db.close();

    }

    /**
     * Método que desactiva las opciones de tiempo
     */
    private void disableTimeOptions() {

        //Marcamos labels de campos en color desactivado
        txtTimeLabel.setTextColor(getResources().getColor(R.color.submenuBackground));
        txtTimerLabel.setTextColor(getResources().getColor(R.color.submenuBackground));
        txtGlobalTime.setTextColor(getResources().getColor(R.color.submenuBackground));

        //Asignamos texto indicando los valores desactivados
        txtTimeEdit.setText(getString(R.string.disable));
        txtTimerValue.setText(getString(R.string.disable));

        btnTimeEdit.setVisibility(View.INVISIBLE);
        btnTimer.setVisibility(View.INVISIBLE);

        swithGlobal.setEnabled(Boolean.FALSE);
    }

    /**
     * Método que activa las opciones de tiempo
     */
    private void enableTimeOptions() {
        //Marcamos labels de campos en color activo
        txtTimeLabel.setTextColor(getResources().getColor(R.color.textLabel));
        txtTimerLabel.setTextColor(getResources().getColor(R.color.textLabel));
        txtGlobalTime.setTextColor(getResources().getColor(R.color.textLabel));

        //Asignamos texto indicando los valores por defecto
        txtTimeEdit.setText(getString(R.string.time_default));
        txtTimerValue.setText(getString(R.string.none));

        //Habilitamos botones por defecto
        btnTimeEdit.setBackgroundResource(R.drawable.time_edit_icon);
        btnTimeEdit.setVisibility(View.VISIBLE);
        btnTimer.setBackgroundResource(R.drawable.block_icon_edit);
        btnTimer.setVisibility(View.VISIBLE);

        swithGlobal.setEnabled(Boolean.TRUE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgTransition:
                dialogSelect(TRANSITION);
                break;

            case R.id.imgTemporizador:
                dialogSelect(TIMER);
                break;

            case R.id.imgEditTime:
                changeTime();
                break;

            case R.id.blurb_comprobacion:
                showInfoBlurb(getString(R.string.checkQuestion), getString(R.string.blurbQuestion));
                break;

            case  R.id.blurb_refuerzo:
                showInfoBlurb(getString(R.string.refuerzo), getString(R.string.blurbRefuerzo));
                break;
            case R.id.blurb_transition:
                showInfoBlurb(getString(R.string.transition), getString(R.string.blurbTransicion));
                break;
            case R.id.blurb_tiempo:
                showInfoBlurb(getString(R.string.time), getString(R.string.blurbTiempo));
                break;

            case R.id.blurb_global:
                showInfoBlurb(getString(R.string.globalTime), getString(R.string.blurbGlobal));
                break;

            case R.id.blurb_timer:
                showInfoBlurb(getString(R.string.temporizador), getString(R.string.blurbTemporizador));
                break;
        }
    }

    /**
     * Funcion que muestra un Dialog con las información del info blurb correspondiente
     */
    public void showInfoBlurb(String title, String message) {
        AlertDialog.Builder constructor = new AlertDialog.Builder(getContext(), R.style.MyAlertDialogStyle);
        constructor.setTitle(title);
        constructor.setMessage(message);
        constructor.show();
    }

    /**
     * Funcion que muestra un Dialog con las opciones posibles
     * de donde un usuario puede agregar una foto
     */
    public void dialogSelect(final int idTipo) {
        //Opciones del dialog con iconos y texto
        final Integer[] iconsTransition = new Integer[] {R.drawable.touch_icon, R.drawable.time_icon};
        final String[] opcionesTransition = getResources().getStringArray(R.array.opciones_transicion);
        final Integer[] iconsTemp = new Integer[] {R.drawable.block_icon, R.drawable.donut_icon, R.drawable.llenado_icon, R.drawable.vaceado_icon};
        final String[] opcionesTemp = getResources().getStringArray(R.array.opciones_temporizador);
        ListAdapter adapter = null;

        AlertDialog.Builder constructor = new AlertDialog.Builder(getContext(), R.style.MyAlertDialogStyle);

        if (idTipo == TRANSITION) {
            adapter = new ArrayAdapterWithIcon(getContext(), opcionesTransition, iconsTransition);
            constructor.setTitle(R.string.transitionType);
            opciones = opcionesTransition;

        } else if (idTipo == TIMER) {
            adapter = new ArrayAdapterWithIcon(getContext(), opcionesTemp, iconsTemp);
            constructor.setTitle(R.string.temporizadorType);
            opciones = opcionesTemp;
        }

        constructor.setAdapter(adapter, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int item) {
                selectOption(opciones[item], idTipo);
            }
        }).show();
    }

    /**
     * Funcion que cambia la imagen y el texto segun la opcion seleccionada
     * en el dialog. Tambien bloquea o desbloquea las opciones de tiempo segun
     * corresponda
     */
    private void selectOption(String opcion, int idTipo) {
        if (idTipo == TRANSITION) {
            if (opcion.equals(getString(R.string.touch))) {
                btnTransition.setBackgroundResource(R.drawable.touch_icon_edit);
                disableTimeOptions();

            } else if (opcion.equals(getString(R.string.time))) {
                btnTransition.setBackgroundResource(R.drawable.time_icon_edit);
                enableTimeOptions();
            }
            txtTransition.setText(opcion);

        } else if (idTipo == TIMER) {
            if (opcion.equals(getString(R.string.none))) {
                btnTimer.setBackgroundResource(R.drawable.block_icon_edit);

            } else if (opcion.equals(getString(R.string.tempDonut))) {
                btnTimer.setBackgroundResource(R.drawable.donut_icon_edit);

            } else if (opcion.equals(getString(R.string.tempLlenado))) {
                btnTimer.setBackgroundResource(R.drawable.llenado_icon_edit);

            } else if (opcion.equals(getString(R.string.tempVaceado))) {
                btnTimer.setBackgroundResource(R.drawable.vaceado_icon_edit);
            }
            txtTimerValue.setText(opcion);
        }
    }

    /**
     * Funcion que muestra un Dialog con la opcion de introducir
     * el tiempo de duracion de la estrategia
     */
    private void changeTime() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View numberPickerView = inflater.inflate(R.layout.number_picker, null);

        final NumberPicker seconds = (NumberPicker) numberPickerView.findViewById(R.id.seconds_picker);
        seconds.setMinValue(1);
        seconds.setMaxValue(420);
        int indexEnd = txtTimeEdit.getText().length() - 1;
        String value = txtTimeEdit.getText().toString().substring(0, indexEnd);
        seconds.setValue(Integer.valueOf(value));

        AlertDialog.Builder constructor = new AlertDialog.Builder(getContext(), R.style.MyAlertDialogStyle);
        constructor.setTitle(getString(R.string.time));

        constructor.setView(numberPickerView);
        constructor.setPositiveButton(getString(R.string.accept),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        txtTimeEdit.setText(seconds.getValue() + "s");
                    }
                });

        constructor.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { }
        });

        AlertDialog helpDialog = constructor.create();
        helpDialog.show();
    }

    public boolean isChange() {

        if (isEdition == false) {
            return true;
        }

        int tiempo = 0, tipoTemporizador = -1;
        boolean globalTime = false, isIconoChange = true;
        String icono, iconoAntiguo;

        db.open();
        Estrategia strategy = db.getStrategyInfo(idEstrategia);

        if (strategy == null) {
            return true;
        }

        icono = ((StrategyFirstTabFragment)firstTab).getRutaIcono();
        iconoAntiguo = db.getStrategyRutaIcono(idEstrategia);

        if(icono == null) {
            if(iconoAntiguo == null) {
                isIconoChange = false;
            }
        } else {
            if(icono.equals(iconoAntiguo)) {
                isIconoChange = false;
            }
        }

        if (txtTransition.getText().equals(getString(R.string.time))) {
            //Valor de tiempo
            int indexEnd = txtTimeEdit.getText().length() - 1;
            String valueTime = txtTimeEdit.getText().toString().substring(0, indexEnd);
            tiempo = Integer.parseInt(valueTime);

            //Valor de temporizador
            if (txtTimerValue == null || txtTimerValue.getText().equals(getString(R.string.none))) {
                tipoTemporizador = -1;
            } else if (txtTimerValue.getText().equals(getString(R.string.tempDonut))) {
                tipoTemporizador = 0;
            } else if (txtTimerValue.getText().equals(getString(R.string.tempVaceado))) {
                tipoTemporizador = 1;
            } else if (txtTimerValue.getText().equals(getString(R.string.tempLlenado))) {
                tipoTemporizador = 2;
            }

            if (swithGlobal.isChecked()) {
                globalTime = true;
            }
        }

        if (strategy.getTime() != tiempo || strategy.getTimerType() != tipoTemporizador ||
                strategy.isGlobalTime() != globalTime ||
                strategy.getQuestion() != switchQuestion.isChecked() ||
                strategy.isReinforcement() != switchRefuerzo.isChecked() ||
                isIconoChange ||
                !((StrategyFirstTabFragment)firstTab).getStrategyName().equals(db.getStrategyStoredName(idEstrategia))) {
            return true;
        }

        db.close();

        return false || ((StrategyFirstTabFragment)firstTab).isChangeCards();
    }

    /**
     * Funcion que guarda una estrategia
     * Primero guarda los parámetros de configuración y luego llama a otro método
     * para guardar los recurso
     */
    public long guardarEstrategia() {
        int tiempo = 0, tipoTemporizador = -1;
        boolean globalTime = false;
        int checkUpdate = 0;

        //Recuperamos nombre y ruta del icono del primer fragmento
        String strategyName = ((StrategyFirstTabFragment)firstTab).getStrategyName();
        String rutaIcono = ((StrategyFirstTabFragment)firstTab).getRutaIcono();

        //Comprobamos si se han añadido tarjetas válidas
        if (!((StrategyFirstTabFragment)firstTab).checkValidCards()) {
            Toast.makeText(getContext(), getContext().getString(R.string.errorEstrategiaSinTarjetas), Toast.LENGTH_SHORT).show();
            return -1;
        }

        if (txtTransition.getText().equals(getString(R.string.touch))) {
            tiempo = 0;
            tipoTemporizador = -1;
            globalTime = false;

        } else if (txtTransition.getText().equals(getString(R.string.time))) {
            //Valor de tiempo
            int indexEnd = txtTimeEdit.getText().length() - 1;
            String valueTime = txtTimeEdit.getText().toString().substring(0, indexEnd);
            tiempo = Integer.parseInt(valueTime);

            //Valor de temporizador
            if (txtTimerValue == null || txtTimerValue.getText().equals(getString(R.string.none))) {
                tipoTemporizador = -1;
            } else if (txtTimerValue.getText().equals(getString(R.string.tempDonut))) {
                tipoTemporizador = 0;
            } else if (txtTimerValue.getText().equals(getString(R.string.tempVaceado))) {
                tipoTemporizador = 1;
            } else if (txtTimerValue.getText().equals(getString(R.string.tempLlenado))) {
                tipoTemporizador = 2;
            }

            if(swithGlobal.isChecked()) {
                globalTime = true;
            }
        }

        //Guardamos estrategia
        DatabaseAdapter db = new DatabaseAdapter(getContext());
        db= db.open();

        if (isEdition) {
            boolean c = switchQuestion.isChecked();
            boolean d = switchRefuerzo.isChecked();
            checkUpdate = db.updatedStrategy(idEstrategia, strategyName, rutaIcono, tiempo, tipoTemporizador, switchQuestion.isChecked(), globalTime, switchRefuerzo.isChecked());

        } else {
            idEstrategia = db.saveNewStrategy(strategyName, rutaIcono, tiempo, tipoTemporizador, switchQuestion.isChecked(), globalTime, switchRefuerzo.isChecked());
        }
        db.close();

        if (idEstrategia == -1 || (isEdition && checkUpdate == 0)) {
            Toast.makeText(getContext(), getString(R.string.errorCrearEstrategia),Toast.LENGTH_SHORT).show();
            Log.i("NewStrategy", getString(R.string.errorCrearEstrategia));
            return -1;

        } else {
            return ((StrategyFirstTabFragment)firstTab).guardarRecursosEstrategia(idEstrategia);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_back_save, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

}
