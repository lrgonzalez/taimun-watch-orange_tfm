package com.orange.taimun_watch.es.utils;

import android.content.Context;
import android.util.Log;

import com.orange.taimun_watch.es.Preferences;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Clase con las funciones necesarias para desencriptar un fichero de nuestro Firebase.
 * Usado en SumStatisticActivity y FullscreenActivity
 *
 * @author Lucia
 */

public class DecryptUtils {
    private Context mContext;

    public DecryptUtils(Context context) {
        mContext = context;
    }

    /**
     * Función que desencripta un fichero y guarda el resultado en otro
     *
     * @param ctx           contexto de la aplicación
     * @param encFile       fichero a desencriptar
     * @param decryptedFile fichero resultante desencriptado
     * @return 0 si ok -1 si hubo algún error
     */
    public int decryptAES(Context ctx, File encFile, File decryptedFile) {
        FileInputStream fis;
        FileOutputStream fos;
        try {
            fis = new FileInputStream(encFile);
            fos = new FileOutputStream(decryptedFile);
        } catch (FileNotFoundException e) {
            Log.e("decryptAES", "No hay fichero \'" + decryptedFile + "\' para desencriptar.");
            return -1;
            //e.printStackTrace();
        }

        // Recuperar sks de KeyStore con las preferencias del PIN y el nombre del grupo
        String pin = Preferences.getPinCode(ctx);
        SecretKeySpec sks = getFromKeyStore(Preferences.getGroupCode(ctx), pin.toCharArray());
        if (sks == null) {
            Log.e("WEAR DECRYPT_AES", "sks es null");
            return -1;
        }

        // Desencriptar en buffer
        try {
            int ptLength;
            byte[] iv = new byte[16];
            fis.read(iv);

            // Inicializar cipher con IV y usar la clave recuperada del KS
            Cipher c = Cipher.getInstance("AES/GCM/NoPadding");
            c.init(Cipher.DECRYPT_MODE, sks, new IvParameterSpec(iv));

            byte[] inputBufferAux = new byte[32 * 128]; // TODO - encontrar tam optimo bloque
            // Leer por trozos
            while ((ptLength = fis.read(inputBufferAux)) != -1) {
                byte[] dec = c.update(inputBufferAux, 0, ptLength);
                fos.write(dec);
            }
            // Parte final
            byte[] dec = c.doFinal();
            fos.write(dec);
            fos.close();
            fis.close();
            return 0;
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Función para recuperar un SecretKeySpec del KeyStore
     *
     * @param alias    nombre de la clave a recuperar
     * @param password contraseña para abrir el KeyStore
     * @return SecretKeySpec solicitado
     */
    private SecretKeySpec getFromKeyStore(String alias, char[] password) {
        try {
            // El KS se recupera del fichero encriptado descargado de Firebase usando la contraseña
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());  // Default type es BC
            File file = new File(mContext.getCacheDir(), "keyStoreMobile");
            FileInputStream fis = new FileInputStream(file);
            //Cargar keystore
            ks.load(fis, password);

            // Obtener clave
            Key key = ks.getKey(alias, password);
            if (key instanceof SecretKeySpec) {
                return (SecretKeySpec) key;
            }
        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
            e.printStackTrace();
        }
        return null;
    }
}
