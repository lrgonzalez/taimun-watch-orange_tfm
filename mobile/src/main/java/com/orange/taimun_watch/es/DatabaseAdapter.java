package com.orange.taimun_watch.es;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.orange.taimun_watch.es.R;

import com.orange.taimun_watch.es.model.Estrategia;
import com.orange.taimun_watch.es.model.ListaEstrategia;
import com.orange.taimun_watch.es.model.ListaRegulacion;
import com.orange.taimun_watch.es.model.Recurso;
import com.orange.taimun_watch.es.model.Usuario;
import com.orange.taimun_watch.es.model.Regulacion;

/**
 * /**
 * Esta clase se encarga de la creacion/actualizacion de la
 * BBDD de la aplicacion.
 *
 * Created by Yux on 20/07/2016.
 */
public class DatabaseAdapter {

    private static final String DATABASE_NAME = "tictactea.db";
    private static final int DATABASE_VERSION = 1;
    private static final int IMG_PRED = 0;
    private static final int IMG_ICON = 1;
    private static final int IMG_SRC = 2;
    public static final int N_PICTOGRAMAS = 39;
    public static final int PIC_JUGAR = 30;
    public static final int PIC_INSPIRAR = 37;
    public static final int PIC_ESPIRAR = 38;
    public static final int PIC_DESCANSAR = 3;
    public static final int PIC_CORRER = 15;
    public static final int PIC_SALTAR = 26;
    public static final int PIC_SENTAR = 39;
    public static final int N_GIFS = 0;
    private final Context context;
    private DatabaseHelper helper;
    private SQLiteDatabase db;

    //Variables de Tabla de Usuarios
    public static final String TABLE_USUARIOS = "Usuarios";
    public static final String ID_USU = "id";
    public static final String NOMBRE= "nombre";
    public static final String SEXO = "sexo";
    public static final String TIPO_RELOJ = "tipoReloj";
    public static final String FOTO = "foto";
    public static final String PASSWORD = "password";
    static final String USER_CREATE =   "CREATE TABLE " + TABLE_USUARIOS +
            " (" + ID_USU + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            NOMBRE + " TEXT NOT NULL, " + SEXO + " TEXT NOT NULL, " +
            TIPO_RELOJ + " TEXT NOT NULL, " + FOTO +  " TEXT, " +
            PASSWORD +  " TEXT);";

    //Variables de Tabla de Imagenes
    public static final String TABLE_IMAGENES = " Imagenes";
    public static final String ID_IMAGEN = "id";
    public static final String RUTA_IMAGEN = "rutaImg";
    public static final String TIPO = "tipo";
    public static final String IMAGEN_CREATE = "CREATE TABLE " + TABLE_IMAGENES +
            " (" + ID_IMAGEN + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            RUTA_IMAGEN + " TEXT NOT NULL, " + TIPO + " INTEGER NOT NULL);";

    //Variables de Tabla de Textos
    public static final String TABLE_TEXTOS = "Textos";
    public static final String ID_TEXTO = "id";
    public static final String LITERAL = "literal";
    public static final String SIZE = "size";
    public static final String COLOR = "color";
    public static final String MODO = "modo";
    public static final String TEXT_CREATE = "CREATE TABLE " + TABLE_TEXTOS +
            " (" + ID_TEXTO + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            LITERAL + " TEXT NOT NULL, " + SIZE + " INTEGER NOT NULL, " +
            COLOR + " INTEGER NOT NULL, " + MODO + " INTEGER NOT NULL);";

    //Variables de Tabla de Estrategias
    public static final String TABLE_ESTRATEGIAS = "Estrategias";
    public static final String ID_ESTRATEGIA = "id";
    public static final String NOMBRE_ESTRATEGIA = "nombre";
    public static final String RUTA_ICONO = "rutaIcono";
    public static final String TIME = "time";
    public static final String TEMPORIZADOR = "temporizador";
    public static final String IS_CARROUSEL = "isCarrousel";
    public static final String QUESTION = "question";
    public static final String IS_GLOBAL_TIME = "isGlobalTime";
    public static final String REFUERZO = "refuerzo";
    public static final String ESTRATEGIA_CREATE = "CREATE TABLE " + TABLE_ESTRATEGIAS +
            " (" + ID_ESTRATEGIA + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            NOMBRE_ESTRATEGIA + " TEXT NOT NULL, " + RUTA_ICONO + " TEXT, " +
            TIME + " INTEGER NOT NULL, " + TEMPORIZADOR + " INTEGER NOT NULL, " +
            IS_CARROUSEL + " BOOLEAN, " + QUESTION + " BOOLEAN NOT NULL, " +
            IS_GLOBAL_TIME + " BOOLEAN NOT NULL, " + REFUERZO + " BOOLEAN NOT NULL);";

    //Variables de Tabla de Regulaciones
    public static final String TABLE_REGULACIONES = "Regulaciones";
    public static final String ID_REGULACION = "id";
    public static final String NOMBRE_REGULACION = "nombre";
    public static final String ICONO_REG = "rutaIcono";
    public static final String VIBRACION = "vibracion";
    public static final String AUDIO = "audio";
    public static final String IS_SELECTOR = "isSelector";
    public static final String IS_SYNC = "isSync";
    public static final String REGULACION_CREATE = "CREATE TABLE " + TABLE_REGULACIONES +
            " (" + ID_REGULACION + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            NOMBRE_REGULACION + " TEXT NOT NULL, " + ICONO_REG + " TEXT, " +
            VIBRACION + " INTEGER NOT NULL, " + AUDIO + " TEXT, " +
            IS_SYNC + " INTEGER NOT NULL, " + IS_SELECTOR + " INTEGER NOT NULL);";


    //Variables de Tabla Tarjetas
    public static final String TABLE_TARJETAS = "Tarjetas";
    public static final String ID_TARJETA = "id";
    public static final String ID_ESTRATEGIA_REF_T = "idEstrategia";
    public static final String ID_RECURSO_REF = "idRecurso";
    public static final String TIPO_RECURSO = "tipoRecurso";
    public static final String TARJETAS_CREATE = "CREATE TABLE " + TABLE_TARJETAS +
            " (" + ID_TARJETA + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ID_ESTRATEGIA_REF_T + " INTEGER NOT NULL, " + ID_RECURSO_REF + " INTEGER NOT NULL, " +
            TIPO_RECURSO + " INTEGER NOT NULL);";

    //Variables de Tabla de Schedule
    public static final String TABLE_SCHEDULE = "Schedule";
    public static final String ID_SCHEDULE = "id";
    public static final String ID_REGULACION_REF_S = "idRegulacion";
    public static final String ID_ESTRATEGIA_REF = "idEstrategia";
    public static final String SCHEDULE_CREATE = "CREATE TABLE " + TABLE_SCHEDULE +
            " (" + ID_SCHEDULE + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ID_REGULACION_REF_S + " INTEGER NOT NULL, " + ID_ESTRATEGIA_REF + " INTEGER NOT NULL);";


    public DatabaseAdapter(Context context) {
        this.context=context;
        helper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase arg0) {
            createTable(arg0);
        }

        /**
         * Funcion que crea las tablas de la BD local.
         * Se crean siete tablas (Usuarios, Texto, Imagen, Estrategias, Regulaciones, Tarjetas y Schedule)
         * @param db
         */
        private void createTable (SQLiteDatabase db) {

            try {
                db.execSQL(USER_CREATE);
                db.execSQL(IMAGEN_CREATE);
                db.execSQL(TEXT_CREATE);
                db.execSQL(ESTRATEGIA_CREATE);
                db.execSQL(REGULACION_CREATE);
                db.execSQL(TARJETAS_CREATE);
                db.execSQL(SCHEDULE_CREATE);
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("DatabaseAdapter", "Upgrading database from version "
                    + oldVersion + " to version " + newVersion);

            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USUARIOS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGENES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEXTOS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ESTRATEGIAS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_REGULACIONES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TARJETAS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE);

            createTable(db);
        }
    }

    public DatabaseAdapter open() throws SQLException {
        db = helper.getWritableDatabase();
        return this;
    }

    public DatabaseAdapter openForRead() throws SQLException {
        db = helper.getReadableDatabase();
        return this;
    }


    public void close(){
        helper.close();
    }


    /*----------------------  INICIALIZACION DE BBDD ---------------------*/
    public long inicializarBBDD (){
        inicializarImagenes();
        inicializarStrPredeterminadas();
        inicializarRegPredeterminadas();
        return inicializarUsuario();
    }

	/*---------------------- FUNCIONES DE ACCESO DE TABLA DE USUARIOS ---------------------*/
    /**
     * Funcion que inserta un usuario en la BD local.
     * @param usuario
     * @return fila afectada o -1 si error
     */
    public long insertUser(Usuario usuario) {
        ContentValues values = new ContentValues();
        values.put(NOMBRE, usuario.getNombre());
        //values.put(SEXO, usuario.getSexo());
        values.put(TIPO_RELOJ, usuario.getTipoReloj());
        values.put(FOTO, usuario.getFoto());
        return db.insert(TABLE_USUARIOS, null, values);
    }

    /**
     * Funcion que inicializa un usuario en la BBDD.
     * @return id de fila afectada o -1 si error
     */
    public long inicializarUsuario () {
        Usuario newUsuario = new Usuario(context.getString(R.string.name_default),context.getString(R.string.man),
                context.getString(R.string.square), null);
        return insertUser(newUsuario);
    }

    /**
     * Funcion que elimina un usuario de la BD local.
     * * @param id, id del usuario.
     * @return true si OK o false si ERROR
     */
    public boolean deleteUser (long id) {
        db.delete(TABLE_USUARIOS, ID_USU + "=" + id, null);
        return db.delete(TABLE_USUARIOS, ID_USU + "=" + id, null) > 0;
    }

    /**
     * Funcion que obtiene un usuario dado su id
     * @param id
     * @return Usuario con la información necesaria
     */
    public Usuario getUser(long id) {
        Usuario usuario = null;

        Cursor cursor= db.query(TABLE_USUARIOS, new String[] {NOMBRE, TIPO_RELOJ, FOTO},
                ID_USU + " = '" + id + "' ",null, null, null, ID_USU + " DESC");

        if (cursor.moveToFirst()) {
            usuario = new Usuario();
            usuario.setNombre(cursor.getString(0));
            //usuario.setSexo(cursor.getString(1));
            usuario.setTipoReloj(cursor.getString(1));
            usuario.setFoto(cursor.getString(2));
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }

        return usuario;
    }

    /**
     * Funcion que devuelve la password de un usuario dado.
     * @param id, id del usuario.
     * @return password del usuario o -1 si no se encuentra.
     */
    public String getUserPassword (long id) {
        Cursor cursor= db.query(TABLE_USUARIOS, new String[] {PASSWORD},
                ID_USU + " = '" + id + "' ",null, null, null, ID_USU + " DESC");

        if (cursor.moveToFirst())
            return cursor.getString(0);
        if (!cursor.isClosed())
            cursor.close();
        return "-1";
    }

    /**
     * Funcion que actualiza la password de un usuario en la BD local.
     * @param id de usuario
     * @param password
     * @return fila afectada o -1 si error
     */
    public long updateUserPassword(long id, String password) {
        ContentValues values = new ContentValues();
        values.put(PASSWORD, password);
        return db.update(TABLE_USUARIOS, values, ID_USU + "=" + id, null);
    }

    /**
     * Funcion que actualiza un usuario en la BD local.
     * @param usuario
     * @return fila afectada o -1 si error
     */
    public long updateUser(long id, Usuario usuario) {
        ContentValues values = new ContentValues();
        values.put(NOMBRE, usuario.getNombre());
        //values.put(SEXO, usuario.getSexo());
        values.put(TIPO_RELOJ, usuario.getTipoReloj());
        values.put(FOTO, usuario.getFoto());
        return db.update(TABLE_USUARIOS, values, ID_USU + "=" + id, null);
    }


	/*---------------------- FUNCIONES DE ACCESO DE TABLA DE IMAGENES ---------------------*/

    /**
     * Funcion que inicializa las imagenes de la App en la BBDD.
     */
    public void inicializarImagenes() {
        Integer id;

        for (int i = 0, j = 0; i < N_PICTOGRAMAS; i++) {
            //Cargamos primero las imágenes y luego los gifs.
            if(i < N_PICTOGRAMAS - N_GIFS) {
                id = context.getResources().getIdentifier("@drawable/pic" + i, "int", "com.orange.taimun_watch.es");
            } else {
                id = context.getResources().getIdentifier("@drawable/pic" + i + "gif" + j, "int", "com.orange.taimun_watch.es");
                j++;
            }

            String ruta = Integer.toString(id);
            Log.d("DEBUG", "Inicializando: " + ruta);
            long check = insertImage(ruta, IMG_PRED);

            if (check < 0)
                Log.i("DatabaseAdapter","Error al inicializar tabla de Imagenes en fila " + i);
        }
    }

    /**
     * Funcion que inicializa las regulaciones predeterminadas.
     */
    public void inicializarRegPredeterminadas() {
        long id;

        //Respirar profundamente
        id = saveNewRegulation(context.getString(R.string.respirar), null, true, false);
        guardarEstrategiaSchedule(id, 1);
        guardarEstrategiaSchedule(id, 2);
        guardarEstrategiaSchedule(id, 1);
        guardarEstrategiaSchedule(id, 2);

        //Saltar y sentarse
        id = saveNewRegulation(context.getString(R.string.saltarsentarse), null, true, false);
        guardarEstrategiaSchedule(id, 5);
        guardarEstrategiaSchedule(id, 6);

        //Dar un paseo y descansar
        id = saveNewRegulation(context.getString(R.string.correrdescansar), null, true, false);
        guardarEstrategiaSchedule(id, 3);
        guardarEstrategiaSchedule(id, 4);

        //Pasear o saltar
        id = saveNewRegulation(context.getString(R.string.corrersaltar), null, true, true);
        guardarEstrategiaSchedule(id, 3);
        guardarEstrategiaSchedule(id, 5);
    }

    /**
     * Funcion que inicializa las estrategias predeterminadas.
     */
    public void inicializarStrPredeterminadas() {
        long id;

        ArrayList<Recurso> r = getImagenesPredeterminadas();

        //Inspirar
        id = saveNewStrategy(context.getString(R.string.inhalar), null, 3, 2, false, false, false);
        guardarTarjetaEstrategia(id, PIC_INSPIRAR, 0);
        //Espirar
        id = saveNewStrategy(context.getString(R.string.exhalar), null, 3, 1, false, false, false);
        guardarTarjetaEstrategia(id, PIC_ESPIRAR, 0);

        //Correr
        id = saveNewStrategy(context.getString(R.string.correr), null, 10, 0, false, false, false);
        guardarTarjetaEstrategia(id, PIC_CORRER, 0);

        //Descansar
        id = saveNewStrategy(context.getString(R.string.descansar), null, 0, -1, false, false, false);
        guardarTarjetaEstrategia(id, PIC_DESCANSAR, 0);

        //Saltar
        id = saveNewStrategy(context.getString(R.string.saltar), null, 15, 0, false, false, false);
        guardarTarjetaEstrategia(id, PIC_SALTAR, 0);

        //Sentar
        id = saveNewStrategy(context.getString(R.string.sentarse), null, 0, -1, false, false, false);
        guardarTarjetaEstrategia(id, PIC_SENTAR, 0);

        //Jugar
        id = saveNewStrategy(context.getString(R.string.jugar), null, 0, -1, false, false, false);
        guardarTarjetaEstrategia(id, PIC_JUGAR, 0);
    }

    /**
     * Funcion que inserta una imagen en la tabla de Imagenes.
     * @param ruta, ruta o id de la imagen.
     * @param tipo de la imagen (0 prefeterminada, 1 icono, 2 recurso propio)
     * @return fila afectada o -1 si error
     */
    public long insertImage(String ruta, int tipo) {
        ContentValues values = new ContentValues();
        values.put(RUTA_IMAGEN, ruta);
        values.put(TIPO, tipo);
        return db.insert(TABLE_IMAGENES, null, values);
    }

    /**
     * Funcion que devuelve las imagenes por defecto de la aplicación
     * Tipo de la imagen (0 prefeterminada)
     * @return lista de recursos
     */
    public ArrayList<Recurso> getImagenesPredeterminadas() {
        ArrayList<Recurso> lstRecursos = new ArrayList<Recurso>();

        Cursor cursor= db.query(TABLE_IMAGENES, new String[] {ID_IMAGEN, RUTA_IMAGEN},
                TIPO + " = '" + IMG_PRED + "' ",null, null, null, ID_IMAGEN + " ASC");

        if (cursor.moveToFirst()) {
            do {
                Recurso recurso = new Recurso();
                recurso.setIdBBDD(cursor.getLong(0));
                recurso.setRutaValor(cursor.getString(1));
                recurso.setTipoRecurso(IMG_PRED); //Imagen
                lstRecursos.add(recurso);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return  lstRecursos;
    }

    /**
     * Funcion que devuelve las imagenes guardadas por el usuario como recurso propio
     * Tipo de la imagen (2 recurso propio)
     * @return lista de recursos
     */
    public ArrayList<Recurso> getImagenesGuardadas() {
        ArrayList<Recurso> lstRecursos = new ArrayList<Recurso>();

        Cursor cursor= db.query(TABLE_IMAGENES, new String[] {ID_IMAGEN, RUTA_IMAGEN},
                TIPO + " = '" + IMG_SRC + "' ",null, null, null, ID_IMAGEN + " ASC");

        if (cursor.moveToFirst()) {
            do {
                Recurso recurso = new Recurso();
                recurso.setIdBBDD(cursor.getLong(0));
                recurso.setRutaValor(cursor.getString(1));
                recurso.setTipoRecurso(1); //Imagen de recurso propio
                lstRecursos.add(recurso);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return  lstRecursos;
    }

    /**
     * Funcion que devuelve el número de registros de imagenes de recurso
     * propio guardadas por el usuario
     * @return numero de registros de imagenes propias
     */
    public long getImagenesGuardadasNumber() {
        long imagesNumber = DatabaseUtils.queryNumEntries(db, TABLE_IMAGENES, TIPO + " = '" + IMG_SRC + "' ");
        return imagesNumber;
    }

    /**
     * Funcion que devuelve el número de registros de imagenes de iconos
     * @return numero de registros de iconos propios
     */
    public long getIconosGuardadosNumber() {
        long imagesNumber = DatabaseUtils.queryNumEntries(db, TABLE_IMAGENES, TIPO + " = '" + IMG_ICON + "' ");
        return imagesNumber;
    }


    /*---------------------- FUNCIONES DE ACCESO DE TABLA DE ESTRATEGIAS ---------------------*/
    /**
     * Funcion que devuelve el número de registros de estrategias
     * @return numero de estrategias
     */
    public long getStrategiesNumber() {
        long strategiesNumber = DatabaseUtils.queryNumEntries(db, TABLE_ESTRATEGIAS);
        return strategiesNumber;
    }

    /**
     * Funcion que guarda una estrategia nueva
     * @param nombre de la estrategia
     * @param rutaIcono ruta o null si es por defecto
     * @param tiempo (0 si transicion, >0 si tiempo)
     * @param tipoTemporizador (0 donut, 1 vaceado, 2 llenado)
     * @param question (0 si no, 1 si sí)
     * @param globalTime (0 si no, 1 si sí)
     * @param refuerzo (0 si no, 1 si sí)
     * @return fila afectada o -1 si error
     */
    public long saveNewStrategy(String nombre, String rutaIcono, int tiempo, int tipoTemporizador, boolean question,
                                boolean globalTime, boolean refuerzo) {
        ContentValues values = new ContentValues();
        values.put(NOMBRE_ESTRATEGIA, nombre);
        values.put(RUTA_ICONO, rutaIcono);
        values.put(TIME, tiempo);
        values.put(TEMPORIZADOR, tipoTemporizador);
        values.put(IS_CARROUSEL, false);
        values.put(QUESTION, question);
        values.put(IS_GLOBAL_TIME, globalTime);
        values.put(REFUERZO, refuerzo);
        return db.insert(TABLE_ESTRATEGIAS, null, values);
    }

    /**
     * Funcion que actualiza una estrategia
     * @param idEstrategia, id de la estrategia
     * @param nombre de la estrategia
     * @param rutaIcono ruta o null si es por defecto
     * @param tiempo (0 si transicion, >0 si tiempo)
     * @param tipoTemporizador (0 donut, 1 vaceado, 2 llenado)
     * @param question (0 si no, 1 si sí)
     * @param globalTime (0 si no, 1 si sí)
     * @param refuerzo (0 si no, 1 si sí)
     * @return fila afectada o -1 si error
     */
    public int updatedStrategy(long idEstrategia, String nombre, String rutaIcono, int tiempo, int tipoTemporizador, boolean question, boolean globalTime, boolean refuerzo) {
        ContentValues values = new ContentValues();
        values.put(NOMBRE_ESTRATEGIA, nombre);
        values.put(RUTA_ICONO, rutaIcono);
        values.put(TIME, tiempo);
        values.put(TEMPORIZADOR, tipoTemporizador);
        values.put(IS_CARROUSEL, false);
        values.put(QUESTION, question);
        values.put(IS_GLOBAL_TIME, globalTime);
        values.put(REFUERZO, refuerzo);
        return db.update(TABLE_ESTRATEGIAS, values, ID_ESTRATEGIA + "=" + idEstrategia, null);
    }

    /**
     * Funcion que devuelve el resumen de las estrategias guardadas
     * @return lista de estrategias
     */
    public ArrayList<ListaEstrategia> getStrategiesOverview() {
        ArrayList<ListaEstrategia> lstEstrategias = new ArrayList<ListaEstrategia>();

        Cursor cursor= db.query(TABLE_ESTRATEGIAS, new String[] {ID_ESTRATEGIA, NOMBRE_ESTRATEGIA, RUTA_ICONO},
                null,null, null, null, ID_ESTRATEGIA + " ASC");

        if (cursor.moveToFirst()) {
            do {
                ListaEstrategia estrategia = new ListaEstrategia();
                long numCards = getCardNumberByStrategy(cursor.getLong(0));
                estrategia.setIdBBDD(cursor.getLong(0));
                estrategia.setNombre(cursor.getString(1));
                estrategia.setRutaIcono(cursor.getString(2));
                estrategia.setNumTarjetas(numCards);
                lstEstrategias.add(estrategia);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return  lstEstrategias;
    }

    /**
     * Funcion que devuelve el nombre de una estrategia dada.
     * @param idEstrategia, id de la estrategia.
     * @return nombre de la estrategia -1 si no se encuentra.
     */
    public String getStrategyStoredName (long idEstrategia) {
        Cursor cursor= db.query(TABLE_ESTRATEGIAS, new String[] {NOMBRE_ESTRATEGIA},
                ID_ESTRATEGIA + " = '" + idEstrategia + "' ",null, null, null, ID_ESTRATEGIA + " DESC");

        if (cursor.moveToFirst())
            return cursor.getString(0);
        if (!cursor.isClosed())
            cursor.close();
        return "-1";
    }

    /**
     * Funcion que devuelve la ruta del icono de una estrategia dada.
     * @param idEstrategia, id de la estrategia.
     * @return icono de la estrategia, -1 si no se encuentra.
     */
    public String getStrategyRutaIcono (long idEstrategia) {
        Cursor cursor= db.query(TABLE_ESTRATEGIAS, new String[] {RUTA_ICONO},
                ID_ESTRATEGIA + " = '" + idEstrategia + "' ",null, null, null, ID_ESTRATEGIA + " DESC");

        if (cursor.moveToFirst())
            return cursor.getString(0);
        if (!cursor.isClosed())
            cursor.close();
        return "-1";
    }

    public Estrategia getStrategyInfo(long idEstrategia) {
        Estrategia strategy = null;

        Cursor cursor= db.query(TABLE_ESTRATEGIAS, new String[] {TIME, TEMPORIZADOR, QUESTION, IS_GLOBAL_TIME, REFUERZO},
                ID_ESTRATEGIA + " = '" + idEstrategia + "' ",null, null, null, ID_ESTRATEGIA + " DESC");

        if (cursor.moveToFirst()) {
            strategy = new Estrategia();
            strategy.setTime(cursor.getInt(0));
            strategy.setTimerType(cursor.getInt(1));
            strategy.setQuestion(cursor.getInt(2) > 0);
            strategy.setGlobalTime(cursor.getInt(3) > 0);
            strategy.setReinforcement(cursor.getInt(4) > 0);
        }
        if (!cursor.isClosed())
            cursor.close();

        return strategy;
    }

    /**
     * Funcion que devuelve las estrategias de una regulacion dada
     * @param idRegulacion, id de la regulacion.
     * @return array de las estrategias.
     */
    public ArrayList<Estrategia> getStrategiesByRegulation (long idRegulacion, boolean isSincronizacion) {
        ArrayList<Estrategia> estrategias = new ArrayList<Estrategia>();
        Estrategia e;

        String QUERY = "SELECT e.id, e.nombre, e.rutaIcono, e.time, e.isGlobalTime, e.question, e.temporizador, e.isCarrousel, e.refuerzo" +
                " FROM " + TABLE_ESTRATEGIAS + " e INNER JOIN " + TABLE_SCHEDULE + " s ON e." + ID_ESTRATEGIA + "=s." + ID_ESTRATEGIA_REF +
                " WHERE s." + ID_REGULACION_REF_S + "=?;";
        Cursor cursor = db.rawQuery(QUERY, new String[] {String.valueOf(idRegulacion)});

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                boolean isGlobalTime = false, isCarrousel = false, reinforcement = false, isQuestion = false;
                ArrayList<Recurso> tarjetas = getTarjetasByEstrategia(id);
                ArrayList<String> steps = new ArrayList<String>();

                for(Recurso r : tarjetas) {
                    if(r.getTipoRecurso() == 1 || r.getTipoRecurso() == 0) {
                        steps.add(getNombreRecursoByTarjeta(r));
                    } else {
                        steps.add("txt" + String.valueOf(r.getIdBBDD()));
                        if (isSincronizacion) {
                            File regFolder = new File(context.getExternalFilesDir(null), "regulation");
                            PhotoUtils pu = new PhotoUtils(context);
                            pu.textToImage(r, regFolder);
                        }
                    }
                }

                if(cursor.getInt(4) == 1) {
                    isGlobalTime = true;
                }

                if(cursor.getInt(5) == 1) {
                    isQuestion = true;
                }

                if(cursor.getInt(7) == 1) {
                    isCarrousel = true;
                }

                if(cursor.getInt(8) == 1 ) {
                    reinforcement = true;
                }

                e = new Estrategia(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3),
                        isGlobalTime, false, isQuestion, cursor.getInt(6), isCarrousel, reinforcement,
                        steps.toArray(new String[0]));

                estrategias.add(e);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return estrategias;
    }

    /**
     * Funcion que elimina la estrategia y sus referencias a partir de un id.
     * @param idEstrategia, id de la estrategia.
     * @return numero de filas afectadas
     */
    public int deleteStrategyById (long idEstrategia) {
       deleteTarjetasByEstrategia(idEstrategia);
       deleteScheduleByStrategy(idEstrategia);
       return db.delete(TABLE_ESTRATEGIAS, ID_ESTRATEGIA + "=" + idEstrategia, null);
    }

    /**
     * Funcion que elimina todas las estrategias de la tabla.
     * @return numero de filas afectadas
     */
    public int deleteAllEstrategias () {
        return db.delete(TABLE_ESTRATEGIAS, "1", null);
    }

    /**
     * Funcion que guarda una estrategia en la tabla.
     * @param idRegulacion de la regulación
     * @return numero de schedule
     */
//    public long guardarEstrategia(ListaEstrategia e) {
//        ContentValues values = new ContentValues();
//        values.put(ID_REGULACION_REF_S, idRegulacion);
//        values.put(ID_ESTRATEGIA_REF, idEstrategia);
//        return db.insert(TABLE_SCHEDULE, null, values);
//    }

    /*---------------------- FUNCIONES DE ACCESO DE TABLA DE REGULACIONES ---------------------*/

    public int deleteRegulationById(long idRegulacion) {
        deleteScheduleByRegulation(idRegulacion);
        return db.delete(TABLE_REGULACIONES, ID_REGULACION + " = " + idRegulacion, null);
    }

    /**
     * Funcion que devuelve una regulación a partir del ID
     * @param idRegulacion id de la regulación solicitada.
     * @return Regulacion solicitada.
     */
    public Regulacion getRegulationById(long idRegulacion) {
        return new Regulacion((int) idRegulacion, getRegulationStoredName(idRegulacion), getRegulationVibration(idRegulacion),
                getRegulationSelector(idRegulacion), getRegulationAudio(idRegulacion), getRegulationSchedule(idRegulacion),
                getStrategiesByRegulation(idRegulacion, true).toArray(new Estrategia[0]));
    }

    /**
     * Funcion que devuelve el schedule de una regulacion a partir del ID.
     * @param idRegulacion id de la regulación.
     * @return array de enteros con el schedule de la regulación.
     */
    public ArrayList<Integer> getRegulationSchedule(long idRegulacion) {
        ArrayList<Integer> schedule = new ArrayList<Integer>();

        Cursor cursor= db.query(TABLE_SCHEDULE, new String[] {ID_ESTRATEGIA_REF},
                ID_REGULACION_REF_S + " = '" + idRegulacion + "' ", null, null, null, ID_REGULACION + " DESC");

        if(cursor.moveToFirst()) {
            do {
                Integer i;
                i = cursor.getInt(0);
                schedule.add(i);
            } while (cursor.moveToNext());
        }

        cursor.close();
        //Invertimos la coleccion para obtenerla en el orden correcto.
        Collections.reverse(schedule);
        return schedule;
    }

    /**
     * Funcion que devuelve el campo vibracion de una regulación a partir de una id.
     * @param idRegulacion id de la regulación.
     * @return true si tiene vibración, false si no.
     */
    public boolean getRegulationVibration(long idRegulacion) {
        int vibration;
        boolean isVibration = false;

        Cursor cursor= db.query(TABLE_REGULACIONES, new String[] {VIBRACION},
                ID_REGULACION + " = '" + idRegulacion + "' ", null, null, null, ID_REGULACION + " DESC");

        if (cursor.moveToFirst()) {
            vibration = cursor.getInt(0);
        } else {
            return false;
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        if(vibration == 1) {
            isVibration = true;
        }

        return isVibration;
    }

    /**
     * Funcion que devuelve el campo selector de una regulación a partir de una id.
     * @param idRegulacion id de la regulación.
     * @return true si es un selector, false si no.
     */
    public boolean getRegulationSelector(long idRegulacion) {
        int selector;
        boolean isSelector = false;

        Cursor cursor= db.query(TABLE_REGULACIONES, new String[] {IS_SELECTOR},
                ID_REGULACION + " = '" + idRegulacion + "' ", null, null, null, ID_REGULACION + " DESC");

        if (cursor.moveToFirst()) {
            selector = cursor.getInt(0);
        } else {
            return false;
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        if(selector == 1) {
            isSelector = true;
        }

        return isSelector;
    }

    /**
     * Funcion que devuelve el campo audio de una regulación a partir de una id.
     * @param idRegulacion id de la regulación.
     * @return Cadena con el path del audio.
     */
    public String getRegulationAudio(long idRegulacion) {
        String audio;

        Cursor cursor= db.query(TABLE_REGULACIONES, new String[] {IS_SELECTOR},
                ID_REGULACION + " = '" + idRegulacion + "' ", null, null, null, ID_REGULACION + " DESC");

        if (cursor.moveToFirst()) {
            audio = cursor.getString(0);
        } else {
            return "";
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        return audio;
    }

    /**
     * Funcion que devuelve el número de registros de regulaciones
     * @return numero de regulaciones
     */
    public long getRegulationsNumber() {
        long regulationsNumber = DatabaseUtils.queryNumEntries(db, TABLE_REGULACIONES);
        return regulationsNumber;
    }

    /**
     * Funcion que guarda una regulación nueva
     * @param nombre de la regulación
     * @param rutaIcono ruta o null si es por defecto
     * @param vibracion (0 si no, 1 si sí)
     * @param isSelector (0 si no, 1 si sí)
     * @return fila afectada o -1 si error
     */
    public long saveNewRegulation(String nombre, String rutaIcono, boolean vibracion, boolean isSelector) {
        ContentValues values = new ContentValues();
        values.put(NOMBRE_REGULACION, nombre);
        values.put(ICONO_REG, rutaIcono);
        values.put(VIBRACION, vibracion);
        values.put(IS_SYNC, 0);
        values.put(IS_SELECTOR, isSelector);
        return db.insert(TABLE_REGULACIONES, null, values);
    }

    /**
     * Funcion que actualiza una regulación
     * @param idRegulacion id de la regulación
     * @param nombre de la regulación
     * @param rutaIcono ruta o null si es por defecto
     * @param vibracion (0 si no, 1 si sí)
     * @param isSelector (0 si no, 1 si sí)
     * @return fila afectada o -1 si error
     */
    public int updateRegulation(long idRegulacion, String nombre, String rutaIcono, boolean vibracion, boolean isSelector) {
        ContentValues values = new ContentValues();
        values.put(NOMBRE_REGULACION, nombre);
        values.put(ICONO_REG, rutaIcono);
        values.put(VIBRACION, vibracion);
        values.put(IS_SYNC, 0);
        values.put(IS_SELECTOR, isSelector);
        return db.update(TABLE_REGULACIONES, values, ID_REGULACION + "=" + idRegulacion, null);
    }

    /**
     * Funcion que devuelve el resumen de las regulaciones guardadas
     * @return lista de regulaciones
     */
    public ArrayList<ListaRegulacion> getRegulationsOverview() {
        ArrayList<ListaRegulacion> lstRegulaciones = new ArrayList<ListaRegulacion>();

        Cursor cursor= db.query(TABLE_REGULACIONES, new String[] {ID_REGULACION, NOMBRE_REGULACION, ICONO_REG, IS_SELECTOR, IS_SYNC},
                null,null, null, null, ID_REGULACION + " ASC");

        if (cursor.moveToFirst()) {
            do {
                ListaRegulacion regulacion = new ListaRegulacion();
                long numEstrategias = getStrategyNumberByRegulation(cursor.getLong(0));
                regulacion.setIdBBDD(cursor.getLong(0));
                regulacion.setNombre(cursor.getString(1));
                regulacion.setRutaIcono(cursor.getString(2));
                regulacion.setIsSelector(cursor.getInt(3));
                regulacion.setIsSync(cursor.getInt(4));
                regulacion.setNumeroEstrategias(numEstrategias);
                lstRegulaciones.add(regulacion);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return  lstRegulaciones;
    }

    /**
     * Funcion que devuelve el nombre de una regulacion dada.
     * @param idRegulacion, id de la regulacion.
     * @return nombre de la regulacion -1 si no se encuentra.
     */
    public String getRegulationStoredName (long idRegulacion) {
        Cursor cursor= db.query(TABLE_REGULACIONES, new String[] {NOMBRE_REGULACION},
                ID_REGULACION + " = '" + idRegulacion + "' ",null, null, null, ID_REGULACION + " DESC");

        if (cursor.moveToFirst())
            return cursor.getString(0);
        if (!cursor.isClosed())
            cursor.close();
        return "-1";
    }

    /**
     * Funcion que devuelve la ruta del icono de una regulacion dada.
     * @param idRegulacion, id de la regulacion.
     * @return icono de la regulacion, -1 si no se encuentra.
     */
    public String getRegulationRutaIcono (long idRegulacion) {
        Cursor cursor= db.query(TABLE_REGULACIONES, new String[] {RUTA_ICONO},
                ID_REGULACION + " = '" + idRegulacion + "' ",null, null, null, ID_REGULACION + " DESC");

        if (cursor.moveToFirst())
            return cursor.getString(0);
        if (!cursor.isClosed())
            cursor.close();
        return "-1";
    }

    /**
     * Funcion que elimina las estrategias de una regulacion dada
     * @param idRegulacion, id de la estrategia.
     * @return numero de filas afectadas
     */
    public int deleteEstrategiasByRegulacion (long idRegulacion) {
        return db.delete(TABLE_SCHEDULE, ID_REGULACION_REF_S + "=" + idRegulacion, null);
    }

    /*---------------------- FUNCIONES DE ACCESO DE TABLA DE TARJETAS ---------------------*/
    /**
     * Funcion que guarda una tarjeta de una estrategia
     * @param idEstrategia de estrategia
     * @param idRecurso
     * @param tipoRecurso (0 si tabla Imagenes o 1 si tabla Textos)
     * @return numero de regulaciones
     */
    public long guardarTarjetaEstrategia(long idEstrategia, long idRecurso, int tipoRecurso) {
        ContentValues values = new ContentValues();
        values.put(ID_ESTRATEGIA_REF_T, idEstrategia);
        values.put(ID_RECURSO_REF, idRecurso);
        values.put(TIPO_RECURSO, tipoRecurso);
        return db.insert(TABLE_TARJETAS, null, values);
    }

    /**
     * Funcion que devuelve el número de tarjetas de una estrategia dada
     * @param idEstrategia
     * @return numero de registros de imagenes propias
     */
    public long getCardNumberByStrategy(long idEstrategia) {
        long cardNumber = DatabaseUtils.queryNumEntries(db, TABLE_TARJETAS, ID_ESTRATEGIA_REF_T + " = '" + idEstrategia + "' ");
        return cardNumber;
    }

    /**
     * Funcion que devuelve la lista de Recursos de una estrategia
     * @param idEstrategia
     * @return lista de recursos de estrategia
     */
    public ArrayList<Recurso> getTarjetasByEstrategia(long idEstrategia) {
        ArrayList<Recurso> lstTarjetas=null;
        lstTarjetas = new ArrayList<Recurso>();

        //Recursos de imágenes predeterminadas.
        String QUERY = "SELECT i.*, t.id" +
                " FROM " + TABLE_TARJETAS + " t INNER JOIN " + TABLE_IMAGENES + " i ON t." + TIPO_RECURSO + "=0 AND t." + ID_RECURSO_REF + "=i." + ID_IMAGEN +
                " WHERE t." + ID_ESTRATEGIA_REF_T + "=?;";

        Cursor cursor = db.rawQuery(QUERY, new String[] {String.valueOf(idEstrategia)});

        if (cursor.moveToFirst()) {
            do {
                Recurso recurso = new Recurso();
                recurso.setIdBBDD(cursor.getInt(0));
                recurso.setRutaValor(cursor.getString(1));
                recurso.setTipoRecurso(0);
                recurso.setOrder(cursor.getInt(3));
                lstTarjetas.add(recurso);
            } while (cursor.moveToNext());
        }

        cursor.close();

        //Recursos de imágenes propias.
        QUERY = "SELECT i.*, t.id" +
                " FROM " + TABLE_TARJETAS + " t INNER JOIN " + TABLE_IMAGENES + " i ON t." + TIPO_RECURSO + "=1 AND t." + ID_RECURSO_REF + "=i." + ID_IMAGEN +
                " WHERE t." + ID_ESTRATEGIA_REF_T + "=?;";

        Log.d("QUERY", QUERY);

        cursor = db.rawQuery(QUERY, new String[] {String.valueOf(idEstrategia)});
        Log.d ("QUERY", cursor.toString());
        if (cursor.moveToFirst()) {
            do {
                Recurso recurso = new Recurso();
                recurso.setIdBBDD(cursor.getInt(0));
                recurso.setRutaValor(cursor.getString(1));
                recurso.setTipoRecurso(1);
                recurso.setOrder(cursor.getInt(3));
                Log.d("QUERY", "Order: " + cursor.getInt(3));
                lstTarjetas.add(recurso);
            } while (cursor.moveToNext());
        }

        cursor.close();

        //Recursos de texto
        QUERY = "SELECT txt.*, t.id" +
                " FROM " + TABLE_TARJETAS + " t INNER JOIN " + TABLE_TEXTOS + " txt ON t." + TIPO_RECURSO + "=2 AND t." + ID_RECURSO_REF + "=txt." + ID_TEXTO +
                " WHERE t." + ID_ESTRATEGIA_REF_T + "=?;";

        Log.d("QUERY", QUERY);

        cursor = db.rawQuery(QUERY, new String[] {String.valueOf(idEstrategia)});

        if (cursor.moveToFirst()) {
            do {
                Recurso recurso = new Recurso();
                recurso.setIdBBDD(cursor.getInt(0));
                recurso.setRutaValor(cursor.getString(1));
                recurso.setTipoRecurso(2);
                recurso.setSize(cursor.getInt(2));
                recurso.setColor(cursor.getInt(3));
                recurso.setModo(cursor.getInt(4));
                recurso.setOrder(cursor.getInt(5));
                lstTarjetas.add(recurso);
            } while (cursor.moveToNext());
        }
        cursor.close();

        //Annadir caso de recurso propio.

        if (!lstTarjetas.isEmpty()) {
            Collections.sort(lstTarjetas, new Comparator<Recurso>() {
                public int compare(Recurso r1, Recurso r2) {
                    return r1.getOrder() - r2.getOrder();
                }
            });
        }

        return lstTarjetas;
    }

    /**
     * Funcion que elimina las tarjetas de una estrategia dada
     * @param idEstrategia, id de la estrategia.
     * @return numero de filas afectadas
     */
    public int deleteTarjetasByEstrategia (long idEstrategia) {

        //Primero eliminamos entradas de texto
        String QUERY = "DELETE FROM " + TABLE_TEXTOS + " WHERE " + ID_TEXTO + " IN (SELECT " + ID_RECURSO_REF + " FROM " + TABLE_TARJETAS + " WHERE " + TIPO_RECURSO + "=1);";
        Cursor cursor = db.rawQuery(QUERY, null);
        cursor.moveToFirst();
        cursor.close();
        return db.delete(TABLE_TARJETAS, ID_ESTRATEGIA_REF_T + "=" + idEstrategia, null);
    }

    public String getNombreRecursoByTarjeta (Recurso r) {

        if (r.getTipoRecurso() == 0 || r.getTipoRecurso() == 1) {
            Cursor cursor= db.query(TABLE_IMAGENES, new String[] {RUTA_IMAGEN},
                    ID_IMAGEN + " = '" + r.getIdBBDD() + "' ", null, null, null, ID_REGULACION + " DESC");

            if (cursor.moveToFirst()) {
                return cursor.getString(0);
            }

            cursor.close();
        }



        return "";
    }

    /*---------------------- FUNCIONES DE ACCESO DE TABLA DE SCHEDULE ---------------------*/
    public int deleteScheduleByStrategy(long idEstrategia) {
        return db.delete(TABLE_SCHEDULE, ID_ESTRATEGIA_REF + "=" + idEstrategia, null);
    }

    public int deleteScheduleByRegulation(long idRegulacion) {
        return db.delete(TABLE_SCHEDULE, ID_REGULACION_REF_S + "=" + idRegulacion, null);
    }

    /**
     * Funcion que guarda una estrategia de una regulación
     * @param idRegulacion de la regulación
     * @param idEstrategia
     * @return numero de schedule
     */
    public long guardarEstrategiaSchedule(long idRegulacion, long idEstrategia) {
        ContentValues values = new ContentValues();
        values.put(ID_REGULACION_REF_S, idRegulacion);
        values.put(ID_ESTRATEGIA_REF, idEstrategia);
        return db.insert(TABLE_SCHEDULE, null, values);
    }

    /**
     * Funcion que devuelve el número de estrategias de una regulacion dada
     * @param idRegulacion
     * @return numero de registros de estrategias
     */
    public long getStrategyNumberByRegulation(long idRegulacion) {
        long strategyNumber = DatabaseUtils.queryNumEntries(db, TABLE_SCHEDULE, ID_REGULACION_REF_S + " = '" + idRegulacion + "' ");
        return strategyNumber;
    }

    /**
     * Funcion que devuelve la lista de estrategias de una regulacion dada
     * @param idRegulacion, id de la regulacion.
     * @return numero de filas afectadas
     */
    public ArrayList<ListaEstrategia> getStrategiesOverviewByRegulation (long idRegulacion) {
        ArrayList<ListaEstrategia> lstEstrategias = new ArrayList<ListaEstrategia>();

        String QUERY = "SELECT e.id, e.nombre, e.rutaIcono" +
                " FROM " + TABLE_ESTRATEGIAS + " e INNER JOIN " + TABLE_SCHEDULE + " s ON e." + ID_ESTRATEGIA + "=s." + ID_ESTRATEGIA_REF +
                " WHERE s." + ID_REGULACION_REF_S + "=?;";

        Cursor cursor = db.rawQuery(QUERY, new String[] {String.valueOf(idRegulacion)});

        if (cursor.moveToFirst()) {
            do {
                ListaEstrategia estrategia = new ListaEstrategia();
                long numCards = getCardNumberByStrategy(cursor.getLong(0));
                estrategia.setIdBBDD(cursor.getLong(0));
                estrategia.setNombre(cursor.getString(1));
                estrategia.setRutaIcono(cursor.getString(2));
                estrategia.setNumTarjetas(numCards);
                lstEstrategias.add(estrategia);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return  lstEstrategias;

    }

    /*---------------------- FUNCIONES DE ACCESO DE TABLA DE TEXTOS ---------------------*/
    /**
     * Funcion que inserta un texto en la tabla de Textos.
     * @param literal
     * @param size
     * @param color
     * @param modo
     * @return fila afectada o -1 si error
     */
    public long insertText(String literal, int size, int color, int modo) {
        ContentValues values = new ContentValues();
        values.put(LITERAL, literal);
        values.put(SIZE, size);
        values.put(COLOR, color);
        values.put(MODO, modo);
        return db.insert(TABLE_TEXTOS, null, values);
    }
}
