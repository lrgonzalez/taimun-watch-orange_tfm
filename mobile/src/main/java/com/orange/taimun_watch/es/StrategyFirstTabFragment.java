package com.orange.taimun_watch.es;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;

import java.io.File;
import java.util.ArrayList;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.model.Recurso;

/**
 * Created by Yux on 04/06/2016.
 * Modified by Yux on 14/06/2016.
 */

public class StrategyFirstTabFragment extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private TabLayout tabLayout;
    private ViewPager viewPagerSubmenu;
    private ViewPager viewPagerCard;
    private CardPageAdapter cardAdapter;
    private FloatingActionButton addButton;
    private ImageButton btnPrev, btnNext, btnEdit, btnRemove;
    private TextView textCard, textName;
    private int currentPosition;
    private static final int MAX_CARDS = 15;
    private String strategyName = null;
    private ImageView frame, imgIcono;
    private boolean isDefaultIcon = true;
    private static final int TAKE_PHOTO = 1;
    private static final int SELECT_PHOTO = 2;
    private static final int CROP_PHOTO = 3;
    private Uri imgUri = null;
    private PhotoUtils photoUtils;
    private ViewPagerAdapter adapter;
    private int cardCount;
    private DatabaseAdapter db;
    private long idEstrategia = -1;
    private boolean isEdition = false, rutaChanged = false;
    private String rutaEdicion = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        db = new DatabaseAdapter(getContext());
        photoUtils = new PhotoUtils(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.strategy_first_tab_fragment, container, false);
        currentPosition = 0;

        //Comprobamos si es creación o edición de estrategia
        Intent intent = getActivity().getIntent();
        idEstrategia = intent.getLongExtra("strategyId", -1);

        /****************** Gestión de tarjetas ****************/
        //ViewPager para gestionar el contenido de las distintas tarjetas
        viewPagerCard = (ViewPager) view.findViewById(R.id.card_viewpager);

        //Creamos y asignamos el adaptador de los cards del viewpager
        cardAdapter = new CardPageAdapter(getContext());
        viewPagerCard.setAdapter(cardAdapter);
        viewPagerCard.addOnPageChangeListener(this);

        //Configuramos la vista del frame segun el tipo de reloj
        frame = (ImageView) view.findViewById(R.id.typeFrame);
        if (Preferences.getWatchtype(getContext()).equals(Preferences.SQUARE)) {
            frame.setImageResource(R.drawable.square_frame);
        } else {
            frame.setImageResource(R.drawable.round_frame);
        }

        /****************** Opciones de submenu ****************/
        //ViewPager para gestionar los fragment del submenu de opciones
        viewPagerSubmenu = (ViewPager) view.findViewById(R.id.submenu_viewpager);
        setupViewPager(viewPagerSubmenu);

        //Asociamos los tabs del submenu de opciones con el gestor de fragments
        tabLayout = (TabLayout) view.findViewById(R.id.submenu_tabs);
        tabLayout.setupWithViewPager(viewPagerSubmenu);

        /*********************** Gestión de botones y texto *********************/
        textCard = (TextView) view.findViewById(R.id.card_txt_count);
        textName = (TextView) view.findViewById(R.id.strategy_name);
        textName.setText(getStrategyEditName(idEstrategia));
        imgIcono = (ImageView) view.findViewById(R.id.new_strategy_icon);
        setStrategyEditIcono(idEstrategia);
        addButton = (FloatingActionButton) view.findViewById(R.id.btn_add_card);
        btnPrev = (ImageButton) view.findViewById(R.id.btn_preview_card);
        btnNext = (ImageButton) view.findViewById(R.id.btn_next_card);
        btnEdit = (ImageButton) view.findViewById(R.id.strategy_edit);
        //btnRemove = (ImageButton) view.findViewById(R.id.strategy_remove);

        addButton.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        imgIcono.setOnClickListener(this);

        // Creamos una vista inicial que mostrar o cargamos datos si estamos editando
        inicializarCardAdapter(idEstrategia);

        //Mostramos el dialog para la edicion inicial de la estrategia
        if (idEstrategia < 0) {
            editStrategyDialog();
        }

        return view;
    }

    /*
     * Método encargado de construir el cardview o con un recurso nuevo
     * o con la carga de los existentes en la opción de editar
     */
    private void inicializarCardAdapter(long idEstrategia) {
        //Caso de edición de estrategia
        if (idEstrategia > 0) {
            isEdition = true;
            db.open();
            ArrayList<Recurso> lstTarjetas = db.getTarjetasByEstrategia(idEstrategia);
            cardAdapter.setCardViews(lstTarjetas);
            viewPagerCard.setCurrentItem(currentPosition, true);
            db.close();

            //Caso de nueva estrategia, creamos recurso en blanco
        } else {
            Recurso newRecurso = new Recurso();
            newRecurso.setTipoRecurso(0);
            newRecurso.setIdBBDD(-1);
            newRecurso.setRutaValor(String.valueOf(R.drawable.none));
            cardAdapter.addView (newRecurso, currentPosition);
        }

        cardAdapter.notifyDataSetChanged();
        cardCount = cardAdapter.getCount();
        setCardTextCount(1, cardCount);
        setArrowsVisibility(1, cardCount);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_card:
                //Comprobamos limitación de tarjetas
                if (cardAdapter.getCount() == MAX_CARDS) {
                    Toast.makeText(getContext(), getString(R.string.errorMaxNumberCards) + " " + MAX_CARDS, Toast.LENGTH_SHORT).show();

                } else {
                    Recurso newRecurso = new Recurso();
                    newRecurso.setTipoRecurso(0);
                    newRecurso.setIdBBDD(-1);
                    newRecurso.setRutaValor(String.valueOf(R.drawable.none));
                    addView(newRecurso);
                }
                break;

            case R.id.btn_preview_card:
                moveToPreviousPage();
                break;

            case R.id.btn_next_card:
                moveToNextPage();
                break;

            case R.id.strategy_edit:
                editStrategyDialog();
                break;

            case R.id.new_strategy_icon:
                dialogPhoto();
                break;

        }
    }

    @Override
    public void onPageSelected(int position) {
        currentPosition = position;
        setCardTextCount(position + 1, cardAdapter.getCount());
        setArrowsVisibility(position +1, cardAdapter.getCount());

        //Comprobamos si es recurso de tipo texto
        Fragment subMenufirstTab = adapter.getItem(0);
        if (cardAdapter.getCardViewRecurso(currentPosition).getTipoRecurso() == 2) {
            String texto = cardAdapter.getCardViewRecurso(currentPosition).getRutaValor();
            ((SubmenuFirstTabFragment)subMenufirstTab).setCurrentText(texto);

        } else {
            ((SubmenuFirstTabFragment)subMenufirstTab).setCurrentText("");
        }
    }

    /*
     *   Método para actualizar la visibilidad de los botones de 'Next' y 'Preview'
     *   dependiendo de la tarjeta actual visible
     */
    private void setArrowsVisibility(int posActual, int total) {
        if ((posActual == 1) && (posActual == total)) {
            btnPrev.setVisibility(View.INVISIBLE);
            btnNext.setVisibility(View.INVISIBLE);

        } else if ((posActual == 1) && (posActual != total)){
            btnPrev.setVisibility(View.INVISIBLE);
            btnNext.setVisibility(View.VISIBLE);

        } else if ((posActual > 1) && (posActual == total)) {
            btnPrev.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.INVISIBLE);

        } else {
            btnPrev.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.VISIBLE);
        }
    }

    /*
     *   Método para actualizar el texto indicativo del número de tarjetas y la tarjeta actual
     */
    private void setCardTextCount(int posActual, int total){
        textCard.setText(posActual + " / " + total);
    }

    /*
     *   Método para inicializar el ViewPager principal con los fragmentos que mostrará cada Tab
     *   del submenú de opciones (Texto, Imágenes y Mis recursos)
     */
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(((NewStrategyActivity)getActivity()).getSupportFragmentManager());
        adapter.addFragment(new SubmenuFirstTabFragment(), getResources().getString(R.string.text));
        adapter.addFragment(new SubmenuSecondTabFragment(), getResources().getString(R.string.images));
        adapter.addFragment(new SubmenuThirdTabFragment(), getResources().getString(R.string.my_resources));
        viewPager.setAdapter(adapter);
    }

    /************************ Viewpager methods  ************************/
    public void moveToNextPage() {
        viewPagerCard.setCurrentItem(viewPagerCard.getCurrentItem() + 1);
    }

    public void moveToPreviousPage() {
        viewPagerCard.setCurrentItem(viewPagerCard.getCurrentItem() - 1);
    }

    public void addView (Recurso recurso) {
        int pageIndex = cardAdapter.addView (recurso);
        //Set "newPage" as the currently displayed page
        cardAdapter.notifyDataSetChanged();
        viewPagerCard.setCurrentItem (pageIndex, true);
        replaceView(recurso);
    }

    public void replaceView (Recurso recurso) {
        int pageIndex = cardAdapter.replaceView(viewPagerCard, recurso, currentPosition);
        cardAdapter.notifyDataSetChanged();
        viewPagerCard.setCurrentItem(currentPosition, true);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageScrollStateChanged(int state) {}

    public String getRutaIcono() {
        if (isDefaultIcon) {
            return null;
        }

        if (rutaChanged) {
            //Guardamos icono en carpeta local y en BBDD de Imagenes
            String ruta = getResourceIconFileName();
            photoUtils.guardarRecurso(imgIcono, ruta);
            db = db.open();
            db.insertImage(ruta, 1); //1 icono
            db.close();
            return ruta;
        }

        return rutaEdicion;
    }

    public String getStrategyEditName(long idEstrategia) {
        if (idEstrategia > 0) {
            db.open();
            String nombre = db.getStrategyStoredName(idEstrategia);
            db.close();
            return nombre;

        } else {
            return getStrategyName();
        }
    }

    public String getStrategyName() {
        strategyName = textName.getText().toString();
        if (strategyName == null || strategyName.equals("")) {
            db= db.open();
            long numberStrategies = db.getStrategiesNumber();
            db.close();

            strategyName = getString(R.string.strategy) + " " + (numberStrategies + 1);
        }
        return strategyName;
    }

    public void setStrategyEditIcono(long idEstrategia) {
        if (idEstrategia > 0) {
            db.open();
            String rutaIcono = db.getStrategyRutaIcono(idEstrategia);
            if ((rutaIcono != null) && (!rutaIcono.equals("-1"))) {
                isDefaultIcon = false;
                photoUtils.cargarRecursoUsuario(rutaIcono, imgIcono);
                rutaEdicion = rutaIcono;
            }
            db.close();
        }
    }

    public boolean isChangeCards() {
        ArrayList<Recurso> recursos = cardAdapter.getCardViews();
        int i = 0;

        db = db.open();

        ArrayList<Recurso> recursosAntiguos = db.getTarjetasByEstrategia(idEstrategia);

        if(recursos.size() != recursosAntiguos.size()) {
            return true;
        } else {
            for (Recurso r : recursos) {
                if(!r.equals(recursosAntiguos.get(i))) {
                    return true;
                }
                i++;
            }
        }

        return false;
    }

    public long guardarRecursosEstrategia (long idEstrategia) {
        long check = 0;

        ArrayList<Recurso> recursos = cardAdapter.getCardViews();
        db = db.open();

        //En caso de edición, borramos las tarjetas antiguas
        if(isEdition) {
            int checkDelete = db.deleteTarjetasByEstrategia(idEstrategia);
            if (checkDelete <= 0) {
                Toast.makeText(getContext(), getString(R.string.errorGuardarTarjetas), Toast.LENGTH_SHORT).show();
                Log.i("NewStrategy", getString(R.string.errorGuardarTarjetas));
                return -1;
            }
        }

        for (int i=0; i < recursos.size(); i++) {
            Recurso recurso = recursos.get(i);

            Log.d("RECURSO", "Ruta: " + recurso.getRutaValor().toString());
            Log.d("RECURSO", "Id: " + recurso.getIdBBDD());
            Log.d("RECURSO", "Tipo: " + recurso.getTipoRecurso());
            Log.d("RECURSO", "Orden: " + recurso.getOrder());

            //Recursos de tipo imagen
            if (recurso.getIdBBDD() >= 0) {
                if (recurso.getTipoRecurso() == 0 ) {
                    check = db.guardarTarjetaEstrategia(idEstrategia, recurso.getIdBBDD(), 0); //Predeterminado
                } else if ( recurso.getTipoRecurso() == 1) {
                    check = db.guardarTarjetaEstrategia(idEstrategia, recurso.getIdBBDD(), 1); //Propio
                }
                //Recurso de tipo texto
            } else if (recurso.getTipoRecurso() == 2) {
                //Obtener id de recurso de texto - > guardar texto
                long idTexto = guardarTexto(recurso);
                if (idTexto != -1) {
                    check = db.guardarTarjetaEstrategia(idEstrategia, idTexto, 2); //Texto
                } else {
                    check = -1;
                }
            }

            if (check == -1) {
                Toast.makeText(getContext(), getString(R.string.errorGuardarTarjetas), Toast.LENGTH_SHORT).show();
                Log.i("NewStrategy", getString(R.string.errorGuardarTarjetas));
                break;
            }
        }

        db.close();
        return check;
    }

    private long guardarTexto(Recurso recurso) {
        String literal = recurso.getRutaValor();
        int color = recurso.getColor();
        int size = recurso.getSize();
        int modo = recurso.getModo();

        DatabaseAdapter db = new DatabaseAdapter(getContext());
        db= db.open();
        long idTexto = db.insertText(literal, size, color, modo);
        db.close();

        return  idTexto;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_next, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_next) {
            ((NewStrategyActivity)getActivity()).moveToFragment(NewStrategyActivity.SETTINGS_POSITION);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Funcion que muestra un Dialog con las opciones para editar
     * el nombre de la estrategia
     */
    private void editStrategyDialog() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View popupStrategyView = inflater.inflate(R.layout.popup_strategy, null);

        AlertDialog.Builder constructor = new AlertDialog.Builder(getContext());
        constructor.setView(popupStrategyView);

        constructor.setPositiveButton(getString(R.string.accept),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) popupStrategyView.findViewById(R.id.edit_txt_name);
                        String newName = (String) editText.getText().toString();
                        if (!newName.equals("")){
                            textName.setText(newName);
                        }
                    }
                });

        constructor.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { }
        });

        AlertDialog helpDialog = constructor.create();
        helpDialog.show();
    }

    /**
     * Funcion que muestra un Dialog con las opciones posibles
     * de donde un usuario puede agregar una foto
     */
    public void dialogPhoto() {
        //Opciones del dialog con iconos y texto
        final Integer[] icons = new Integer[] {R.drawable.camera_android_icon, R.drawable.gallery_icon_android, R.drawable.delete_icon_android};
        final String[] opciones = getResources().getStringArray(R.array.opciones_foto_array);
        ListAdapter adapter = new ArrayAdapterWithIcon(getContext(), opciones, icons);

        AlertDialog.Builder constructor = new AlertDialog.Builder(getContext(), R.style.MyAlertDialogStyle);

        constructor.setTitle(R.string.select_image);
        constructor.setAdapter(adapter, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int item) {
                addIcon(opciones[item]);
            }
        }).show();
    }

    /**
     * Funcion que pone un icono de estrategia
     * cargado desde galeria o tomado desde la camara
     */
    public void addIcon(String opcion) {
        Intent intent=null;
        int code=-1;

        try {
            if (opcion.equals(getResources().getString(R.string.camera))) {
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                code= TAKE_PHOTO;

                //Creamos almacenamiento temporal
                File tempDir = PhotoUtils.createTemporaryFile("user", ".jpg", getContext());
                tempDir.delete();
                imgUri = Uri.fromFile(tempDir);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
                startActivityForResult(intent, code);

            } else if (opcion.equals(getResources().getString(R.string.gallery))) {
                intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("image/*");
                code = SELECT_PHOTO;
                startActivityForResult(intent, code);

            }else if (opcion.equals(getResources().getString(R.string.delete_photo))){
                imgIcono.setImageResource(R.drawable.icono_estrategia);
                isDefaultIcon = true;
            }

        } catch (Exception e) {
            Log.v(getClass().getSimpleName(),getResources().getString(R.string.errorFoto));
            Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.errorFoto), Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * Método que se encarga de asignar el recurso cargado
     * desde la cámara o la galería y editarlo con crop
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        File croppedImageFile = new File(getActivity().getFilesDir(), "tmp.jpg");

        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == TAKE_PHOTO) {
                //Gestionamos la accion de crop
                Uri croppedImage = Uri.fromFile(croppedImageFile);
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                cropImage.setSourceImage(imgUri);

                startActivityForResult(cropImage.getIntent(getContext()), CROP_PHOTO);

            } else if (requestCode == SELECT_PHOTO) {
                Uri selectedImage = data.getData();

                //Gestionamos la accion de crop
                Uri croppedImage = Uri.fromFile(croppedImageFile);
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                cropImage.setSourceImage(selectedImage);

                startActivityForResult(cropImage.getIntent(getContext()), CROP_PHOTO);

            } else if (requestCode == CROP_PHOTO) {
                imgIcono.setImageBitmap(BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath()));
                isDefaultIcon = false;
                rutaChanged = true;
            }
        }
    }

    /**
     * Método que se encarga de asignar un nombre de fichero
     * con el que guardar un recurso
     */
    public String getResourceFileName(){
        //Obtenemos numero de recursos propios de la BBDD
        DatabaseAdapter db = new DatabaseAdapter(getContext());
        db= db.open();
        long numberResources = db.getImagenesGuardadasNumber();
        db.close();

        return PhotoUtils.resourceFile + numberResources;
    }

    /**
     * Método que se encarga de asignar un nombre de fichero
     * con el que guardar un recurso de tipo icono
     */
    public String getResourceIconFileName(){
        //Obtenemos numero de recursos propios de la BBDD
        DatabaseAdapter db = new DatabaseAdapter(getContext());
        db= db.open();
        long numberResources = db.getIconosGuardadosNumber();
        db.close();

        return PhotoUtils.iconsFile + numberResources;
    }

    /*
     * Método que comprueba que la estrategia contenga tarjetas válidas
     */
    public boolean checkValidCards() {
        ArrayList<Recurso> recursos = cardAdapter.getCardViews();

        for (int i=0; i < recursos.size(); i++) {
            if ((recursos.get(i).getTipoRecurso() == 2) || (recursos.get(i).getIdBBDD() >= 0)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEdition() {
        return isEdition;
    }

    public long getIdEstrategia() {
        return idEstrategia;
    }
}
