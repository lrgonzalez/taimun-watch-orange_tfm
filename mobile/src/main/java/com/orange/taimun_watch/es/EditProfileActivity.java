package com.orange.taimun_watch.es;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;

import java.io.File;

import com.orange.taimun_watch.es.model.Usuario;

/**
 * Created by Yux on 23/05/2016.
 */
public class EditProfileActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {
    private static final int TAKE_PHOTO = 1;
    private static final int SELECT_PHOTO = 2;
    private static final int CROP_PHOTO = 3;
    private RadioGroup sexoGroup, tipoGroup;
    private TextView txtSexo, txtTipoReloj, txtEstrategias, txtRegulaciones, labelEditPhoto;
    private EditText txtUsername;
    private ImageButton btnCamara;
    private ImageView profilePhoto;
    private Uri imgUri = null;
    private boolean userImgDefault, photoChanged = false;
    private DatabaseAdapter db;
    private PhotoUtils photoUtils;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_activity);

        //Recuperamos toolbar y la asignamos como SupportActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.new_profile);

        //Recuperamos foto de perfil
        profilePhoto = (ImageView) findViewById(R.id.profilePhoto);

        //Gestión de textos
        txtEstrategias = (TextView) findViewById(R.id.txtNumEstrategias);
        txtRegulaciones = (TextView) findViewById(R.id.txtNumRegulaciones);
        //txtSexo = (TextView) findViewById(R.id.txtSexo);
        txtTipoReloj = (TextView) findViewById(R.id.txtTipoReloj);
        labelEditPhoto = (TextView) findViewById(R.id.label_edit_camera);
        labelEditPhoto.setOnClickListener(this);
        txtUsername = (EditText) findViewById(R.id.user_name);

        //Acciones de los radiobuttons
        //sexoGroup = (RadioGroup) findViewById(R.id.genderGroup);
        tipoGroup = (RadioGroup) findViewById(R.id.watchTypeGroup);
        //sexoGroup.setOnCheckedChangeListener(this);
        tipoGroup.setOnCheckedChangeListener(this);

        //Gestion de botones
        btnCamara = (ImageButton) findViewById(R.id.btn_profile_camara);
        btnCamara.setOnClickListener(this);

        //Cargar datos de usuario
        photoUtils = new PhotoUtils(this);
        loadUserData();

        //Cargamos la información de sus estrategias y regulaciones
        setStrategiesAndRegulationNumber();

    }

    /*
     * Metodo para obtener y cargar los datos del usuario
     */
    private void loadUserData() {
        DatabaseAdapter db = new DatabaseAdapter(this);
        db.open();
        Usuario usuario = db.getUser(Preferences.getId(this));
        db.close();

        if(usuario != null) {
            txtUsername.setText(usuario.getNombre());

            /**
            if(usuario.getSexo().equals(getString(R.string.man))){
                sexoGroup.check(R.id.radioButton_man);
                txtSexo.setText(getString(R.string.man));
            } else {
                sexoGroup.check(R.id.radioButton_woman);
                txtSexo.setText(getString(R.string.woman));
            }
            */

            if(usuario.getTipoReloj().equals(getString(R.string.square))){
                tipoGroup.check(R.id.radioButton_square);
                txtTipoReloj.setText(getString(R.string.square));
            } else {
                tipoGroup.check(R.id.radioButton_round);
                txtTipoReloj.setText(getString(R.string.round));
            }

            if(usuario.getFoto() == null){
                profilePhoto.setImageResource(R.drawable.logo_user);
                userImgDefault = true;
                photoChanged = false;
            } else {
                photoUtils.cargarRecursoUsuario(usuario.getFoto(), profilePhoto);
                userImgDefault = false;
            }
        }
    }

    /**
     * Funcion que cambia las opciones de genero y tipo de reloj
     * segun la seleccion del usuario
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            /*case (R.id.radioButton_man):
                txtSexo.setText(getString(R.string.man));
                break;
            case (R.id.radioButton_woman):
                txtSexo.setText(getString(R.string.woman));
                break;
             */
            case (R.id.radioButton_round):
                txtTipoReloj.setText(getString(R.string.round));
                break;
            case (R.id.radioButton_square):
                txtTipoReloj.setText(getString(R.string.square));
                break;
        }
    }

    /**
     * Funcion que cambia la foto de perfil del usuario o la
     * elimina (poniendo una por defecto), segun la eleccion
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_profile_camara:
                dialogPhoto();
                break;

            case R.id.label_edit_camera:
                dialogPhoto();
                break;
        }
    }

    /*
     * Metodo para inicializar el número de estrategias y regulaciones del usuario
     */
    private void setStrategiesAndRegulationNumber() {
        DatabaseAdapter db = new DatabaseAdapter(this);
        db.open();
        long strategiesNumber = db.getStrategiesNumber();
        long regulationsNumber = db.getRegulationsNumber();
        db.close();

        if (strategiesNumber == 1){
            txtEstrategias.setText(strategiesNumber + " " + getString(R.string.strategy));
        } else {
            txtEstrategias.setText(strategiesNumber + " " + getString(R.string.strategies));
        }

        if(regulationsNumber == 1){
            txtRegulaciones.setText(regulationsNumber + " " + getString(R.string.regulation));
        } else {
            txtRegulaciones.setText(regulationsNumber + " " + getString(R.string.regulations));
        }
    }

    /**
     * Comprueba los valores introducidos por el usuario antes de
     * actualizar sus datos en el Sistema
     * @return 0 si OK o -1 si error
     */
    private int actualizarPerfil() {
        Usuario usuario = new Usuario();

        String name = txtUsername.getText().toString();
        if(name.equals("")) {
            name = getString(R.string.name_default);
        }
        usuario.setNombre(name);

        /**
        int genero = sexoGroup.getCheckedRadioButtonId();
        if (genero == R.id.radioButton_man) {
            usuario.setSexo(getString(R.string.man));
        } else {
            usuario.setSexo(getString(R.string.woman));
        }*/

        int tipoReloj = tipoGroup.getCheckedRadioButtonId();
        if (tipoReloj == R.id.radioButton_square) {
            usuario.setTipoReloj(getString(R.string.square));
        } else {
            usuario.setTipoReloj(getString(R.string.round));
        }

        if(userImgDefault){
            usuario.setFoto(null);
        } else {
            if (photoChanged) {
                photoUtils.guardarFoto(profilePhoto);
                usuario.setFoto(PhotoUtils.fotoFile);
            }
        }

        //Actualizamos usuario en BBDD
        db = new DatabaseAdapter(this);
        db= db.open();
        long check = db.updateUser(Preferences.getId(this), usuario);
        db.close();

        if (check==-1) {
            Toast.makeText(EditProfileActivity.this, getString(R.string.errorActualizarDatos),Toast.LENGTH_SHORT).show();
            Log.i("EditProfileActivity", getString(R.string.errorAxtualizarUsuario));
            return -1;

        } else {
            actualizarPreferencias(usuario);
        }

        return 0;
    }

    /**
     * Actualiza las preferencias de la app con los
     * nuevos datos del usuario
     */
    private void actualizarPreferencias(Usuario usuario) {
        Preferences.setUsername(this, usuario.getNombre());
        Preferences.setWatchtype(this, getWatchTypeValue(usuario.getTipoReloj()));
        Preferences.setUserphoto(this, usuario.getFoto());
    }

    /*  ***********************************
    Opciones del menu de la toolbar
    ***********************************
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            actualizarPerfil();
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Funcion que muestra un Dialog con las opciones posibles
     * de donde un usuario puede agregar una foto
     */
    public void dialogPhoto() {
        //Opciones del dialog con iconos y texto
        final Integer[] icons = new Integer[] {R.drawable.camera_android_icon, R.drawable.gallery_icon_android, R.drawable.delete_icon_android};
        final String[] opciones = getResources().getStringArray(R.array.opciones_foto_array);
        ListAdapter adapter = new ArrayAdapterWithIcon(this, opciones, icons);

        AlertDialog.Builder constructor = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);

        constructor.setTitle(R.string.select_image);
        constructor.setAdapter(adapter, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int item) {
                addPhoto(opciones[item]);
            }
        }).show();
    }

    /**
     * Funcion que pone una foto de imagen del usuario
     * cargada desde galeria o tomada desde la camara
     */
    public void addPhoto(String opcion) {
        Intent intent=null;
        int code=-1;

        try {
            if (opcion.equals(getResources().getString(R.string.camera))) {
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                code= TAKE_PHOTO;

                //Creamos almacenamiento temporal
                File tempDir = PhotoUtils.createTemporaryFile("user", ".jpg", this);
                tempDir.delete();
                imgUri = Uri.fromFile(tempDir);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
                startActivityForResult(intent, code);

            } else if (opcion.equals(getResources().getString(R.string.gallery))) {
                intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("image/*");
                code = SELECT_PHOTO;
                startActivityForResult(intent, code);

            }else if (opcion.equals(getResources().getString(R.string.delete_photo))){
                profilePhoto.setImageResource(R.drawable.logo_user);
                userImgDefault = true;
            }

        } catch (Exception e) {
            Log.v(getClass().getSimpleName(),getResources().getString(R.string.errorFoto));
            Toast toast = Toast.makeText(this, getResources().getString(R.string.errorFoto), Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * Método que se encarga de asignar el recurso cargado
     * desde la cámara o la galería y editarlo con crop
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        File croppedImageFile = new File(getFilesDir(), "tmp.jpg");

        if (resultCode == RESULT_OK) {
            if (requestCode == TAKE_PHOTO) {
                //Gestionamos la accion de crop
                Uri croppedImage = Uri.fromFile(croppedImageFile);
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                cropImage.setSourceImage(imgUri);

                startActivityForResult(cropImage.getIntent(this), CROP_PHOTO);

            } else if (requestCode == SELECT_PHOTO) {
                Uri selectedImage = data.getData();

                //Gestionamos la accion de crop
                Uri croppedImage = Uri.fromFile(croppedImageFile);
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                cropImage.setSourceImage(selectedImage);

                startActivityForResult(cropImage.getIntent(this), CROP_PHOTO);

            } else if (requestCode == CROP_PHOTO) {
                profilePhoto.setImageBitmap(BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath()));
                userImgDefault = false;
                photoChanged = true;
            }
        }
    }

    private String getWatchTypeValue(String type) {
        String value;
        if (type.equals(getString(R.string.square))) {
            value = Preferences.SQUARE;
        } else {
            value = Preferences.ROUND;
        }
        return value;
    }

}
