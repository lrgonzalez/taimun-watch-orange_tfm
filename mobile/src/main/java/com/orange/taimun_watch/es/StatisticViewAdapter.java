package com.orange.taimun_watch.es;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.orange.taimun_watch.es.model.Estadistica;

/**
 * Created by lucia on 01/02/2017.
 */

public class StatisticViewAdapter extends RecyclerView.Adapter<StatisticViewAdapter.ItemViewHolder> {
    private ArrayList<Estadistica> estadisticas;
    private final OnItemClickListener listener;

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        /*Elementos dentro de la vista de la tarjeta.*/
        public TextView nombreEstadistica; //fecha

        public ItemViewHolder(View itemView) {
            super(itemView);
            nombreEstadistica = (TextView) itemView.findViewById(R.id.statistic_name);
        }

        /*Funcion de asignacion del listener a la vista.*/
        public void bind(final Estadistica estadistica, final StatisticViewAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(estadistica);
                }
            });
        }
    }

    public StatisticViewAdapter(ArrayList<Estadistica> estadisticas, final StatisticViewAdapter.OnItemClickListener listener) {
        this.estadisticas = estadisticas;
        this.listener = listener;
    }

    @Override
    public StatisticViewAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.statistic_cardview, parent, false);

        StatisticViewAdapter.ItemViewHolder itemViewHolder = new StatisticViewAdapter.ItemViewHolder(view);
        return itemViewHolder;
    }

    /*Funcion que devuelve los datos de una vista en una posicion especificada y los carga en el ViewHolder*/
    @Override
    public void onBindViewHolder(final StatisticViewAdapter.ItemViewHolder holder, int position) {
        //Obtenemos la lista de estadisticas que se encuentra en la posicion especificada.
        Estadistica estadistica = estadisticas.get(position);

        //Asignamos nombre.
        holder.nombreEstadistica.setText(estadistica.getNombre());

        //Llamamos a bind para asignar el listener a la vista.
        holder.bind(estadistica, listener);
    }

    @Override
    public int getItemCount() {
        return estadisticas.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Estadistica item);
    }

    public void updateStatistics(ArrayList<Estadistica> listaEstadisticas) {
        this.estadisticas = listaEstadisticas;
    }
}
