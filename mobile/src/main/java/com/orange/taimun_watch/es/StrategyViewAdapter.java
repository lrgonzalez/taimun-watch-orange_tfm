package com.orange.taimun_watch.es;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.model.ListaEstrategia;


/**
 * @author Antonio
 */
public class StrategyViewAdapter extends RecyclerView.Adapter<StrategyViewAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {

    private ArrayList<ListaEstrategia> estrategias;
    private boolean strategy;
    private PhotoUtils photoUtils;
    private Context context;
    private final OnItemClickListener listener;
    private boolean isEditable = false;
    private boolean fromRegulation = false;

    private final List<String> mItems = new ArrayList<>();

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        /*Elementos dentro de la vista de la tarjeta.*/
        public ImageView imgIcon;
        public TextView nombreEstrategia, numeroTarjetas;

        public ItemViewHolder(View itemView) {
            super(itemView);
            imgIcon = (ImageView) itemView.findViewById(R.id.strategy_icon);
            nombreEstrategia = (TextView) itemView.findViewById(R.id.strategy_name);
            numeroTarjetas = (TextView) itemView.findViewById(R.id.strategy_cards);
        }

        /*Funcion de asignacion del listener a la vista.*/
        public void bind(final ListaEstrategia estrategia, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(estrategia);
                }
            });
        }
    }

    public StrategyViewAdapter(ArrayList<ListaEstrategia> estrategias, Context context,
                               boolean isEditable, final OnItemClickListener listener,
                               boolean strategy, boolean fromRegulation) {
        this.estrategias = estrategias;
        this.context = context;
        this.photoUtils = new PhotoUtils(context);
        this.listener = listener;
        this.isEditable = isEditable;
        this.strategy = strategy;
        this.fromRegulation = fromRegulation;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(fromRegulation) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strategy_cardview_from_regulation, parent, false);
        } else if(!isEditable) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strategy_cardview, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.strategy_editable_cardview, parent, false);
        }

        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    /*Funcion que devuelve los datos de una vista en una posicion especificada y los carga en el ViewHolder*/
    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        //Obtenemos la lista de estrategias que se encuentra en la posicion especificada.
        ListaEstrategia estrategia = estrategias.get(position);

        //Asignamos nombre.
        holder.nombreEstrategia.setText(estrategia.getNombre());

        //Cargamos imagen de icono.
        if(estrategia.getRutaIcono() == null) {
            holder.imgIcon.setImageResource(R.drawable.icono_estrategia);
        } else {
            photoUtils.cargarRecursoUsuario(estrategia.getRutaIcono(), holder.imgIcon);
        }

        //Cargamos numero de tarjetas.
        if (estrategia.getNumTarjetas() == 1) {
            holder.numeroTarjetas.setText(estrategia.getNumTarjetas() + " " + context.getString(R.string.card));
        } else {
            holder.numeroTarjetas.setText(estrategia.getNumTarjetas() + " " + context.getString(R.string.cards));
        }

        //Llamamos a bind para asignar el listener a la vista.
        if(!isEditable) {
            holder.bind(estrategia, listener);
        }
    }

    @Override
    public void onItemDismiss(int position) {

        final int pos = position;

        if(this.strategy == true ) {
            //Diálogo de guardado.
            //Instantiate an AlertDialog.Builder with its constructor
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this.context);

            //Chain together various setter methods to set the dialog characteristics
            builder.setMessage(R.string.dialog_borrado)
                    .setTitle(R.string.dialog_title_borrado)
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            DatabaseAdapter db = new DatabaseAdapter(context);
                            db.open();
                            db.deleteStrategyById(estrategias.get(pos).getIdBBDD());
                            db.close();

                            estrategias.remove(pos);
                            notifyItemRemoved(pos);

                        } }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                            notifyItemChanged(pos);
                } });

            //Get the AlertDialog from create()
            android.app.AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            estrategias.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        ListaEstrategia prev = estrategias.remove(fromPosition);
        estrategias.add(toPosition > fromPosition ? toPosition - 1 : toPosition, prev);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public int getItemCount() {
        return estrategias.size();
    }

    public void updateStrategies(ArrayList<ListaEstrategia> listaEstrategias) {
        this.estrategias = listaEstrategias;
    }

    public interface OnItemClickListener {
        void onItemClick(ListaEstrategia item);
    }
}
