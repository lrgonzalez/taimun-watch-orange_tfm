package com.orange.taimun_watch.es;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by LR.283463 on 29/10/2018.
 */

public class GroupsActivity extends AppCompatActivity {

    private EditText groupName;
    private EditText txtPIN;
    private String pin;
    private Button btnJoin;
    private Button btnCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.groups_fragment);

        //Recuperamos toolbar y la asignamos como SupportActionBar
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(R.string.pin);

        //Recuperamos elemento de vista
        groupName = (EditText) findViewById(R.id.group_value);
        txtPIN = (EditText) findViewById(R.id.pin_value);

        //Recuperamos valor de pin
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        pin = sharedPreferences.getString(getString(R.string.pin), "");


        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                join();
            }
        });
    }


    private void join(){
        // Unirse al grupo

        // Guardar nombre grupo en preferencias
        // TODO comprobar en algun punto por aqui que existe antes de guardarlo en preferencias
        Preferences.setGroupCode(getApplicationContext(), groupName.getText().toString());
        // Llamar a exportar para actualizar ficheros de firebase de ese usuario
        // 1. ksFile ya esta en ese repo, bajar y añadir a nuestro keyStore. TODO que pasa si ya lo teniamos? Comprobar que el grupo no esta ya en el keystore
        // 2. usar el firebaseDownload de Datareceiver para bajar la lista de ficheros y todos los que no tenga
        // 3. Llamar al reloj con PIN_SYNC para que se coja el nuevo ks, pin y grupo
    }
}
