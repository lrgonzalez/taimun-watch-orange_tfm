package com.orange.taimun_watch.es;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;

import java.io.File;
import java.util.ArrayList;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.model.Estrategia;
import com.orange.taimun_watch.es.model.ListaEstrategia;

/**
 * Created by Yux on 08/08/2016.
 */
public class NewRegulationActivity extends AppCompatActivity implements View.OnClickListener {
    private boolean isDefaultIcon = true;
    private static final int TAKE_PHOTO = 1;
    private static final int SELECT_PHOTO = 2;
    private static final int CROP_PHOTO = 3;
    private Uri imgUri = null;
    private PhotoUtils photoUtils;
    private String regulationName = null;
    private ImageView imgIcono;
    private ImageButton btnEdit;
    private TextView textName;
    private boolean isSelector = false;
    private ArrayList<ListaEstrategia> lstEstrategias = null, lstEstrategiasSeleccionadas;
    private ArrayList<String> nombreEstrategias = null;
    private FloatingActionButton btnAddStrategy;
    private RecyclerView recyclerView;
    private StrategyViewAdapter  strategyAdapter;
    private long idRegulacion = -1;
    private boolean isEdition = false, rutaChanged = false;
    private DatabaseAdapter db;
    private String rutaEdicion = "";
    private DialogFragment df;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_regulation_activity);
        db = new DatabaseAdapter(this);
        photoUtils = new PhotoUtils(this);

        //Comprobamos si es creación o edición de estrategia
        Intent intent = getIntent();
        idRegulacion = intent.getLongExtra("regulationId", -1);

        //Recuperamos toolbar y la asignamos como SupportActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (idRegulacion > 0 ) {
            isEdition = true;
            getSupportActionBar().setTitle(R.string.edit_regulation);
        } else {
            getSupportActionBar().setTitle(R.string.new_regulation);
        }

        //Gestion de textos e imagenes
        textName = (TextView) findViewById(R.id.regulation_name);
        textName.setText(getStrategyEditName(idRegulacion));
        imgIcono = (ImageView) findViewById(R.id.regulation_icon);
        setRegulationEditIcono(idRegulacion);
        btnEdit = (ImageButton) findViewById(R.id.regulation_edit);

        //Obtenemos boton de añadir estrategia y le asignamos su listener
        btnAddStrategy = (FloatingActionButton) findViewById(R.id.btn_add_strategyList);

        btnAddStrategy.setOnClickListener(this);
        imgIcono.setOnClickListener(this);
        btnEdit.setOnClickListener(this);

        //Inicializamos recyclerView
        recyclerView = (RecyclerView) findViewById(R.id.lst_strategies_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        //Inicializamos la lista de estrategias disponibles a mostrar para seleccionar
        inicializarListaEstrategias();
        //Actualizar lista con contenido si estamos editando la regulación
        inicializarListaEstrategiasSeleccionadas();

        //Asignamos adaptador para permitir drag and swipe
        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(strategyAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

        //Vemos el dialog de edicion
        if (idRegulacion < 0) {
            editRegulationDialog();
        }

    }

    private void inicializarListaEstrategiasSeleccionadas() {
        //Caso de edicicion
        if (idRegulacion > 0) {
            db.open();
            lstEstrategiasSeleccionadas = db.getStrategiesOverviewByRegulation(idRegulacion);
            db.close();

            //Caso nueva
        } else {
            lstEstrategiasSeleccionadas = new ArrayList<ListaEstrategia>();
        }

        strategyAdapter = new StrategyViewAdapter(lstEstrategiasSeleccionadas, this, true, null, false, false);
        recyclerView.setAdapter(strategyAdapter);
    }

    private void inicializarListaEstrategias() {
        db = new DatabaseAdapter(this);
        db = db.open();
        lstEstrategias = db.getStrategiesOverview();
        db.close();

        if ((lstEstrategias != null) && (!lstEstrategias.isEmpty())) {
            nombreEstrategias = new ArrayList<String>();
            for (int i=0; i<lstEstrategias.size(); i++) {
                nombreEstrategias.add(lstEstrategias.get(i).getNombre());
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regulation_icon:
                dialogPhoto();
                break;

            case R.id.regulation_edit:
                editRegulationDialog();
                break;

            case R.id.btn_add_strategyList:
                addStrategyDialog();
                break;

        }
    }

    /*  ***********************************
    Opciones del menu de la toolbar
    ***********************************
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        final MenuItem item2 = item;

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if(isChange()) {
                    //Diálogo de guardado.
                    //Instantiate an AlertDialog.Builder with its constructor
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

                    //Chain together various setter methods to set the dialog characteristics
                    builder.setMessage(R.string.dialog_guardado)
                            .setTitle(R.string.dialog_title_guardado)
                            .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    guardarRegulacion();
                                    finish();
                                }
                            }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

                    //Get the AlertDialog from create()
                    android.app.AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    finish();
                }

                break;
            case R.id.action_save:
                long check = guardarRegulacion();
                if (check >= 0) {
                    this.finish();
                }
                break;
        }

        return true;
    }

    /**
     * Funcion que muestra un Dialog con las opciones para editar
     * las caracteristicas de la regulacion
     */
    private void editRegulationDialog() {
        LayoutInflater inflater = getLayoutInflater();
        final View popupRegulationView = inflater.inflate(R.layout.popup_regulation, null);

        AlertDialog.Builder constructor = new AlertDialog.Builder(this);
        constructor.setView(popupRegulationView);

        constructor.setPositiveButton(getString(R.string.accept),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) popupRegulationView.findViewById(R.id.edit_txt_name);
                        String newName = (String) editText.getText().toString();
                        if (!newName.equals("")){
                            textName.setText(newName);
                        }

                        CheckBox selector = (CheckBox) popupRegulationView.findViewById(R.id.selector_checkbox);
                        isSelector = selector.isChecked();
                    }
                });

        constructor.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { }
        });

        AlertDialog helpDialog = constructor.create();
        helpDialog.show();
    }

    /**
     * Funcion que muestra un Dialog con las lista de
     * estatregias para añadir
     */
    private void addStrategyDialog() {
        if ((nombreEstrategias == null) || (nombreEstrategias.isEmpty())) {
            Log.v(getClass().getSimpleName(), getResources().getString(R.string.errorNoneEstrategias));
            Toast.makeText(NewRegulationActivity.this, getResources().getString(R.string.errorNoneEstrategias),Toast.LENGTH_SHORT).show();

        } else {
            // Create and show the dialog.
            df = new StrategiesDialogFragment();
            df.show(getSupportFragmentManager(), "dialog");
        }

    }

    public void addStrategy(ListaEstrategia e) {
        lstEstrategiasSeleccionadas.add(e);
        strategyAdapter.notifyItemInserted(lstEstrategiasSeleccionadas.size() -1);
        df.dismiss();
    }

    public String getStrategyEditName(long idRegulacion) {
        if (idRegulacion > 0) {
            db.open();
            String nombre = db.getRegulationStoredName(idRegulacion);
            db.close();
            return nombre;

        } else {
            return getRegulationName();
        }
    }

    public String getRegulationName() {
        regulationName = textName.getText().toString();
        if (regulationName == null || regulationName.equals("")) {
            DatabaseAdapter db = new DatabaseAdapter(this);
            db= db.open();
            long numberRegulations = db.getRegulationsNumber();
            db.close();

            regulationName = getString(R.string.regulation) + " " + (numberRegulations + 1);
        }
        return regulationName;
    }

    public String getRutaIcono() {
        if (isDefaultIcon) {
            return null;
        }

        if (rutaChanged) {
            //Guardamos icono en carpeta local y en BBDD de Imagenes
            String ruta = getResourceIconFileName();
            photoUtils.guardarRecurso(imgIcono, ruta);
            db = db.open();
            db.insertImage(ruta, 1); //1 icono
            db.close();
            return ruta;
        }

        return rutaEdicion;
    }

    public void setRegulationEditIcono(long idRegulacion) {
        if (idRegulacion > 0) {
            db.open();
            String rutaIcono = db.getRegulationRutaIcono(idRegulacion);
            if ((rutaIcono != null) && (!rutaIcono.equals("-1"))) {
                isDefaultIcon = false;
                photoUtils.cargarRecursoUsuario(rutaIcono, imgIcono);
                rutaEdicion = rutaIcono;
            }
            db.close();
        }
    }

    /**
     * Método que se encarga de asignar un nombre de fichero
     * con el que guardar un recurso de tipo icono
     */
    public String getResourceIconFileName(){
        //Obtenemos numero de recursos propios de la BBDD
        db= db.open();
        long numberResources = db.getIconosGuardadosNumber();
        db.close();

        return PhotoUtils.iconsFile + numberResources;
    }

    /**
     * Funcion que muestra un Dialog con las opciones posibles
     * de donde un usuario puede agregar una foto
     */
    public void dialogPhoto() {
        //Opciones del dialog con iconos y texto
        final Integer[] icons = new Integer[] {R.drawable.camera_android_icon, R.drawable.gallery_icon_android, R.drawable.delete_icon_android};
        final String[] opciones = getResources().getStringArray(R.array.opciones_foto_array);
        ListAdapter adapter = new ArrayAdapterWithIcon(this, opciones, icons);

        AlertDialog.Builder constructor = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);

        constructor.setTitle(R.string.select_image);
        constructor.setAdapter(adapter, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int item) {
                addIcon(opciones[item]);
            }
        }).show();
    }

    /**
     * Funcion que pone un icono de estrategia
     * cargado desde galeria o tomado desde la camara
     */
    public void addIcon(String opcion) {
        Intent intent=null;
        int code=-1;

        try {
            if (opcion.equals(getResources().getString(R.string.camera))) {
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                code= TAKE_PHOTO;

                //Creamos almacenamiento temporal
                File tempDir = PhotoUtils.createTemporaryFile("user", ".jpg", this);
                tempDir.delete();
                imgUri = Uri.fromFile(tempDir);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
                startActivityForResult(intent, code);

            } else if (opcion.equals(getResources().getString(R.string.gallery))) {
                intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("image/*");
                code = SELECT_PHOTO;
                startActivityForResult(intent, code);

            }else if (opcion.equals(getResources().getString(R.string.delete_photo))){
                imgIcono.setImageResource(R.drawable.icon_estrategia);
                isDefaultIcon = true;
            }

        } catch (Exception e) {
            Log.v(getClass().getSimpleName(),getResources().getString(R.string.errorFoto));
            Toast toast = Toast.makeText(this, getResources().getString(R.string.errorFoto), Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * Método que se encarga de asignar el recurso cargado
     * desde la cámara o la galería y editarlo con crop
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        File croppedImageFile = new File(getFilesDir(), "tmp.jpg");

        if (resultCode == RESULT_OK) {
            if (requestCode == TAKE_PHOTO) {
                //Gestionamos la accion de crop
                Uri croppedImage = Uri.fromFile(croppedImageFile);
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                cropImage.setSourceImage(imgUri);

                startActivityForResult(cropImage.getIntent(this), CROP_PHOTO);

            } else if (requestCode == SELECT_PHOTO) {
                Uri selectedImage = data.getData();

                //Gestionamos la accion de crop
                Uri croppedImage = Uri.fromFile(croppedImageFile);
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                cropImage.setSourceImage(selectedImage);

                startActivityForResult(cropImage.getIntent(this), CROP_PHOTO);

            } else if (requestCode == CROP_PHOTO) {
                imgIcono.setImageBitmap(BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath()));
                isDefaultIcon = false;
                rutaChanged = true;
            }
        }
    }

    private boolean isChange() {

        if (isEdition == false) {
            return true;
        }

        //Recuperamos nombre y ruta del icono
        String regulationName = getRegulationName();
        String icono, iconoAntiguo;
        boolean isIconoChange = true;

        DatabaseAdapter db = new DatabaseAdapter(this);
        db = db.open();

        icono = getRutaIcono();
        iconoAntiguo = db.getRegulationRutaIcono(idRegulacion);

        if(icono == null) {
            if(iconoAntiguo == null) {
                isIconoChange = false;
            }
        } else {
            if(icono.equals(iconoAntiguo)) {
                isIconoChange = false;
            }
        }

        if(isIconoChange || !db.getRegulationStoredName(idRegulacion).equals(getRegulationName())) {
            return true;
        }

        return false || isChangeStrategies();
    }

    private boolean isChangeStrategies() {
        DatabaseAdapter db = new DatabaseAdapter(this);
        db = db.open();

        ArrayList<Estrategia> estrategiasAntiguas = db.getStrategiesByRegulation(idRegulacion, false);

        if(estrategiasAntiguas.size() != lstEstrategiasSeleccionadas.size()) {
            return true;
        }


        for(int i = 0; i < estrategiasAntiguas.size(); i++) {
            if(estrategiasAntiguas.get(i).getId() != lstEstrategiasSeleccionadas.get(i).getIdBBDD()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Funcion que guarda una regulación
     * Primero guarda los parámetros de configuración y luego llama a otro método
     * para guardar los estrategias asociadas
     */
    private long guardarRegulacion() {
        int checkUpdate = 0;
        DatabaseAdapter db = new DatabaseAdapter(this);

        if (lstEstrategiasSeleccionadas == null || lstEstrategiasSeleccionadas.isEmpty()) {
            Toast.makeText(this, getString(R.string.errorRegulacionSinEstrategias), Toast.LENGTH_SHORT).show();
            return -1;

        } else {
            db = db.open();

            //En caso de edición, borramos las estrategias antiguas
            if(isEdition) {
                int checkDelete = db.deleteEstrategiasByRegulacion(idRegulacion);
                if (checkDelete <= 0) {
                    Toast.makeText(this, getString(R.string.errorGuardarEstrategias), Toast.LENGTH_SHORT).show();
                    Log.i("NewRegulation", getString(R.string.errorGuardarEstrategias));
                    return -1;
                }
            }

            //Recuperamos nombre y ruta del icono
            String regulationName = getRegulationName();
            String rutaIcono = getRutaIcono();

            //Guardamos regulacion
            if (isEdition) {
                checkUpdate = db.updateRegulation(idRegulacion, regulationName, rutaIcono, true, isSelector);
            } else {
                idRegulacion = db.saveNewRegulation(regulationName, rutaIcono, true, isSelector);
            }

            db.close();

            if (idRegulacion == -1 || (isEdition && checkUpdate == 0)){
                Toast.makeText(this, getString(R.string.errorCrearRegulacion), Toast.LENGTH_SHORT).show();
                Log.i("NewRegulationActivity", getString(R.string.errorCrearRegulacion));
                return -1;

            } else {
                return guardarEstrategiasRegulacion(idRegulacion);
            }
        }
    }

    private long guardarEstrategiasRegulacion(long idRegulacion) {
        long check = 0;

        db = db.open();

        for (int i=0; i < lstEstrategiasSeleccionadas.size(); i++) {
            ListaEstrategia estrategia = lstEstrategiasSeleccionadas.get(i);
            check = db.guardarEstrategiaSchedule(idRegulacion, estrategia.getIdBBDD());

            if (check == -1) {
                Toast.makeText(this, getString(R.string.errorGuardarEstrategias), Toast.LENGTH_SHORT).show();
                Log.i("NewRegulationActivity", getString(R.string.errorGuardarEstrategias));
                break;
            }
        }

        db.close();
        return check;
    }

    @Override
    public void onBackPressed() {
        if(isChange()) {
            //Diálogo de guardado.
            //Instantiate an AlertDialog.Builder with its constructor
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

            //Chain together various setter methods to set the dialog characteristics
            builder.setMessage(R.string.dialog_guardado)
                    .setTitle(R.string.dialog_title_guardado)
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            guardarRegulacion();
                            finish();
                        }
                    }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            //Get the AlertDialog from create()
            android.app.AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            super.onBackPressed();
        }
    }
}
