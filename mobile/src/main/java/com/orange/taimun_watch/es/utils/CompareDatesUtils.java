package com.orange.taimun_watch.es.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase con la funcion que compara fechas para su ordenación.
 *
 * @author Lucia
 */

public class CompareDatesUtils {

    /**
     * Compara las dos fechas indicadas para ver cuál es mayor o menor o si son iguales.
     *
     * @param fechaIni primera fecha a comparar
     * @param fechaFin segunda fecha a comparar
     * @param patronFormato formato en el que vienen las fechas dadas, por ejemplo, "dd/MM/yyyy"
     * @return 1 si fechaIni es mayor que fechaFin, -1 en caso contrario, 0 si son iguales, 2 si hay error.
     */
    public static int comparaFechas(String fechaIni, String fechaFin, String patronFormato) { // "dd/MM/yyyy" "dd_MM_yyyy"
        SimpleDateFormat sdf = new SimpleDateFormat(patronFormato);
        Date dateIni = null;
        Date dateEnd = null;
        try {
            dateIni = sdf.parse(fechaIni);
//            System.out.println("date1 : " + sdf.format(dateIni));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            dateEnd = sdf.parse(fechaFin);
//            System.out.println("date2 : " + sdf.format(dateEnd));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (dateIni != null && dateEnd != null) {
            if (dateIni.compareTo(dateEnd) > 0) { //16 > 15
//                System.out.println("Date1 is after Date2");
                return 1;
            } else if (dateIni.compareTo(dateEnd) < 0) {
//                System.out.println("Date2 is after Date1");
                return -1;
            } else {
//                System.out.println("Date1 is equal Date2");
                return 0;
            }
        }
        return 2;
    }
}
