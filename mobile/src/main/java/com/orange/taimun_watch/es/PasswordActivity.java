package com.orange.taimun_watch.es;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Yux on 21/08/2016.
 */
public class PasswordActivity extends AppCompatActivity {
    private EditText txtPIN;
    private String pin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password);

        //Recuperamos toolbar y la asignamos como SupportActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.pin);

        //Recuperamos elemento de vista
        txtPIN = (EditText) findViewById(R.id.pin_value);

        //Recuperamos valor de pin
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        pin = sharedPreferences.getString(getString(R.string.pin), "");
    }

    /*  ***********************************
    Opciones del menu de la toolbar
    ***********************************
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_check, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_check) {
            if (txtPIN.getText().toString().equals("")) {
                Toast.makeText(this, getString(R.string.errorPinEmpty), Toast.LENGTH_SHORT).show();
            }
            else {
                if (checkUserPassword()) {
                    Intent main = new Intent(PasswordActivity.this, MainActivity.class);
                    startActivity(main);
                    this.finish();

                } else {
                    Toast.makeText(this, getString(R.string.errorPin), Toast.LENGTH_SHORT).show();
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean checkUserPassword() {
        return pin.equals(txtPIN.getText().toString());
    }
}
