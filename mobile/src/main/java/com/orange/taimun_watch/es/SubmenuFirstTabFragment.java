package com.orange.taimun_watch.es;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;

import org.xdty.preference.colorpicker.ColorPickerDialog;
import org.xdty.preference.colorpicker.ColorPickerSwatch;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.model.Recurso;

/**
 * Created by Yux on 20/06/2016.
 * Modified by Yux on 03/07/2016.
 */
public class SubmenuFirstTabFragment extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private ImageButton btnColorPicker, btnEditText;
    private int selectedColor, selectedMode, selectedSize;
    private FragmentManager fragmentManager;
    private static final int SMALL=22, NORMAL=29, HUGE=39;
    private static final int PHRASE=0, MAYUS=1, MINUS=2;
    private String currentText = null;
    private RadioGroup modoGroup, sizeGroup;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.submenu_first_tab_fragment, container, false);

        //Obtenemos fragment manager
        fragmentManager = getActivity().getFragmentManager();

        //Obtenemos botones
        btnColorPicker = (ImageButton) view.findViewById(R.id.btn_colorPicker);
        btnEditText = (ImageButton) view.findViewById(R.id.btn_editText);
        btnEditText.setOnClickListener(this);
        btnColorPicker.setOnClickListener(this);

        //Acciones de los radiobuttons
        modoGroup = (RadioGroup) view.findViewById(R.id.modeGroup);
        sizeGroup = (RadioGroup) view.findViewById(R.id.sizeGroup);
        modoGroup.setOnCheckedChangeListener(this);
        sizeGroup.setOnCheckedChangeListener(this);

        //Inicializamos configuraciones iniciales del texto
        currentText = "";
        selectedColor = ContextCompat.getColor(getContext(), R.color.blue);
        selectedMode = 0;
        selectedSize = NORMAL;
        modoGroup.check(R.id.radioButton_phrase);
        sizeGroup.check(R.id.radioButton_normal);

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_colorPicker:
                colorPickerDialog();
                break;

            case R.id.btn_editText:
                editTextDialog();
                break;
        }
    }

    /**
     * Metodo que se encarga de crear un recurso nuevo de tipo texto
     * y de pasarselo al fragmento que lo reemplace en el view
     */
    private void addNewText() {
        Recurso newRecurso = new Recurso();
        newRecurso.setRutaValor(currentText);
        newRecurso.setSize(selectedSize);
        newRecurso.setColor(selectedColor);
        newRecurso.setTipoRecurso(2);
        newRecurso.setIdBBDD(-1);
        newRecurso.setModo(selectedMode);

        Fragment firstTab = ((NewStrategyActivity)getActivity()).getFirstTabFragment();
        ((StrategyFirstTabFragment)firstTab).replaceView(newRecurso);
    }

    private void colorPickerDialog() {
        int[] mColors = getResources().getIntArray(R.array.dialogColors);
        String tag = "color_picker";

        ColorPickerDialog dialog = ColorPickerDialog.newInstance(R.string.select_color,
                mColors,selectedColor, 5, ColorPickerDialog.SIZE_SMALL);

        dialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {
            @Override
            public void onColorSelected(int color) {
                selectedColor = color;
                if ((currentText != null) && (!currentText.equals(""))) {
                    addNewText();
                }
            }
        });

        dialog.show(fragmentManager, tag);
    }

    public String getCurrentText() {
        return currentText;
    }

    public void setCurrentText(String currentText) {
        this.currentText = currentText;
    }

    /**
     * Funcion que cambia las opciones de configuracion del texto
     * segun la seleccion del usuario
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case (R.id.radioButton_phrase):
                selectedMode = PHRASE;
                break;
            case (R.id.radioButton_mayus):
                selectedMode = MAYUS;
                break;
            case (R.id.radioButton_minus):
                selectedMode = MINUS;
                break;
            case (R.id.radioButton_small):
                selectedSize = SMALL;
                break;
            case (R.id.radioButton_normal):
                selectedSize = NORMAL;
                break;
            case (R.id.radioButton_big):
                selectedSize = HUGE;
                break;
        }

        if ((currentText != null) && (!currentText.equals(""))) {
            addNewText();
        }
    }

    /**
     * Funcion que muestra un Dialog con un texto editable
     */
    private void editTextDialog() {
        AlertDialog.Builder constructor = new AlertDialog.Builder(getContext(), R.style.MyAlertDialogStyle);
        constructor.setTitle(getString(R.string.inputText));

        final EditText edittext = new EditText(getContext());
        edittext.setText(currentText);
        edittext.setPadding(36,36,36,36);
        constructor.setView(edittext);

        constructor.setPositiveButton(getString(R.string.accept),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        currentText = edittext.getText().toString();
                        if ((currentText != null) && (!currentText.equals(""))) {
                            addNewText();
                        }
                    }
                });

        constructor.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { }
        });

        AlertDialog helpDialog = constructor.create();
        helpDialog.show();
    }
}