package com.orange.taimun_watch.es;

import com.orange.taimun_watch.es.model.Estrategia;
import com.orange.taimun_watch.es.model.Regulacion;

import android.util.JsonWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Clase que contiene las funciones necesarias para la conversión de una regulación
 * de la memoria a un fichero JSON.
 *
 * @author Antonio Díaz Escudero
 */
public class utilJSON {

    /**
     * Función que toma la regulación pasada por argumentos y la vuelca a un fichero JSON.
     *
     * @param out stream de salida para la cadena del JSON.
     * @param regulation regulacion que se quiere volcar a JSON.
     * @return void
     */
    public void writeRegulation(OutputStream out, Regulacion regulation) throws IOException {
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
        writer.setIndent("  ");
        writer.beginObject();

        writer.name("Regulation");
        //Empezamos a imprimimos el cuerpo de la regulación.
        writer.beginObject();
        writer.name("Id").value(regulation.getId());
        writer.name("Name").value(regulation.getName());
        writer.name("Vibration").value(regulation.isVibration());
        //De momento el campo Audio siempre es null.
        //writer.name("Audio").value(regulation.getAudio());
        writer.name("Audio").value("null");
        //Imprimimos la array de RegulationSchedule
        writeRegulationSchedule(writer, regulation);
        //Imprimimos las estrategias.
        writeStrategies(writer, regulation);
        writer.endObject();

        writer.endObject();
        writer.close();
    }

    /**
     * Función que vuelca la array RegulationSchedule de la regulación que se está convirtiendo.
     *
     * @param writer objeto JsonWriter que se está utilizando para volcar la regulación.
     * @param regulation regulación que se está volcando al JSON.
     * @return void
     */
    private void writeRegulationSchedule(JsonWriter writer, Regulacion regulation) throws IOException {
        writer.name("RegulationSchedule");
        if(regulation.isSelector()) {
            writer.beginArray();
        }

        writer.beginArray();
        for(int i : regulation.getRegulationSchedule()) {
            writer.value(i);
        }

        if(regulation.isSelector()) {
            //Id de la opcion de terminar.
            writer.value(-1);
            writer.endArray();
        }

        writer.endArray();
    }

    /**
     * Función que vuelca todas las estrategias de la regulación que se está convirtiendo.
     *
     * @param writer objeto JsonWriter que se está utilizando para volcar la regulación.
     * @param regulation regulación que se está volcando al JSON.
     * @return void
     */
    private void writeStrategies(JsonWriter writer, Regulacion regulation) throws IOException {
        writer.name("Strategies");
        writer.beginArray();
        for(Estrategia e : regulation.getStrategies()) {
            writer.beginObject();
            writer.name("Id").value(e.getId());
            writer.name("Name").value(e.getName());
            writer.name("Icon").value(e.getIcon());
            writer.name("Time").value(e.getTime());
            writer.name("isGlobalTime").value(e.isGlobalTime());
            writer.name("Question").value(e.getQuestion());
            writer.name("TimerType").value(e.getTimerType());
            writer.name("isCarrusel").value(e.isCarrusel());
            writer.name("Reinforcement").value(e.isReinforcement());
            //Imprimimos los nombres de las imagenes de las estrategias.
            writeSteps(writer, e);
            writer.endObject();
        }

        writer.endArray();
    }

    /**
     * Función que vuelca todas los pasos de una regulacion a JSON.
     *
     * @param writer objeto JsonWriter que se está utilizando para volcar la regulación.
     * @param estrategia estrategia que se está volcando al JSON.
     * @return void
     */
    private void writeSteps(JsonWriter writer, Estrategia estrategia) throws IOException {
        writer.name("Steps");
        writer.beginArray();
        for (String s : estrategia.getSteps()) {
            writer.value(s + ".png");
        }

        writer.endArray();
    }
}
