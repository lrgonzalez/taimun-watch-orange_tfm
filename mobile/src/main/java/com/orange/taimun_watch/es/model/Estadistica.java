package com.orange.taimun_watch.es.model;

import java.util.Comparator;

import static com.orange.taimun_watch.es.utils.CompareDatesUtils.comparaFechas;

/**
 * Clase del objeto Estadistica con los atributos necesarios para mostras las estadísticas
 * básicas y avanzadas.
 *
 * @author Lucia
 */

public class Estadistica {
    private int Id;
    private String acelerometro;
    private String detectorPasos;
    private String giroscopio;
    private String pulsometro;
    private String nombre;  //el nombre sera la fecha
    private String eventos;

    //las cadenas son las rutas a los ficheros
    public Estadistica(String fecha, String acelerometro, String detectorPasos, String giroscopio, String pulsometro, String events) {
        this.acelerometro = acelerometro;
        this.detectorPasos = detectorPasos;
        this.giroscopio = giroscopio;
        this.pulsometro = pulsometro;
        this.nombre = fecha;
        this.eventos = events;
    }

    public String getNombre() {
        return nombre;
    }

    public int getId() {
        return Id;
    }

    public String getAcelerometro() {
        return acelerometro;
    }

    public String getDetectorPasos() {
        return detectorPasos;
    }

    public String getGiroscopio() {
        return giroscopio;
    }

    public String getPulsometro() {
        return pulsometro;
    }

    public String getEventos() {
        return eventos;
    }


    public static Comparator<Estadistica> fechaComparator = new Comparator<Estadistica>() {
        public int compare(Estadistica estadistica1, Estadistica estadistica2) {
            String fechaEstadistica1 = estadistica1.getNombre();
            String fechaEstadistica2 = estadistica2.getNombre();

            return comparaFechas(fechaEstadistica1, fechaEstadistica2, "dd/MM/yyyy");
        }
    };
}
