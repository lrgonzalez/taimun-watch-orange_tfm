package com.orange.taimun_watch.es;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.List;

import com.orange.taimun_watch.es.model.Recurso;


/**
 * Created by Yux on 01/07/2016.
 */
public class IconsAdapter extends RecyclerView.Adapter<IconsAdapter.MyViewHolder> {

    private List<Recurso> recursos;
    private Context mContext;
    private static final int TIPO_IMG_PRED = 0;
    private static final int TIPO_IMG_SRC = 1;
    private PhotoUtils photoUtils;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail_icon;

        public MyViewHolder(View view) {
            super(view);
            thumbnail_icon = (ImageView) view.findViewById(R.id.icon_item);
        }
    }

    public IconsAdapter(Context context, List<Recurso> recursos) {
        mContext = context;
        this.recursos = recursos;
        photoUtils = new PhotoUtils(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.icons_thumbnail, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String rutaRecurso = recursos.get(position).getRutaValor();

        //Imagen predeterminada
        if (recursos.get(position).getTipoRecurso() == TIPO_IMG_PRED) {
            Integer image = Integer.parseInt(rutaRecurso);
            Glide.with(mContext).load(image).asBitmap()
                    .thumbnail(0.25f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.thumbnail_icon);

            //Imagen guardada en fichero de recursos propios
        } else if (recursos.get(position).getTipoRecurso() == TIPO_IMG_SRC) {
            String filePath = photoUtils.getFilePathRecurso(recursos.get(position).getRutaValor());
            File fileRecurso = new File(filePath);

            Glide.with(mContext).load(fileRecurso)
                    .thumbnail(0.25f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.thumbnail_icon);
        }
    }

    @Override
    public int getItemCount() {
        return recursos.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private IconsAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final IconsAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}

