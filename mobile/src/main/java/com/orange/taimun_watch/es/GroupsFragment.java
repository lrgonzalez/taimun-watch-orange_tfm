package com.orange.taimun_watch.es;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.wearable.DataMap;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import javax.crypto.spec.SecretKeySpec;

import static com.google.android.gms.measurement.AppMeasurement.Param.TIMESTAMP;
import static com.orange.taimun_watch.es.DataReceiver.download;
import static com.orange.taimun_watch.es.DataReceiver.get_fecha;
import static com.orange.taimun_watch.es.utils.CompareDatesUtils.comparaFechas;

/**
 * Vista de selección de grupo de Firebase
 *
 * @author Lucia R.G.
 */
public class GroupsFragment extends Fragment implements View.OnClickListener {

    private EditText groupName;
    private EditText txtPIN;
    private TextView actualGroup;

    private static final String SYNC_TYPE = "sync_type";
    private static final String PIN_SYNC = "pin_sync";
    private static final String MOB_TO_WEAR = "/mob_to_wear";

    private FirebaseStorage storage = FirebaseStorage.getInstance();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment.
        View view = inflater.inflate(R.layout.groups_fragment, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Mis grupos");

        // Inicializamos elementos del layout.
        groupName = (EditText) view.findViewById(R.id.group_value);
        txtPIN = (EditText) view.findViewById(R.id.pin_value);
        actualGroup = (TextView) view.findViewById(R.id.actual_group);

        actualGroup.setText(String.format("%s %s", getString(R.string.grupo_actual), Preferences.getGroupCode(getContext())));
//        pin = txtPIN.getText().toString();
//        grupo = groupName.getText().toString();

        // Botones
        Button btnJoinGroup = (Button) view.findViewById(R.id.btn_join_group);
        btnJoinGroup.setOnClickListener(this);
        Button btnNewGroup = (Button) view.findViewById(R.id.btn_new_group);
        btnNewGroup.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_join_group:
                Log.v("GROUPS MOVIL", "Boton unirse");
                joinGroup();
                break;
            case R.id.btn_new_group:
                Log.v("GROUPS MOVIL", "Boton nuevo grupo");
                createGroup();
                break;
        }
    }

    private void createGroup() {
        // Solo hay que comprobar que el PIN se haya rellenado
        /*if (!validate()) {
            //onLoginFailed();
            return;
        }*/
        // Llamar a updateWear y establecer el nuevo PIN y grupo como preferencias
        updateWear(true);

        String pin = txtPIN.getText().toString();
        Preferences.setPinCode(getContext(), pin);
        actualGroup.setText(String.format("%s %s", getString(R.string.grupo_actual), Preferences.getGroupCode(getContext())));

//        Preferences.setGroupCode(getContext(), grupo);

        // Realizar proceso de bajar los archivos nuevos. HABRIA QUE ESPERAR QUE EL RELOJ LO SUBA PRIMERO Y LUEGO YA DESCARAGAR...
       /* int resDownloadSensors = firebaseDownload("sensors", getContext());
        int resDownloadEvents = firebaseDownload("events", getContext());

        if (resDownloadSensors != -1 && resDownloadEvents != -1) {
            Log.v("GROUPS MOVIL", "CREATE GROUP Sensors y events descargados con éxito");
            Toast.makeText(getContext(), "Has creado satisfactoriamente el grupo \'" +
                    Preferences.getGroupCode(getContext()) + "\'.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "La descarga de ficheros del grupo  \'" +
                    Preferences.getGroupCode(getContext()) + "\' ha fallado.", Toast.LENGTH_LONG).show();
        }*/
    }


    /**
     * Función que se une a un grupo existente de Firebase, descarga los ficheros que no se posean
     * aún, descarga y guarda el ksFile en el KeyStore del dispositivo (si el PIN es correcto) y
     * actualiza el grupo y el ksFile del reloj.
     * <p>
     * TODO - Si el reloj no está escuchando, no se debería cambiar de grupo
     */
    private void joinGroup() {
        // Comprobar que los campos se han rellenado
        if (!validate()) {
            //onLoginFailed();
            return;
        }

        // Bajar lista_ficheros y ficheros
        int resDownloadSensors = firebaseDownload("sensors", getContext());
        int resDownloadEvents = firebaseDownload("events", getContext());

        String pin = txtPIN.getText().toString();
        String groupNameStr = groupName.getText().toString();

        if (resDownloadSensors != -1 && resDownloadEvents != -1) {
            Log.v("GROUPS MOVIL", "Sensors y events descargados con éxito");
            // Bajar ksFile TODO que pasa si ya lo teniamos? Comprobar que el grupo no esta ya en el keystore
            int resDownloadKs = firebaseDownloadKs(getContext());
            if (resDownloadKs != -1) {
                Preferences.setPinCode(getContext(), pin);
                Preferences.setGroupCode(getContext(), groupNameStr);
                actualGroup.setText(String.format("%s %s", getString(R.string.grupo_actual), groupNameStr));
                // TODO - Llamar al reloj con PIN_SYNC para que se coja el nuevo ks, pin y grupo
                updateWear(false);
                Toast.makeText(getContext(), "Has cambiado satisfactoriamente al grupo \'" + groupNameStr + "\'.", Toast.LENGTH_LONG).show();
            }
        } else {
            // Borrar carpeta grupo inexistente creada
            String path = getContext().getExternalFilesDir(null).getAbsolutePath();
            File directory = new File(path + "/logs/" + groupNameStr + "/sensors");
            directory.delete();
            directory = new File(path + "/logs/" + groupNameStr + "/events");
            directory.delete();
            directory = new File(path + "/logs/" + groupNameStr);
            directory.delete();

            // TODO mensaje de error mas claro: puede ser que haya fallado la descarga del fichero pero si exista el grupo
            Toast.makeText(getContext(), "Lo sentimos, el grupo \'" + groupNameStr + "\' no existe en la base de datos.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Envía el PIN y el nuevo nombre de grupo al reloj para que se sincronice con el móvil y
     * sepa a dónde tiene que subir ahora los ficheros y con qué clave.
     * <p>
     * También crea el sks y ks del nuevo grupo y pin si estamos en creando un nuevo grupo.
     *
     * @param nuevoGrupo boolean true si estamos creando un grupo nuevo, false si solo nos hemos
     *                   unido a un grupo ya creado
     */
    private void updateWear(boolean nuevoGrupo) {
        // Enviar PIN y grupo al reloj
        Preferences.setNumFiles(getContext(), 0);
        Preferences.setNumFilesActual(getContext(), 0);
        Preferences.setFinishSync(getContext(), false);

        GoogleConnection gCon = new GoogleConnection(getContext());
        if (gCon.connect() == null) {
            Log.e("SYN", "Error al establecer la conexión con la API de Google.");
        }
        String pin = txtPIN.getText().toString();
        String grupo = groupName.getText().toString();

        //Se crea el Data Map y se envía a la Data Layer.
        DataMap dataMap = new DataMap();
        dataMap.putLong(TIMESTAMP, System.currentTimeMillis());
        dataMap.putString(SYNC_TYPE, PIN_SYNC);
        dataMap.putString("PIN_CODE", pin);

        if (nuevoGrupo) {
            // Guardar hash encriptado ficheros en keystore
            SecretKeySpec sks;
            // Generar una cadena de 6 caracteres aleatorios
            String gen = GenerateRandomString.randomString(6); // alias
            // Generar hash con SHA256
            byte[] bHash = SHA256(gen); //TODO generar hash con otro random de 8 letras
            // y con ese hash, crear el sks
            sks = new SecretKeySpec(bHash, "AES");
            // Guardar en keystore
            saveToKeyStore(gen, pin.toCharArray(), sks, getContext());

            dataMap.putString("GROUP_CODE", gen);  // Alias para ks
            Preferences.setGroupCode(getContext(), gen); // Guardar nombre del nuevo grupo en preferencias

            // crear carpetas
            createFolders();

            File ksFile = new File(getContext().getCacheDir(), "keyStoreMobile");

            // Subir ksFile a Firebase
            FirebaseStorage storage = FirebaseStorage.getInstance();
            // El nombre de la carpeta son los 6 digitos del grupo
            StorageReference sensorFileRef = storage.getReference();
            StorageReference groupFileRef = sensorFileRef.child(gen + "/ksFile");
            // Iniciar subida
            Uri uriKsFile = Uri.fromFile(ksFile);
            UploadTask uploadTask = groupFileRef.putFile(uriKsFile);
            StorageTask status = uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //Obtener URL de descarga para mostrarla
                    Uri url = taskSnapshot.getDownloadUrl();
                    Log.i("FIREBASE", "URL del ks: " + url);
                }
            });
            while (status.isInProgress()) ;
            {
                Log.i("MOVIL", "Subiendo ksFile...");
            }
        } else {
            dataMap.putString("GROUP_CODE", grupo);
        }

        gCon.sendData(MOB_TO_WEAR, dataMap);
    }

    /**
     * Solo descarga el fichero encriptado con el key store de la carpeta del grupo de Firebase.
     *
     * @param ctx contexto de la aplicación
     * @return 0 si OK
     */
    private int firebaseDownloadKs(Context ctx) {
        String grupo = groupName.getText().toString();
        StorageReference folderRef = storage.getReference().child(grupo + "/ksFile");
        // Guardar en cache
        File tempName = new File(ctx.getCacheDir(), "keyStoreMobile");

        // DESCARGAR
        download(folderRef, tempName);

        return 0;
    }

    private boolean createFolders(){
        boolean result = true;
        // Guardar la lista en files/logs/(id_reloj)/(sensors|events)/
        String path = getContext().getExternalFilesDir(null).getAbsolutePath(); //Llega a la ruta de files
        File directory = new File(path + "/logs");
        String grupo = Preferences.getGroupCode(getContext());

        // Si no existe la ruta del grupo, crearla
        File groupDir = new File(directory, grupo);
        if (!groupDir.exists()) {
            result = groupDir.mkdir() && result;
            result = new File(groupDir, "sensors").mkdir() && result;
            result = new File(groupDir, "events").mkdir() && result;
        }
        return result;
    }

    /**
     * Descarga una lista de archivos del servidor de Firebase indicado y los guarda en una carpeta.
     * <p>
     * Para ello, consulta los archivos del repositorio, y los de la carpeta local y descarga solo
     * aquellos que aún no posee, o los que han sido actualizados en el repo.
     *
     * @param middlePath sensor o events
     * @param ctx        contexto de la aplicación
     * @return -1 en caso de error, 0 en caso contrario
     */
    private int firebaseDownload(String middlePath, Context ctx) {
        // Descargar la lista de ficheros
        // La carpeta tiene el nombre del grupo
        String grupo = groupName.getText().toString();
        StorageReference folderRef = storage.getReference().child(grupo + "/" + middlePath + "/" + "lista_ficheros.txt");

        // Guardar la lista en files/logs/grupo/(sensors|events)/
        String path = ctx.getExternalFilesDir(null).getAbsolutePath(); //Llega a la ruta de files
        File directory = new File(path + "/logs");
        File groupDir = new File(directory, grupo);
        createFolders();

        File dataFilesDirectory = new File(groupDir, middlePath);
        final File tempName = new File(groupDir, middlePath + "/lista_ficheros.txt");

        // Guardar en carpeta sensor o en events. Se descarga en segundo plano
        download(folderRef, tempName);

        // Leer tempName y guardar los nombres en una lista
        ArrayList<String> ficherosFirebase = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(tempName));
            String ficheroLeido;
            while ((ficheroLeido = br.readLine()) != null) {
                Log.i("GROUPS MOVIL", "Ficheros en lista: " + ficheroLeido);
                ficherosFirebase.add(ficheroLeido);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }

        if (ficherosFirebase.isEmpty()) {
            Log.e("GROUPS MOVIL", "No hay ficheros en firebase");
            return 0;
        }

        // Descargar de Firebase cada uno de esos ficheros y guardarlos en el movil (encriptados)
        String[] ficherosCarpeta = dataFilesDirectory.list();

        Set<String> ficherosNuevos = new HashSet<>();
        // Descargar a partir del último fichero sincronizado por fecha. Ordenar.
        Collections.sort(ficherosFirebase);
        Arrays.sort(ficherosCarpeta);
        // Buscar último dia sincronizado en mi carpeta (el más nuevo)
        String ultimaFecha = "1_1_2000";

        for (String fichCarpeta : ficherosCarpeta) {
            if (!fichCarpeta.equals("lista_ficheros.txt")) {
                int diferenciaFecha = comparaFechas(ultimaFecha, get_fecha(fichCarpeta), "dd_MM_yyyy");
                if (diferenciaFecha == -1) { // 1_1_2000 < 15_10_2018
                    ultimaFecha = get_fecha(fichCarpeta);
                } else if (diferenciaFecha == -2) {
                    Log.e("GROUPS MOVIL", "Error comparando fechas de ficheros (ultimaFecha)");
                }
            }
        }

        // Descargar del ultimo dia (incluido) hasta ahora
        for (String ficheroFB : ficherosFirebase) {
            String fechaFichero = get_fecha(ficheroFB);
            int diferenciaFecha = comparaFechas(ultimaFecha, fechaFichero, "dd_MM_yyyy");
            if (diferenciaFecha == 0 || diferenciaFecha == -1) { // ultimaFecha menor o igual que fechaFichero
                ficherosNuevos.add(ficheroFB);
            } else if (diferenciaFecha == -2) {
                Log.e("GROUPS MOVIL", "Error comparando fechas de ficheros");
            }
        }

        // Descargar todos los que no tenga TODO - Si es la primera vez, no hace falta comprobar cuáles tengo
        for (String fichero : ficherosNuevos) {
            folderRef = storage.getReference().child(grupo + "/" + middlePath + "/" + fichero);
            final File downloadFile = new File(dataFilesDirectory, fichero);
            download(folderRef, downloadFile);
        }

        // Al finalizar, se borran los temporales
        tempName.delete();
        return 0;
    }

    /*public void onLoginFailed() {
        Toast.makeText(getContext(), "Error al unirse al grupo", Toast.LENGTH_LONG).show();

//        btnJoin.setEnabled(true);
    }*/

    /**
     * Validación de los campos de grupo y pin.
     *
     * @return true si los campos están completos, false si alguno está vacío
     */
    public boolean validate() {
        boolean valid = true;
        String pin = txtPIN.getText().toString();
        String grupo = groupName.getText().toString();

        if (grupo.isEmpty()) {
            groupName.setError("introduce el nombre del grupo");
            valid = false;
        } else {
            groupName.setError(null);
        }

        if (pin.isEmpty()) {  // pin.length() < 4 || pin.length() > 10 // TODO - comprobar longitud
            txtPIN.setError("introduce el PIN del grupo");
            valid = false;
        } else {
            txtPIN.setError(null);
        }

        return valid;
    }

    /**
     * FUNCIONES AUXILIARES PARA LA CREACIÓN DEL GRUPO
     */

    public byte[] SHA256(String password) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        digest.reset();
        return digest.digest(password.getBytes());
    }

    /**
     * Genera una cadena de numeros y letras mayúsculas y minúsculas aleatorias.
     */
    public static class GenerateRandomString {
        static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        static final String lower = upper.toLowerCase(Locale.ROOT);

        static final String digits = "0123456789";

        static final String DATA = upper + lower + digits;

        static Random RANDOM = new Random();

        /**
         * Genera una cadena aleatoria de tamaño len.
         *
         * @param len tamaño de la cadena generada
         * @return cadena aleatoria
         */
        static String randomString(int len) {
            StringBuilder sb = new StringBuilder(len);

            for (int i = 0; i < len; i++) {
                sb.append(DATA.charAt(RANDOM.nextInt(DATA.length())));
            }

            return sb.toString();
        }
    }

    /**
     * Guardar en nuestro KeyStore la clave con el alias y el pin recibidos.
     *
     * @param alias    nombre de la clave en KeyStore
     * @param password pin para abrir la clave
     * @param key      clave a guardar
     * @param ctx      contexto de la aplicación
     */
    public void saveToKeyStore(String alias, char[] password, SecretKeySpec key, Context ctx) {
        KeyStore ks = null;
        try {
            // Inicializar KS vacío
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null);

            // Crear entrada de la clave en el KS
            KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(password);
            ks.setEntry(alias, new KeyStore.SecretKeyEntry(key), protParam);

            // Guardamos el fichero del ks en caché para guardar luego en KS
            File file = new File(ctx.getCacheDir(), "keyStoreMobile");
            FileOutputStream fos = new FileOutputStream(file);
            ks.store(fos, password);

            // Solo para debug
            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                String param = aliases.nextElement();
                System.out.println(param);
                Log.i("SETTINGS ALIAS: ", param);
            }

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
            e.printStackTrace();
        }
    }
}
