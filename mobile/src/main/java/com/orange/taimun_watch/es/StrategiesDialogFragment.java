package com.orange.taimun_watch.es;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.model.ListaEstrategia;

public class StrategiesDialogFragment extends DialogFragment {
    private RecyclerView recyclerView;
    private StrategyViewAdapter adapter;
    private ArrayList<ListaEstrategia> lstEstrategia;
    private TextView txtNone;
    private DatabaseAdapter db;
    // this method create view for your Dialog
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflate layout with recycler view
        View view = inflater.inflate(R.layout.strategies_dialog_fragment, container, false);

        //Inicializamos elementos del layout.
        txtNone = (TextView) view.findViewById(R.id.no_strategies);
        recyclerView = (RecyclerView) view.findViewById(R.id.strategies_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        inicializarListaEstrategia();
        updateStrategyAdapter();

        return view;
    }

    public void inicializarListaEstrategia(){

        db = new DatabaseAdapter(getActivity());
        db.open();

        long strategiesNumber = db.getStrategiesNumber();

        //Cargamos la lista de estrategias en el recyclerview
        if (strategiesNumber > 0) {
            txtNone.setVisibility(View.INVISIBLE);
            lstEstrategia = db.getStrategiesOverview();

            //Si no hay estrategias mostramos un texto
        } else {
            txtNone.setVisibility(View.VISIBLE);
        }

        db.close();
    }

    public void updateStrategyAdapter() {
        StrategyViewAdapter sa = (StrategyViewAdapter) recyclerView.getAdapter();

        if(sa != null) {
            if(lstEstrategia != null) {
                sa.updateStrategies(lstEstrategia);
                sa.notifyDataSetChanged();
            }
        } else {
            if(lstEstrategia != null) {
                adapter = new StrategyViewAdapter(lstEstrategia, getContext(), false, new StrategyViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(ListaEstrategia estrategia) {
                        NewRegulationActivity activity = (NewRegulationActivity) getActivity();
                        activity.addStrategy(estrategia);
                    }
                }, false, true);

                recyclerView.setAdapter(adapter);
            }
        }
    }
}
