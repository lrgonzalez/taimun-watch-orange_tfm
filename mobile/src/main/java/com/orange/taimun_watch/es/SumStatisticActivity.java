package com.orange.taimun_watch.es;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.orange.taimun_watch.es.graficas.FullscreenActivity;
import com.orange.taimun_watch.es.model.EstadisticaDetalle;
import com.orange.taimun_watch.es.utils.DecryptUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Vista de visualizacion basica con los eventos del dia seleccionado
 *
 * @author Lucia R.G.
 */

public class SumStatisticActivity extends AppCompatActivity {
    private TextView dateName;
    private StatisticsItemAdapter adapter;
    private RecyclerView recyclerView;
    private TextView txtNone;
    private ArrayList<EstadisticaDetalle> lstDetalle = new ArrayList<>();
    private ProgressDialog pd = null;
    AsyncTask asyncT;

    public static final int NOMBRE = 0; //fecha
    public static final int EVENTS = 1;  //rutas
    public static final int PULSOMETRO = 2;

    String[] listaExtras;
    ArrayList<String> listaIniEvento = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sum_statistic_activity);

        //Recogemos la ruta del fichero del que sacar los datos
        Intent intent = getIntent();
        listaExtras = intent.getStringArrayExtra("listaExtras");

        //Recuperamos toolbar y la asignamos como SupportActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Mostrar titulo en la barra
        getSupportActionBar().setTitle(R.string.statistics);

        //Inicializar textos del layout
        dateName = (TextView) findViewById(R.id.fecha_registro); //Fecha como titulo
        dateName.setText(String.format("%s %s", getResources().getString(R.string.register), listaExtras[NOMBRE]));

        //Inicializamos recyclerView
        recyclerView = (RecyclerView) findViewById(R.id.events_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        //si no hay eventos
        txtNone = (TextView) findViewById(R.id.no_statistics);

        this.pd = ProgressDialog.show(this, getApplicationContext().getResources().getString(R.string.loading),
                getApplicationContext().getResources().getString(R.string.reading_data), true, false);

        //Inicializamos la lista de detecciones del dia para mostrar
        asyncT = new DownloadTask().execute(listaExtras[EVENTS], listaExtras[PULSOMETRO]);
    }

    private class DownloadTask extends AsyncTask<String, Void, ArrayList<EstadisticaDetalle>> {
        @Override
        protected ArrayList<EstadisticaDetalle> doInBackground(String... params) {
            String pathEvents = params[0];
            String pathPulsometro = params[1];
            //Acceso al fichero de pulsaciones
            if (pathPulsometro != null && pathEvents != null) {
                String ficheroPulso;
                try {
                    /*Desencriptar fichero "pathPulsometro"*/
                    File decryptedPulsometro = desencriptar(pathPulsometro);
                    if (decryptedPulsometro == null) {
                        return null;
                    }

                    FileInputStream fileInputStream = new FileInputStream(decryptedPulsometro);
                    InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    bufferedReader.readLine(); //saltar la primera linea

                    while ((ficheroPulso = bufferedReader.readLine()) != null) {
                        stringBuilder.append(ficheroPulso).append(System.getProperty("line.separator"));
                    }
                    fileInputStream.close();
                    ficheroPulso = stringBuilder.toString(); //en line esta tod lo del fichero Pulsometro
                    bufferedReader.close();

                    String[] lineaPulso = ficheroPulso.split("\n"); //string en cada pos. una linea
                    Arrays.sort(lineaPulso); //ordenar por timestamp

                    //recorrer el fichero events buscando "ev positiva"
                    String line;
                    lstDetalle = new ArrayList<>();
                    try {
                        /*Desencriptar fichero "pathEvents"*/
                        File decryptedEvents = desencriptar(pathEvents);

                        fileInputStream = new FileInputStream(decryptedEvents);
                        inputStreamReader = new InputStreamReader(fileInputStream);
                        bufferedReader = new BufferedReader(inputStreamReader);
                        stringBuilder = new StringBuilder();

                        bufferedReader.readLine(); //saltar la primera linea

                        while ((line = bufferedReader.readLine()) != null) {
                            stringBuilder.append(line).append(System.getProperty("line.separator"));
                        }
                        fileInputStream.close();
                        line = stringBuilder.toString(); //en line esta tod lo del fichero events del dia
                        bufferedReader.close();

                        String[] lineAux = line.split("\n"); //string en cada pos. una linea
                        Arrays.sort(lineAux);

                        String[] words;
                        String[] words2;
                        String[] nombreReg;
                        String[] lpm;
                        long fechaHora; //formato timestamp
                        Timestamp stamp;
                        String timestampIni = "";
                        Date date;
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); //todo se puede poner tiempo local
                        String formattedDateIni = "";
                        String formattedDateFin;
                        String detalleInicio = "";
                        String detalleFin = "";
                        String nombreFull = "";
                        String regId = "";
                        Boolean busy = false; //si ya esta con una regulacion, esperar a que acabe

                        for (int i = 0; i < lineAux.length; i++) { //para cada linea del fichero
                            words = lineAux[i].split("\t"); //palabras de la linea i (timestamp + ev)
                            if (words[1].contains("EVALUACION_POSITIVA") && !busy) {
                                busy = true;
                                nombreFull = "";
                                lpm = words[1].split(" "); //los latidos vienen espaciado, no tabulado
                                int rounded = (int) Float.parseFloat(lpm[1]);
                                timestampIni = words[0];
                                fechaHora = Long.parseLong(words[0]);
                                stamp = new Timestamp(fechaHora);
                                date = new Date(stamp.getTime());
                                formattedDateIni = sdf.format(date);

                                //buscar nombre de regulacion
                                for (int j = 1; i + j < lineAux.length; j++) {
                                    words2 = lineAux[i + j].split("\t");
                                    if (words2[1].contains("INICIO_REGULACION")) {
                                        nombreReg = words2[1].split(" ");  // El nombre de la reg viene con espacios
                                        int k;
                                        for (k = 1; k < nombreReg.length - 1; k++) { // la ultima palabra es el id
                                            nombreFull = nombreFull.concat(nombreReg[k] + " ");
                                        }
                                        regId = nombreReg[k];
                                        break;
                                    }
                                }
                                //mostrar situacion
                                detalleInicio = getResources().getString(R.string.start_hour) + " " +
                                        formattedDateIni + "  " + rounded + " " + getResources().getString(R.string.latidos_minuto);
                            }
                            //solo hacer si ha encontrado ev_pos
                            if (words[1].contains("FIN_REGULACION")) { //buscar en el fich.de pulsaciones en este timestamp o cercano
                                String[] palabraPulso = lineaPulso[0].split("\t"); //primera linea fichero
                                //Buscar en fichero pulso la medida de pulso mas aproximada a esa hora
                                long distance = Math.abs(Long.parseLong(palabraPulso[0]) - Long.parseLong(words[0]));
                                int idx = 0;
                                for (int j = 1; j < lineaPulso.length; j++) {
                                    palabraPulso = lineaPulso[j].split("\t");
                                    long jdistance = Math.abs(Long.parseLong(palabraPulso[0]) - Long.parseLong(words[0])); //ver dstancia
                                    if (jdistance < distance) {
                                        idx = j;
                                        distance = jdistance;
                                    }
                                }
                                lpm = lineaPulso[idx].split("\t");

                                int rounded = (int) Float.parseFloat(lpm[2]); // redondear lpm
                                //lpm = words[1].split(" ");
                                fechaHora = Long.parseLong(words[0]);
                                stamp = new Timestamp(fechaHora);
                                date = new Date(stamp.getTime());
                                formattedDateFin = sdf.format(date);
                                //mostrar situacion
                                detalleFin = getResources().getString(R.string.end_hour) + "\t\t\t\t" + formattedDateFin
                                        + "  " + rounded + " " + getResources().getString(R.string.latidos_minuto);

                                EstadisticaDetalle estDet = new EstadisticaDetalle(getResources().getString(R.string.regulation)
                                        + " " + nombreFull, detalleInicio, detalleFin, regId);
                                lstDetalle.add(estDet);
                                listaIniEvento.add(timestampIni); //timestamp en string
                                busy = false;
                            }
                        }
                        //Aqui lstDetalle ya esta inicializada
                        decryptedPulsometro.delete();
                        decryptedEvents.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return lstDetalle;
        }

        /**
         * Obtiene la ruta y el nombre del fichero a desencriptar y llama a decryptAES
         *
         * @param path ruta completa del fichero a desencriptar
         * @return fichero desencriptado
         */
        private File desencriptar(String path) {
            // full path incluido nombre fichero
            File encryptedFile = new File(path);
            String[] pathParts = path.split("/");
            String nombreFichero = pathParts[pathParts.length - 1];
            String absolutePath = path.replace(nombreFichero, "");
            File decryptedFile = new File(absolutePath, "desencriptado.txt");

            DecryptUtils decryptUtils = new DecryptUtils(getApplicationContext());
            if (decryptUtils.decryptAES(getApplicationContext(), encryptedFile, decryptedFile) == -1) { // fichero a desenc, fichero final
                Log.e("MOVIL SumStatistic", "ERROR al desencriptar " + encryptedFile);
                decryptedFile.delete();
                return null;
            }

            return decryptedFile;
        }

        @Override
        protected void onPostExecute(ArrayList<EstadisticaDetalle> result) {
            // Pass the result data back to the main activity
            SumStatisticActivity.this.lstDetalle = result;

            if (SumStatisticActivity.this.pd != null) {
                if (result == null) {
                    Toast.makeText(SumStatisticActivity.this, "No tienes permisos para " +
                            "leer este fichero. Revisa tu PIN.", Toast.LENGTH_LONG).show();
                }
                updateStatisticsItemAdapter();
                SumStatisticActivity.this.pd.dismiss();
            }
        }
    }

    /************************************
     * Opciones del menu de la toolbar
     * **********************************
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_to_advanced, menu); //icono a graficas avanzadas
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_to_advanced:
                Intent intent = new Intent(getApplicationContext(), FullscreenActivity.class);
                intent.putExtra("listaExtras", listaExtras);
                intent.putExtra("listaIniEvento", listaIniEvento);
                startActivity(intent);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateStatisticsItemAdapter() {
        StatisticsItemAdapter sa = (StatisticsItemAdapter) recyclerView.getAdapter();

        if (lstDetalle == null || lstDetalle.size() == 0) {
            txtNone.setVisibility(View.VISIBLE);
        } else {
            txtNone.setVisibility(View.INVISIBLE);
        }

        if (sa != null) {
            if (lstDetalle != null) {
                sa.updateStatistics(lstDetalle);
                sa.notifyDataSetChanged();
            }
        } else {
            if (lstDetalle != null) {
                /*Aqui se inicializa la gestion de regulaciones*/
                adapter = new StatisticsItemAdapter(lstDetalle, this, new StatisticsItemAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(EstadisticaDetalle stat) {
                        final DatabaseAdapter db = new DatabaseAdapter(getApplicationContext());
                        db.open();
                        //Regulacion regulacion = db.getRegulationById(Long.parseLong(stat.getIdRegulacion()));
                        if (!db.getStrategiesOverviewByRegulation(Long.parseLong(stat.getIdRegulacion())).isEmpty()) {
                            //Navegamos a la regulacion asociada al evento
                            Intent intent = new Intent(getApplicationContext(), NewRegulationActivity.class);
                            intent.putExtra("regulationId", Long.parseLong(stat.getIdRegulacion()));

                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Esta regulación ya no existe", Toast.LENGTH_LONG).show();
                        }
                        db.close();
                    }
                });
                recyclerView.setAdapter(adapter);
                //db.close();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();

    /*
    * The device may have been rotated and the activity is going to be destroyed
    * you always should be prepared to cancel your AsnycTasks before the Activity
    * which created them is going to be destroyed.
    * And dont rely on mayInteruptIfRunning
    */
        if (asyncT != null) {
            asyncT.cancel(false);
        }
    }
}