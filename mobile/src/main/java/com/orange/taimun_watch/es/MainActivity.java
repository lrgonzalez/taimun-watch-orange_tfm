package com.orange.taimun_watch.es;

/**
 * Created by Yux on 10/05/2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.orange.taimun_watch.es.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final int HOME_POSITION = 0;
    public static final int STRATEGY_POSITION = 1;
    public static final int REGULATION_POSITION = 2;
    public static final int STATISTIC_POSITION = 3;
    public static final int GROUPS_POSITION = 4;
    private static final String MENU_STATE = "menuState";
    private static final String STRATEGY_TAG = "strategyFragment";
    private static final String REGULATION_TAG = "regulationFragment";
    private static final String STATISTICS_TAG = "statisticsFragment";
    private static final String GROUPS_TAG = "groupsFragment";
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private ImageView profilePhoto;
    private TextView profileUsername;
    private PhotoUtils photoUtils;
    private int menuOptionSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        //Recuperamos toolbar y la asignamos como SupportActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Obtenemos y configuramos menu DrawerLayout
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Inicializamos el nombre y la foto del usuario en el drawer
        photoUtils = new PhotoUtils(this);
        inicializarDrawer();

        //Seleccionamos fragment a mostrar por defecto
        selectFragment(HOME_POSITION);
        navigationView.getMenu().getItem(HOME_POSITION).setChecked(true);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        navigationView.getMenu().getItem(menuOptionSelected).setChecked(true);
        inicializarDrawer();
    }

    /**
     * Metodo para obtener el nombre y la foto del usuario y
     * asignarlo en el menu drawer
     */
    private void inicializarDrawer() {
        View headerView = navigationView.getHeaderView(0);
        profilePhoto = (ImageView) headerView.findViewById(R.id.profilePhotoMenu);
        profileUsername = (TextView) headerView.findViewById(R.id.profileUsernameMenu);

        String name = Preferences.getUsername(this);
        String photo = Preferences.getUserphoto(this);

        //Inicializamos nomnbre de usuario
        if (name == null) {
            profileUsername.setText(getString(R.string.name_default));
        } else {
            profileUsername.setText(name);
        }

        //Inicializamos img de usuario
        if (photo == null) {
            profilePhoto.setImageResource(R.drawable.logo_user);
        } else {
            photoUtils.cargarRecursoUsuario(photo, profilePhoto);
        }

        profilePhoto.setOnClickListener(this);
        profileUsername.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        int count = getSupportFragmentManager().getBackStackEntryCount();

        Log.i("FRAGMENT", "Stack = " + count);

        // TODO - Arreglar peta a veces
        Fragment f = getSupportFragmentManager().getFragments().get(count - 1);

        if (f.getClass() == HomeFragment.class) {
            super.onBackPressed();
            this.finish();
        } else if (count > 1) {
            for (; count > 1; count--) {
                getSupportFragmentManager().popBackStack();
            }
        } else {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
                this.finish();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_home:
                item.setChecked(true);
                selectFragment(HOME_POSITION);
                menuOptionSelected = HOME_POSITION;
                break;
            case R.id.nav_strategies:
                item.setChecked(true);
                selectFragment(STRATEGY_POSITION);
                menuOptionSelected = STRATEGY_POSITION;
                break;
            case R.id.nav_regulation:
                selectFragment(REGULATION_POSITION);
                menuOptionSelected = REGULATION_POSITION;
                break;
            case R.id.nav_settings:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.nav_tutorial:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, TutorialActivity.class));
                break;
            case R.id.nav_statistics:
                item.setChecked(true);
                selectFragment(STATISTIC_POSITION);
                menuOptionSelected = STATISTIC_POSITION;
                break;
            case R.id.nav_groups:
                item.setChecked(true);
                selectFragment(GROUPS_POSITION);
                menuOptionSelected = GROUPS_POSITION;
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    /*  **************************************************************
        Método de control de fragments a mostrar en la actividad
     *  *************************************************************/
    public void selectFragment(int fragmentPos) {
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;

        switch (fragmentPos) {
            case HOME_POSITION:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                HomeFragment homeFragment = new HomeFragment();
                fragmentTransaction.replace(R.id.fragment_menu, homeFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case STRATEGY_POSITION:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                StrategyFragment strategyFragment = new StrategyFragment();
                fragmentTransaction.replace(R.id.fragment_menu, strategyFragment, STRATEGY_TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case REGULATION_POSITION:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                RegulationFragment regulationFragment = new RegulationFragment();
                fragmentTransaction.replace(R.id.fragment_menu, regulationFragment, REGULATION_TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case STATISTIC_POSITION:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                StatisticFragment statisticFragment = new StatisticFragment();
                fragmentTransaction.replace(R.id.fragment_menu, statisticFragment, STATISTICS_TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case GROUPS_POSITION:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                GroupsFragment groupsFragment = new GroupsFragment();
                fragmentTransaction.replace(R.id.fragment_menu, groupsFragment, GROUPS_TAG);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }

    public NavigationView getNavigationView() {
        return navigationView;
    }

    /*  ***********************************
        Acceso al perfil de usuario
        ***********************************
    */
    @Override
    public void onClick(View v) {
        drawer.closeDrawer(GravityCompat.START);
        startActivity(new Intent(this, ProfileActivity.class));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Guardamos la opción de menú seleccionada
        savedInstanceState.putInt(MENU_STATE, menuOptionSelected);

        super.onSaveInstanceState(savedInstanceState);
    }

    public void setMenuOptionSelected(int menuOptionSelected) {
        this.menuOptionSelected = menuOptionSelected;
    }
}
