package com.orange.taimun_watch.es.graficas;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.jjoe64.graphview.series.Series;
import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.utils.DecryptUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * Clase para el manejo de los datos y representacion de los mismos en la vista de grafica avanzada
 *
 * @author Lucia
 */
public class ScalingX {
    private Context mContext;
    static int mNumLabels;
    static ProgressDialog pd = null;

    private static final int MAX_GRAFICA = 3000;

    public void onCreate(FullscreenActivity activity, final String nombre, final ArrayList iniEvents,
                         final ArrayList ejes, final CharSequence[] nombresEjes, final String pathSensor, final String fecha) {

        mNumLabels = 4;
        mContext = activity.getApplicationContext();

        pd = ProgressDialog.show(activity, mContext.getResources().getString(R.string.loading),
                mContext.getResources().getString(R.string.reading_data), true, false);

        // Start a new thread that will download all the data
        new DownloadTask().execute(activity, nombre, iniEvents, ejes, nombresEjes, pathSensor, fecha, mContext);
    }

    static class DownloadTask extends AsyncTask<Object, Void, String> {
        @Override
        protected String doInBackground(Object... params) {
            FullscreenActivity full = (FullscreenActivity) params[0];
            GraphView gv = (GraphView) full.findViewById(R.id.graph);
            String string = (String) params[1]; //nombre
            ArrayList arrayList = (ArrayList) params[2]; //iniEvents
            ArrayList arrl = (ArrayList) params[3]; //ejes
            String[] nombre = (String[]) params[4]; //nombresEjes
            String string2 = (String) params[5]; //pathSensor
            String string3 = (String) params[6]; //fecha
            Context mContext = (Context) params[7]; //contexto

            initGraph(mContext, gv, string, arrayList, arrl, nombre, string2, string3);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            if (pd != null) {
                pd.dismiss();
            }
        }
    }

    static void initGraph(final Context mContext, GraphView graph, String nombreFich,
                          ArrayList iniEvents, ArrayList ejes, CharSequence[] nombresEjes, String pathSensor, String fecha) {
        String path;
        String titulo;
        final String unidades;

        switch (nombreFich) { //casos especiales y resto
            case "pasos":
                path = pathSensor;
                titulo = mContext.getResources().getString(R.string.pasos);
                unidades = mContext.getResources().getString(R.string.pasos);
                break;
            case "acelerometro":
                path = pathSensor;
                titulo = mContext.getResources().getString(R.string.acelerometro);
                unidades = mContext.getResources().getString(R.string.metros_cuadrados) /*+ Html.fromHtml("<sup>2</sup>")*/;
                //unidades = "m2/s";
                break;
            case "giroscopio":
                path = pathSensor;
                titulo = mContext.getResources().getString(R.string.giroscopio);
                unidades = mContext.getResources().getString(R.string.grados_angulares);
                break;
            case "pulsometro":
                path = pathSensor;
                titulo = mContext.getResources().getString(R.string.pulsometro);
                unidades = mContext.getResources().getString(R.string.pulsaciones);
                break;
            default:
                path = pathSensor;
                titulo = nombreFich;
                unidades = "uds";
        }

        // Eliminar gráfica(s) anterior
        graph.removeAllSeries();

        // Los puntos se obtienen de los logs del reloj
        int nLineas = 0;
        String line;
        try {
            /*Desencriptar snsrFile*/
            File encryptedFile = new File(path);
            if (!encryptedFile.getName().contains("des-")) { // Si ya tiene "des-" en el nombre, ya ha sido desencriptado
                String[] pathParts = path.split("/");
                String nombreFichero = pathParts[pathParts.length - 1];
                String absolutePath = path.replace(nombreFichero, "");
                File decryptedFile = new File(absolutePath, "des-" + encryptedFile.getName());
                DecryptUtils decryptUtils = new DecryptUtils(mContext);
                if (decryptUtils.decryptAES(mContext, encryptedFile, decryptedFile) == -1) { // fichero a desenc, fichero final
                    Log.e("MOVIL ScalingX", "ERROR al desencriptar " + encryptedFile);
                    return;
                }
                path = decryptedFile.getPath();
            }

            FileInputStream fileInputStream = new FileInputStream(new File(path));
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            bufferedReader.readLine(); //saltar la primera

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + System.getProperty("line.separator"));
                nLineas++;
            }
            fileInputStream.close();
            line = stringBuilder.toString();  //t0do el fichero
            bufferedReader.close();

            Log.v("SCALINGX", "termina lectura fichero");

            String[] lineAux = line.split("\n"); //lineaux estan todas las lineas separadas
            String[] words;

            Arrays.sort(lineAux); //ordena por timestamp

            int i;
            int div = nLineas;  //si tiene menos del maximo
            int incr = 1;
            int nPuntos = MAX_GRAFICA; // 3000 lineas = 3000 puntos de un eje
            if (ejes != null) { // Si hay 2 ejes el nro puntos TOTAL es 3000 (1500 lineas cada eje)
                nPuntos /= ejes.size();
                Log.v("SCALINGX", "nPuntos " + nPuntos);
            }
            //si el fich tiene mas de 3000 lineas se limita (tope de la grafica)
            if (nLineas > nPuntos) {
                double x = (double) nLineas / nPuntos;
                div = (int) (nLineas / x); // div es la cantidad de puntos reales del array tras el muestreo
                incr = (int) x;
            }
            Log.v("SCALINGX", "nLineas " + nLineas);
            Log.v("SCALINGX", "" + div);

            ArrayList<DataPoint[]> dataPointEjes = new ArrayList<>();
            DataPoint[] points = null;

            if (ejes != null) {
                for (int eje = 0; eje < ejes.size(); eje++) { //por cada eje añadimos un DataPoint[]
                    DataPoint[] pointsEje = new DataPoint[div];
                    dataPointEjes.add(pointsEje);
                }
            } else {
                points = new DataPoint[div];
            }

            ArrayList<String> tiemposValidos = new ArrayList<>();
            int j;
            for (i = 0, j = 0; i < div; i++, j += incr) {
                words = lineAux[j].split("\t"); //palabras de la linea j (va saltandose lineas segun incr)
                tiemposValidos.add(words[0]); //para busqueda ptos criticos
                long timestamp = Long.parseLong(words[0]);
                Date date1 = new Date(timestamp);
                if (ejes != null) {
                    for (int eje = 0; eje < ejes.size(); eje++) {
                        float Y = Float.parseFloat(words[(int) ejes.get(eje) + 2]);  // columna del eje: las dos primeras no valen
                        dataPointEjes.get(eje)[i] = new DataPoint(date1, Y);
                    }
                } else {
                    float Y = Float.parseFloat(words[2]);
                    points[i] = new DataPoint(date1, Y); // points contiene cada dato del fich ya muestreado
                }
            }

            Log.v("SCALINGX", "termina tiempos validos");
            double maxY = 0;
            double minY = maxY;
            double setMinX = 0;
            double setMaxX = 0;

            // Crear un LineGraphSeries por eje
            ArrayList<LineGraphSeries<DataPoint>> seriesEjes = new ArrayList<>();
            LineGraphSeries<DataPoint> series = null;

            PointsGraphSeries<DataPoint> bgSerie;

            if (ejes != null) {
                for (int eje = 0; eje < ejes.size(); eje++) {
                    for (i = 0; i < dataPointEjes.get(eje).length; i++) {
                        // eje Y
                        if (maxY < dataPointEjes.get(eje)[i].getY()) { //actualizar max
                            maxY = dataPointEjes.get(eje)[i].getY();
                        }
                        if (minY > dataPointEjes.get(eje)[i].getY()) { //actualizar min
                            minY = dataPointEjes.get(eje)[i].getY();
                        }
                    }
                    LineGraphSeries<DataPoint> seriesAux;

                    seriesAux = new LineGraphSeries<>(dataPointEjes.get(eje));
                    seriesEjes.add(seriesAux);

                    setMinX = dataPointEjes.get(eje)[0].getX();
                    setMaxX = dataPointEjes.get(eje)[i / 2].getX(); // zoom inicial
                }

                // Establecemos la distancia inicial que se muestra del grafico
                graph.getViewport().setMinX(setMinX);
                graph.getViewport().setMaxX(setMaxX);
                graph.getViewport().setXAxisBoundsManual(true);

                //coger tiempos crisis de "iniEvents" para ajustar las y
                String tiempoIniEvento;
                DataPoint[] crisisPoints = new DataPoint[iniEvents.size()];

                for (i = 0; i < iniEvents.size(); i++) { //recorre iniEvents->hora inicio evento
                    tiempoIniEvento = iniEvents.get(i).toString();  //tiempo del fichero eventos
                    String tiemposPuntos = tiemposValidos.get(0);  //inicializacion
                    long distance = Math.abs(Long.parseLong(tiemposPuntos) - Long.parseLong(tiempoIniEvento)); //dist inicial
                    int idx = 0;
                    for (j = 1; j < tiemposValidos.size(); j++) { //buscar pulsaciones mas proximas a ese tiempo
                        tiemposPuntos = tiemposValidos.get(j);
                        long jdistance = Math.abs(Long.parseLong(tiemposPuntos) - Long.parseLong(tiempoIniEvento)); //ver dstancia
                        if (jdistance < distance) {
                            idx = j;  //tiemposValidos lleva el mismo indice que points
                            distance = jdistance;
                        }
                    }
                    crisisPoints[i] = new DataPoint(Double.parseDouble(tiemposValidos.get(idx)), 0); //guardar x,y.
                }

                bgSerie = new PointsGraphSeries<>(crisisPoints);

                /*bgSerie.setShape(PointsGraphSeries.Shape.TRIANGLE);
                bgSerie.setColor(Color.RED);
                bgSerie.setSize(15);
                bgSerie.setTitle(mContext.getResources().getString(R.string.punto_crisis));*/

            } else {
                maxY = points.length > 0 ? points[0].getY() : 0;
                minY = maxY;
                for (i = 0; i < points.length; i++) { // Rehacemos el array y buscamos maxY minY
                    //eje de pulsaciones (Y)
                    if (maxY < points[i].getY()) { //actualizar max
                        maxY = points[i].getY();
                    }
                    if (minY > points[i].getY()) { //acualizar min
                        minY = points[i].getY();
                    }
                }
                series = new LineGraphSeries<>(points);

                // Establecemos la distancia inicial que se muestra del grafico
                graph.getViewport().setMinX(points.length > 0 ? points[0].getX() : 0);
                graph.getViewport().setMaxX(points.length > 0 ? points[i / 2].getX() : 0);
                graph.getViewport().setXAxisBoundsManual(true);

                //coger tiempos crisis de "iniEvents" para ajustar las y
                String tiempoIniEvento;
                DataPoint[] barPoints = new DataPoint[iniEvents.size()];

                if (points.length > 1) {
                    for (i = 0; i < iniEvents.size(); i++) { //recorre iniEvents->hora inicio evento
                        tiempoIniEvento = iniEvents.get(i).toString();  //tiempo del fichero eventos
                        String tiemposPuntos = tiemposValidos.get(0);  //inicializacion
                        long distance = Math.abs(Long.parseLong(tiemposPuntos) - Long.parseLong(tiempoIniEvento)); //dist inicial
                        int idx = 0;
                        for (j = 1; j < tiemposValidos.size(); j++) { //buscar pulsaciones mas proximas a ese tiempo
                            tiemposPuntos = tiemposValidos.get(j);
                            long jdistance = Math.abs(Long.parseLong(tiemposPuntos) - Long.parseLong(tiempoIniEvento)); //ver dstancia
                            if (jdistance < distance) {
                                idx = j;  //tiemposValidos lleva el mismo indice que points
                                distance = jdistance;
                            }
                        }
                        barPoints[i] = new DataPoint(points[idx].getX(), points[idx].getY()); //guardar x,y.
                    }
                } else {
                    barPoints = new DataPoint[0];
//                    barPoints[0] = new DataPoint(0, 0);
                }
                bgSerie = new PointsGraphSeries<>(barPoints);

                /*bgSerie.setShape(PointsGraphSeries.Shape.TRIANGLE);
                bgSerie.setColor(Color.RED);
                bgSerie.setSize(15);
                bgSerie.setTitle(mContext.getResources().getString(R.string.punto_crisis));*/
                //}
            }

            bgSerie.setShape(PointsGraphSeries.Shape.TRIANGLE);
            bgSerie.setColor(Color.RED);
            bgSerie.setSize(15);
            bgSerie.setTitle(mContext.getResources().getString(R.string.punto_crisis));

            /*****Opciones de la grafica*****/
            // establecemos el maximo y minimo que se muestra del eje Y
            graph.getViewport().setYAxisBoundsManual(true);
            graph.getViewport().setMinY(minY);
            graph.getViewport().setMaxY(maxY);

            // para el zoom de la grafica
            graph.getViewport().setScalable(true);

            // para el desplazamiento de la grafica
            graph.getViewport().setScrollable(true);
            // optional styles
            graph.setTitleTextSize(65);
            //graph.setTitleColor(Color.BLUE);
            //graph.getGridLabelRenderer().setVerticalAxisTitleTextSize(40);
            graph.getGridLabelRenderer().setVerticalAxisTitleColor(Color.BLUE);
            //graph.getGridLabelRenderer().setHorizontalAxisTitleTextSize(40);
            graph.getGridLabelRenderer().setHorizontalAxisTitleColor(Color.BLUE);

            OnDataPointTapListener I = new OnDataPointTapListener() {
                @Override
                public void onTap(Series series, DataPointInterface dataPoint) {
                    DecimalFormat percentageFormat = new DecimalFormat("00.00");
                    String rounded = percentageFormat.format(dataPoint.getY()); //redondear a dos decimales

                    Toast.makeText(mContext, rounded + " " + unidades, Toast.LENGTH_SHORT).show();
                }
            };

            // crear 5 colores max (que luego repita)
            ArrayList<Integer> colors = new ArrayList<>();
            colors.add(Color.rgb(204, 153, 255));
            colors.add(Color.rgb(0, 255, 153));
            colors.add(Color.rgb(255, 204, 0));
            colors.add(Color.rgb(102, 153, 255));
            colors.add(Color.rgb(255, 102, 102));

            //Titulo de la leyenda
            if (series != null) {
                if (!Objects.equals(nombreFich, "pulsometro")) {
                    CharSequence nobreCol;
                    if (nombresEjes != null) {
                        nobreCol = nombresEjes[0]; //solo tendra un valor
                    } else { // Solo por si acaso
                        nobreCol = "meassure";
                    }
                    String nombreEje = (String) nobreCol;
                    series.setTitle(nombreEje);
                } else {
                    // Nombre linea grafico
                    series.setTitle(mContext.getResources().getString(R.string.ritmo_cardiaco));
                }
                // register tap on series callback
                series.setOnDataPointTapListener(I);
                // Serie principal
                graph.addSeries(series);
            }
            if (ejes != null) {
                for (int rColor = 0, eje = 0; eje < ejes.size(); eje++, rColor++) {
                    String nombreEje = null; //si ejes != null nombresEjes tmbn != null
                    if (nombresEjes != null) {
                        String s = String.valueOf(ejes.get(eje));
                        CharSequence nomEje = nombresEjes[Integer.parseInt(s)];
                        nombreEje = (String) nomEje;
                        //nombreEje = (String) nombresEjes[Integer.parseInt((String) ejes.get(eje))];
                    }
                    LineGraphSeries<DataPoint> serieEje = seriesEjes.get(eje);
                    serieEje.setTitle(nombreEje);
                    // register tap on series callback
                    serieEje.setOnDataPointTapListener(I);
                    if (rColor == 5) rColor = 0; //reset
                    serieEje.setColor(colors.get(rColor));
                    graph.addSeries(serieEje);
                }
            }

            // Serie de ptos. crisis
            graph.addSeries(bgSerie);

            // Nombres ejes y chart
            graph.setTitle(titulo + " " + mContext.getResources().getString(R.string.for_day) + " " + fecha);
            graph.getGridLabelRenderer().setVerticalAxisTitle(unidades);

            // formato de la etiqueta de la fecha
            DateFormat miDateFormat = android.text.format.DateFormat.getTimeFormat(graph.getContext());
            graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(graph.getContext(), miDateFormat));
            graph.getGridLabelRenderer().setNumHorizontalLabels(mNumLabels);

            // estilo de la leyenda
            graph.getLegendRenderer().setVisible(true);
            graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
            graph.getLegendRenderer().setMargin(30);

        } catch (FileNotFoundException ex) {
            Log.d("Not found", ex.getMessage());
        } catch (IOException ex) {
            Log.d("IO excp", ex.getMessage());
        }
        Log.v("SCALINGX", "FIN");
    }
}