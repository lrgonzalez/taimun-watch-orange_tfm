package com.orange.taimun_watch.es.model;

/**
 * Created by Yux on 21/07/2016.
 */
public class Usuario {
    private String nombre;
    private String sexo;
    private String tipoReloj;
    private String foto;

    public Usuario() { }

    public Usuario(String nombre, String sexo, String tipoReloj, String foto) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.tipoReloj = tipoReloj;
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTipoReloj() {
        return tipoReloj;
    }

    public void setTipoReloj(String tipoReloj) {
        this.tipoReloj = tipoReloj;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
