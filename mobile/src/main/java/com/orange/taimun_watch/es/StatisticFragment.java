package com.orange.taimun_watch.es;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.orange.taimun_watch.es.model.Estadistica;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static android.app.Activity.RESULT_OK;


/**
 * Fragmento para la visualizacion de las tarjetas de graficas de cada dia con
 * selector de fecha.
 *
 * @author Lucia
 */

public class StatisticFragment extends Fragment implements View.OnClickListener {

    private ArrayList<Estadistica> lstEstadisticas = new ArrayList<>();
    private StatisticViewAdapter adapter;
    private View view;
    private TextView txtNone;
    private RecyclerView recyclerView;

    private EditText mDateDisplay;
    private EditText mDateDisplay2;
    private int mYear;
    private int mMonth;
    private int mDay;
    static final int CALENDAR_VIEW_FIN = 0;
    static final int CALENDAR_VIEW_ID = 1;
    static final int TIPO_SENSORS = 0;
    static final int TIPO_EVENTS = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment.
        view = inflater.inflate(R.layout.statistic_fragment, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.statistics);

        //Inicializamos elementos del layout.
        mDateDisplay = (EditText) view.findViewById(R.id.dateDisplay);
        mDateDisplay2 = (EditText) view.findViewById(R.id.dateDisplay2);

        txtNone = (TextView) view.findViewById(R.id.no_statistics);
        recyclerView = (RecyclerView) view.findViewById(R.id.statistics_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(linearLayoutManager);

        //Obtenemos boton de añadir elegir fecha y le asignamos su listener
        Button btnPickDateCalendar = (Button) view.findViewById(R.id.btn_pickDate_calendar); //calendar
        btnPickDateCalendar.setOnClickListener(this);

        Button btnPickDateCalendar2 = (Button) view.findViewById(R.id.btn_pickDate_calendar2); //calendar
        btnPickDateCalendar2.setOnClickListener(this);
        // Y al de mostrar todos
        Button btnPickDateTodo = (Button) view.findViewById(R.id.btn_pickDate_todo); //calendar
        btnPickDateTodo.setOnClickListener(this);

        // Fecha actual del dispositivo
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        // display the current date
        //displayDate(); //para elegir con ruleta

        displayCalendarViewDate(); //para elegir fecha con el calendario. Pone la fecha actual

        return view;
    }

    /*---------------------- FUNCIONES DEL CALENDARIO Y SELECTOR---------------------*/
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_pickDate_calendar:
                Intent intent = new Intent(getActivity(), MyCalendar.class);
                startActivityForResult(intent, CALENDAR_VIEW_ID);
                break;
            case R.id.btn_pickDate_calendar2:
                Intent intent2 = new Intent(getActivity(), MyCalendar.class);
                startActivityForResult(intent2, CALENDAR_VIEW_FIN);
                break;
            case R.id.btn_pickDate_todo:
                // borrar seleccion
                mDateDisplay.setText(R.string.initial_date);
                displayCalendarViewDate();
                onResume();
                break;
        }
    }

    //Recoge los datos de la fecha
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case CALENDAR_VIEW_ID:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();

                    mDateDisplay = (EditText) getActivity().findViewById(R.id.dateDisplay);
                    mDateDisplay.setText(bundle.getString("dateSelected"));
                    break;
                }
                //fecha hasta
            case CALENDAR_VIEW_FIN:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();

                    mDateDisplay2 = (EditText) getActivity().findViewById(R.id.dateDisplay2);
                    mDateDisplay2.setText(bundle.getString("dateSelected"));
                    break;
                }
        }
    }

    // actualiza la fecha seleccionada
    private void displayDate() {
        mDateDisplay.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(mDay).append("/")
                        .append(mMonth + 1).append("/")
                        .append(mYear).append(" "));
    }

    // actualiza la fecha del dispositivo
    private void displayCalendarViewDate() {
        mDateDisplay2.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(mDay).append("/")
                        .append(mMonth + 1).append("/")
                        .append(mYear).append(" "));
    }

    // callback recibido al modificar la fecha
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    mDay = dayOfMonth;
                    mMonth = monthOfYear;
                    mYear = year;
                    displayDate();
                }
            };

    /*---------------------- FUNCIONES DE TARJETAS DE GRAFICAS ---------------------*/

    public void inicializarListaEstadisticas() {
        //acceder a carpeta de logs/sensors y comprobar que hay ficheros
        if (getPath(TIPO_SENSORS) != null) {
            File directory = new File(getPath(TIPO_SENSORS));
            File[] contents = directory.listFiles();

            // the directory file is not really a directory
            if (contents == null) {
                Log.e("MOVIL", "Error comprobando carpeta de sensores");
                Log.d("path=", getPath(TIPO_SENSORS));
            }
            //Si no hay estadisticas mostramos un texto
            if (contents == null || contents.length == 0) {
                txtNone.setVisibility(View.VISIBLE);
            }
            //Cargamos la lista de estadisticas en el recyclerview
            else {
                txtNone.setVisibility(View.INVISIBLE);
                lstEstadisticas = new ArrayList<>();
                lstEstadisticas = getStatisticsOverview();
                if (lstEstadisticas == null)
                    txtNone.setVisibility(View.VISIBLE);
            }
        } else {
            txtNone.setVisibility(View.VISIBLE);
        }
    }

    public void updateStatisticAdapter() {
        StatisticViewAdapter sa = (StatisticViewAdapter) recyclerView.getAdapter();
        if (sa != null) {
            if (lstEstadisticas != null) {
                sa.updateStatistics(lstEstadisticas);
                sa.notifyDataSetChanged();
            }
        } else {
            if (lstEstadisticas != null) {
                /*Aqui se inicializara la visualizacion de la grafica o graficas*/
                adapter = new StatisticViewAdapter(lstEstadisticas, new StatisticViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Estadistica stat) {
                        String[] listaExtras = {stat.getNombre(), stat.getEventos(), stat.getPulsometro()};
                        //Navegamos a Intent pasándole la ruta del fichero
                        Intent intent = new Intent(getActivity(), SumStatisticActivity.class);
                        intent.putExtra("listaExtras", listaExtras);
                        startActivity(intent);
                    }
                });
                recyclerView.setAdapter(adapter);
            }
        }
    }

    public String getPath(int tipo) {
        Context mContext = getContext();
        String path = mContext.getExternalFilesDir(null).getAbsolutePath(); //Llega a la ruta  de files
        //String idReloj = Settings.Secure.getString(mContext.getContentResolver(),
        //      Settings.Secure.ANDROID_ID);

        File directory = new File(path + "/logs");
        File[] contents = directory.listFiles();
        if (contents == null) { //si no hay ningun archivo en logs
            return null;
        }
        // Cambiamos la ruta al nombre del grupo
//        String idReloj = contents[0].getName();
        String grupo = Preferences.getGroupCode(mContext);

        if (tipo == TIPO_SENSORS) {
            return path + "/logs/" + grupo + "/sensors"; //+dias
        } else if (tipo == TIPO_EVENTS) {
            return path + "/logs/" + grupo + "/events";
        }
        return null;
    }

    //Recorre los logs/sensor para coger una estadistica por fecha
    public ArrayList<Estadistica> getStatisticsOverview() {
        String fechaIni = String.valueOf(mDateDisplay.getText()); //Fechas seleccionadas
        String fechaFin = String.valueOf(mDateDisplay2.getText());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date dateIni = null;
        Date dateEnd = null;
        Date dateFich = null;
        if (!fechaIni.equals(getResources().getString(R.string.initial_date))) {
            try {
                dateIni = sdf.parse(fechaIni);
                //System.out.println("date1 : " + sdf.format(dateIni));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        try {
            dateEnd = sdf.parse(fechaFin);
            //System.out.println("date2 : " + sdf.format(dateEnd));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (dateIni != null && dateEnd != null) {
            if (dateIni.compareTo(dateEnd) > 0) { //16 > 15
                System.out.println("Date1 is after Date2");
                //error, no hacer nada
            }
        }

        String path = getPath(TIPO_SENSORS);
        File[] sensorsFiles = new File(path).listFiles();
        ArrayList<String> agrupados = new ArrayList<>(); //lista de sensores ya agrupados
        //Por cada fichero de la carpeta de sensors agrupar los de la misma fecha en una Estadistica
        for (File snsrFile : sensorsFiles) {
            String fecha = obtenerFecha(snsrFile.getName());
            String auxCad = fecha.substring(0, fecha.length() - 4); //quitar .txt para titulo
            auxCad = auxCad.replace('_', '/'); //cambiar separadores
            if (dateIni != null && dateEnd != null) {
                try {
                    dateFich = sdf.parse(auxCad);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            // Que la del fichero sea la misma fecha o este en medio. O que no se haya definido fecha inicial
            if (dateFich == null || ((dateFich.compareTo(dateIni) == 0 || dateFich.compareTo(dateIni) > 0)
                    && (dateFich.compareTo(dateEnd) == 0 || dateFich.compareTo(dateEnd) < 0))) {

                if (agrupados.isEmpty() || !agrupados.contains(snsrFile.getName())) { //si aun no ha sido agrupado
                    String eventos = getPath(TIPO_EVENTS) + "/events-" + fecha; //la fecha es igual para el event
                    String acelerometro = null;
                    String detectorPasos = null;
                    String giroscopio = null;
                    String pulsometro = null;
                    for (File snsrFile2 : sensorsFiles) { //comprobar los demas de la misma fecha
                        if (snsrFile2.getName().contains(fecha)) {
                            if (snsrFile2.getName().contains("acelerometro")) {
                                acelerometro = path + "/" + snsrFile2.getName(); //guardar path
                                agrupados.add(snsrFile2.getName());
                            } else if (snsrFile2.getName().contains("detectorPasos")) {
                                detectorPasos = path + "/" + snsrFile2.getName(); //guardar path
                                agrupados.add(snsrFile2.getName());
                            } else if (snsrFile2.getName().contains("giroscopio")) {
                                giroscopio = path + "/" + snsrFile2.getName();
                                agrupados.add(snsrFile2.getName());
                            } else if (snsrFile2.getName().contains("pulsometro")) {
                                pulsometro = path + "/" + snsrFile2.getName();
                                agrupados.add(snsrFile2.getName());
                            } else { //otros
                                agrupados.add(snsrFile2.getName());
                            }
                        }
                    }
                    //crear la estadistica con los path de cada uno
                    Estadistica est = new Estadistica(auxCad, acelerometro, detectorPasos, giroscopio, pulsometro, eventos);
                    lstEstadisticas.add(est);
                }
            }
        }
        // Se ordenan las fechas de menor a mayor
        Collections.sort(lstEstadisticas, Estadistica.fechaComparator);
        // Se establece la menor de las fechas disponibles como "fecha_desde"
        String[] nombreParts = lstEstadisticas.get(0).getNombre().split("/");
        if (nombreParts.length > 2) {
            String dia = nombreParts[0];
            String mes = nombreParts[1];
            String anyo = nombreParts[2];
            mDateDisplay.setText(
                    new StringBuilder()
                            .append(dia).append("/")
                            .append(mes).append("/")
                            .append(anyo).append(" "));
            return lstEstadisticas; //estadisticas de sensores agrupadas por fechas
        }
        return null;
    }

    //Funcion que convierte string tipo acelerometro-19_1_2017.txt en 19_1_2017.txt
    private static String obtenerFecha(String filename) {
        String[] parts = filename.split("-");
        if (parts.length < 2)
            return "NO_DATE";
        return parts[1];
    }

    @Override
    public void onResume() {
        super.onResume();
        inicializarListaEstadisticas();
        updateStatisticAdapter();
    }

}