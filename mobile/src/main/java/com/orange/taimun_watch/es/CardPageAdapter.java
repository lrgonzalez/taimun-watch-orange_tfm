package com.orange.taimun_watch.es;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.ArrayList;

import com.orange.taimun_watch.es.model.Recurso;

/**
 * Modified by Yux on 27/06/2016.
 */

public class CardPageAdapter extends PagerAdapter {

    private ArrayList<Recurso> cardViews = new ArrayList<Recurso>();
    private LayoutInflater mLayoutInflater;
    private Context context;
    private PhotoUtils photoUtils;

    public CardPageAdapter(Context context) {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
        photoUtils = new PhotoUtils(context);
    }

    @Override
    public int getItemPosition (Object object) {
        int index = cardViews.indexOf(object);
        if (index == -1)
            return POSITION_NONE;
        else
            return index;
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.  Called when ViewPager needs a page to display. Since
    // all our pages are persistent, we simply retrieve it from our "views" ArrayList.
    @Override
    public Object instantiateItem (ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.card_pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.cardPager_image);
        TextView textView = (TextView) itemView.findViewById(R.id.cardPager_text);

        //Hacemos la carga del recurso segun el tipo que sea
        if (cardViews.get(position).getTipoRecurso() == 0) {
            //Para tipo de imagen predeterminada
            Integer drawableId = Integer.parseInt(cardViews.get(position).getRutaValor());

            Glide.with(context).load(drawableId)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

        } else if (cardViews.get(position).getTipoRecurso() == 1) {
            String filePath = photoUtils.getFilePathRecurso(cardViews.get(position).getRutaValor());
            File fileRecurso = new File(filePath);

            Glide.with(context).load(fileRecurso)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

        } else if (cardViews.get(position).getTipoRecurso() == 2) {
            Recurso txtRecurso = cardViews.get(position);
            String literal = txtRecurso.getRutaValor();

            //Configuramos modo de texto
            if (txtRecurso.getModo() == 0) {
                textView.setText(literal);
            } else if (txtRecurso.getModo() == 1) {
                textView.setText(literal.toUpperCase());
            } else if (txtRecurso.getModo() == 2) {
                textView.setText(literal.toLowerCase());
            }

            //Color y tamaño
            textView.setTextColor(txtRecurso.getColor());
            textView.setTextSize(txtRecurso.getSize());
        }

        container.addView(itemView);

        return itemView;
    }

    //-----------------------------------------------------------------------------
    // Used by ViewPager.  Called when ViewPager no longer needs a page to display.
    @Override
    public void destroyItem (ViewGroup container, int position, Object object) {
        //container.removeView (cardViews.get(position).getView());
    }

    @Override
    public int getCount() {
        return cardViews.size();
    }

    @Override
    public boolean isViewFromObject (View view, Object object) {
        return view == object;
    }

    //-----------------------------------------------------------------------------
    // Add "view" to right end of "cardViews".
    // Returns the position of the new view.
    // The app should call this to add pages; not used by ViewPager.
    public int addView (Recurso recurso) {
        return addView (recurso, cardViews.size());
    }

    //-----------------------------------------------------------------------------
    // Add "view" at "position" to "cardViews".
    // Returns position of new view.
    // The app should call this to add pages; not used by ViewPager.
    public int addView (Recurso recurso, int position) {
        cardViews.add (position, recurso);
        return position;
    }

    //-----------------------------------------------------------------------------
    // Replace "view" at "position" of "cardViews".
    // Returns position of replaced view.
    public int replaceView (ViewPager pager, Recurso recurso, int position) {
        //cardViews.set(position, v);
        removeView(pager, position);
        addView(recurso, position);

        return position;
    }

    //-----------------------------------------------------------------------------
    // Removes "view" from "views".
    // Retuns position of removed view.
    // The app should call this to remove pages; not used by ViewPager.
    public int removeView (ViewPager pager, Recurso recurso) {
        return removeView (pager, cardViews.indexOf(recurso));
    }

    //-----------------------------------------------------------------------------
    // Removes the "view" at "position" from "views".
    // Retuns position of removed view.
    // The app should call this to remove pages; not used by ViewPager.
    public int removeView (ViewPager pager, int position) {
        // ViewPager doesn't have a delete method; the closest is to set the adapter
        // again.  When doing so, it deletes all its views.  Then we can delete the view
        // from from the adapter and finally set the adapter to the pager again.  Note
        // that we set the adapter to null before removing the view from "views" - that's
        // because while ViewPager deletes all its views, it will call destroyItem which
        // will in turn cause a null pointer ref.
        pager.setAdapter (null);
        cardViews.remove (position);
        pager.setAdapter (this);

        return position;
    }

    //-----------------------------------------------------------------------------
    // Returns the "view" at "position".
    // The app should call this to retrieve a view; not used by ViewPager.
    /*public View getView (int position) {
        return cardViews.get(position).getView();
    }*/

    // Metodo para obtener el array de recursos
    public ArrayList<Recurso> getCardViews() {
        return cardViews;
    }

    // Metodo para obtener un recurso dada una posicion
    public Recurso getCardViewRecurso(int posicion) {
        return cardViews.get(posicion);
    }

    public void setCardViews(ArrayList<Recurso> cardViews) {
        this.cardViews = cardViews;
    }
}