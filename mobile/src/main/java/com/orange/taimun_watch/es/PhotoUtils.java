package com.orange.taimun_watch.es;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import com.orange.taimun_watch.es.model.Recurso;

/**
 * Created by Yux on 11/07/2016.
 */

/**
 * Esta clase contiene los metodos necesarios para la manipulacion
 * de imagenes y fotos de la aplicacion
 * @author Comunidad de desarrolladores
 * @Modificada por Yussy Chinchay
 */
public class PhotoUtils {
    private static Context mContext;
    private Options generalOptions;
    public static final String dirRecursos = "Recursos";
    public static final String fotoFile = "profilePhoto";
    public static final String resourceFile = "resource";
    public static final String iconsFile = "icons";
    private static final int SMALL=22, NORMAL=29, HUGE=39;

    public PhotoUtils(Context context) {
        mContext = context;
    }

    public static File createTemporaryFile(String part, String ext,	Context myContext) throws Exception {
        String path = myContext.getExternalCacheDir().getAbsolutePath() + "/temp/";
        File tempDir = new File(path);
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public Bitmap getImage(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream is = null;

        try {
            is = mContext.getContentResolver().openInputStream(uri);
            BitmapFactory.decodeStream(is, null, options);
            is.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.generalOptions = options;
        return scaleImage(options, uri, 600);
    }

    public static int nearest2pow(int value) {
        return value == 0 ? 0
                : (32 - Integer.numberOfLeadingZeros(value - 1)) / 2;
    }

    public Bitmap scaleImage(BitmapFactory.Options options, Uri uri, int targetWidth) {

        if (options == null)
            options = generalOptions;

        Bitmap bitmap = null;
        double ratioWidth = ((float) targetWidth) / (float) options.outWidth;
        double ratioHeight = ((float) targetWidth) / (float) options.outHeight;
        double ratio = Math.min(ratioWidth, ratioHeight);
        int dstWidth = (int) Math.round(ratio * options.outWidth);
        int dstHeight = (int) Math.round(ratio * options.outHeight);
        ratio = Math.floor(1.0 / ratio);
        int sample = nearest2pow((int) ratio);

        options.inJustDecodeBounds = false;
        if (sample <= 0) {
            sample = 1;
        }

        options.inSampleSize = (int) sample;
        options.inPurgeable = true;
        try {
            InputStream is;
            is = mContext.getContentResolver().openInputStream(uri);
            bitmap = BitmapFactory.decodeStream(is, null, options);
            if (sample > 1)
                bitmap = Bitmap.createScaledBitmap(bitmap, dstWidth, dstHeight,	true);
            is.close();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return bitmap;
    }

    /**
     * Metodo para cargar imagen guardada en la carpeta interna
     * del usuario
     */
    public void cargarRecursoUsuario(String recursoName, ImageView fotoView) {
        //Cogemos ruta de directorio de Imagenes
        ContextWrapper cw= new ContextWrapper(mContext);
        File dirFile = cw.getDir(dirRecursos, Context.MODE_PRIVATE);

        String filePath = dirFile.toString() + "/" + recursoName + ".png";
        fotoView.setImageDrawable(Drawable.createFromPath(filePath));
        fotoView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    /**
     * Metodo para guardar foto del usuario en la carpeta interna
     * del mismo
     */
    public void guardarFoto(ImageView fotoUser) {
        ContextWrapper cw = new ContextWrapper(mContext);
        File dirImages = cw.getDir(dirRecursos, Context.MODE_PRIVATE);
        File myPath = new File(dirImages, fotoFile + ".png");
        Bitmap bitmap = ((BitmapDrawable)fotoUser.getDrawable()).getBitmap();

        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream(myPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
        }catch (FileNotFoundException ex){
            ex.printStackTrace();
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Metodo para guardar foto del usuario en la carpeta interna
     * del mismo
     */
    public void  guardarRecurso(ImageView recurso, String ruta) {
        ContextWrapper cw = new ContextWrapper(mContext);
        File dirImages = cw.getDir(dirRecursos, Context.MODE_PRIVATE);
        File myPath = new File(dirImages, ruta + ".png");
        Bitmap bitmap = ((BitmapDrawable)recurso.getDrawable()).getBitmap();

        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream(myPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
        }catch (FileNotFoundException ex){
            ex.printStackTrace();
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Metodo para obtener el filepath de un recurso dado su nombre
     * @param nombre de recurso
     */
    public String getFilePathRecurso(String nombre) {
        //Cogemos ruta de directorio de Imagenes
        ContextWrapper cw = new ContextWrapper(mContext);
        File dirFile = cw.getDir(dirRecursos, Context.MODE_PRIVATE);

        String filePath = dirFile.toString() + "/" + nombre + ".png";

        return filePath;
    }

    /**
     * Método para convertir un recurso de texto en imagen al sincronizar
     * una regulación.
     * @param txtRecurso recurso de texto.
     * @param dst fichero donde quiere la imagen del texto.
     * @return nombre de la imagen guardada en la
     *         carpeta regulations.
     */
    public String textToImage(Recurso txtRecurso, File dst) {
        TextView textView = new TextView(mContext);
        String literal = txtRecurso.getRutaValor();
        String centredLiteral;
        long id = txtRecurso.getIdBBDD();

        textView.layout(0, 0, 100, 100);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);

        //Color y tamaño
        textView.setTextSize(txtRecurso.getSize()/4);
        textView.setTextColor(txtRecurso.getColor());
        textView.setBackgroundColor(Color.WHITE);
        textView.setWidth(500);
        textView.setHeight(500);

        switch(txtRecurso.getSize()) {
            case SMALL:
                centredLiteral = "\n\n" + literal;
                break;
            default:
                centredLiteral = "\n" + literal;
                break;
        }

        //Configuramos modo de texto
        if (txtRecurso.getModo() == 0) {
            textView.setText(centredLiteral);
        } else if (txtRecurso.getModo() == 1) {
            textView.setText(centredLiteral.toUpperCase());
        } else if (txtRecurso.getModo() == 2) {
            textView.setText(centredLiteral.toLowerCase());
        }

        //Guardamos la imagen en la carpeta.
        File txt = new File(dst.getAbsolutePath(), "txt" + String.valueOf(id) + ".png");
        viewToImage(textView, txt);
        return literal;
    }

    /**
     * Método para convertir una vista en una imagen y guardarla.
     * @param v vista que quiere convertirse.
     * @param dst fichero donde quiere guardarse la imagen.
     */
    public void viewToImage(View v, File dst) {
        try {

            v.setDrawingCacheEnabled(true);

            Bitmap b = v.getDrawingCache();

            OutputStream os = new BufferedOutputStream(new FileOutputStream(dst));
            b.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}