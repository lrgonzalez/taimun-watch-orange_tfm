package com.orange.taimun_watch.es;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.model.ListaEstrategia;

/**
 * Created by Yux on 10/05/2016.
 */

public class StrategyFragment extends Fragment implements View.OnClickListener {
    private TextView txtNone;
    private RecyclerView recyclerView;
    private ArrayList<ListaEstrategia> lstEstrategia;
    private StrategyViewAdapter adapter;
    private DatabaseAdapter db;
    private View view;
    private long strategiesNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.v("STRATEGY_FRAGMENT", "OnCreate()");
        // Inflate the layout for this fragment.
        view = inflater.inflate(R.layout.strategy_fragment, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.strategies);

        //Inicializamos elementos del layout.
        txtNone = (TextView) view.findViewById(R.id.no_strategies);
        recyclerView = (RecyclerView) view.findViewById(R.id.strategies_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        //Obtenemos boton de añadir estrategia y le asignamos su listener
        final FloatingActionButton btnAddStrategy = (FloatingActionButton) view.findViewById(R.id.btn_add_strategy);
        btnAddStrategy.setOnClickListener(this);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    // Scroll Down
                    if (btnAddStrategy.isShown()) {
                        btnAddStrategy.hide();
                    }
                } else if (dy <= 0) {
                    // Scroll Up
                    if (!btnAddStrategy.isShown()) {
                        btnAddStrategy.show();
                    }
                }
            }

        });

        return view;
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent((MainActivity) getActivity(), NewStrategyActivity.class));
    }

    public void inicializarListaEstrategia(){

        db = new DatabaseAdapter(getContext());
        db.open();

        strategiesNumber = db.getStrategiesNumber();

        //Cargamos la lista de estrategias en el recyclerview
        if (strategiesNumber > 0) {
            txtNone.setVisibility(View.INVISIBLE);
            lstEstrategia = db.getStrategiesOverview();

        //Si no hay estrategias mostramos un texto
        } else {
            txtNone.setVisibility(View.VISIBLE);
        }

        db.close();
    }

    public void updateStrategyAdapter() {
        StrategyViewAdapter sa = (StrategyViewAdapter) recyclerView.getAdapter();

        if(sa != null) {
            if(lstEstrategia != null) {
                sa.updateStrategies(lstEstrategia);
                sa.notifyDataSetChanged();
            }
        } else {
            if(lstEstrategia != null) {
                adapter = new StrategyViewAdapter(lstEstrategia, getContext(), false, new StrategyViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(ListaEstrategia estrategia) {
                        //Navegamos a Intent pasándole el id de la estrategia
                        Intent intent = new Intent((MainActivity) getActivity(), NewStrategyActivity.class);
                        intent.putExtra("strategyId", estrategia.getIdBBDD());
                        startActivity(intent);
                    }
                }, true, false);

                recyclerView.setAdapter(adapter);

                //Asignamos adaptador para permitir drag and swipe
                ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(adapter);
                ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                touchHelper.attachToRecyclerView(recyclerView);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        inicializarListaEstrategia();
        updateStrategyAdapter();
    }
}
