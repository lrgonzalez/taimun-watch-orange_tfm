package com.orange.taimun_watch.es;

/**
 * Created by Yux on 09/08/2016.
 */

public interface ItemTouchHelperAdapter {

    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}