package com.orange.taimun_watch.es.model;

/**
 * Created by lucia on 20/02/2017.
 */

public class EstadisticaDetalle {

    private String nombreRegulacion;
    private String detalleInicio;
    private String detalleFin;
    private String idRegulacion;

    public EstadisticaDetalle(String nombreRegulacion, String detalleInicio, String detalleFin, String idRegulacion){
        this.nombreRegulacion = nombreRegulacion;
        this.detalleInicio = detalleInicio;
        this.detalleFin = detalleFin;
        this.idRegulacion = idRegulacion;
    }

    public String getNombreRegulacion() {
        return nombreRegulacion;
    }

    public String getDetalleInicio() {
        return detalleInicio;
    }

    public String getDetalleFin() {
        return detalleFin;
    }

    public String getIdRegulacion() {
        return idRegulacion;
    }
}
