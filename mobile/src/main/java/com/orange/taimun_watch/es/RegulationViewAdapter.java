package com.orange.taimun_watch.es;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import com.orange.taimun_watch.es.utils.FileUtils;
import com.orange.taimun_watch.es.model.ListaRegulacion;
import com.orange.taimun_watch.es.model.Regulacion;

/**
 * Created by Yux on 11/08/2016.
 */
public class RegulationViewAdapter extends RecyclerView.Adapter<RegulationViewAdapter.ViewHolder> implements ItemTouchHelperAdapter{
    private ArrayList<ListaRegulacion> regulaciones;
    private PhotoUtils photoUtils;
    private Context context;
    private final OnItemClickListener listener;

    // Provee una referencia a cada item dentro de una vista y acceder a ellos facilmente
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // Cada uno de los elementos de mi vista
        public ImageView imgIcon;
        public ImageButton imgSync;
        public TextView nombreRegulacion, descripcion;

        public ViewHolder(View v){
            super(v);
            imgIcon = (ImageView) v.findViewById(R.id.regulation_icon);
            imgSync = (ImageButton) v.findViewById(R.id.imgSync);
            nombreRegulacion = (TextView) v.findViewById(R.id.regulation_name);
            descripcion = (TextView) v.findViewById(R.id.regulation_description);
        }

        public void bind(final ListaRegulacion regulacion, final OnItemClickListener listener, final Context context) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(regulacion);
                }
            });

            imgSync.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Toast.makeText(context, "Sincronizando, por favor espere.", Toast.LENGTH_LONG).show();
                    /*Borramos el contenido de la carpeta.*/
                    FileUtils fu = new FileUtils(context);
                    fu.deleteRegulation();

                    DatabaseAdapter db = new DatabaseAdapter(context);
                    db.open();
                    Regulacion r = db.getRegulationById(regulacion.getIdBBDD());
                    db.close();
                    r.synchronizeRegulation(context);
                }
            });
        }
    }

    //Constructor
    public RegulationViewAdapter(ArrayList<ListaRegulacion> regulaciones, Context context, OnItemClickListener listener) {
        this.regulaciones = regulaciones;
        this.context = context;
        photoUtils = new PhotoUtils(context);
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RegulationViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflo la vista (vista padre)
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.regulation_cardview, parent, false);

        // creo el grupo de vistas
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    // Reemplaza en contenido de la vista
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        ListaRegulacion regulacion = regulaciones.get(position);

        //Asignamos nombre
        viewHolder.nombreRegulacion.setText(regulacion.getNombre());

        //Cargamos imagen de icono
        if(regulacion.getRutaIcono() == null) {
            viewHolder.imgIcon.setImageResource(R.drawable.icon_estrategia);
        } else {
            photoUtils.cargarRecursoUsuario(regulacion.getRutaIcono(), viewHolder.imgIcon);
        }

        //Cargamos numero de estrategias
        if (regulacion.getNumeroEstrategias() == 1) {
            viewHolder.descripcion.setText(regulacion.getNumeroEstrategias() + " " + context.getString(R.string.strategy));
        } else {
            viewHolder.descripcion.setText(regulacion.getNumeroEstrategias() + " " + context.getString(R.string.strategies));
        }

        //Mostramos informacion de tipo selector
        if (regulacion.getIsSelector() == 1) {
            viewHolder.descripcion.setText(viewHolder.descripcion.getText() + " - " + context.getString(R.string.selector));
        }

        if (regulacion.getIsSync() == 1) {
            viewHolder.imgSync.setBackgroundResource(R.drawable.icon_unsync);
        }

        //Asignamos listener
        viewHolder.bind(regulaciones.get(position), listener, context);
    }

    // Retorna el tamano de nuestra data
    @Override
    public int getItemCount() {
        return regulaciones.size();
    }

    public interface OnItemClickListener {
        void onItemClick(ListaRegulacion item);
    }

    public void updateRegulations(ArrayList<ListaRegulacion> regulaciones) {
        this.regulaciones = regulaciones;
    }

    /*
     * Implementamos métodos de la interfaz para permitir el drag and swipe
     */
    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(regulaciones, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(regulaciones, i, i - 1);
            }
        }

        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {

        final int pos = position;
        //Diálogo de guardado.
        //Instantiate an AlertDialog.Builder with its constructor
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this.context);

        //Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.dialog_borrado)
                .setTitle(R.string.dialog_title_borrado)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        DatabaseAdapter db = new DatabaseAdapter(context);
                        db.open();
                        db.deleteRegulationById(regulaciones.get(pos).getIdBBDD());
                        db.close();
                        regulaciones.remove(pos);
                        notifyItemRemoved(pos);

                    } }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                notifyItemChanged(pos);
            } });

        //Get the AlertDialog from create()
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
}