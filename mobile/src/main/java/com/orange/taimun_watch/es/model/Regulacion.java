package com.orange.taimun_watch.es.model;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.google.android.gms.wearable.DataMap;

import com.orange.taimun_watch.es.GoogleConnection;
import com.orange.taimun_watch.es.PhotoUtils;
import com.orange.taimun_watch.es.Preferences;
import com.orange.taimun_watch.es.utils.FileUtils;
import com.orange.taimun_watch.es.utilJSON;

/**
 * Created by Yux on 22/08/2016.
 */
public class Regulacion {
    private int Id;
    private String Name;
    private boolean Vibration;
    private boolean Selector;
    private String Audio;
    private ArrayList<Integer> RegulationSchedule;
    private Estrategia[] Strategies;

    private static final String MOB_TO_WEAR = "/mob_to_wear";
    private static final String TIMESTAMP = "timestamp";
    private static final String SYNC_TYPE = "sync_type";
    private static final String REGULATION_SYNC_KEY = "sync_reg";

    public Regulacion(int id, String name, boolean vibration, boolean selector, String audio, ArrayList<Integer> regulationSchedule, Estrategia[] strategies) {
        Id = id;
        Name = name;
        Vibration = vibration;
        Audio = audio;
        RegulationSchedule = regulationSchedule;
        Strategies = strategies;
        Selector = selector;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public boolean isVibration() {
        return Vibration;
    }

    public void setVibration(boolean vibration) {
        Vibration = vibration;
    }

    public String getAudio() {
        return Audio;
    }

    public void setAudio(String audio) {
        Audio = audio;
    }

    public ArrayList<Integer> getRegulationSchedule() {
        return RegulationSchedule;
    }

    public void setRegulationSchedule(ArrayList<Integer> regulationSchedule) {
        RegulationSchedule = regulationSchedule;
    }

    public Estrategia[] getStrategies() {
        return Strategies;
    }

    public void setStrategies(Estrategia[] strategies) {
        Strategies = strategies;
    }

    public boolean isSelector() { return Selector; }

    public void setSelector(boolean selector) { Selector = selector; }

    /****************************************************************
     * Métodos relativos a la sincronización de regulaciones
     ****************************************************************/

    /**
     * Funcion que vuelca la regulacion en el archivo pasado por argumentos
     * @param c contexto de la app.
     * @param fileOut fichero de salida.
     * @author Antonio Díaz Escudero
     */
    private void regulationToJson(Context c, File fileOut) {
        String regJSON;
        utilJSON ujson = new utilJSON();

        Log.i("SYN", "Convirtiendo regulacion " + this.getId() + " a JSON.");
        try {
            OutputStream out = new FileOutputStream(fileOut);
            ujson.writeRegulation(out, this);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("SYN", "Error al pasar al regulacion a formato JSON.");
        }

        return;
    }

    /**
     * Función que guarda los archivos necesarios para sincronizar la regulación y llama a los
     * métodos que interactuan con la data layer.
     * @param c contexto de la app.
     * @author Antonio Díaz Escudero
     */
    public int synchronizeRegulation(Context c) {

        GoogleConnection gCon = new GoogleConnection(c);
        PhotoUtils pu = new PhotoUtils(c);
        FileUtils fu = new FileUtils(c);

        /*Comprobamos si existe la carpeta regulation.*/
        File folder = c.getExternalFilesDir(null);
        File regFolder = new File(folder, "regulation");
        boolean check = regFolder.exists();
        if (!check) {
            new File(regFolder.getAbsolutePath()).mkdir();
        }

        Log.i("SYN", "Volcando regulación a JSON");
        File fileReg = new File(folder.getAbsolutePath() + "/regulation", this.getId() + ".json");
        regulationToJson(c, fileReg);

        /*Enviamos el icono de las estrategias. */
        for(Estrategia e : this.getStrategies()) {
            String icono = e.getIcon();
            if(icono != null) {
                if (icono.contains("icon")) {
                    File src = new File(pu.getFilePathRecurso(icono));
                    File dst = new File(regFolder.getAbsolutePath(), icono + ".png");
                    fu.copyFile(src, dst);
                }
            }
        }

        /*Recorremos los steps de las estrategias para copiar los recursos a la carpeta de sincronización.*/
        for(Estrategia e : this.getStrategies()) {
            for(String s : e.getSteps()) {
                //Si es un recurso propio lo copiamos de la carpeta.
                if(s.contains("resource")) {
                    File src = new File(pu.getFilePathRecurso(s));
                    File dst = new File(regFolder.getAbsolutePath(), s + ".png");
                    fu.copyFile(src, dst);
                } else if (!s.contains("txt")){ //Si es una imagen predeterminada.
                    fu.resourceToPNG(Integer.parseInt(s), regFolder);
                }
            }
        }

        //Se actualizan las preferencias.
        Preferences.setNumFiles(c, 0);
        Preferences.setNumFilesActual(c, 0);
        Preferences.setFinishSync(c, false);

        if(gCon.connect() == null) {
            Log.e("SYN", "Error al establecer la conexión con la API de Google.");
        }

        //Se crea el Data Map y se envía a la Data Layer.
        DataMap dataMap = new DataMap();
        dataMap.putLong(TIMESTAMP, System.currentTimeMillis());
        dataMap.putString(SYNC_TYPE, REGULATION_SYNC_KEY);
        gCon.sendData(MOB_TO_WEAR, dataMap);

        return 0;
    }
}