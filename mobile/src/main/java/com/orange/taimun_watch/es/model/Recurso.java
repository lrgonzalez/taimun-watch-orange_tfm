package com.orange.taimun_watch.es.model;

/**
 * Created by Yux on 02/08/2016.
 */
public class Recurso {
    long idBBDD;
    String rutaValor;
    int tipoRecurso; //0 predeterminado, 1 propios, 2 texto
    int modo;
    int color;
    int size;
    int order;

    public Recurso() { }

    public Recurso(long idBBDD, String rutaValor, int tipoRecurso) {
        this.idBBDD = idBBDD;
        this.rutaValor = rutaValor;
        this.tipoRecurso = tipoRecurso;
    }

    public long getIdBBDD() {
        return idBBDD;
    }

    public void setIdBBDD(long idBBDD) {
        this.idBBDD = idBBDD;
    }

    public String getRutaValor() {
        return rutaValor;
    }

    public void setRutaValor(String rutaValor) {
        this.rutaValor = rutaValor;
    }


    public int getTipoRecurso() {
        return tipoRecurso;
    }

    public void setTipoRecurso(int tipoRecurso) {
        this.tipoRecurso = tipoRecurso;
    }

    public int getModo() {
        return modo;
    }

    public void setModo(int modo) {
        this.modo = modo;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recurso recurso = (Recurso) o;

        if (idBBDD != recurso.idBBDD) return false;
        if (tipoRecurso != recurso.tipoRecurso) return false;
        if (modo != recurso.modo) return false;
        if (color != recurso.color) return false;
        if (size != recurso.size) return false;
        if (order != recurso.order) return false;

        return rutaValor.equals(recurso.rutaValor);
    }

    @Override
    public int hashCode() {
        int result = (int) (idBBDD ^ (idBBDD >>> 32));
        result = 31 * result + rutaValor.hashCode();
        result = 31 * result + tipoRecurso;
        result = 31 * result + modo;
        result = 31 * result + color;
        result = 31 * result + size;
        result = 31 * result + order;
        return result;
    }
}
