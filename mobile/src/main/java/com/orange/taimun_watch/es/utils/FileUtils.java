package com.orange.taimun_watch.es.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Esta clase contiene los metodos necesarios para la manipulacion
 * de ficheros.
 * @author Antonio Díaz Escudero.
 */
public class FileUtils {
    private static Context mContext;

    public FileUtils(Context context) {
        mContext = context;
    }

    public void resourceToPNG(int resourceId, File folder) {

        Bitmap resource = BitmapFactory.decodeResource(mContext.getResources(), resourceId);

        try {
            File file = new File(folder, resourceId + ".png");
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            resource.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void copyFile(File source, File dest) {
        InputStream is = null;
        OutputStream os = null;

        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }

            is.close();
            os.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Método que borra el contenido de la carpeta temporal regulation.
     */
    public void deleteRegulation() {
        /*Comprobamos si existe la carpeta regulation.*/
        File folder = mContext.getExternalFilesDir(null);
        File regFolder = new File(folder, "regulation");
        boolean check = regFolder.exists();
        if (check) {
            /*Borramos el contenido de la carpeta.*/
            File[] regList = regFolder.listFiles();

            Log.i("SYN", "Borrando ficheros de sincronizaciones anteriores.");
            for(File f : regList) {
                f.delete();
            }
        }
    }
}
