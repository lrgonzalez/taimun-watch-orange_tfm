package com.orange.taimun_watch.es;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.model.Recurso;

/**
 * Created by Yux on 05/06/2016.
 * Modified by Yux on 02/07/2016.
 */
public class SubmenuSecondTabFragment extends Fragment {

    private ArrayList<Recurso> iconsList = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.submenu_second_tab_fragment, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.lst_icon_thumbnails);
        recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 5));
        //Para mejorar el rendimiento
        recyclerView.setHasFixedSize(true);

        loadIconList();
        IconsAdapter adapter = new IconsAdapter(view.getContext(), iconsList);
        recyclerView.setAdapter(adapter);

        //Añadimos listener
        recyclerView.addOnItemTouchListener(new IconsAdapter.RecyclerTouchListener(getContext(), recyclerView, new IconsAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Fragment firstTab = ((NewStrategyActivity)getActivity()).getFirstTabFragment();
                ((StrategyFirstTabFragment)firstTab).replaceView(iconsList.get(position));
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        return view;
    }

    /*
        Método para cargar los pictogramas de la aplicación
     */
    public void loadIconList(){
        //Obtenemos recursos de imagen desde la BBDD
        DatabaseAdapter db = new DatabaseAdapter(getContext());
        db = db.open();
        iconsList = db.getImagenesPredeterminadas();
        db.close();
    }

}

