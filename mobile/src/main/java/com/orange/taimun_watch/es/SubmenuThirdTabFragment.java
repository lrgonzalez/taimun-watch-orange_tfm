package com.orange.taimun_watch.es;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;

import java.io.File;
import java.util.ArrayList;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.model.Recurso;

/**
 * Created by Yux on 03/08/2016.
 */
public class SubmenuThirdTabFragment extends Fragment implements View.OnClickListener {

    private ArrayList<Recurso> recursosList = null;
    private ImageButton btnNewRecurso;
    private static final int TAKE_PHOTO = 1;
    private static final int SELECT_PHOTO = 2;
    private static final int CROP_PHOTO = 3;
    private static final int NUM_PRED = 40;
    private Uri imgUri = null;
    private PhotoUtils photoUtils;
    private IconsAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.submenu_third_tab_fragment, container, false);

        photoUtils = new PhotoUtils(getContext());

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.lst_recursos_thumbnails);
        recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 5));
        //Para mejorar el rendimiento
        recyclerView.setHasFixedSize(true);

        //Obtenemos boton de añadir estrategia y le asignamos su listener
        FloatingActionButton btnAddStrategy = (FloatingActionButton) view.findViewById(R.id.btn_add_strategy);
        btnAddStrategy.setOnClickListener(this);

        loadRecursosPropiosList();
        adapter = new IconsAdapter(view.getContext(), recursosList);
        recyclerView.setAdapter(adapter);

        //Añadimos listener
        recyclerView.addOnItemTouchListener(new IconsAdapter.RecyclerTouchListener(getContext(), recyclerView, new IconsAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Fragment firstTab = ((NewStrategyActivity)getActivity()).getFirstTabFragment();
                ((StrategyFirstTabFragment)firstTab).replaceView(recursosList.get(position));
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        return view;
    }

    /*
     *   Método para cargar los recursos propios guardados en la aplicación
     */
    public void loadRecursosPropiosList(){
        //Obtenemos recursos de imagen desde la BBDD
        DatabaseAdapter db = new DatabaseAdapter(getContext());
        db= db.open();
        recursosList = db.getImagenesGuardadas();
        db.close();
    }

    /*
     *   Listener del boton de añadir nuevo recurso que abre un dialog con
     *   las opciones desde donde seleccionar una imagen
     */
    @Override
    public void onClick(View v) {
        final Integer[] icons = new Integer[] {R.drawable.camera_android_icon, R.drawable.gallery_icon_android};
        final String[] opciones = getResources().getStringArray(R.array.opciones_recurso_array);
        ListAdapter adapter = new ArrayAdapterWithIcon(getContext(), opciones, icons);

        AlertDialog.Builder constructor = new AlertDialog.Builder(getContext(), R.style.MyAlertDialogStyle);

        constructor.setTitle(R.string.select_image);
        constructor.setAdapter(adapter, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int item) {
                addRecurso(opciones[item]);
            }
        }).show();
    }

    /**
     * Funcion que pone una foto de imagen del usuario
     * cargada desde galeria o tomada desde la camara
     */
    public void addRecurso(String opcion) {
        Intent intent=null;
        int code=-1;

        try {
            if (opcion.equals(getResources().getString(R.string.camera))) {
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                code= TAKE_PHOTO;

                //Creamos almacenamiento temporal
                File tempDir = PhotoUtils.createTemporaryFile("user", ".jpg", getContext());
                tempDir.delete();
                imgUri = Uri.fromFile(tempDir);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
                startActivityForResult(intent, code);

            } else if (opcion.equals(getResources().getString(R.string.gallery))) {
                intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                intent.setType("image/*");
                code = SELECT_PHOTO;
                startActivityForResult(intent, code);

            }

        } catch (Exception e) {
            Log.v(getClass().getSimpleName(),getResources().getString(R.string.errorFoto));
            Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.errorFoto), Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * Método que se encarga de asignar el recurso cargado
     * desde la cámara o la galería y editarlo con crop
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        File croppedImageFile = new File(getActivity().getFilesDir(), "tmp.jpg");

        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == TAKE_PHOTO) {
                //Gestionamos la accion de crop
                Uri croppedImage = Uri.fromFile(croppedImageFile);
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                cropImage.setSourceImage(imgUri);

                startActivityForResult(cropImage.getIntent(getContext()), CROP_PHOTO);

            } else if (requestCode == SELECT_PHOTO) {
                Uri selectedImage = data.getData();

                //Gestionamos la accion de crop
                Uri croppedImage = Uri.fromFile(croppedImageFile);
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                cropImage.setSourceImage(selectedImage);

                startActivityForResult(cropImage.getIntent(getContext()), CROP_PHOTO);

            } else if (requestCode == CROP_PHOTO) {
                Recurso newRecurso = new Recurso();
                ImageView image = new ImageView(getContext());
                image.setImageBitmap(BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath()));
                newRecurso.setTipoRecurso(1);
                newRecurso.setRutaValor(getResourceFileName());

                //Guardamos el recurso en local y en BBDD
                photoUtils.guardarRecurso(image, newRecurso.getRutaValor());

                long idRecurso = saveResourceBBDD(newRecurso.getRutaValor());

                newRecurso.setIdBBDD(idRecurso);

                //Añadimos a lista de recursos
                recursosList.add(newRecurso);
                adapter.notifyItemInserted(recursosList.size() -1);
            }
        }
    }

    /**
     * Método que se encarga de asignar un nombre de fichero
     * con el que guardar un recurso
     */
    public String getResourceFileName(){
        //Obtenemos numero de recursos propios de la BBDD
        DatabaseAdapter db = new DatabaseAdapter(getContext());
        db= db.open();
        long numberResources = db.getImagenesGuardadasNumber();
        db.close();

        return PhotoUtils.resourceFile + numberResources;
    }

    /**
     * Método que se encarga de guardar un recurso en BBDD
     * @param name es la ruta del recurso
     */
    public long saveResourceBBDD(String name){
        DatabaseAdapter db = new DatabaseAdapter(getContext());
        db= db.open();
        long check = db.insertImage(name, 2); //tipo recurso propio

        db.close();

        if (check == -1) {
            Toast.makeText(getContext(), getString(R.string.errorSaveFoto), Toast.LENGTH_SHORT).show();
            Log.i("NewStrategyActivity", getString(R.string.errorSaveFoto));
            return -1;
        }

        return check;
    }
}
