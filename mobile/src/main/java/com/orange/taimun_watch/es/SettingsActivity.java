package com.orange.taimun_watch.es;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.wearable.DataMap;

/**
 * Created by Yux on 01/08/2016.
 */

@SuppressWarnings("deprecation")
public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {

    private static final String TIMESTAMP = "timestamp";
    private static final String SYNC_TYPE = "sync_type";
    private static final String LOG_SYNC_KEY = "sync_log";
    private static final String MOB_TO_WEAR = "/mob_to_wear";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        //Configuramos la toolbar
        AppBarLayout bar;
        LinearLayout root = (LinearLayout) findViewById(android.R.id.list).getParent().getParent().getParent();
        bar = (AppBarLayout) LayoutInflater.from(this).inflate(R.layout.app_bar, root, false);
        root.addView(bar, 0);
        Toolbar toolbar = (Toolbar) bar.getChildAt(0);
        toolbar.setTitle(R.string.settings);

        //Selecciona el listener que escucha la acción de pulsar el botón de la toolbar.
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        //findPreference(getString(R.string.backup_key)).setOnPreferenceClickListener(new SettingsOnclick(getString(R.string.backup)));
        //findPreference(getString(R.string.restore_key)).setOnPreferenceClickListener(new SettingsOnclick(getString(R.string.restore_backup)));
        findPreference(getString(R.string.export_key)).setOnPreferenceClickListener(new SettingsOnclick(getString(R.string.export_logs)));

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i("SETTINGS MOBILE", "preferences changed: " + key);

    }

    private void exportUserLogs() {
        Toast.makeText(this, getString(R.string.export_process), Toast.LENGTH_LONG).show();

        GoogleConnection gCon = new GoogleConnection(getApplicationContext());
        if (gCon.connect() == null) {
            Log.e("SYN", "Error al establecer la conexión con la API de Google.");
        }

        Preferences.setNumFiles(getApplicationContext(), 0);
        Preferences.setNumFilesActual(getApplicationContext(), 0);
        Preferences.setFinishSync(getApplicationContext(), false);

        DataMap dataMap = new DataMap();
        dataMap.putLong(TIMESTAMP, System.currentTimeMillis());
        dataMap.putString(SYNC_TYPE, LOG_SYNC_KEY);
        gCon.sendData(MOB_TO_WEAR, dataMap);
    }

    /*
    //TODO - Completar OnClickListener.
    private void createBackup() {
        AlertDialog.Builder alert = new AlertDialog.Builder(SettingsActivity.this);
        alert.setTitle(getString(R.string.backup));
        alert.setMessage(getString(R.string.backup_process));
        alert.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "Esta función no está disponible en la Beta.", Toast.LENGTH_LONG).show();
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), null);
        alert.show();
    }

    //TODO - Completar OnCLickListener.
    private void restoreBackup() {
        AlertDialog.Builder alert = new AlertDialog.Builder(SettingsActivity.this);
        alert.setTitle(getString(R.string.restore_backup));
        alert.setMessage(getString(R.string.restore_process));
        alert.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "Esta función no está disponible en la Beta.", Toast.LENGTH_LONG).show();
            }
        });

        alert.setNegativeButton(getString(R.string.cancel), null);
        alert.show();
    }
    */

    class SettingsOnclick implements OnPreferenceClickListener {
        private String prefKey;

        public SettingsOnclick(String prefKey) {
            this.prefKey = prefKey;
        }

        @Override
        public boolean onPreferenceClick(Preference preference) {
            String key = preference.getKey();

            /*
                if (key.equals(getString(R.string.backup_key))) {
                    createBackup();
                }
                if (key.equals(getString(R.string.restore_key))) {
                    restoreBackup();
                 }
            */

            if (key.equals(getString(R.string.export_key))) {
                exportUserLogs();
            }

            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }
}
