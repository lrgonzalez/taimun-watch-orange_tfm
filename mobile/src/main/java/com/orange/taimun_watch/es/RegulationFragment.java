package com.orange.taimun_watch.es;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.orange.taimun_watch.es.model.ListaRegulacion;

/**
 * Created by Yux on 26/05/2016.
 */
public class RegulationFragment extends Fragment implements View.OnClickListener {
    private TextView txtNone;
    private RecyclerView recyclerView;
    private RegulationViewAdapter regulationAdapter;
    private DatabaseAdapter db;
    private ArrayList<ListaRegulacion> lstRegulacion;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.regulation_fragment, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.regulations);

        //Inicializamos elementos del layout
        txtNone = (TextView) view.findViewById(R.id.no_regulations);
        recyclerView = (RecyclerView) view.findViewById(R.id.regulations_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        //Obtenemos boton de añadir estrategia y le asignamos su listener
        final FloatingActionButton btnAddStrategy = (FloatingActionButton) view.findViewById(R.id.btn_add_regulation);
        btnAddStrategy.setOnClickListener(this);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    // Scroll Down
                    if (btnAddStrategy.isShown()) {
                        btnAddStrategy.hide();
                    }
                } else if (dy <= 0) {
                    // Scroll Up
                    if (!btnAddStrategy.isShown()) {
                        btnAddStrategy.show();
                    }
                }
            }

        });

        return view;
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent((MainActivity) getActivity(), NewRegulationActivity.class));
    }

    public void inicializarListaRegulaciones(){
        DatabaseAdapter db = new DatabaseAdapter(getContext());
        db.open();

        long regulationsNumber = db.getRegulationsNumber();

        //Cargamos la lista de regulaciones en el recyclerview
        if (regulationsNumber > 0) {
            txtNone.setVisibility(View.INVISIBLE);
            lstRegulacion = db.getRegulationsOverview();

            /*Si hay alguna regulacion sin estrategias la borramos de la base de datos.*/
            for (ListaRegulacion lr : lstRegulacion) {
                if(lr.getNumeroEstrategias() == 0) {
                    db.deleteRegulationById(lr.getIdBBDD());
                }
            }

            lstRegulacion = db.getRegulationsOverview();

            if(lstRegulacion.size() == 0) {
                lstRegulacion = null;
                txtNone.setVisibility(View.VISIBLE);
                return;
            }

            regulationAdapter = new RegulationViewAdapter(lstRegulacion, getContext(), new RegulationViewAdapter.OnItemClickListener(){
                @Override public void onItemClick(ListaRegulacion regulacion) {
                    //Navegamos a Intent pasándole el id de la regulación.
                    Intent intent = new Intent((MainActivity) getActivity(), NewRegulationActivity.class);
                    intent.putExtra("regulationId", regulacion.getIdBBDD());
                    startActivity(intent);
                }
            });

            //Si no hay estrategias mostramos un texto
        } else {
            txtNone.setVisibility(View.VISIBLE);
        }

        db.close();
    }

    public void updateRegulationAdapter() {
        RegulationViewAdapter ra = (RegulationViewAdapter) recyclerView.getAdapter();

        if(ra != null) {
            if(lstRegulacion != null) {
                ra.updateRegulations(lstRegulacion);
                ra.notifyDataSetChanged();
            }
        } else {
            if (lstRegulacion != null) {
                regulationAdapter = new RegulationViewAdapter(lstRegulacion, getContext(), new RegulationViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(ListaRegulacion regulacion) {
                        //Navegamos a Intent pasándole el id de la regulacion
                        Intent intent = new Intent((MainActivity) getActivity(), NewRegulationActivity.class);
                        intent.putExtra("regulationId", regulacion.getIdBBDD());
                        startActivity(intent);
                    }
                });

                recyclerView.setAdapter(regulationAdapter);

                //Asignamos adaptador para permitir drag and swipe
                ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(regulationAdapter);
                ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                touchHelper.attachToRecyclerView(recyclerView);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        inicializarListaRegulaciones();
        updateRegulationAdapter();
    }
}
