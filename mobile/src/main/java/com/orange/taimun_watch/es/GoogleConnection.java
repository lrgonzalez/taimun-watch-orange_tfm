package com.orange.taimun_watch.es;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by Javier on 30/11/2016.
 */
public class GoogleConnection implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener  {

    private GoogleApiClient googleClient;

    public GoogleConnection(Context c) {
        googleClient = new GoogleApiClient.Builder(c)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public GoogleApiClient connect() {
        /*Iniciamos la conexion con el cliente de google para la sincronización. */

        if(!googleClient.isConnected()) {
            googleClient.connect();
        }

        return googleClient;
    }

    public void sendData(String path, DataMap data) {
        new SendToDataLayerThread(path, data).start();
    }

    /*Callbacks Google Client. */
    /**
     * Funcion llamada cuando el cliente de Google se conecta
     *
     * @param bundle Paquete con datos de conexion
     */
    @Override
    public void onConnected(Bundle bundle) {
        Log.d("Google Client", "Conexión establecida.");
    }

    /**
     * Funcion llamada cuando la conexion se cierra
     *
     * @param i Causa del cierre
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.i("Google Client", "Conexión suspendida.");
    }

    /**
     * Funcion llamada cuando la conexion falla
     *
     * @param connectionResult Resultado por el que la conexion ha fallado
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("Google Client", "Conexión fallida.");
    }

    /**
     * Clase privada que se ejecuta en otro hilo para enviar mensajes al reloj
     */
    class SendToDataLayerThread extends Thread {
        String path;
        DataMap dataMap;

        /**
         * Constructor de la clase
         *
         * @param p Path
         * @param data Paquete de datos
         */
        SendToDataLayerThread(String p, DataMap data) {
            path = p;
            dataMap = data;

            if(!googleClient.isConnected())
                googleClient.connect();
        }

        /**
         * Funcion que envia el paquete de datos
         */
        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();

            for (Node node : nodes.getNodes()) {
                    // Construct a DataRequest and send over the data layer
                    //path + "id del nodo actual."
                    PutDataMapRequest putDMR = PutDataMapRequest.create(path);
                    putDMR.getDataMap().putAll(dataMap);
                    putDMR.setUrgent();
                    PutDataRequest request = putDMR.asPutDataRequest();
                    DataApi.DataItemResult result = Wearable.DataApi.putDataItem(googleClient, request).await();
                    if (result.getStatus().isSuccess()) {
                        Log.i("MOV_TO_WEAR", "DataMap: " + dataMap + " sent to: " + node.getDisplayName());
                    } else {
                        Log.i("MOV_TO_WEAR", "ERROR: failed to send DataMap");
                    }
            }
        }
    }
}
