package com.orange.taimun_watch.es;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.orange.taimun_watch.es.model.Estadistica;
import com.orange.taimun_watch.es.model.EstadisticaDetalle;

/**
 * Created by lucia on 16/02/2017.
 */

public class StatisticsItemAdapter extends RecyclerView.Adapter<StatisticsItemAdapter.ItemViewHolder> {

    private ArrayList<EstadisticaDetalle> items;  //los 3 textos de la tarjeta
    private final StatisticsItemAdapter.OnItemClickListener listener;
    private Context context;

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        /*Elementos dentro de la vista de la tarjeta.*/
        public TextView nombreRegulacion;
        public TextView inicioRegulacion;
        public TextView finRegulacion;

        public ItemViewHolder(View itemView) {
            super(itemView);
            nombreRegulacion = (TextView) itemView.findViewById(R.id.regulation_name);
            inicioRegulacion = (TextView) itemView.findViewById(R.id.txt_inicio_reg);
            finRegulacion = (TextView) itemView.findViewById(R.id.txt_fin_reg);
        }

        /*Funcion de asignacion del listener a la vista.*/
        public void bind(final EstadisticaDetalle item, final StatisticsItemAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public StatisticsItemAdapter(ArrayList<EstadisticaDetalle> items, Context context, final StatisticsItemAdapter.OnItemClickListener listener) {
        this.items = items;
        //this.context = context;
        this.listener = listener;
    }

    /*Asignamos el formato de tarjeta indicado*/
    @Override
    public StatisticsItemAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.statistic_detail_cardview, parent, false);

        StatisticsItemAdapter.ItemViewHolder itemViewHolder = new StatisticsItemAdapter.ItemViewHolder(view);
        return itemViewHolder;
    }

    /*Funcion que devuelve los datos de una vista en una posicion especificada y los carga en el ViewHolder*/
    @Override
    public void onBindViewHolder(final StatisticsItemAdapter.ItemViewHolder holder, int position) {
        //Obtenemos la lista de estadisticas que se encuentra en la posicion especificada.
        EstadisticaDetalle item = items.get(position);

        //Asignamos nombre.
        holder.nombreRegulacion.setText(item.getNombreRegulacion());
        holder.inicioRegulacion.setText(item.getDetalleInicio());
        holder.finRegulacion.setText(item.getDetalleFin());

        holder.bind(item, listener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(EstadisticaDetalle item);
    }

    public void updateStatistics(ArrayList<EstadisticaDetalle> itemsArray) {
        this.items = itemsArray;
    }
}

