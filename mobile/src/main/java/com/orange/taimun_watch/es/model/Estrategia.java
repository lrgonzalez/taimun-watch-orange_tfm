package com.orange.taimun_watch.es.model;

/**
 * Created by Yux on 22/08/2016.
 */
public class Estrategia {
    private int Id;
    private String Name;
    private String Icon;
    private int Time;
    private boolean isGlobalTime;
    private boolean StressCondition;
    private boolean Question;
    private int TimerType;
    private boolean isCarrusel;
    private boolean Reinforcement;
    private String[] Steps;

    public Estrategia() { }

    public Estrategia(int id, String name, String icon, int time, boolean isGlobalTime,
                      boolean stressCondition, boolean question, int timerType, boolean isCarrusel,
                      boolean reinforcement, String[] steps) {
        Id = id;
        Name = name;
        Icon = icon;
        Time = time;
        this.isGlobalTime = isGlobalTime;
        StressCondition = stressCondition;
        Question = question;
        TimerType = timerType;
        this.isCarrusel = isCarrusel;
        Reinforcement = reinforcement;
        Steps = steps;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String icon) {
        Icon = icon;
    }

    public int getTime() {
        return Time;
    }

    public void setTime(int time) {
        Time = time;
    }

    public boolean isGlobalTime() {
        return isGlobalTime;
    }

    public void setGlobalTime(boolean globalTime) {
        isGlobalTime = globalTime;
    }

    public boolean isStressCondition() {
        return StressCondition;
    }

    public void setStressCondition(boolean stressCondition) {
        StressCondition = stressCondition;
    }

    public boolean getQuestion() {
        return Question;
    }

    public void setQuestion(boolean question) {
        Question = question;
    }

    public int getTimerType() {
        return TimerType;
    }

    public void setTimerType(int timerType) {
        TimerType = timerType;
    }

    public boolean isCarrusel() {
        return isCarrusel;
    }

    public void setCarrusel(boolean carrusel) {
        isCarrusel = carrusel;
    }

    public boolean isReinforcement() {
        return Reinforcement;
    }
    public void setReinforcement(boolean reinforcement) {
        Reinforcement = reinforcement;
    }

    public String[] getSteps() {
        return Steps;
    }

    public void setSteps(String[] steps) {
        Steps = steps;
    }

}
