package com.orange.taimun_watch.es;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

/**
 * Esta clase contiene funciones para fijar u obtener valores
 * de las preferencias de la aplicación.
 * Created by Yux on 22/07/2016.
 */
public class Preferences extends PreferenceActivity {

    private final static String ID_KEY = "idUsuario";
    public final static long ID_DEFAULT = -1;
    private final static String USERNAME_KEY = "userName";
    public final static String USERNAME_DEFAULT = null;
    private final static String USERPHOTO_KEY = "userPhoto";
    public final static String USERPHOTO_DEFAULT = null;
    private final static String WATCHTYPE_KEY = "tipoReloj";
    public final static String WATCHTYPE_DEFAULT = null;
    private final static String BBDD_INI_KEY = "iniBBDD";
    public final static boolean BBDD_INI_DEFAULT = false;
    public final static String SQUARE = "square";
    public final static String ROUND = "round";

    private final static String NUM_FILES_KEY = "numFiles";
    private final static int NUM_FILES_DEFAULT = 0;

    private final static String NUM_FILES_ACTUAL_KEY = "numFilesActual";
    private final static int NUM_FILES_ACTUAL_DEFAULT = 0;

    private final static String FINISH_SYNC_KEY = "finishSync";
    private final static boolean FINISH_SYNC_DEFAULT = false;

    private final static String IS_MERGING_KEY = "merge";
    private final static boolean IS_MERGING_DEFAULT = false;

    private final static String GROUP_CODE_KEY = "groupCode";
    private final static String GROUP_CODE_DEFAULT = "";

    private final static String PIN_KEY = "pinCode";
    private final static String PIN_DEFAULT = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static long getId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getLong(ID_KEY, ID_DEFAULT);
    }

    public static void setId(Context context, long id) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(ID_KEY, id);
        editor.commit();
    }

    public static String getUsername(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(USERNAME_KEY, USERNAME_DEFAULT);
    }

    public static void setUsername(Context context, String name) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(USERNAME_KEY, name);
        editor.commit();
    }

    public static String getUserphoto(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(USERPHOTO_KEY, USERPHOTO_DEFAULT);
    }

    public static void setUserphoto(Context context, String photo) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(USERPHOTO_KEY, photo);
        editor.commit();
    }

    public static String getWatchtype(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(WATCHTYPE_KEY, WATCHTYPE_DEFAULT);
    }

    public static void setWatchtype(Context context, String tipoReloj) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(WATCHTYPE_KEY, tipoReloj);
        editor.commit();
    }

    public static boolean getBBDDini(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(BBDD_INI_KEY, BBDD_INI_DEFAULT);
    }

    public static void setBBDDini(Context context, boolean valor) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(BBDD_INI_KEY, valor);
        editor.commit();
    }

    /**
     * Actualizacion el numero total de ficheros a enviar/recibir
     *
     * @param context  Contexto
     * @param numFiles Numero de ficheros
     */
    public static void setNumFiles(Context context, int numFiles) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(NUM_FILES_KEY, numFiles);
        editor.apply();
    }

    /**
     * Obtencion del numero de total de ficheros a enviar/recibir
     *
     * @param context Contexto
     * @return Numero de ficheros
     */
    public static int getNumFiles(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(NUM_FILES_KEY, NUM_FILES_DEFAULT);
    }

    /**
     * Actualizacion de los ficheros enviados/recibidos
     *
     * @param context  Contexto
     * @param numFiles Numero de ficheros
     */
    public static void setNumFilesActual(Context context, int numFiles) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(NUM_FILES_ACTUAL_KEY, numFiles);
        editor.apply();
    }

    /**
     * Obtencion del numero de ficheros recibidos/enviados
     *
     * @param context
     * @return
     */
    public static int getNumFilesActual(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(NUM_FILES_ACTUAL_KEY, NUM_FILES_ACTUAL_DEFAULT);
    }


    /**
     * Indicacion de si se ha terminado de sincronizar ficheros
     *
     * @param context Contexto
     * @param sync    true/false
     */
    public static void setFinishSync(Context context, boolean sync) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(FINISH_SYNC_KEY, sync);
        editor.apply();
    }


    /**
     * Indicacion de si se ha terminado de sincronizar ficheros
     *
     * @param context Context
     * @return true/false
     */
    public static boolean getFinishSync(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(FINISH_SYNC_KEY, FINISH_SYNC_DEFAULT);
    }

    /**
     * Indicacion de si se esta mezclando ficheros
     *
     * @param context Contexto
     * @param merge   true/false
     */
    public static void setMerge(Context context, boolean merge) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_MERGING_KEY, merge);
        editor.apply();
    }


    /**
     * Indicacion de si se ha terminado de sincronizar ficheros
     *
     * @param context Context
     * @return true/false
     */
    public static boolean isMerging(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(IS_MERGING_KEY, IS_MERGING_DEFAULT);
    }

    public static void setGroupCode(Context context, String nombre) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(GROUP_CODE_KEY, nombre);
        editor.apply();
    }

    public static String getGroupCode(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(GROUP_CODE_KEY, GROUP_CODE_DEFAULT);
    }

    public static void setPinCode(Context context, String pin) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PIN_KEY, pin);
        editor.apply();
    }

    public static String getPinCode(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(PIN_KEY, PIN_DEFAULT);
    }
}
