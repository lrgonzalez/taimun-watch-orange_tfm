package com.orange.taimun_watch.es;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.wearable.Channel;
import com.google.android.gms.wearable.ChannelApi;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import static com.orange.taimun_watch.es.utils.CompareDatesUtils.comparaFechas;


/**
 * Clase encargada de recibir y enviar mensajes y ficheros.
 * <p/>
 * Para los ficheros que se reciban mal, se reintenta una vez mas pidiendo que se reenvie
 * el fichero. En caso de volver a recibirlo mal, se pedira el siguiente fichero.
 *
 * @author Javier Cuadrado Manso
 */
public class DataReceiver extends WearableListenerService
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    //Comandos para ennviar o recibir mensajes o ficheros
    private static final String MOB_TO_WEAR = "/mob_to_wear";
    private static final String WEAR_TO_MOB = "/wear_to_mob";

    private static final String START_SYNC_LOG_FILES = "/start_sync_log_files";
    private static final String SEND_LOG_FILE = "/send_log_file";
    private static final String SEND_LOG_FILE_AGAIN = "/send_log_file_again";
    private static final String LOG_FILE = "/log_file";
    private static final String NO_MORE_LOGS = "/no_more_logs";

    private static final String TIMESTAMP = "timestamp";
    private static final String READY_TO_SYNC = "ready";
    private static final String NOT_READY_TO_SYNC = "not_ready";

    private static final String READY_TO_SYNC_REGULATION = "ready_reg";

    private static final String NO_MORE_FILES = "/no_more_files";
    private static final String FILE = "/file";
    private static final String SEND_IMAGE_FILE_AGAIN = "/send_image_file_again";
    private static final String SEND_NEXT_IMAGE_FILE = "/send_next_image_file";

    private Toast mToast;


    private GoogleApiClient googleClient;
    private File recivedFile;
    private boolean channelOK = true;

    //Buffer para enviar o recibir ficheros enteros o troceados
    private static byte[] buffer;
    private static int bufferIndex;

    private static final int MAX_PAYLOAD = 95000; //El maximo es 100KB, pero dejamo espacio para mas datos
    private static final String TIMESTAMP_KEY = "timestamp";
    private static final String FILE_NAME = "filename";
    private static final String BUFFER_INDEX_KEY = "buffer_index";
    private static final String BUFFER_KEY = "buffer";
    private static final String IS_FINAL_PART_KEY = "isFinalPart";
    private static final String FILE_LENGTH = "file_length";

    private FirebaseStorage storage = FirebaseStorage.getInstance();

    /*
     * Funcion para crear y conectar el cliente de Google
     */
    @Override
    public void onCreate() {
        Log.i("ESTADO CONEXION", "OnCreate");

        mToast = Toast.makeText(getApplicationContext(), "Sincronización finalizada.", Toast.LENGTH_SHORT);

        super.onCreate();
        if (googleClient == null) {
            googleClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }

        if (!googleClient.isConnected()) {
            googleClient.connect();
        }
    }

    /**
     * Funcion que desconecta el cliente de Google cuando se destruye la clase
     */
    @Override
    public void onDestroy() {
        Log.i("ESTADO CONEXION", "onDestroy");
        mToast.cancel();
        if (googleClient != null)
            if (!googleClient.isConnected())
                googleClient.disconnect();
        super.onDestroy();
    }

    /**
     * Funcion para recibir mensajes
     *
     * @param messageEvent mensaje recibido a traves de un evento de mensaje
     */
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        System.out.println("ON MESSAJE RECEIVED " + messageEvent.getPath());
        String path = messageEvent.getPath();
        Log.d("MOVIL", "Path:" + path);

        switch (path) {
            //Mensaje inicial desde el reloj al movil
            case WEAR_TO_MOB:
                final String message = new String(messageEvent.getData());

                switch (message) {
                    //Caso en el que el reloj no esta listo para empezar la sincronizacion
                    case NOT_READY_TO_SYNC:
                        Log.i("MOVIL", NOT_READY_TO_SYNC);
                        Handler mHandler = new Handler(getMainLooper());
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "El reloj no esta listo", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Log.e("MOVIL", "wear NOT ready to sync");
                        break;
                    //Caso en el que el reloj esta preparado para recibir la nueva regulacion
                    case READY_TO_SYNC_REGULATION:
                        Log.i("MOVIL", "wear" + READY_TO_SYNC_REGULATION);

                        Preferences.setNumFiles(getApplicationContext(), countParts());
                        DataReceiver.bufferIndex = 0;
                        DataReceiver.buffer = null;
                        sendFile();
                        break;
                    //Caso en el que el movil recibe la cantidad de logs que le va a enviar
                    //el reloj
                    default:
                        /*try {
                            int e = Integer.parseInt(message);
                            Preferences.setNumFiles(getApplicationContext(), e);
                        } catch (Exception e) {
                            Preferences.setNumFiles(getApplicationContext(), 9999);
                        }*/

                        Log.i("MOVIL", "wear ready to sync");
                        DataMap dataMap = new DataMap();
                        dataMap.putLong(TIMESTAMP_KEY, System.currentTimeMillis());

                        DataReceiver.bufferIndex = 0;
                        DataReceiver.buffer = null;

                        new SendToDataLayerThread(START_SYNC_LOG_FILES, dataMap).start();
                        break;
                }
                break;
            //Caso en el que el reloj indica que no hay mas logs por enviar
            /*case NO_MORE_LOGS:
                Preferences.setFinishSync(getApplicationContext(), true);

                DataReceiver.bufferIndex = 0;
                DataReceiver.buffer = null;

                Preferences.setMerge(getApplicationContext(), true);

                this.mergeLogs();
                mToast.show();
                break;*/
            case NO_MORE_FILES:
                firebaseDownload("sensors", getApplicationContext());
                firebaseDownload("events", getApplicationContext());
                mToast.show();
                break;
            //Caso en el que el reloj pide de nuevo el trozo de fichero que se estaba enviando
            case SEND_IMAGE_FILE_AGAIN:
                Log.i("MOVIL", SEND_IMAGE_FILE_AGAIN);
                sendFile();
                break;
            //Caso en el que el reloj pide el siguiente trozo de fichero
            //porque se ha recibido el anterior correctamente
            case SEND_NEXT_IMAGE_FILE:
                Log.i("MOVIL", SEND_NEXT_IMAGE_FILE);
                Preferences.setNumFilesActual(getApplicationContext(),
                        Preferences.getNumFilesActual(getApplicationContext()) + 1);
                //aumento del indice del buffer, para continuar con la siguiente parte del fichero
                DataReceiver.bufferIndex += DataReceiver.MAX_PAYLOAD;
                //Si el indice del buffer supera su tamaño, significa que se ha termiando de enviar el fichero
                if (DataReceiver.buffer != null) {
                    if (DataReceiver.bufferIndex >= DataReceiver.buffer.length) {
                        DataReceiver.bufferIndex = 0;
                        DataReceiver.buffer = null;
                        moveLastFileSended();
                    }
                }

                //Envio del nuevo fichero o de la siguiente parte del fichero
                sendFile();

                break;
            //Caso erroneo no previsto o en el que ha habido un cambio de datos
            //pero tambien se ha recibido un mensaje
            default:
                Log.e("MOVIL", "Mensaje no reconocido. ¿Path equivocado?");
                super.onMessageReceived(messageEvent);
                break;
        }
    }

    /**
     * Descarga una lista de archivos del servidor de Firebase indicado y los guarda en una carpeta.
     * <p>
     * Para ello, consulta los archivos del repositorio, y los de la carpeta local y descarga solo
     * aquellos que aún no posee, o los que han sido actualizados en el repo.
     *
     * @param middlePath sensor o events
     * @param ctx        contexto de la aplicación
     */
    private void firebaseDownload(String middlePath, Context ctx) {
        // Descargar la lista de ficheros
        // La carpeta tiene el nombre del grupo
        String grupo = Preferences.getGroupCode(ctx);
        StorageReference folderRef = storage.getReference().child(grupo + "/" + middlePath + "/" + "lista_ficheros.txt");

        // Guardar la lista en files/logs/(id_reloj)/(sensors|events)/
        String path = ctx.getExternalFilesDir(null).getAbsolutePath(); //Llega a la ruta de files
        File directory = new File(path + "/logs");
//        File[] contents = directory.listFiles();
//        String idReloj = contents[0].getName();
        String dataFilesPath = directory + "/" + grupo + "/" + middlePath;
        final File tempName = new File(dataFilesPath, "lista_ficheros.txt");

        // Guardar en carpeta sensor o en events. Se descarga en segundo plano
        download(folderRef, tempName);

        // Leer tempName y guardar los nombres en una lista
        ArrayList<String> ficherosFirebase = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(tempName));
            String ficheroLeido;
            while ((ficheroLeido = br.readLine()) != null) {
                Log.i("MOVIL", "Ficheros en lista: " + ficheroLeido);
                ficherosFirebase.add(ficheroLeido);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (ficherosFirebase.isEmpty()) {
            Log.e("MOVIL", "No hay ficheros en firebase");
            return;
        }

        // Descargar de Firebase cada uno de esos ficheros y guardarlos en el movil (encriptados)
        File dataFilesDirectory = new File(dataFilesPath);
        String[] ficherosCarpeta = dataFilesDirectory.list();

        Set<String> ficherosNuevos = new HashSet<>();
        // Descargar a partir del último fichero sincronizado por fecha. Ordenar.
        Collections.sort(ficherosFirebase);
        Arrays.sort(ficherosCarpeta);
        // Buscar último dia sincronizado en mi carpeta (el más nuevo)
        String ultimaFecha = "1_1_2000";

        for (String fichCarpeta : ficherosCarpeta) {
            if (!fichCarpeta.equals("lista_ficheros.txt")) {
                int diferenciaFecha = comparaFechas(ultimaFecha, get_fecha(fichCarpeta), "dd_MM_yyyy");
                if (diferenciaFecha == -1) { // 1_1_2000 < 15_10_2018
                    ultimaFecha = get_fecha(fichCarpeta);
                } else if (diferenciaFecha == -2) {
                    Log.e("MOVIL", "Error comparando fechas de ficheros (ultimaFecha)");
                }
            }
        }

        // Descargar del ultimo dia (incluido) hasta ahora
        for (String ficheroFB : ficherosFirebase) {
            String fechaFichero = get_fecha(ficheroFB);
            int diferenciaFecha = comparaFechas(ultimaFecha, fechaFichero, "dd_MM_yyyy");
            if (diferenciaFecha == 0 || diferenciaFecha == -1) { // ultimaFecha menor o igual que fechaFichero
                ficherosNuevos.add(ficheroFB);
            } else if (diferenciaFecha == -2) {
                Log.e("MOVIL", "Error comparando fechas de ficheros");
            }
        }

        // Descargar todos los que no tenga
        for (String fichero : ficherosNuevos) {
            folderRef = storage.getReference().child(grupo + "/" + middlePath + "/" + fichero);
            final File downloadFile = new File(dataFilesPath, fichero);
            download(folderRef, downloadFile);
        }

        // Al finalizar, se borran los temporales
        tempName.delete();
    }

    /**
     * Función auxiliar de firebaseDownload donde realmente se descarga de Firebase.
     *
     * @param referenciaFireBase de dónde se descarga
     * @param ficheroDescargar   fichero donde se guarda lo descargado
     */
    public static void download(StorageReference referenciaFireBase, final File ficheroDescargar) {
        StorageTask status = referenciaFireBase.getFile(ficheroDescargar).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // Local temp file has been created
                Log.i("MOVIL", "Fichero descargado. taskSnapshot: " + taskSnapshot);
                ficheroDescargar.setWritable(true);
                ficheroDescargar.setReadable(true);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                Log.e("MOVIL", "Error al descargar fichero lista: " + exception);
            }
        });
        Log.i("MOVIL", "Fichero en descarga esperando...");
        while (status.isInProgress()); /*{
            Log.i("MOVIL", "Fichero lista en descarga esperando...");
        }*/
        // TODO - se podrían guardar los nombres de los que han fallado para tenerlos en cuenta
    }

    /**
     * Obtiene la fecha a partir de una cadena del tipo "pulsometro-22_2_2022"
     *
     * @param nombreFichero cadena completa de la que obtener la fecha
     * @return la fecha en formato dd_MM_yyyy
     */
    public static String get_fecha(String nombreFichero) {
        String[] partesNombre = nombreFichero.split("-");
        String[] partesDia = partesNombre[1].split("\\.");
        return partesDia[0];
    }


    /**
     * Funcion que recoge los bloques de datos recibidos
     *
     * @param dataEvents Bloque de datos recibido a traves de un evento de datos
     */
    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        super.onDataChanged(dataEvents);

        if (googleClient == null) {
            googleClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }

        if (!googleClient.isConnected()) {
            googleClient.connect();
        }

        for (DataEvent event : dataEvents) {
            Log.i("MOVIL", "Path recibido: " + event.getDataItem().getUri().getPath());
            Log.i("MOVIL", "Mensaje recibido: " + event.getDataItem().getData());

            // Comprobacion del tipo de datos
            if (event.getType() == DataEvent.TYPE_CHANGED) {

                // Comprobacion del path
                String path = event.getDataItem().getUri().getPath();

                switch (path) {
                    //Caso en el que se ha recibido un fichero o un fragmento de fichero de log
                    case LOG_FILE: {
                        Log.i("MOVIL", LOG_FILE);
                        DataItem item = event.getDataItem();
                        DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();

                        try {
                            getReceivedFile(dataMap);
                            DataMap dtm = new DataMap();
                            dtm.putLong(TIMESTAMP, System.currentTimeMillis());
                            new SendToDataLayerThread(SEND_LOG_FILE, dtm).start();
                        } catch (Exception e) {
                            Log.e("MOVIL", "Error, excepcion al recibir fichero: " + e.getMessage());
                            DataMap dtm = new DataMap();
                            dtm.putLong(TIMESTAMP, System.currentTimeMillis());
                            new SendToDataLayerThread(SEND_LOG_FILE_AGAIN, dtm).start();
                        }

                        break;
                    }
                    //Casos para ignorar los eventos de mensajes
                    case FILE:
                        break;
                    case SEND_IMAGE_FILE_AGAIN:
                        break;
                    /*case NO_MORE_FILES:
                        mToast.show();
                        break;*/
                    case SEND_LOG_FILE:
                        break;
                    case SEND_LOG_FILE_AGAIN:
                        break;
                    default:
                        Log.e("MOVIL", "OnDataChanged - Path no reconocido. ¿De quien es este comando?: " + path);
                        break;
                }
            }
        }
    }

    /**
     * Funcion llamada cuando el cliente de Google se conecta
     *
     * @param bundle Paquete con datos de conexion
     */
    @Override
    public void onConnected(Bundle bundle) {
        Log.i("ESTADO CONEXION", "onConnected");
    }

    /**
     * Funcion llamada cuando la conexion se cierra
     *
     * @param i Causa del cierre
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.i("ESTADO CONEXION", "onConnectionSuspended");
    }

    /**
     * Funcion llamada cuando la conexion falla
     *
     * @param connectionResult Resultado por el que la conexion ha fallado
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("ESTADO CONEXION", "onConnectionFailed " + connectionResult.getErrorMessage());
    }

    /**
     * Funcion que envia un fichero o fragmento de fichero si supera el umbral establecido
     */
    private void sendFile() {

        //Obtencion del primer fichero no enviado.
        File file = getFirstFile();

        if (file != null) {
            Log.d("MOVIL", "getFirstFile(): " + file.getName());
        }

        //Si no hay fichero, se envia el mensaje de que no hay mas ficheros.
        if (file == null) {
            Preferences.setFinishSync(getApplicationContext(), true);

            DataMap dataMap = new DataMap();
            dataMap.putLong(TIMESTAMP_KEY, System.currentTimeMillis());

            new SendToDataLayerThread(NO_MORE_FILES, dataMap).start();

            //Si hay fichero, se envia entero o su primer fragmento si el tamano
            //de cola ha sido superado
        } else {
            String fileName = file.getName();

            //Si el tamano del fichero es mayor que el de la carga establecida
            if (file.length() >= MAX_PAYLOAD) {
                DataMap dataMap = new DataMap();

                //Si el indice del buffer esta a 0, sigifica que se ha de preparar un nuevo buffer
                //porque se va a comenzar con un nuevo fichero
                if (DataReceiver.bufferIndex == 0) {

                    DataReceiver.buffer = new byte[(int) file.length()];
                    try {
                        FileInputStream fileInputStream = new FileInputStream(file);
                        fileInputStream.read(DataReceiver.buffer);
                        fileInputStream.close();
                    } catch (FileNotFoundException e) {
                        Log.e("MOVIL", "File Not Found.");
                        return;
                    } catch (IOException e1) {
                        Log.e("MOVIL", "Error Reading The File.");
                        return;
                    }

                }

                //A partir del indice del buffer, se cogen tantos bytes como pueda almacenar la cola
                //o tantos bytes restantes que le queden al fichero
                byte[] buffer_tmp;
                //Caso en el que se envia el ultimo fragmento del fichero de tamano que reste
                if (bufferIndex + MAX_PAYLOAD > buffer.length) {
                    buffer_tmp = Arrays.copyOfRange(buffer,
                            bufferIndex, buffer.length);
                    dataMap.putBoolean(IS_FINAL_PART_KEY, true);
                    //Borramos el fichero
                    //file.delete();

                    //Caso en el que se envia un fragmento de fichero igual al tamano de cola establecido
                } else {
                    buffer_tmp = Arrays.copyOfRange(buffer,
                            bufferIndex, bufferIndex + MAX_PAYLOAD);
                    dataMap.putBoolean(IS_FINAL_PART_KEY, false);
                }

                //Completar el paquete de datos con datos del buffer y el fichero
                dataMap.putLong(TIMESTAMP_KEY, System.currentTimeMillis());
                dataMap.putString(FILE_NAME, fileName);
                dataMap.putInt(BUFFER_INDEX_KEY, bufferIndex);
                dataMap.putByteArray(BUFFER_KEY, buffer_tmp);
                dataMap.putLong(FILE_LENGTH, file.length());

                Log.i("MOVIL", "DataMap enviado: FILE. Fichero: " + fileName + ". "
                        + bufferIndex + " de " + buffer.length);
                new SendToDataLayerThread(FILE, dataMap).start();

                //Caso en el que se envia el fichero entero porque es mas pequeno que la cola
            } else {
                DataReceiver.buffer = new byte[(int) file.length()];
                try {
                    FileInputStream fileInputStream = new FileInputStream(file);
                    fileInputStream.read(DataReceiver.buffer);
                    fileInputStream.close();
                } catch (FileNotFoundException e) {
                    Log.e("MOVIL", "File Not Found.");

                } catch (IOException e1) {
                    Log.e("MOVIL", "Error Reading The File.");
                }

                DataMap dataMap = new DataMap();
                dataMap.putLong(TIMESTAMP_KEY, System.currentTimeMillis());
                dataMap.putString(FILE_NAME, fileName);
                dataMap.putInt(BUFFER_INDEX_KEY, bufferIndex);
                dataMap.putByteArray(BUFFER_KEY, buffer);
                dataMap.putBoolean(IS_FINAL_PART_KEY, true);
                dataMap.putLong(FILE_LENGTH, file.length());

                Log.i("MOVIL", "Mensaje enviado: FILE. Fichero: " + fileName + ". "
                        + bufferIndex + " de " + buffer.length);
                new SendToDataLayerThread(FILE, dataMap).start();

            }
        }
    }

    /**
     * Funcion que devuelve el primer fichero para enviar
     *
     * @return Primer fichero para enviar o null si no hay fichero
     */
    private File getFirstFile() {
        Log.i("FILES_MOV", "-Carpeta" + getApplicationContext().getExternalFilesDir(null).getAbsolutePath() + "/regulation-");
        //Obtencion de los ficheros de la carpeta regulation
        File folder = new File(getApplicationContext().getExternalFilesDir(null), "regulation");
        //File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File[] files = folder.listFiles();

        //Si tiene ficheros, se devuelve el primero
        if (files.length > 0) {
            for (File f : files) {
                Log.i("getFirstFile(): ", f.getName());
            }

            /*Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });*/
            return files[0];
        } else
            Log.i("FILES", "La carpeta folder esta vacía.");
        return null;
    }

    /**
     * Funcion que elimina el primer fichero proque se ha enviado
     */
    private void moveLastFileSended() {
        File fileSended = getFirstFile();
        if (fileSended == null)
            return;
        Log.d("FILE", "Eliminamos el fichero enviado.");
        fileSended.delete();
    }

    /**
     * Funcion que cuenta las partes de fichero a enviarse
     *
     * @return Partes de fichero a enviar
     */
    private int countParts() {
        int count = 0;
        File folder = new File(getApplicationContext().getExternalFilesDir(null), "regulation");
        //File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File[] files = folder.listFiles();

        for (File f : files) {
            Log.d("MOVIL", "Files counted:" + f.getName());
            count += f.length() / DataReceiver.MAX_PAYLOAD;
            if (f.length() % MAX_PAYLOAD != 0)
                count++;
        }
        return count;
    }

    /**
     * Funcion que crea las carpetas contenedoras de los ficheros a enviar o recibir
     *
     * @param ctx Context
     */
    public static void checkDirectories(Context ctx) {
        File folder = ctx.getExternalFilesDir(null);
        boolean check = new File(folder, "received").exists();
        if (!check)
            new File(folder.getAbsolutePath() + "/received").mkdir();
        check = new File(folder, "regulation").exists();
        if (!check)
            new File(folder.getAbsolutePath() + "/regulation").mkdir();
    }

    /**
     * Funcion que crea el fichero con el contenido obtenido del mensaje
     *
     * @param filename Nombre del fichero
     * @param content  Contenido del mensaje
     * @Deprecated Obsoleto porque ya no se usan canales
     */
    @Deprecated
    private void saveMessageOnFile(String filename, String content) {
        try {
            FileOutputStream fos = null;
            if (filename.contains("events"))
                fos = new FileOutputStream(new File(this.getApplicationContext().getExternalFilesDir(null) + "/events", filename), false);
            else
                fos = new FileOutputStream(new File(this.getApplicationContext().getExternalFilesDir(null) + "/sensors", filename), false);
            OutputStreamWriter osw = new OutputStreamWriter(fos);

            osw.write(content);

            osw.flush();
            osw.close();
            fos.flush();
            fos.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Funcion que recoje un fragmento de fichero en el buffer y lo convierte a fichero
     * si era el ultimo.
     *
     * @param dataMap Paquete de datos
     */
    private void getReceivedFile(DataMap dataMap) {
        String fileName = dataMap.getString(FILE_NAME);
        byte[] partial_buffer = dataMap.getByteArray(BUFFER_KEY);
        int buf_index = dataMap.getInt(BUFFER_INDEX_KEY);
        boolean isFinal = dataMap.getBoolean(IS_FINAL_PART_KEY);
        long fileLength = dataMap.getLong(FILE_LENGTH);

        //Si el buffer no existe, se crea uno nuevo con el tamano del fichero a recibir
        if (buffer == null)
            buffer = new byte[(int) fileLength];

        //Concatenar el buffer recibido al anterior
        System.arraycopy(partial_buffer, 0, buffer, buf_index, partial_buffer.length);

        //Si el fragmento recibido es el ultimo, se convierte el buffer a un fichero tal cual se ha recibido
        if (isFinal) {
            try {
                FileOutputStream fos = null;
                /*if (fileName.contains("events"))
                    fos = new FileOutputStream(new File(this.getApplicationContext().getExternalFilesDir(null) + "/events", fileName), false);
                else
                    fos = new FileOutputStream(new File(this.getApplicationContext().getExternalFilesDir(null) + "/sensors", fileName), false);*/

                File f = new File(this.getApplicationContext().getExternalFilesDir(null) + "/received");
                if (!f.exists()) {
                    f.mkdir();
                }

                fos = new FileOutputStream(new File(this.getApplicationContext().getExternalFilesDir(null)
                        + "/received", fileName), false);

                /*FileOutputStream fos = new FileOutputStream(
                        getApplicationContext().getFilesDir() + "/" + fileName);*/

                fos.write(buffer);
                fos.close();

                Preferences.setNumFilesActual(getApplicationContext(),
                        Preferences.getNumFilesActual(getApplicationContext()) + 1);
            } catch (FileNotFoundException ex) {
                Log.e("MOVIL", "Error en getReceiveFile: FileNotFoundException : " + ex);
            } catch (IOException ioe) {
                Log.e("MOVIL", "Error en getReceiveFile: IOException : " + ioe);
            }

            //Liberar el buffer tras escribirse el fichero
            buffer = null;
            return;
        }

    }

    /**
     * Funcion que mezcla los logs por dia y sensor o evento
     */
    private void mergeLogs() {
        File[] receivedFiles = new File(getExternalFilesDir(null).getAbsolutePath(),
                "/received").listFiles();
        Arrays.sort(receivedFiles, new Comparator<File>() {
            public int compare(File f1, File f2) {
                return f1.getName().compareTo(f2.getName());
            }
        });
        //Por cada fichero de la carpeta de recibidos

        for (File rcvFile : receivedFiles) {
            File dstFolder;
            String dstFilename;

            String watchId = loadWatchId(rcvFile.getName());

            File logs = new File(getExternalFilesDir(null).getAbsolutePath() + "/logs");
            if (!logs.exists()) {
                logs.mkdir();
                logs.setExecutable(true);
                logs.setReadable(true);
                logs.setWritable(true);
            }

            File watchFolder = new File(logs.getAbsolutePath(), "/" + watchId);
            if (!watchFolder.exists()) {
                watchFolder.mkdir();
                watchFolder.setExecutable(true);
                watchFolder.setReadable(true);
                watchFolder.setWritable(true);
                Log.i("MOVIL", "Creando carpeta sensors...");
                File f = new File(watchFolder.getAbsolutePath() + "/sensors");
                boolean state = f.mkdir();
                if (!state) {
                    Log.e("MOVIL", "Error creando carpeta de sensores");
                    continue;
                }
                f.setExecutable(true);
                f.setReadable(true);
                f.setWritable(true);
                Log.i("MOVIL", "Creando carpeta events...");
                f = new File(watchFolder.getAbsolutePath() + "/events");
                state = f.mkdir();
                if (!state) {
                    Log.e("MOVIL", "Error creando carpeta de eventos");
                    continue;
                }
                f.setExecutable(true);
                f.setReadable(true);
                f.setWritable(true);
            }

            //Se prepara el nombre del fichero segun el dia y se selecciona la carpeta
            if (rcvFile.getName().contains("events")) {
                dstFolder = new File(watchFolder, "/events");
                dstFilename = "events-" + loadDay(rcvFile.getName());
            } else {
                dstFolder = new File(watchFolder, "/sensors");
                if (rcvFile.getName().contains("acelerometro"))
                    dstFilename = "acelerometro-" + loadDay(rcvFile.getName());
                else if (rcvFile.getName().contains("giroscopio"))
                    dstFilename = "giroscopio-" + loadDay(rcvFile.getName());
                else if (rcvFile.getName().contains("pulsometro"))
                    dstFilename = "pulsometro-" + loadDay(rcvFile.getName());
                else if (rcvFile.getName().contains("detectorPasos"))
                    dstFilename = "detectorPasos-" + loadDay(rcvFile.getName());
                else if (rcvFile.getName().contains("contadorPasos"))
                    dstFilename = "contadorPasos-" + loadDay(rcvFile.getName());
                else
                    continue;
            }
            try {
                FileInputStream fis = new FileInputStream(rcvFile);
                byte[] tmpBuf = new byte[(int) rcvFile.length()];
                fis.read(tmpBuf);
                fis.close();

                File dstFile = new File(dstFolder, dstFilename);
                FileOutputStream fos = null;
                dstFile.setWritable(true);

                //Si el fichero existe, se concatena su contenido al final de este
                if (dstFile.exists())
                    fos = new FileOutputStream(dstFile, true);
                    //Si no existe, se escribe una cabecera
                else {
                    fos = new FileOutputStream(dstFile, false);
                    if (dstFilename.contains("pulsometro") || dstFilename.contains("detectorPasos"))
                        fos.write(new String("timestamp\tAccuracy\tmeasure\n").getBytes());
                    else if (dstFilename.contains("acelerometro") || dstFilename.contains("giroscopio"))
                        fos.write(new String("timestamp\tAccuracy\tx\ty\tz\n").getBytes());
                    else
                        fos.write(new String("timestamp\tevent\n").getBytes());
                }

                //Escritura de todos los datos recibidos
                fos.write(tmpBuf);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.e("MOVIL", "Error mezclando los ficheros. No se pudo encontrar el fichero");
                e.printStackTrace();
                continue;
            } catch (IOException e) {
                Log.e("MOVIL", "Error mezclando los ficheros. Error I/O");
                e.printStackTrace();
                continue;
            }
        }

        //Una vez mezclados los ficheros, se eliminan los temporales
        for (File rcvFile : receivedFiles) {
            rcvFile.delete();
        }

        Preferences.setMerge(getApplicationContext(), false);
    }

    /**
     * Funcion que crea el String identificativo de la fecha actual
     *
     * @return String del dia
     */
    private static String makeDay() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH) + "_" + c.get(Calendar.MONTH) + "_"
                + c.get(Calendar.YEAR);
    }

    /**
     * Funcion que obtiene la fecha del nombre del fichero para saber si existe o no
     *
     * @param filename Nombre del fichero
     * @return Fecha del fichero
     */
    private static String loadDay(String filename) {
        String[] parts = filename.split("-");
        if (parts.length < 4)
            return makeDay();
        return parts[3];
    }

    /**
     * Funcion que devuelve el identificador del reloj del nombre del fichero
     *
     * @param filename Nombre del fichero
     * @return Id del reloj
     */
    private static String loadWatchId(String filename) {
        String[] parts = filename.split("-");
        if (parts.length < 4)
            return "NO_WATCH_ID";
        return parts[0];
    }


    /**
     * Clase privada que se ejecuta en otro hilo para enviar paquetes de datos al reloj
     */
    class SendToDataLayerThread extends Thread {
        String path;
        DataMap dataMap;

        /**
         * Constructor de la clase
         *
         * @param p    Path
         * @param data Paquete de datos
         */
        SendToDataLayerThread(String p, DataMap data) {
            path = p;
            dataMap = data;
        }

        /**
         * Funcion que envia el paquete de datos inmediatamente
         */
        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
            for (Node node : nodes.getNodes()) {
                // Construct a DataRequest and send over the data layer
                //path + "id del nodo actual."
                PutDataMapRequest putDMR = PutDataMapRequest.create(path);
                putDMR.getDataMap().putAll(dataMap);
                PutDataRequest request = putDMR.asPutDataRequest();
                request.setUrgent();
                DataApi.DataItemResult result = Wearable.DataApi.putDataItem(googleClient, request).await();
                if (result.getStatus().isSuccess()) {
                    Log.v("MOVIL", "DataMap sent to: " + node.getDisplayName());
                } else {
                    // Log an error
                    Log.v("MOVIL", "ERROR: failed to send DataMap to " + node.getDisplayName());
                }
            }
        }
    }

    /**************************************************************************************************/
    /**************************************************************************************************/
    /************************************FUNCIONES OBSOLETAS*******************************************/
    /**************************************************************************************************/
    /**************************************************************************************************/

    /**
     * Clase privada que envia en un nuevo hilo un fichero de medidas a traves de un canal
     */
    class SendFile extends Thread {
        String path;
        File file;

        /**
         * Constructor que guarda el path y el fichero
         *
         * @param p Path
         * @param f Fichero
         */
        SendFile(String p, File f) {
            path = p;
            file = f;
        }

        /**
         * Funcion que envia el fichero
         */
        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi
                    .getConnectedNodes(googleClient).await();
            Node node = nodes.getNodes().get(0);

            ChannelApi.OpenChannelResult result = Wearable.ChannelApi.openChannel(googleClient,
                    node.getId(), this.path).await();
            if (result == null) {
                Log.e("MOVIL", "Error: result null");
                return;
            }
            if (!result.getStatus().isSuccess()) {
                Log.e("MOVIL", "Error: result not success");
            }

            Channel channel = result.getChannel();
            if (channel == null) {
                Log.e("MOVIL", "Error: channel es null | " + result.getStatus().getStatusMessage());

            }

            Status st = channel.sendFile(googleClient, Uri.fromFile(file)).await();
            if (st.getStatus().isSuccess()) {
                Log.v("MOVIL", "Archivo " + file.getName() + " enviado con exito a " + node.getDisplayName());
                channel.close(googleClient);
            } else {
                Log.e("MOVIL", "Archivo " + file.getName() + " no enviado a " + node.getDisplayName());
                channel.close(googleClient);
            }

        }
    }

    /**
     * Funcion que prepara el archivo temporal que contendra el log
     * que esta enviando el reloj.
     *
     * @param channel Canal abierto por el que se recibira un fichero
     * @deprecated Obsoleto porque ya no se usan canales, en su lugar se usan paquetes de datos
     */
    @Deprecated
    @Override
    public void onChannelOpened(Channel channel) {
        Log.i("ESTADO CONEXION MOVIL", "OnChannelOpened");

        if (channel.getPath().equals(LOG_FILE)) {
            Log.i("MOVIL", "Fichero recibido por channel");

            //Creacion del fichero temporal
            this.recivedFile = new File(this.getApplicationContext().getExternalFilesDir(null).getPath() + "tmp.txt");

            try {
                this.recivedFile.createNewFile();
            } catch (IOException e) {
                return;
            }
            //Inicio de la recepcion del fichero
            channel.receiveFile(googleClient, Uri.fromFile(this.recivedFile), false).await();
        } else {
            Log.e("MOVIL", "Fichero recibido por canal no reconocido");
            Log.e("MOVIL", "Path: " + channel.getPath());
        }
        super.onChannelOpened(channel);
    }

    /**
     * Funcion llamada al finalizar la recepcion del fichero.
     * Se encarga de procesar el fichero recivido y, de estar todo en orden,
     * reponder indicando que fichero se ha recivido.
     *
     * @param channel Canal por el que se ha recibido un fichero
     * @param i       Causa por la que se ha cerrado el canal
     * @param i1      Error especifico
     * @deprecated Obsoleta porque no admite ficheros de mas de 100KB. Se usan ficheros troceados
     * recibidos en paquetes de datos
     */
    /*@Deprecated
    @Override
    public void onInputClosed(Channel channel, int i, int i1) {
        Log.i("MOVIL", "onInputClosed: " + i);
        //Si existe algun error en la recepcion
        if (i != ChannelApi.ChannelListener.CLOSE_REASON_NORMAL) {
            if (!this.channelOK) {
                //Dos errores seguidos, enviamos un mensaje para intentar recivir mas ficheros

                this.channelOK = true;
                this.recivedFile.delete();
                super.onInputClosed(channel, i, i1);
                DataMap dataMap = new DataMap();
                dataMap.putLong(TIMESTAMP_KEY, new Date().getTime());
                super.onInputClosed(channel, i, i1);
                new SendToDataLayerThread(SEND_LOG_FILE, dataMap).start();
                Preferences.setNumFilesActual(getApplicationContext(),
                        Preferences.getNumFilesActual(getApplicationContext()) + 1);
                return;
            } else {

                this.channelOK = false;
                this.recivedFile.delete();
                super.onInputClosed(channel, i, i1);

                //Primer error, intentamos que se nos reenvie el fichero
                DataMap dataMap = new DataMap();
                dataMap.putLong(TIMESTAMP_KEY, new Date().getTime());
                super.onInputClosed(channel, i, i1);
                new SendToDataLayerThread(SEND_LOG_FILE_AGAIN, dataMap).start();
                return;
            }
        }
        //En caso de que la recepcion del fichero sea correcta
        this.channelOK = true;

        Log.i("MOVIL", "Fichero recibido");

        //Prepacion del fichero para su procesado
        if (this.recivedFile != null) {
            String message = new String();
            StringBuilder fileContents = new StringBuilder((int) this.recivedFile.length());
            Scanner scanner = null;
            String filename = new String();
            boolean fileOk = true;
            try {
                scanner = new Scanner(this.recivedFile);
                if (scanner == null)
                    throw new FileNotFoundException("Error: El escaner no ha detectado un fichero");

                if (scanner.hasNextLine())
                    filename = scanner.nextLine();
                else
                    throw new Exception("Fichero vacio");

                while (scanner.hasNextLine()) {
                    fileContents.append(scanner.nextLine() + "\n");
                }
                message = fileContents.toString();
            } catch (FileNotFoundException e) {
                Log.e("MOVIL", "Error con el fichero recivido: " + e.getMessage());
                fileOk = false;
            } catch (Exception e) {
                Log.e("MOVIL", "Error: El fichero esta vacio");
                fileOk = false;
            } finally {
                scanner.close();
            }

            if (fileOk) {
                this.saveMessageOnFile(filename, message);
            }
            this.recivedFile.delete();
        } else {
            Log.e("MOVIL", "Fichero nulo");
        }
        Preferences.setNumFilesActual(getApplicationContext(),
                Preferences.getNumFilesActual(getApplicationContext()) + 1);

        //Enviar nuevo mensaje
        DataMap dataMap = new DataMap();
        dataMap.putLong(TIMESTAMP, System.currentTimeMillis());
        //dataMap.putString("filename", channel.getPath().split("/")[2]);
        super.onInputClosed(channel, i, i1);
        new SendToDataLayerThread(SEND_LOG_FILE, dataMap).start();
    }*/

    /**
     * Funcion llamada cuando se cierra el canal de fichero saliente
     *
     * @param channel              Canal
     * @param closeReason          Razon de cierre
     * @param appSpecificErrorCode Error especifico
     * @deprecated
     */
    @Deprecated
    @Override
    public void onOutputClosed(Channel channel, int closeReason, int appSpecificErrorCode) {
        Log.i("MOVIL", "path: " + channel.getPath()
                + "cloaseReason " + closeReason + "appErrorCode:  " + appSpecificErrorCode);
        super.onOutputClosed(channel, closeReason, appSpecificErrorCode);
    }
}
