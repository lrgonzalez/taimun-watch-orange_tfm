package com.orange.taimun_watch.es.model;

/**
 * Created by Yux on 05/08/2016.
 */
public class ListaEstrategia {
    private String nombre;
    private String rutaIcono;
    private long numTarjetas;
    private long idBBDD;

    public ListaEstrategia() { }

    public ListaEstrategia(String nombre, String rutaIcono, long numTarjetas, long idBBDD) {
        this.nombre = nombre;
        this.rutaIcono = rutaIcono;
        this.numTarjetas = numTarjetas;
        this.idBBDD = idBBDD;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRutaIcono() {
        return rutaIcono;
    }

    public void setRutaIcono(String rutaIcono) {
        this.rutaIcono = rutaIcono;
    }

    public long getNumTarjetas() {
        return numTarjetas;
    }

    public void setNumTarjetas(long numTarjetas) {
        this.numTarjetas = numTarjetas;
    }

    public long getIdBBDD() {
        return idBBDD;
    }

    public void setIdBBDD(long idBBDD) {
        this.idBBDD = idBBDD;
    }
}
