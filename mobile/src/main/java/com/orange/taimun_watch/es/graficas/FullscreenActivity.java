package com.orange.taimun_watch.es.graficas;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.utils.DecryptUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.orange.taimun_watch.es.SumStatisticActivity.NOMBRE;
import static com.orange.taimun_watch.es.SumStatisticActivity.PULSOMETRO;

/**
 * Clase intermedia que selecciona qué datos mostrar en la gráfica avanzada
 *
 * @author Lucia
 */
public class FullscreenActivity extends AppCompatActivity {
    //    private BaseExample mLogic;
    private ScalingX mLogic;
    private AlertDialog radioDialog;
    private AlertDialog multiDialog;

    String[] listaExtras;
    ArrayList<String> listaIniEvento;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Recogemos la ruta del fichero a graficar
        Intent intent = getIntent();
        listaExtras = intent.getStringArrayExtra("listaExtras");
        listaIniEvento = intent.getStringArrayListExtra("listaIniEvento");  //horas inicio eventos

        setContentView(R.layout.statistic_fullscreen_advanced);

        //Recuperamos toolbar y la asignamos como SupportActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.basic_statistics);
        try {
            mLogic = ScalingX.class.newInstance();
            mLogic.onCreate(FullscreenActivity.this, "pulsometro", listaIniEvento, null, null, listaExtras[PULSOMETRO],
                    listaExtras[NOMBRE]); //crea gráfica pulsometro por defecto*/
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**********************************
     * Opciones del menu de la toolbar
     * **********************************
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chart_filter, menu); //icono a graficas avanzadas
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Boton para cambiar de grafica
        switch (item.getItemId()) {
            case R.id.chart_filter:
                final ArrayList<Boolean> ejes = new ArrayList<>();
                final ArrayList<CharSequence[]> colsNames = new ArrayList<>();
                //buscar otros ficheros de esta fecha. Sacar ruta de pulsometro.
                String semiPath = listaExtras[PULSOMETRO];
                int i = semiPath.length() - 2;
                int finPath = semiPath.length();
                // Desde el final de la cadena se va quitando hasta quedarnos con la ruta generica de sensores
                for (char c = semiPath.charAt(i); c != '/'; c = semiPath.charAt(finPath)) {
                    finPath--;
                }
                semiPath = semiPath.substring(0, finPath);
                File[] sensorsFiles = new File(semiPath).listFiles();
                ArrayList<String> opcionesSensores = new ArrayList<>();
                final ArrayList<String> pathSensores = new ArrayList<>();

                for (File snsrFile : sensorsFiles) {
                    String aux = snsrFile.getName().replace('_', '/');
                    if (aux.contains(listaExtras[NOMBRE])) {
                        //añadir opcion del nombre sin fecha
                        String[] nombre = snsrFile.getName().split("-");
                        /*Desencriptar snsrFile*/
                        String path = semiPath + "/" + snsrFile.getName();
                        if (!snsrFile.getName().contains("des-")) { // Si ya tiene "des-" en el nombre, ya ha sido desencriptado
                            opcionesSensores.add(nombre[0]);
                            File encryptedFile = new File(path);
                            File decryptedFile = new File(semiPath, "des-" + snsrFile.getName());
                            DecryptUtils decryptUtils = new DecryptUtils(getApplicationContext());
                            if (decryptUtils.decryptAES(getApplicationContext(), encryptedFile, decryptedFile) == -1) { // fichero a desenc, fichero final
                                Log.e("MOVIL decryptAES", "ERROR al desencriptar " + encryptedFile);
                                return false; //??
                            }
                            Log.v("MOVIL decryptAES", " OK al desencriptar " + encryptedFile);
                            path = decryptedFile.getPath();
                            pathSensores.add(path);

                            // Comprobar si tiene ejes
                            FileInputStream fileInputStream;
                            try {
                                fileInputStream = new FileInputStream(new File(path));
                                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                                try {
                                    String cabeceras = bufferedReader.readLine(); //leer la primera linea

                                    String[] columnas = cabeceras.split("\t");

                                    if (columnas.length > 3) { // mas de tres columnas es que tiene ejes
                                        ejes.add(true);
                                        CharSequence[] axisItems2 = new String[columnas.length - 2]; // las dos primeras columnas no son ejes
                                        for (i = 0; i < columnas.length - 2; i++) {
                                            axisItems2[i] = columnas[i + 2];
                                        }
                                        colsNames.add(axisItems2);
                                    } else if (columnas.length > 2) { // Al menos tiene que tener 3 columnas (time, accuracy, measure)
                                        ejes.add(false);
                                        CharSequence[] axisItems2 = new String[1];
                                        axisItems2[0] = columnas[2];
                                        colsNames.add(axisItems2);  //no tiene mas que un valor
                                    }else{
                                        // El fichero no cumple los requisitos para ser mostrado como gráfica
                                        opcionesSensores.remove(nombre[0]);
                                        pathSensores.remove(path);
                                    }
                                    fileInputStream.close();
                                    bufferedReader.close();

                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                int tamCharSeq = opcionesSensores.size();
                final CharSequence[] dialogItems = new String[tamCharSeq];

                for (i = 0; i < tamCharSeq; i++) { //pasar a CharSequence []
                    dialogItems[i] = opcionesSensores.get(i);
                }

                final ArrayList seletedItems = new ArrayList();

                final DialogInterface.OnMultiChoiceClickListener dialInterfaceListener = new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog2, int indexSelected, boolean isChecked) {
                        if (isChecked) {
                            seletedItems.add(indexSelected); //añadir a lista
                        } else if (seletedItems.contains(indexSelected)) { //si ya estaba se elimina
                            seletedItems.remove(Integer.valueOf(indexSelected));
                        }
                    }
                };
                // Creating and Building the Dialog
                final AlertDialog.Builder checkBuilder = new AlertDialog.Builder(this);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getApplicationContext().getResources().getString(R.string.desired_view));
                builder.setSingleChoiceItems(dialogItems, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, final int item) {
                        switch (item) {
                            default:
                                //comprobar si ejes o no
                                if (!ejes.get(item)) { //sin ejes
                                    try {
                                        mLogic = ScalingX.class.newInstance();
                                        mLogic.onCreate(FullscreenActivity.this, String.valueOf(dialogItems[item]),
                                                listaIniEvento, null, colsNames.get(item), pathSensores.get(item), listaExtras[NOMBRE]);
                                    } catch (InstantiationException e) {
                                        e.printStackTrace();
                                    } catch (IllegalAccessException e) {
                                        e.printStackTrace();
                                    }
                                } else { //con ejes
                                    checkBuilder.setTitle(getApplicationContext().getResources().getString(R.string.axis_to_display));
                                    checkBuilder.setMultiChoiceItems(colsNames.get(item), null, dialInterfaceListener)
                                            .setPositiveButton(getApplicationContext().getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int id) {
                                                    if (!seletedItems.isEmpty()) {
                                                        try {
                                                            mLogic = ScalingX.class.newInstance();
                                                            mLogic.onCreate(FullscreenActivity.this, String.valueOf(dialogItems[item]), listaIniEvento,
                                                                    seletedItems, colsNames.get(item), pathSensores.get(item), listaExtras[NOMBRE]);
                                                        } catch (InstantiationException e) {
                                                            e.printStackTrace();
                                                        } catch (IllegalAccessException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        Toast.makeText(getApplicationContext(),
                                                                getApplicationContext().getResources().getString(R.string.at_least_one),
                                                                Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            })
                                            .setNegativeButton(getApplicationContext().getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //  Vaciar la lista
                                                    seletedItems.clear();
                                                }
                                            });

                                    multiDialog = checkBuilder.create();
                                    multiDialog.show();
                                }
                        }
                        radioDialog.dismiss();
                    }
                });
                radioDialog = builder.create();
                radioDialog.show();
                return true;
            case android.R.id.home:  //boton atras
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("FullScreenAct", "ON_DESTROY");
    }

    @Override
    protected void onStop() { //hidden
        super.onStop();
        // Eliminar los ficheros desencriptados de la carpeta
        String pathPulsom = listaExtras[PULSOMETRO];
        String[] pathParts = pathPulsom.split("/");
        String nombreFichero = pathParts[pathParts.length - 1];
        String absolutePath = pathPulsom.replace(nombreFichero, "");
        File[] sensorsFiles = new File(absolutePath).listFiles();
        for (File archivo : sensorsFiles) {
            if (archivo.getName().contains("des-")) {
                archivo.delete();
            }
        }
    }
}