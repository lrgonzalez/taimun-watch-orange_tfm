package com.orange.taimun_watch.es;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

/**
 * Created by Yux on 03/05/2016.
 */
public class HomeFragment extends Fragment implements View.OnClickListener{
    private ImageButton btnStrategy;
    private ImageButton btnRegulation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.home_fragment, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.app_name);

        //Obtenemos botones y asignamos listener
        btnStrategy = (ImageButton) view.findViewById(R.id.btn_strategy);
        btnRegulation = (ImageButton) view.findViewById(R.id.btn_regulation);
        btnStrategy.setOnClickListener(this);
        btnRegulation.setOnClickListener(this);

        //Tutorial y contacto
        FloatingActionButton btnAddTutorial = (FloatingActionButton) view.findViewById(R.id.btn_tutorial);
        btnAddTutorial.setOnClickListener(this);

        FloatingActionButton btnMail = (FloatingActionButton) view.findViewById(R.id.btn_mail);
        btnMail.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();

        if (viewId == R.id.btn_strategy) {
            ((MainActivity)getActivity()).setMenuOptionSelected(MainActivity.STRATEGY_POSITION);
            ((MainActivity)getActivity()).selectFragment(MainActivity.STRATEGY_POSITION);
            ((MainActivity)getActivity()).getNavigationView().getMenu().getItem(MainActivity.STRATEGY_POSITION).setChecked(true);
        } else if (viewId == R.id.btn_regulation) {
            ((MainActivity)getActivity()).setMenuOptionSelected(MainActivity.REGULATION_POSITION);
            ((MainActivity)getActivity()).selectFragment(MainActivity.REGULATION_POSITION);
            ((MainActivity)getActivity()).getNavigationView().getMenu().getItem(MainActivity.REGULATION_POSITION).setChecked(true);
        } else if (viewId == R.id.btn_tutorial) {
            ((MainActivity)getActivity()).setMenuOptionSelected(MainActivity.REGULATION_POSITION);
            startActivity(new Intent(this.getContext(), TutorialActivity.class));
        } else if (viewId == R.id.btn_mail) {
            String[] dir = new String[1];
            dir[0] = "taimun@uam.es";
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, dir);
            intent.putExtra(Intent.EXTRA_SUBJECT, "Taimun report");
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }
}
