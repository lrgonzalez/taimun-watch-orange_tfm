package com.orange.taimun_watch.es;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.orange.taimun_watch.es.R;
/**
 * Esta clase contiene la actividad inicial de la App.
 * Contiene la animacion de splash inicial.
 * Created by Yux on 23/07/2016.
 */
public class SplashActivity extends Activity {
    private Animation fadein, fadein2, fadein3, fadeout, fadeout2, fadeout3;
    private ImageView logo;
    private DatabaseAdapter db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.splash);

        inicializaBBDD();

        //Iniciamos animacion de splash
        fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);
        fadein2 = AnimationUtils.loadAnimation(this, R.anim.fadein);
        fadein3 = AnimationUtils.loadAnimation(this, R.anim.fadein);
        fadeout = AnimationUtils.loadAnimation(this, R.anim.alpha);
        fadeout2 = AnimationUtils.loadAnimation(this, R.anim.alpha);
        fadeout3 = AnimationUtils.loadAnimation(this, R.anim.alpha);

        logo = (ImageView) findViewById(R.id.logo_splash);
        logo.setImageResource(R.drawable.logo_fundacion_orange);

        logo.startAnimation(fadein);

        //Listener que comienza el fadeout
        fadein.setAnimationListener (new AnimationListener () {
            public void onAnimationEnd (Animation animation) {
              logo.startAnimation(fadeout);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation animation) {}
        });

        //Listener que cambia el logo del imagiew
        fadeout.setAnimationListener (new AnimationListener () {
            public void onAnimationEnd (Animation animation) {
                //Esperamos un poco antes de empezar la segunda animación
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        logo.setImageResource(R.drawable.amilab_logo);
                        logo.startAnimation(fadein2);
                    }
                }, 500);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation animation) {}
        });

        //Listener que comienza el fadeout
        fadein2.setAnimationListener (new AnimationListener () {
            public void onAnimationEnd (Animation animation) {
                logo.startAnimation(fadeout2);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation animation) {}
        });

        //Listener que cambia el logo del imagiew
        fadeout2.setAnimationListener (new AnimationListener () {
            public void onAnimationEnd (Animation animation) {
                //Esperamos un poco antes de empezar la segunda animación
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        logo.setImageResource(R.drawable.logo_ipp);
                        logo.startAnimation(fadein3);
                    }
                }, 500);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation animation) {}
        });

        //Listener que comienza el fadeout
        fadein3.setAnimationListener (new AnimationListener () {
            public void onAnimationEnd (Animation animation) {
                logo.startAnimation(fadeout3);
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation animation) {}
        });

        //Listener que inicia otra actividad cuando termina la animacion
        fadeout3.setAnimationListener (new AnimationListener () {
            public void onAnimationEnd (Animation animation) {
                Intent activity;
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String pin = sharedPreferences.getString(getString(R.string.pin), "");

                Log.i("SPLASHACTIVITY MOBILE", "PIN: " + pin);

                //Si hay PIN de seguridad navegamos a la pantalla de password
                if (!pin.equals("")) {
                    activity = new Intent(SplashActivity.this, PasswordActivity.class);
                } else {
                    activity = new Intent(SplashActivity.this, MainActivity.class);
                }

                startActivity(activity);
                SplashActivity.this.finish();
            }
            @Override
            public void onAnimationRepeat(Animation arg0) {}
            @Override
            public void onAnimationStart(Animation animation) {}
        });

    }


    private void inicializaBBDD() {
        if (Preferences.getBBDDini(this) == Preferences.BBDD_INI_DEFAULT) {
            db = new DatabaseAdapter(this);
            db.open();
            long idUsuario = db.inicializarBBDD();
            db.close();

            //Inicializamos valores de preferencias
            Preferences.setBBDDini(this, true);
            Preferences.setId(this, idUsuario);
            Preferences.setWatchtype(this, getString(R.string.round));
        }
    }

}
