package com.orange.taimun_watch.es.model;

/**
 * Created by Yux on 10/08/2016.
 */
public class ListaRegulacion {

    private long idBBDD;
    private String nombre;
    private String rutaIcono;
    private long numeroEstrategias;
    private int isSelector;
    private int isSync;

    public ListaRegulacion() { }

    public ListaRegulacion(long idBBDD, String nombre, String rutaIcono, long numeroEstrategias, int isSelector, int isSync) {
        this.idBBDD = idBBDD;
        this.nombre = nombre;
        this.rutaIcono = rutaIcono;
        this.numeroEstrategias = numeroEstrategias;
        this.isSelector = isSelector;
        this.isSync = isSync;
    }

    public long getIdBBDD() {
        return idBBDD;
    }

    public void setIdBBDD(long idBBDD) {
        this.idBBDD = idBBDD;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRutaIcono() {
        return rutaIcono;
    }

    public void setRutaIcono(String rutaIcono) {
        this.rutaIcono = rutaIcono;
    }

    public long getNumeroEstrategias() {
        return numeroEstrategias;
    }

    public void setNumeroEstrategias(long numeroEstrategias) {
        this.numeroEstrategias = numeroEstrategias;
    }

    public int getIsSelector() {
        return isSelector;
    }

    public void setIsSelector(int isSelector) {
        this.isSelector = isSelector;
    }

    public int getIsSync() {
        return isSync;
    }

    public void setIsSync(int isSync) {
        this.isSync = isSync;
    }
}
