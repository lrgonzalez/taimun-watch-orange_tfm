package com.orange.taimun_watch.es;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.orange.taimun_watch.es.model.Usuario;

/**
 * Created by Yux on 20/05/2016.
 */
public class ProfileActivity extends AppCompatActivity {
    private TextView txtUsername, txtEstrategias, txtRegulaciones, txtSexo, txtReloj;
    private ImageView profilePhoto, imgGender, imgReloj;
    private PhotoUtils photoUtils;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);

        //Recuperamos toolbar y la asignamos como SupportActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.profile);

        //Recuperamos textos de la pantalla
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        //txtSexo = (TextView) findViewById(R.id.txtSexo);
        txtReloj = (TextView) findViewById(R.id.txtReloj);
        txtEstrategias = (TextView) findViewById(R.id.txtEstrategias);
        txtRegulaciones = (TextView) findViewById(R.id.txtRegulaciones);

        //Recuperamos imageviews de la vista
        profilePhoto = (ImageView) findViewById(R.id.profilePhoto);
        //imgGender = (ImageView) findViewById(R.id.imgGender);
        imgReloj = (ImageView) findViewById(R.id.imgReloj);

        //Clase auxiliar de fotos
        photoUtils = new PhotoUtils(this);

        //Cargamos datos del usuario
        loadUserData();

        //Cargamos la información de sus estrategias y regulaciones
        setStrategiesAndRegulationNumber();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadUserData();
    }

    /*
     * Metodo para obtener y cargar los datos del usuario
     */
    private void loadUserData() {
        DatabaseAdapter db = new DatabaseAdapter(this);
        db.open();
        Usuario usuario = db.getUser(Preferences.getId(this));
        db.close();

        if(usuario != null) {
            txtUsername.setText(usuario.getNombre());
            //txtSexo.setText(usuario.getSexo());
            txtReloj.setText(usuario.getTipoReloj());

            /**
            if(usuario.getSexo().equals(getString(R.string.man))){
                imgGender.setImageResource(R.drawable.man_icon);
            } else {
                imgGender.setImageResource(R.drawable.woman_icon);
            }*/

            if(usuario.getTipoReloj().equals(getString(R.string.square))){
                imgReloj.setImageResource(R.drawable.square_watch);
            } else {
                imgReloj.setImageResource(R.drawable.round_watch);
            }

            if(usuario.getFoto() == null){
                profilePhoto.setImageResource(R.drawable.logo_user);
            } else {
                photoUtils.cargarRecursoUsuario(usuario.getFoto(), profilePhoto);
            }
        }
     }

    /*
     * Metodo para inicializar el número de estrategias y regulaciones del usuario
     */
    private void setStrategiesAndRegulationNumber() {
        DatabaseAdapter db = new DatabaseAdapter(this);
        db.open();
        long strategiesNumber = db.getStrategiesNumber();
        long regulationsNumber = db.getRegulationsNumber();
        db.close();

        if (strategiesNumber == 1){
            txtEstrategias.setText(strategiesNumber + " " + getString(R.string.strategy));
        } else {
            txtEstrategias.setText(strategiesNumber + " " + getString(R.string.strategies));
        }

        if(regulationsNumber == 1){
            txtRegulaciones.setText(regulationsNumber + " " + getString(R.string.regulation));
        } else {
            txtRegulaciones.setText(regulationsNumber + " " + getString(R.string.regulations));
        }
    }


    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    /*  ***********************************
    Opciones del menu de la toolbar
    ***********************************
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_edit) {
            startActivity(new Intent(this, EditProfileActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }


}
