package com.orange.taimun_watch.es;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.app.AlertDialog;
import android.view.MenuItem;

import com.orange.taimun_watch.es.R;
/**
 * Created by Yux on 03/06/2016.
 */

public class NewStrategyActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    public final static int CARDS_POSITION = 0;
    public final static int SETTINGS_POSITION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_strategy_activity);

        //Recuperamos toolbar y la asignamos como SupportActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Comprobamos si es creación o edición de estrategia
        Intent intent = getIntent();
        long idEstrategia = intent.getLongExtra("strategyId", -1);
        if (idEstrategia > 0) {
            getSupportActionBar().setTitle(R.string.edit_strategy);
        } else {
            getSupportActionBar().setTitle(R.string.new_strategy);
        }

        //ViewPager para gestionar los fragment
        viewPager = (ViewPager) findViewById(R.id.new_strategy_viewpager);
        setupViewPager(viewPager);

        //Asociamos los tabs con el gestor de fragments
        tabLayout = (TabLayout) findViewById(R.id.strategy_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    /*
        Método para inicializar el ViewPager con los fragmentos que mostrará cada Tab
     */
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new StrategyFirstTabFragment(), getResources().getString(R.string.cards));
        adapter.addFragment(new StrategySecondTabFragment(), getResources().getString(R.string.settings));
        viewPager.setAdapter(adapter);
    }

    public Fragment getFirstTabFragment(){
        return adapter.getItem(0);
    }

    public Fragment getSecondTabFragment() { return adapter.getItem(1); }
    /**************************************************************
        Método de control de fragments a mostrar en la actividad
    /**************************************************************/
    public void moveToFragment(int fragmentPos) {
        viewPager.setCurrentItem(fragmentPos);
    }

    @Override
    public void onBackPressed() {

        final StrategySecondTabFragment strategySecondTabFragment = (StrategySecondTabFragment) getSecondTabFragment();
        if(strategySecondTabFragment.isChange() == true) {
            //Nos movemos al fragmento de settings.
            moveToFragment(NewStrategyActivity.SETTINGS_POSITION);

            //Diálogo de guardado.
            //Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            //Chain together various setter methods to set the dialog characteristics
            builder.setMessage(R.string.dialog_guardado)
                    .setTitle(R.string.dialog_title_guardado)
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            strategySecondTabFragment.guardarEstrategia();
                            finish();

                        } }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                } });

            //Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        final MenuItem item2 = item;

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                final StrategySecondTabFragment strategySecondTabFragment = (StrategySecondTabFragment) getSecondTabFragment();
                if(strategySecondTabFragment.isChange() == true) {
                    //Nos movemos al fragmento de settings.
                    moveToFragment(NewStrategyActivity.SETTINGS_POSITION);

                    //Diálogo de guardado.
                    //Instantiate an AlertDialog.Builder with its constructor
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    //Chain together various setter methods to set the dialog characteristics
                    builder.setMessage(R.string.dialog_guardado)
                            .setTitle(R.string.dialog_title_guardado)
                            .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    strategySecondTabFragment.guardarEstrategia();
                                    finish();

                                } }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        } });

                    //Get the AlertDialog from create()
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    finish();
                }
                break;

            case R.id.action_save:
                final StrategySecondTabFragment SecondTab = (StrategySecondTabFragment) getSecondTabFragment();
                long check = SecondTab.guardarEstrategia();
                if (check >= 0) {
                    finish();
                }
                break;
            case R.id.action_next:
                this.moveToFragment(NewStrategyActivity.SETTINGS_POSITION);
                break;
            case R.id.action_back:
                this.moveToFragment(NewStrategyActivity.CARDS_POSITION);
                break;
        }

        return true;
    }
}

