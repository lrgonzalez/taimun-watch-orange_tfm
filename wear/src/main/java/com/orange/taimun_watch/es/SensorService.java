package com.orange.taimun_watch.es;

import android.app.IntentService;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.orange.taimun_watch.es.utils.Preferences;

/**
 * Clase que obtiene las medidas de los sensores registrados
 *
 * @author Javier Cuadrado Manso
 */
public class SensorService extends IntentService implements SensorEventListener {
    private SensorAdmin sensorAdmin;
    private boolean running = false;

    private PowerManager.WakeLock wakeLock;

    /**
     * Constructor de servicio de sensores
     */
    public SensorService() {
        super("sensorService");
        Log.i("SensorService", "CONSTRUCTOR");
    }

    /**
     * Funcion que lee la medida del sensor cardaco y la imprime por pantalla.
     * Tambien se guarda esta medida junto al timestamp en un fichero de texto
     *
     * @param sensorEvent
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        this.sensorAdmin.onSensorChanged(sensorEvent);
    }

    /**
     * Funcion que indica en que momento ha cambiado la precision del medidor cardiaco
     * accuracy = 1 => Baja precision; accuracy = 2 => Precision normal; accuracy = 3 => Alta precision
     *
     * @param sensor
     * @param i
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    /**
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
    }

    /**
     * Funcion que crea el servicio y registra los sensores
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("SensorService", "ON START COMMAND");
        if (this.running) return START_STICKY;
        this.running = true;

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "SensorService");
        wakeLock.acquire();

        this.sensorAdmin = SensorAdmin.getInstance(this.getApplicationContext());
        this.sensorAdmin.registerListeners(this);

        //resgistro del observador en los sensores
        if (this.sensorAdmin.getAcSensor() != null)
            this.sensorAdmin.getAcSensor().addObserver(this.sensorAdmin);
        if (this.sensorAdmin.getGirSensor() != null)
            this.sensorAdmin.getGirSensor().addObserver(this.sensorAdmin);
        if (this.sensorAdmin.getHeartSensor() != null)
            this.sensorAdmin.getHeartSensor().addObserver(this.sensorAdmin);
        if (this.sensorAdmin.getStepDetector() != null)
            this.sensorAdmin.getStepDetector().addObserver(this.sensorAdmin);
        if(this.sensorAdmin.getStepCounter() != null)
            this.sensorAdmin.getStepCounter().addObserver(this.sensorAdmin);

        SensorAdmin.resetTimeWindow();

        return START_STICKY;
    }

    /**
     * Funcion que cierra los registros y los ficheros al destruirse el hilo
     */
    @Override
    public void onDestroy() {
        this.running = false;
        Log.i("SensorService", "onDestroy");
        this.sensorAdmin.unregisterListeners(this);
        this.sensorAdmin.clearMeasures();
        SensorAdmin.resetTimeWindow();

        Preferences.setRunning(getApplicationContext(), false);

        wakeLock.release();

        super.onDestroy();
    }

    /**
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
