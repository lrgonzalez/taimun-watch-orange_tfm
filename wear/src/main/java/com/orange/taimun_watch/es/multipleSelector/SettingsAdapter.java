package com.orange.taimun_watch.es.multipleSelector;

import android.content.Context;
import android.support.wearable.view.CircledImageView;
import android.support.wearable.view.WearableListView;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.orange.taimun_watch.es.R;

/**
 *
 * Clase auxiliar del adaptador de las listas utilizadas en el selector multiple.
 *
 * Basada en el ejemplo de "Creating a Custom Wearable List View" del libro
 * "Step by Step Android Wear" de Alex HO.
 *
 * @author Javier Cuadrado Manso
 *
 */
public class SettingsAdapter extends WearableListView.Adapter {

    private final Context context;
    private final List<SettingsItems> items;

    public SettingsAdapter(Context context, List<SettingsItems> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public WearableListView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new WearableListView.ViewHolder(new SettingsItemView(context));
    }

    @Override
    public void onBindViewHolder(WearableListView.ViewHolder viewHolder, final int position) {
        SettingsItemView SettingsItemView = (SettingsItemView) viewHolder.itemView;
        final SettingsItems item = items.get(position);

        TextView textView = (TextView) SettingsItemView.findViewById(R.id.text);
        textView.setText(item.title);

        final CircledImageView imageView = (CircledImageView) SettingsItemView.findViewById(R.id.image);
        imageView.setImageDrawable(item.iconRes);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}