package com.orange.taimun_watch.es.utils;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Clase que administra los logs de eventos de interaccion con el reloj.
 * Las funciones son estaticas para poder acceder a ellas desde cualquier parte
 *
 * @author Javier Cuadrado Manso
 */
public class EventLog implements Parcelable {
    private static ArrayList<EventRecord> records;

    /*public EventLog (){
        this.records = new ArrayList<>();
    }*/

    public static void initRecord() {
        if (records == null)
            records = new ArrayList<>();
    }

    public static void clearLog() {
        records.clear();
    }

    public static ArrayList<EventRecord> getRecords() {
        return records;
    }

    public static void setRecords(ArrayList<EventRecord> r) {
        records = r;
    }

    public static void addRecord(EventRecord record) {
        records.add(record);
    }

    public static boolean isLogNull() {
        return records == null;
    }

    /**
     * Funcion que crea las carpetas contenedoras
     *
     * @param ctx Contexto
     */
    public static void checkDirectory(Context ctx) {
        File folder = ctx.getFilesDir();
        boolean check = new File(folder, "events").exists();
        if (!check)
            new File(folder.getAbsolutePath() + "/events").mkdir();
        check = new File(folder, "readedEvents").exists();
        if (!check)
            new File(folder.getAbsolutePath() + "/readedEvents").mkdir();
    }

    /**
     * Funcion que escribe los eventos en un fichero de texto
     *
     * @param ctx     Contexto
     * @param watchId Id del reloj
     */
    public static void writeEventLog(Context ctx, String watchId) {
        final Context c = ctx;
        final String wi = watchId;
        final ArrayList<EventRecord> rds = (ArrayList<EventRecord>) records.clone();
        clearLog();
        new Thread(new Runnable() {
            public void run() {
                writeEventLogThread(c, wi, rds);
            }
        }).start();
    }

    /**
     * Funcion ejecutada en un hilo que escribe los eventos en un log. Se ha realizado en un hilo
     * para no frenar al hilo principal
     *
     * @param ctx     Contexto
     * @param watchId Identificador del reloj
     * @param rds     Eventos
     */
    private static void writeEventLogThread(final Context ctx, final String watchId, ArrayList<EventRecord> rds) {
        String fileName = watchId + "-events-" + EventLog.makeDate() + ".txt";

        try {
            File f = new File(ctx.getFilesDir() + "/events", fileName);
            if (!f.exists())
                f.createNewFile();
            FileOutputStream fos = new FileOutputStream(f, false);
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            //osw.write(fileName + "\n");
            //osw.write("timestamp\ttipo\n");

            for (EventRecord e : rds)
                osw.write(e.toString() + "\n");
            osw.flush();
            osw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Funcion que crea un string con la fecha actual
     *
     * @return String con la fecha actual
     */
    private static String makeDate() {
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH) + 1;
        return c.get(Calendar.HOUR_OF_DAY) + "_" + c.get(Calendar.MINUTE) + "_"
                + c.get(Calendar.SECOND) + "-" + c.get(Calendar.DAY_OF_MONTH) + "_" + month + "_"
                + c.get(Calendar.YEAR);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(records);
    }

    public static final Parcelable.Creator<EventLog> CREATOR = new Creator<EventLog>() {
        public EventLog createFromParcel(Parcel source) {
            ArrayList<EventRecord> r = new ArrayList<>();
            source.readTypedList(r, EventRecord.CREATOR);
            EventLog el = new EventLog();
            el.setRecords(r);
            return el;
        }

        public EventLog[] newArray(int size) {
            return new EventLog[size];
        }
    };


}
