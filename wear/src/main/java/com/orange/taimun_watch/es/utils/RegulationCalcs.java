package com.orange.taimun_watch.es.utils;

/**
 * @author Javier Cuadrado Manso
 */
public class RegulationCalcs {
    public static final int GRADO = 3;

    /*Tiempos*/
    public static final String ACCELEROMETER_TIME = "ACCELEROMETER_TIME";
    public static final String GYROSCOPE_TIME = "GYROSCOPE_TIME";
    public static final String HEARTRATE_TIME = "HEARTRATE_TIME";
    public static final String STEPDETECTOR_TIME = "STEPDETECTOR_TIME";

    /**
     * Acelerometro
     **/

    //Estadisticas
    public static final String ACCELEROMETER_X_MEAN = "ACCELEROMETER_X_MEAN";
    public static final String ACCELEROMETER_Y_MEAN = "ACCELEROMETER_Y_MEAN";
    public static final String ACCELEROMETER_Z_MEAN = "ACCELEROMETER_Z_MEAN";

    public static final String ACCELEROMETER_X_VAR = "ACCELEROMETER_X_VAR";
    public static final String ACCELEROMETER_Y_VAR = "ACCELEROMETER_Y_VAR";
    public static final String ACCELEROMETER_Z_VAR = "ACCELEROMETER_Z_VAR";

    public static final String ACCELEROMETER_X_StdDvt = "ACCELEROMETER_X_StdDvt";
    public static final String ACCELEROMETER_Y_StdDvt = "ACCELEROMETER_Y_StdDvt";
    public static final String ACCELEROMETER_Z_StdDvt = "ACCELEROMETER_Z_StdDvt";

    public static final String ACCELEROMETER_X_RMS = "ACCELEROMETER_X_RMS";
    public static final String ACCELEROMETER_Y_RMS = "ACCELEROMETER_Y_RMS";
    public static final String ACCELEROMETER_Z_RMS = "ACCELEROMETER_Z_RMS";

    public static final String ACCELEROMETER_X_MAD = "ACCELEROMETER_X_MAD";
    public static final String ACCELEROMETER_Y_MAD = "ACCELEROMETER_Y_MAD";
    public static final String ACCELEROMETER_Z_MAD = "ACCELEROMETER_Z_MAD";

    public static final String ACCELEROMETER_X_MEDIAN = "ACCELEROMETER_X_MEDIAN";
    public static final String ACCELEROMETER_Y_MEDIAN = "ACCELEROMETER_Y_MEDIAN";
    public static final String ACCELEROMETER_Z_MEDIAN = "ACCELEROMETER_Z_MEDIAN";

    //Estructurales

    public static final String ACCELEROMETER_X_PR_G0 = "ACCELEROMETER_X_PR_G0";
    public static final String ACCELEROMETER_X_PR_G1 = "ACCELEROMETER_X_PR_G1";
    public static final String ACCELEROMETER_X_PR_G2 = "ACCELEROMETER_X_PR_G2";
    public static final String ACCELEROMETER_X_PR_G3 = "ACCELEROMETER_X_PR_G3";
    public static final String ACCELEROMETER_Y_PR_G0 = "ACCELEROMETER_Y_PR_G0";
    public static final String ACCELEROMETER_Y_PR_G1 = "ACCELEROMETER_Y_PR_G1";
    public static final String ACCELEROMETER_Y_PR_G2 = "ACCELEROMETER_Y_PR_G2";
    public static final String ACCELEROMETER_Y_PR_G3 = "ACCELEROMETER_Y_PR_G3";
    public static final String ACCELEROMETER_Z_PR_G0 = "ACCELEROMETER_Z_PR_G0";
    public static final String ACCELEROMETER_Z_PR_G1 = "ACCELEROMETER_Z_PR_G1";
    public static final String ACCELEROMETER_Z_PR_G2 = "ACCELEROMETER_Z_PR_G2";
    public static final String ACCELEROMETER_Z_PR_G3 = "ACCELEROMETER_Z_PR_G3";


    //Transicionales

    public static final String ACCELEROMETER_X_TREND = "ACCELEROMETER_X_TREND";
    public static final String ACCELEROMETER_Y_TREND = "ACCELEROMETER_Y_TREND";
    public static final String ACCELEROMETER_Z_TREND = "ACCELEROMETER_Z_TREND";

    public static final String ACCELEROMETER_X_MOC = "ACCELEROMETER_X_MOC";
    public static final String ACCELEROMETER_Y_MOC = "ACCELEROMETER_Y_MOC";
    public static final String ACCELEROMETER_Z_MOC = "ACCELEROMETER_Z_MOC";

    /**
     * Giroscopio
     **/

    //Estadisticas
    public static final String GYROSCOPE_X_MEAN = "GYROSCOPE_X_MEAN";
    public static final String GYROSCOPE_Y_MEAN = "GYROSCOPE_Y_MEAN";
    public static final String GYROSCOPE_Z_MEAN = "GYROSCOPE_Z_MEAN";

    public static final String GYROSCOPE_X_VAR = "GYROSCOPE_X_VAR";
    public static final String GYROSCOPE_Y_VAR = "GYROSCOPE_Y_VAR";
    public static final String GYROSCOPE_Z_VAR = "GYROSCOPE_Z_VAR";

    public static final String GYROSCOPE_X_StdDvt = "GYROSCOPE_X_StdDvt";
    public static final String GYROSCOPE_Y_StdDvt = "GYROSCOPE_Y_StdDvt";
    public static final String GYROSCOPE_Z_StdDvt = "GYROSCOPE_Z_StdDvt";

    public static final String GYROSCOPE_X_RMS = "GYROSCOPE_X_RMS";
    public static final String GYROSCOPE_Y_RMS = "GYROSCOPE_Y_RMS";
    public static final String GYROSCOPE_Z_RMS = "GYROSCOPE_Z_RMS";

    public static final String GYROSCOPE_X_MAD = "GYROSCOPE_X_MAD";
    public static final String GYROSCOPE_Y_MAD = "GYROSCOPE_Y_MAD";
    public static final String GYROSCOPE_Z_MAD = "GYROSCOPE_Z_MAD";

    public static final String GYROSCOPE_X_MEDIAN = "GYROSCOPE_X_MEDIAN";
    public static final String GYROSCOPE_Y_MEDIAN = "GYROSCOPE_Y_MEDIAN";
    public static final String GYROSCOPE_Z_MEDIAN = "GYROSCOPE_Z_MEDIAN";

    //Estructurales

    public static final String GYROSCOPE_X_PR_G0 = "GYROSCOPE_X_PR_G0";
    public static final String GYROSCOPE_X_PR_G1 = "GYROSCOPE_X_PR_G1";
    public static final String GYROSCOPE_X_PR_G2 = "GYROSCOPE_X_PR_G2";
    public static final String GYROSCOPE_X_PR_G3 = "GYROSCOPE_X_PR_G3";
    public static final String GYROSCOPE_Y_PR_G0 = "GYROSCOPE_Y_PR_G0";
    public static final String GYROSCOPE_Y_PR_G1 = "GYROSCOPE_Y_PR_G1";
    public static final String GYROSCOPE_Y_PR_G2 = "GYROSCOPE_Y_PR_G2";
    public static final String GYROSCOPE_Y_PR_G3 = "GYROSCOPE_Y_PR_G3";
    public static final String GYROSCOPE_Z_PR_G0 = "GYROSCOPE_Z_PR_G0";
    public static final String GYROSCOPE_Z_PR_G1 = "GYROSCOPE_Z_PR_G1";
    public static final String GYROSCOPE_Z_PR_G2 = "GYROSCOPE_Z_PR_G2";
    public static final String GYROSCOPE_Z_PR_G3 = "GYROSCOPE_Z_PR_G3";


    //Transicionales

    public static final String GYROSCOPE_X_TREND = "GYROSCOPE_X_TREND";
    public static final String GYROSCOPE_Y_TREND = "GYROSCOPE_Y_TREND";
    public static final String GYROSCOPE_Z_TREND = "GYROSCOPE_Z_TREND";

    public static final String GYROSCOPE_X_MOC = "GYROSCOPE_X_MOC";
    public static final String GYROSCOPE_Y_MOC = "GYROSCOPE_Y_MOC";
    public static final String GYROSCOPE_Z_MOC = "GYROSCOPE_Z_MOC";

    /**
     * Pulsometro
     **/

    //Estadisticas
    public static final String HEARTRATE_MEAN = "HEARTRATE_MEAN";

    public static final String HEARTRATE_VAR = "HEARTRATE_VAR";

    public static final String HEARTRATE_StdDvt = "HEARTRATE_StdDvt";

    public static final String HEARTRATE_RMS = "HEARTRATE_RMS";

    public static final String HEARTRATE_MAD = "HEARTRATE_MAD";

    public static final String HEARTRATE_MEDIAN = "HEARTRATE_MEDIAN";

    //Estructurales

    public static final String HEARTRATE_PR_G0 = "HEARTRATE_PR_G0";
    public static final String HEARTRATE_PR_G1 = "HEARTRATE_PR_G1";
    public static final String HEARTRATE_PR_G2 = "HEARTRATE_PR_G2";
    public static final String HEARTRATE_PR_G3 = "HEARTRATE_PR_G3";

    //Transicionales

    public static final String HEARTRATE_TREND = "HEARTRATE_TREND";

    public static final String HEARTRATE_MOC = "HEARTRATE_MOC";


    /**
     * Detector de pasos
     **/

    //Estadisticas
    public static final String STEPDETECTOR_MEAN = "STEPDETECTOR_MEAN";

    public static final String STEPDETECTOR_VAR = "STEPDETECTOR_VAR";

    public static final String STEPDETECTOR_StdDvt = "STEPDETECTOR_StdDvt";

    public static final String STEPDETECTOR_RMS = "STEPDETECTOR_RMS";

    public static final String STEPDETECTOR_MAD = "STEPDETECTOR_MAD";

    public static final String STEPDETECTOR_MEDIAN = "STEPDETECTOR_MEDIAN";

    //Estructurales

    public static final String STEPDETECTOR_PR_G0 = "STEPDETECTOR_PR_G0";
    public static final String STEPDETECTOR_PR_G1 = "STEPDETECTOR_PR_G1";
    public static final String STEPDETECTOR_PR_G2 = "STEPDETECTOR_PR_G2";
    public static final String STEPDETECTOR_PR_G3 = "STEPDETECTOR_PR_G3";

    //Transicionales

    public static final String STEPDETECTOR_TREND = "STEPDETECTOR_TREND";

    public static final String STEPDETECTOR_MOC = "STEPDETECTOR_MOC";

    /**
     * Precision
     **/

    public static final String ACCELEROMETER_ACCURACY_MEAN = "ACCELEROMETER_ACCURACY_MEAN";
    public static final String GYROSCOPE_ACCURACY_MEAN = "GYROSCOPE_ACCURACY_MEAN";
    public static final String HEARTRATE_ACCURACY_MEAN = "HEARTRATE_ACCURACY_MEAN";
    public static final String STEPDETECTOR_ACCURACY_MEAN = "STEPDETECTOR_ACCURACY_MEAN";

}
