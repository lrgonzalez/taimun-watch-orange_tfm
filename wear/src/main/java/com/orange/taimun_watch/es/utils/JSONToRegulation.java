package com.orange.taimun_watch.es.utils;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.orange.taimun_watch.es.Regulacion.Regulation;
import com.orange.taimun_watch.es.Regulacion.Strategy;


/**
 * Clase utilizada para leer un fichero JSON y guardarlo en una regulacion.
 *
 * @author Javier Cuadrado Manso
 */
public class JSONToRegulation {

    /**
     * Funcion para leer un fichero  y devolverlo en formato String
     *
     * @param filename nombre del fichero con su ruta
     * @return
     */
    public static String readFile(String filename) {
        String result = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Funcion para leer el fichero de ejemplo del JSON
     *
     * @param ctx
     * @return
     */
    public static String readFile(Context ctx) {
        String result = null;
        try {
            BufferedReader br;
            if (Preferences.DEBUG)
                br = new BufferedReader(new InputStreamReader(ctx.getAssets().open("regulacion.json")));
            else {
                File regFiles = new File (ctx.getFilesDir() + "/regulation_files");
                File files[] = regFiles.listFiles();
                File json = null;
                for (File f : files)
                    if (f.getName().contains(".json"))
                        json = f;
                if (json == null) {
                    Log.e("JSONToRegulation", "Error: json no encontrado.");
                    return null;
                }

                br = new BufferedReader(new FileReader(json.getAbsolutePath()));
            }

            StringBuilder sb = new StringBuilder();

            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return result;
    }

    /**
     * Funcion que devuelve una regulacion a partir del string de un JSON
     *
     * @param jsonString el string del JSON
     * @return la regulacion
     */
    public static Regulation parseJSON(String jsonString) {
        Regulation r = null;
        try {
            JSONObject jx = new JSONObject(jsonString);
            JSONObject jo = jx.getJSONObject("Regulation");

            /*Creacion del objeto regulacion*/
            r = new Regulation(jo.getInt("Id"));
            r.setName(jo.getString("Name"));
            r.setVibration(jo.getBoolean("Vibration"));

            if(jo.getString("Audio").contains("null")) {
                r.setAudio(null);
            } else {
                r.setAudio(jo.getString("Audio"));
            }

            ArrayList<ArrayList<Integer>> schedule = new ArrayList<>();
            JSONArray sca = jo.getJSONArray("RegulationSchedule");
            for (int i = 0; i < sca.length(); i++) {
                ArrayList<Integer> x = new ArrayList<>();
                Object obj = sca.get(i);
                if (obj instanceof JSONObject || obj instanceof Integer)
                    x.add(sca.getInt(i));
                else if (obj instanceof JSONArray)
                    for (int j = 0; j < sca.getJSONArray(i).length(); j++)
                        x.add(sca.getJSONArray(i).getInt(j));
                schedule.add(x);
            }

            r.setSchedule(schedule);

            JSONArray strategies = jo.getJSONArray("Strategies");
            for (int i = 0; i < strategies.length(); i++) {
                JSONObject strObj = strategies.getJSONObject(i);

                /*Creacion del objeto estrategia*/
                Strategy str = new Strategy(strObj.getInt("Id"));
                str.setName(strObj.getString("Name"));
                str.setIcon(strObj.getString("Icon"));
                str.setTime(strObj.getInt("Time"));
                str.setIsGlobalTime(strObj.getBoolean("isGlobalTime"));
                str.setQuestion(strObj.getBoolean("Question"));
                str.setTimerType(strObj.getInt("TimerType"));
                str.setIsCarrusel(strObj.getBoolean("isCarrusel"));
                str.setReinforcement(strObj.getBoolean("Reinforcement"));

                JSONArray steps = strObj.getJSONArray("Steps");
                for (int j = 0; j < steps.length(); j++) {
                    String step = steps.getString(j);
                    str.addStep(step);
                }
                r.addStrategy(str);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return r;
    }
}