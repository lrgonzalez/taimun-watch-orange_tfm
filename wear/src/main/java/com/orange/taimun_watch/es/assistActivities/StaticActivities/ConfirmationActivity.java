package com.orange.taimun_watch.es.assistActivities.StaticActivities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.orange.taimun_watch.es.MainActivity;
import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.CarruselActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimeGif;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimePicture;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.Preferences;

/**
 * Clase que administra la logica de las actividades de asistencia
 *
 * @author Javier Cuadrado Manso
 */
public class ConfirmationActivity extends AssistActivity {

    public TextView mTextView;
    private int confimationOption;
    private CountDownTimer reVibrate = null;

    /**
     * Funcion que crea la actividad
     *
     * @param savedInstanceState Instancia del estado guardado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selector2);

        Log.i("Confirmation", "onCreate");

        //Carga del texto en funcion de la configuracion de la regulacion
        final String txt ="¿Estás bien?";
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);

        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) findViewById(R.id.confirmText);
                mTextView.setText(txt);
            }
        });
        this.reVibrate = new CountDownTimer(millisToVibrate, millisToVibrate) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(500);
                this.start();
            }
        }.start();
    }

    /**
     * Listener del toque de la respuesta afirmativa
     *
     * @param v View del boton afirmativo
     */
    public void touch1(View v) {
        if (!this.allowTouch)
            return;
        Log.i("S2", "touch1");
        this.reVibrate.cancel();
        confimationOption = 1;
        touched = true;
        EventLog.addRecord(new EventRecord(EventRecord.PREGUNTA_SI));
        EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
        EventLog.writeEventLog(getApplicationContext(),
                Settings.Secure.getString(getApplicationContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID));
        this.allowTouch = false;
        new NextStepTask().execute();

    }

    /**
     * Listener del toque de la respuesta negativa
     *
     * @param v View del boton negativo
     */
    public void touch2(View v) {
        if (!this.allowTouch)
            return;
        this.reVibrate.cancel();
        Log.i("S2", "touch2");
        confimationOption = 0;
        touched = true;
        EventLog.addRecord(new EventRecord(EventRecord.PREGUNTA_NO));
        this.allowTouch = false;
        new NextStepTask().execute();

    }

    /**
     * Actividad que, al detectar que se pone en pausa la app, la restaura en unos segundos
     */
    @Override
    public void onPause() {
        if (!Preferences.getNewActivity(getApplicationContext())) {
            this.allowTouch = false;
            postExecute = new Handler();
            postExecute.postDelayed(new RestoreActivity(), 1000);
            EventLog.addRecord(new EventRecord(EventRecord.SALIDA_REGULACION));
        }
        reVibrate.cancel();
        super.onPause();
    }

    /**
     * Funcion que devuelve el texto en funcion de lo indicado en la regulacion
     *
     * @param option Opcion escogida en la regulacion
     * @return La pregunta correspondiente
     */
    public String question(int option) {
        switch (option) {
            case 1:
                return getResources().getString(R.string.question1);
            default:
                return getResources().getString(R.string.questionError);
        }
    }

    /**
     * Clase privada encargada de restaurar esta actividad si se ha cerrado
     */
    private class RestoreActivity implements Runnable {

        /**
         * Funcion que restaura la actividad
         */
        @Override
        public void run() {
            //Si la aplicacion no esta en pantalla se volvera a abrir la actividad.
            if (!Preferences.getCharging(getApplicationContext())) {
                Log.i("Confirmation", "restarting app");
                Preferences.setLaunchApp(getApplicationContext(), false);

                Intent i = new Intent(ConfirmationActivity.this, ConfirmationActivity.class);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                return;
            }

        }
    }

    /**
     * Clase privada que carga la actividad siguiente
     */
    private class NextStepTask extends AsyncTask {
        /**
         * Funcion que carga la actividad siguiente
         *
         * @param params Parametros de la funcion no utilizados
         * @return null
         */
        @Override
        protected Object doInBackground(Object[] params) {
            Log.i("Confirmation", "running background");

            //Si se ha seleccionado una opcion, se inicia la comprobacion
            if (touched) {
                touched = false;
                Intent i = null;
                //Si era una estrategia escogida desde un selector, se pone la opcion a -1
                //para indicar que se vuelve al array de estrategias
                regulation.setOption(-1);

                //Si la opcion es positiva, hay refuerzo y no se ha detectado estres, se carga el refuerzo
                if (confimationOption == 1 && regulation.getActualStrategy().hasReinforcement()) {
                    EventLog.addRecord(new EventRecord(EventRecord.REFUERZO_POSITIVO));
                    i = new Intent(ConfirmationActivity.this, PositiveReinforcementActivity.class);
                    i.putExtra("question", true);
                    //Si la opcion es positiva y las otras dos condiciones no se cumplen , la regulacion ha terminado
                } else if (confimationOption == 1) {
                    EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
                    EventLog.writeEventLog(getApplicationContext(),
                            Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                    Settings.Secure.ANDROID_ID));
                    Preferences.setEndRegulation(getApplicationContext(), true);
                    i = new Intent(ConfirmationActivity.this, MainActivity.class);
                    //En caso contrario, se continua con la estrategia
                } else {
                    if (!regulation.nextStrategy()) {
                        regulation.restartRegulation();
                        regulation.nextStrategy();
                        regulation.getActualStrategy().restartStrategy();

                        if(!regulation.isSelector()) {
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_REGULACION + regulation.getName()));
                            regulation.getActualStrategy().nextStep();
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getActualStrategy().getName()));
                            i = getIntentStep();
                        } else {
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + "Selector"));
                            Log.d("ConfirmationActivity", EventRecord.INICIO_ESTRATEGIA + "Selector");
                            i = new Intent(ConfirmationActivity.this, MultipleSelectorActivity.class);
                        }
                    } else {
                        EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getActualStrategy().getName()));
                        if (regulation.isSelector()) {
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Selector"));
                            i = new Intent(ConfirmationActivity.this, MultipleSelectorActivity.class);
                        } else {
                            //Si es simple, se reinicia y se comprueba que tiene pasos
                            regulation.getActualStrategy().restartStrategy();
                            if (regulation.getActualStrategy().isCarrusel()) {
                                EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Carrusel"));
                                i = new Intent(ConfirmationActivity.this, CarruselActivity.class);
                            } else if (!regulation.getActualStrategy().nextStep()) {
                                Log.e("Confirmation", "Error: La estrategia " +
                                        regulation.getActualStrategy().getId() + " no tiene pasos");
                                i = new Intent(ConfirmationActivity.this, MainActivity.class);
                            } else {
                                //SI tiene pasos, al coger el primero se clasifica
                                i = getIntentStep();
                                if (i == null) {
                                    allowTouch = true;
                                    return null;
                                }
                            }
                        }
                    }
                }

                Preferences.setNewActivity(getApplicationContext(), true);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                allowTouch = true;
                Thread.currentThread().interrupt();
                return null;
            }
            touched = false;
            allowTouch = true;
            Preferences.setNewActivity(getApplicationContext(), false);
            return null;
        }

        /**
         * Funcion que indica cual es la actividad a la que ir
         *
         * @return Intent de la siguiente actividad
         */
        private Intent getIntentStep() {
            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + regulation.getActualStrategy().getActualStep()));
            String s = regulation.getActualStrategy().getActualStep();
            int time = regulation.getActualStrategy().getTime();
            switch (AssistActivity.getStepType(s, time)) {
                case AssistActivity.STATIC_GIF:
                    return new Intent(ConfirmationActivity.this, GifActivity.class);
                case AssistActivity.STATIC_PICTURE:
                    return new Intent(ConfirmationActivity.this, ContentPictureActivity.class);
                case AssistActivity.TIME_PICTURE:
                    return new Intent(ConfirmationActivity.this, TimePicture.class);
                case AssistActivity.TIME_GIF:
                    return new Intent(ConfirmationActivity.this, TimeGif.class);
                default:
                    Log.e("ConfirmationActivity", "Error: La estretagia esta mal formada");
                    return new Intent(ConfirmationActivity.this, MainActivity.class);
            }
        }
    }
}