package com.orange.taimun_watch.es.assistActivities.StaticActivities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import java.io.FileInputStream;
import java.io.IOException;

import com.orange.taimun_watch.es.MainActivity;
import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.CarruselActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimeGif;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimePicture;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.Preferences;
import com.orange.taimun_watch.es.widget.GifMovieView;

/**
 * Clase que maneja la logica de la actividad de gif sin contador de tiempo
 *
 * @author Javier Cuadrado Manso
 */
public class GifActivity extends AssistActivity {

    private FileInputStream fis;
    private CountDownTimer reVibrate = null;

    /**
     * Clase que carga el gif correspondiente al paso actual indicado por la regulacion
     *
     * @param savedInstanceState Instancia del estado guardado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Log.i("GifActivity", "onCreate");

        String sourceName = this.regulation.getActualStrategy().getActualStep();

        Movie mv = null;
        try {
            if (Preferences.DEBUG) {
                mv = Movie.decodeStream(getApplicationContext().getAssets().open(sourceName));
            } else {
                fis = new FileInputStream(getApplicationContext().getFilesDir() + "/regulation_files/" + sourceName);
                mv = Movie.decodeStream(fis);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(mv == null)
            try {
                mv = Movie.decodeStream(getApplicationContext().getAssets().open("error.gif"));
            } catch (IOException e) {
                e.printStackTrace();
            }

        final GifMovieView gif1 = (GifMovieView) findViewById(R.id.gif1);
        gif1.setMovie(mv);

        this.reVibrate = new CountDownTimer(millisToVibrate, millisToVibrate) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(4000);
                this.start();
            }
        }.start();
    }

    /**
     * Funcion para pausar o continuar con el movimiento del gif
     *
     * @param v View
     */
    @Deprecated
    public void onGifClick(View v) {
        //GifMovieView gif = (GifMovieView) v;
        //gif.setPaused(!gif.isPaused());
    }

    /**
     * Funcion que ejecuta la tarea de pasar al siguiente paso
     *
     * @param v View de la pantalla
     */
    public void touch(View v) {
        EventLog.addRecord(new EventRecord(EventRecord.TOQUE));
        if (!this.allowTouch)
            return;
        this.reVibrate.cancel();
        this.touched = true;
        this.allowTouch = false;
        new NextStepTask().execute();
    }

    /**
     * Funcion que restaura la actividad si se estaba saliendo de la app
     */
    @Override
    public void onPause() {
        if (!Preferences.getNewActivity(getApplicationContext())) {
            this.allowTouch = false;
            postExecute = new Handler();
            postExecute.postDelayed(new RestoreActivity(), 1000);
            EventLog.addRecord(new EventRecord(EventRecord.SALIDA_REGULACION));
        }
        if (!Preferences.DEBUG)
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        reVibrate.cancel();
        super.onPause();
    }

    /**
     * Clase privada encargada de restaurar la actividad si se ha salido de esta
     */
    private class RestoreActivity implements Runnable {

        /**
         * Funcion que restaura la actividad
         */
        @Override
        public void run() {
            // Log.i("GifActivity", "RestoreActivity");

            //Si la aplicacion no esta en pantalla se intentará vover a abrir la actividad.
            if (!Preferences.getCharging(getApplicationContext())) {
                Log.i("GifActivity", "restarting app");
                Preferences.setLaunchApp(getApplicationContext(), false);

                Intent i = new Intent(GifActivity.this, GifActivity.class);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                //Thread.currentThread().interrupt();
                return;
            }
        }
    }

    /**
     * Clase privada encargada de avanzar al paso siguiente
     */
    private class NextStepTask extends AsyncTask {

        /**
         * Funcion que avanza al paso siguiente
         *
         * @param params null
         * @return null
         */
        @Override
        protected Object doInBackground(Object[] params) {
            Log.i("GifActivity", "running background");

            //Condicion de paso siguiente:
            //Que se halla realizado el gesto
            if (hasGesture() && touched) {
                touched = false;
                Intent i = null;

                //Si no tiene paso...
                if (!regulation.getActualStrategy().nextStep()) {
                    //...La estrategia ha terminado y se comprueba si tiene una pregunta final
                    if (regulation.getActualStrategy().getQuestion() != false) {
                        EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Pregunta"));
                        i = new Intent(GifActivity.this, ConfirmationActivity.class);
                    } else if (regulation.getActualStrategy().hasReinforcement()
                            && !Preferences.getIsPositiveLastWindow(getApplicationContext())) {
                        EventLog.addRecord(new EventRecord(EventRecord.REFUERZO_POSITIVO));
                        i = new Intent(GifActivity.this, PositiveReinforcementActivity.class);
                    } else {
                        //Si era una estrategia escogida desde un selector, se pone la opcion a -1
                        //para indicar que se vuelve al array de estrategias
                        regulation.setOption(-1);
                        //Si no tiene pregunta, se pasa a la siguiente estrategia
                        //Si no tiene siguiente estrategia, la regulacion ha terminado
                        if (!regulation.nextStrategy()) {
                            EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
                            EventLog.writeEventLog(getApplicationContext(),
                                    Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                            Settings.Secure.ANDROID_ID));
                            Preferences.setEndRegulation(getApplicationContext(), true);
                            i = new Intent(GifActivity.this, MainActivity.class);
                        }
                        //En caso contrario, se comprueba si esta estrategia es simple o un selector
                        else {
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getActualStrategy().getName()));
                            if (regulation.isSelector()) {
                                EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "SelectorMultiple"));
                                i = new Intent(GifActivity.this, MultipleSelectorActivity.class);
                            } else {
                                //Si es simple, se reinicia y se comprueba que tiene pasos
                                regulation.getActualStrategy().restartStrategy();
                                if (regulation.getActualStrategy().isCarrusel()) {
                                    EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Carrusel"));
                                    i = new Intent(GifActivity.this, CarruselActivity.class);
                                } else if (!regulation.getActualStrategy().nextStep()) {
                                    Log.e("Gif", "Error: La estrategia " +
                                            regulation.getActualStrategy().getId() + " no tiene pasos");
                                    i = new Intent(GifActivity.this, MainActivity.class);
                                } else {
                                    //Si tiene pasos, al coger el primero se clasifica
                                    i = getIntentStep();
                                    if (i == null) {
                                        if (!Preferences.DEBUG)
                                            try {
                                                fis.close();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        allowTouch = true;
                                        return null;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    i = getIntentStep();
                    if (i == null) {
                        if (!Preferences.DEBUG)
                            try {
                                fis.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        allowTouch = true;
                        return null;
                    }
                }
                if (!Preferences.DEBUG)
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                Preferences.setNewActivity(getApplicationContext(), true);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                allowTouch = true;
                Thread.currentThread().interrupt();
                return null;
            }
            touched = false;
            allowTouch = true;
            Preferences.setNewActivity(getApplicationContext(), false);
            return null;
        }

        /**
         * Funcion que devuelve al siguiente actividad
         *
         * @return Intent de la siguiente actividad
         */
        private Intent getIntentStep() {
            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + regulation.getActualStrategy().getActualStep()));
            String s = regulation.getActualStrategy().getActualStep();
            int time = regulation.getActualStrategy().getTime();

            switch (AssistActivity.getStepType(s, time)) {
                case AssistActivity.STATIC_GIF:
                    //Si es un paso del mismo tipo que el actual,
                    //se cargan las fuentes y se continua sin
                    // cargar la nueva actividad

                    final String sourceName = s;
                    final GifMovieView gif1 = (GifMovieView) findViewById(R.id.gif1);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                final Movie mv = Movie.decodeStream(getApplicationContext().getAssets().open(sourceName));
                                gif1.setMovie(mv);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    reVibrate.start();
                    return null;
                case AssistActivity.STATIC_PICTURE:
                    return new Intent(GifActivity.this, ContentPictureActivity.class);
                case AssistActivity.TIME_PICTURE:
                    return new Intent(GifActivity.this, TimePicture.class);
                case AssistActivity.TIME_GIF:
                    return new Intent(GifActivity.this, TimeGif.class);
                default:
                    Log.e("Gif", "Error: La estretagia esta mal formada");
                    return new Intent(GifActivity.this, TimePicture.class);
            }
        }
    }
}
