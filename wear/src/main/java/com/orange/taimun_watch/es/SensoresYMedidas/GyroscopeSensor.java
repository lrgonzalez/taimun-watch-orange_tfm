package com.orange.taimun_watch.es.SensoresYMedidas;

import android.content.Context;
import android.hardware.Sensor;

import java.util.Calendar;

/**
 * Clase que administra el sensor del giroscopio
 *
 * @author Javier Cuadrado Manso
 */
public class GyroscopeSensor extends ObservableSensor {
    Context context; //PARA DEBUG

    /**
     * Constructor de la clase
     *
     * @param gyr Sensor de giroscopio
     * @param ctx Contexto
     */
    public GyroscopeSensor(Sensor gyr, Context ctx) {
        super(gyr);
        this.context = ctx;
    }

    /**
     * Actualiza las medidas tomadas
     *
     * @param accuracy precision
     * @param x        medida x
     * @param y        medida y
     * @param z        medida z
     */
    public void setNewMeasures(int accuracy, float x, float y, float z) {
        if(accuracy < 0)
            return;
        Calendar cal = Calendar.getInstance();
        this.medidasSensor.medidas.add(new Measure(cal.getTimeInMillis(), accuracy, x, y, z));
        setChanged();
        notifyObservers();
    }
}
