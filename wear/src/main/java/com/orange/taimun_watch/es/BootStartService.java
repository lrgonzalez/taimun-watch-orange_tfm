package com.orange.taimun_watch.es;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.Preferences;

/**
 * Clase que para el servicio cuando se acaba la bateria o cuando se pone a cargar el reloj
 * En caso de que se haya apagado el reloj, se cambian las prefrerencias indicando que
 * el servicio ya no esta corriendo
 *
 * @author Javier Cuadrado Manso
 */
public class BootStartService extends BroadcastReceiver {

    /**
     * Funcion que recibe los eventos relacionados con la carga del dispositivo
     *
     * @param context Contexto
     * @param intent  Intent que ha llamado al escuchador
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BATTERY_LOW)
                || intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {
            Log.i("BSS", "CHARGING");
            Intent i = new Intent(context, SensorService.class);
            context.stopService(i);
            Preferences.setRunning(context, false);

            if (!Preferences.getAssist(context)) {
                Preferences.setCharging(context, true);
                Preferences.setAssist(context, false);

                EventLog.writeEventLog(context, Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID));
            }
        } else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Preferences.setRunning(context, false);
            Preferences.setAssist(context, false);
            Preferences.setCharging(context, false);
        }
    }
}
