package com.orange.taimun_watch.es.assistActivities.StaticActivities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.orange.taimun_watch.es.MainActivity;
import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.CarruselActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimeGif;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimePicture;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.Preferences;

/**
 * Clase que administra la logica de la actividad de refuerzo positivo
 *
 * @author Javier Cuadrado Manso
 */
public class PositiveReinforcementActivity extends AssistActivity {

    private ImageView source = null;

    /**
     * Funcion que carga la imagen del paso actual indicada por la regulacion
     *
     * @param savedInstanceState Instancia del estado guardado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_picture);

        Log.i("PRActivity", "onCreate");

        source = (ImageView) findViewById(R.id.MainContentCPA);
        //String src = this.regulation.getActualStrategy().getReinforcement();

        Bitmap bm = null;
        if(Preferences.DEBUG)
            bm = super.getBitmapFromAsset("refuerzo.png");
        else
            //bm = BitmapFactory.decodeFile(getApplicationContext().getFilesDir() + "/regulation_files/" + "refuerzo.png");
            bm = super.getBitmapFromAsset("refuerzo.png");
        if (bm == null)
            this.source.setImageBitmap(super.getBitmapFromAsset("error.png"));
        else
            this.source.setImageBitmap(bm);

    }

    /**
     * Funcion que recoje el toque en la pantalla y comienza la tarea de avanzar al siguiente paso
     *
     * @param v View de la pantalla
     */
    public void touch(View v) {
        EventLog.addRecord(new EventRecord(EventRecord.TOQUE));
        if(!this.allowTouch)
            return;
        this.touched = true;
        this.allowTouch = false;
        new NextStepTask().execute();
    }

    /**
     * Funcion que reactiva el paso en caso de que el usuario salga de la actividad
     */
    @Override
    public void onPause() {
        if (!Preferences.getNewActivity(getApplicationContext())) {
            this.allowTouch = false;
            postExecute = new Handler();
            postExecute.postDelayed(new RestoreActivity(), 1000);
            EventLog.addRecord(new EventRecord(EventRecord.SALIDA_REGULACION));
        }
        super.onPause();
    }

    /**
     * Clase privada encargada de restaurar la actividad en caso de salirse de ella
     */
    private class RestoreActivity implements Runnable {

        /**
         * Funcion que restaura la actividad
         */
        @Override
        public void run() {
            //Si el reloj no se esta cargando, se restaura la actividad
            if (!Preferences.getCharging(getApplicationContext())) {
                Log.i("PRActivity", "restarting app");
                Preferences.setLaunchApp(getApplicationContext(), false);

                Intent i = new Intent(PositiveReinforcementActivity.this, PositiveReinforcementActivity.class);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                //Thread.currentThread().interrupt();
                return;
            }
        }
    }

    /**
     * Clase privada encargada de avanzar al siguiente paso
     */
    private class NextStepTask extends AsyncTask {
        /**
         * Funcion que avanza al siguiente paso
         *
         * @param params null
         * @return null
         */
        @Override
        protected Object doInBackground(Object[] params) {
            Log.i("PRActivity", "NextStepTask");
            //Condicion de paso siguiente
            //Que se halla realizado el toque
            if (touched) {
                //postExecute.removeCallbacksAndMessages(null);
                touched = false;
                Intent i = null;

                //Si era una estrategia escogida desde un selector, se pone la opcion a -1
                //para indicar que se vuelve al array de estrategias
                regulation.setOption(-1);
                //Si no tiene pregunta, se pasa a la siguiente estrategia
                //Si no tiene siguiente estrategia, la regulacion ha terminado
                if (!regulation.nextStrategy()) {
                    EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
                    EventLog.writeEventLog(getApplicationContext(),
                            Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                    Settings.Secure.ANDROID_ID));
                    Preferences.setEndRegulation(getApplicationContext(), true);
                    i = new Intent(PositiveReinforcementActivity.this, MainActivity.class);
                }
                //En caso contrario, se comprueba si esta regulacion es un selector
                else {
                    Intent intent = getIntent();
                    boolean question = intent.getBooleanExtra("question", false);

                    if(question == false) {
                        EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getActualStrategy().getName()));
                        if (regulation.isSelector()) {
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Selector"));
                            i = new Intent(PositiveReinforcementActivity.this, MultipleSelectorActivity.class);
                        } else {
                            //Si es simple, se reinicia y se comprueba que tiene pasos
                            regulation.getActualStrategy().restartStrategy();
                            if (regulation.getActualStrategy().isCarrusel()) {
                                EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Carrusel"));
                                i = new Intent(PositiveReinforcementActivity.this, CarruselActivity.class);
                            } else if (!regulation.getActualStrategy().nextStep()) {
                                Log.e("Picture", "Error: La estrategia " +
                                        regulation.getActualStrategy().getId() + " no tiene pasos");
                                i = new Intent(PositiveReinforcementActivity.this, MainActivity.class);
                            } else {
                                //SI tiene pasos, al coger el primero se clasifica
                                i = getIntentStep();
                                if (i == null) {
                                    allowTouch = true;
                                    return null;
                                }
                            }
                        }
                    } else {
                        EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
                        EventLog.writeEventLog(getApplicationContext(),
                                Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                        Settings.Secure.ANDROID_ID));
                        Preferences.setEndRegulation(getApplicationContext(), true);
                        i = new Intent(PositiveReinforcementActivity.this, MainActivity.class);
                    }
                }

                Preferences.setNewActivity(getApplicationContext(), true);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                allowTouch = true;
                Thread.currentThread().interrupt();
                return null;
            }

            touched = false;
            allowTouch = true;
            Preferences.setNewActivity(getApplicationContext(), false);
            return null;
        }
    }

    private Intent getIntentStep() {
        EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + regulation.getActualStrategy().getActualStep()));
        String s = regulation.getActualStrategy().getActualStep();
        int time = regulation.getActualStrategy().getTime();
        switch (AssistActivity.getStepType(s, time)) {
            case AssistActivity.STATIC_GIF:
                return new Intent(PositiveReinforcementActivity.this, GifActivity.class);
            case AssistActivity.STATIC_PICTURE:
                //Si es un paso del mismo tipo que el actual,
                //se cargan las fuentes y se continua sin
                // cargar la nueva actividad

                String actualStep = regulation.getActualStrategy().
                        getActualStep();

                Log.d("ASSIST", "getIntentStep() - actualStep: " + actualStep);

                final Bitmap bm = BitmapFactory.decodeFile(getApplicationContext().getFilesDir() + "/regulation_files/" + actualStep);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (bm == null)
                            source.setImageResource(R.mipmap.ic_launcher);
                        else
                            source.setImageBitmap(bm);
                    }
                });

                return new Intent(PositiveReinforcementActivity.this, ContentPictureActivity.class);
            case AssistActivity.TIME_PICTURE:
                return new Intent(PositiveReinforcementActivity.this, TimePicture.class);
            case AssistActivity.TIME_GIF:
                return new Intent(PositiveReinforcementActivity.this, TimeGif.class);
            default:
                Log.e("StaticPicture", "Error: La estretagia esta mal formada");
                return new Intent(PositiveReinforcementActivity.this, MainActivity.class);
        }
    }


}