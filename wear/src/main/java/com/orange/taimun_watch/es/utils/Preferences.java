package com.orange.taimun_watch.es.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Preferencias TicTacTea
 * Clase que administra las preferencias de la aplicacion.
 *
 * @author Javier Cuadrado Manso
 */
public class Preferences {
    //preferencia para el servicio de los sensores
    private final static String RUNNING_KEY = "isRunning";
    private final static boolean RUNNING_DEFAULT = false;
    //preferencia para comprobar si debe ejecutarse la actividad de relax
    private final static String LAUNCH_APP_KEY = "launchApp";
    private final static boolean LAUNCH_APP_DEFAULT = false;

    //preferencia para encargarse de que main conduzca a assistActivity
    private final static String ASSIST_KEY = "assist";
    private final static boolean ASSIST_DEFAULT = false;
    //Preferencia para indicar que el reloj se ha puesto a cargar
    private final static String CHARGE_WATCH_KEY = "charging";
    private final static boolean CHARGE_WATCH_DEFAULT = false;
    //Preferencia para indicar que se esta cambiando de actividad
    private final static String NEW_ACTIVITY_KEY = "newactivity";
    private final static boolean NEW_ACTIVITY_DEFAULT = false;

    //Preferencia para indicar fin de reguloacion
    private final static String END_REG_KEY = "endregulation";
    private final static boolean END_REG_DEFAULT = false;

    //Preferencia para guardar el valor de la ultima ventana de tiempo
    private final static String IS_POSITIVE_LAST_WINDOW_KEY = "positive";
    private final static boolean IS_POSITIVE_LAST_WINDOW_DEFAULT = false;

    private final static String PIN_CODE_KEY = "pinCode";
    private final static String PIN_CODE_DEFAULT = "";

    private final static String GROUP_CODE_KEY = "groupCode";
    private final static String GROUP_CODE_DEFAULT = "";

    public final static boolean DEBUG = false;

    /**
     * @param context
     * @param running
     */
    public static void setRunning(Context context, boolean running) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(RUNNING_KEY, running);
        editor.apply();
    }

    /**
     * @param context
     * @return
     */
    public static boolean getRunning(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(RUNNING_KEY, RUNNING_DEFAULT);
    }

    /**
     * @param context
     * @param open
     */
    public static void setLaunchApp(Context context, boolean open) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LAUNCH_APP_KEY, open);
        editor.apply();
    }

    /**
     * @param context
     * @return
     */
    public static boolean getLaunchApp(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(LAUNCH_APP_KEY, LAUNCH_APP_DEFAULT);
    }

    /**
     * @param context
     * @param open
     */
    public static void setAssist(Context context, boolean open) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(ASSIST_KEY, open);
        editor.apply();
    }

    /**
     * @param context
     * @return
     */
    public static boolean getAssist(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(ASSIST_KEY, ASSIST_DEFAULT);
    }


    /**
     * @param context
     * @param isCharging
     */
    public static void setCharging(Context context, boolean isCharging) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(CHARGE_WATCH_KEY, isCharging);
        editor.apply();
    }

    /**
     * @param context
     * @return
     */
    public static boolean getCharging(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(CHARGE_WATCH_KEY, CHARGE_WATCH_DEFAULT);
    }

    /**
     * @param context
     * @param newActivity
     */
    public static void setNewActivity(Context context, boolean newActivity) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(NEW_ACTIVITY_KEY, newActivity);
        editor.apply();
    }

    /**
     * @param context
     * @return
     */
    public static boolean getNewActivity(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(NEW_ACTIVITY_KEY, NEW_ACTIVITY_DEFAULT);
    }


    /**
     * @param context
     * @param newActivity
     */
    public static void setEndRegulation(Context context, boolean newActivity) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(END_REG_KEY, newActivity);
        editor.apply();
    }

    /**
     * @param context
     * @return
     */
    public static boolean getEndRegulation(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(END_REG_KEY, END_REG_DEFAULT);
    }

    /**
     * @param context
     * @param isPositive
     */
    public static void setIsPositiveLastWindow(Context context, boolean isPositive) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_POSITIVE_LAST_WINDOW_KEY, isPositive);
        editor.apply();
    }

    /**
     * @param context
     * @return
     */
    public static boolean getIsPositiveLastWindow(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(IS_POSITIVE_LAST_WINDOW_KEY, IS_POSITIVE_LAST_WINDOW_DEFAULT);
    }

    public static void setPinCode(Context context, String pin) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PIN_CODE_KEY, pin);
        editor.apply();
    }

    public static String getPinCode(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PIN_CODE_KEY, PIN_CODE_DEFAULT);
    }

    public static void setGroupCode(Context context, String pin) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(GROUP_CODE_KEY, pin);
        editor.apply();
    }

    public static String getGroupCode(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(GROUP_CODE_KEY, GROUP_CODE_DEFAULT);
    }
}