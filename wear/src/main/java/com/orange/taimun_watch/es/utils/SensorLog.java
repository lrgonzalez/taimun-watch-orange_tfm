package com.orange.taimun_watch.es.utils;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;

import com.orange.taimun_watch.es.SensoresYMedidas.Measure;
import com.orange.taimun_watch.es.SensoresYMedidas.SensorMeasures;

/**
 * Clase que administra los logs de los sensores biometricos
 *
 * @author Javier Cuadrado Manso
 */
public class SensorLog {

    /**
     * Funcion que crea las carpetas que contendran los logs de los sensores
     *
     * @param ctx Contexto
     */
    public static void checkDirectory(Context ctx) {
        File folder = ctx.getFilesDir();
        boolean check = new File(folder, "sensor").exists();
        if (!check)
            new File(folder.getAbsolutePath() + "/sensor").mkdir();
        check = new File(folder, "readedSensor").exists();
        if (!check)
            new File(folder.getAbsolutePath() + "/readedSensor").mkdir();
    }

    /**
     * Clase que vuelca las medidas del sensor en el fichero de texto
     *
     * @param ctx        Contexto
     * @param sensorName Nombre del sensor
     * @param watchId    Identificador del reloj
     * @param measures   Medidas
     */
    public static void writeSensorLog(Context ctx, String sensorName, String watchId, SensorMeasures measures) {
        String fileName = watchId + "-" + sensorName + "-" + SensorLog.makeDate() + ".txt";

        try {
            FileOutputStream fos = new FileOutputStream(new File(ctx.getFilesDir() + "/sensor", fileName), false);
            OutputStreamWriter osw = new OutputStreamWriter(fos);

            for (Measure m : measures.medidas)
                osw.write(m.toString() + "\n");
            osw.flush();
            osw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Funcion que crea un string con la fecha
     *
     * @return String con la fecha
     */
    private static String makeDate() {
        Calendar c = Calendar.getInstance();
        int month = c.get(Calendar.MONTH) + 1;
        return c.get(Calendar.HOUR_OF_DAY) + "_" + c.get(Calendar.MINUTE) + "_"
                + c.get(Calendar.SECOND) + "-" + c.get(Calendar.DAY_OF_MONTH) + "_" + month + "_"
                + c.get(Calendar.YEAR);
    }
}
