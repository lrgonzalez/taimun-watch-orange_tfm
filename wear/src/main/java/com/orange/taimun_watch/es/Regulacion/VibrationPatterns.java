package com.orange.taimun_watch.es.Regulacion;

/**
 * Devuelve el patron de vibracion elegido
 *
 * @author Javier Cuadrado Manso
 */
@Deprecated
public class VibrationPatterns {
    final public static long[] vibrationPattern (int pattern){
        switch (pattern){
            case 0: //No vibration
                long []v0 = {0,0};
                return v0;
            case 1: //short vibration
                long []v1 = {0,2000};
                return v1;
            case 2: //medium vibration
                long []v2 = {0,4000};
                return v2;
            case 3: //large vibration
                long []v3 = {0,6000};
                return v3;
            case 4: //pattern 1
                long []v4 = {0, 2000, 1000, 2000};
                return v4;
            default:
                long []vd = {0,0};
                return vd;
        }
    }
}
