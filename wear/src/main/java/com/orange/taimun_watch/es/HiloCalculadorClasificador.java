package com.orange.taimun_watch.es;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import com.orange.taimun_watch.es.SensoresYMedidas.SensorMeasures;
import com.orange.taimun_watch.es.utils.Calculos;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.Preferences;
import com.orange.taimun_watch.es.utils.RegulationCalcs;

/**
 * Hilo que realiza calculos para comprobar si el usuario necesita asistencia
 *
 * @author Javier Cuadrado Manso
 */
public class HiloCalculadorClasificador extends Thread {
    private SensorMeasures medidasAcelerometro = null;
    private SensorMeasures medidasGiroscopio = null;
    private SensorMeasures medidasPulsometro = null;
    private SensorMeasures medidasDetectorPasos = null;
    private SensorMeasures medidasContadorPasos = null;
    private Context ctx;

    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;

    private HashMap<String, Double> calc;


    /**
     * Constructor de la clase que recoje las medidas obtenidas
     *
     * @param ma Medidas del acelerometro
     * @param mg Medidas del giroscopio
     * @param mp Medidas del pulsometro
     * @param md Medidas del detector de pasos
     * @param c  Contexto
     */
    public HiloCalculadorClasificador(SensorMeasures ma, SensorMeasures mg,
                                      SensorMeasures mp, SensorMeasures md, SensorMeasures mc,
                                      Context c) {

        if(ma != null) {
            this.medidasAcelerometro = new SensorMeasures(ma.medidas);
        }

        if (mg != null) {
            this.medidasGiroscopio = new SensorMeasures(mg.medidas);
        }

        if(mp != null) {
            this.medidasPulsometro = new SensorMeasures(mp.medidas);
        }

        if (md != null) {
            this.medidasDetectorPasos = new SensorMeasures(md.medidas);
        }

        if (mc != null) {
            this.medidasContadorPasos = new SensorMeasures(mc.medidas);
        }

        this.ctx = c;

        this.calc = new HashMap<>();

        powerManager = (PowerManager) c.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "SensorService");
        wakeLock.acquire();
    }

    /**
     * Funcion que crea el HashMap con los calculos obtenidos
     * TODO: añadir medidas del contador de pasos en el caso de ser necesario.
     */
    public void doHashMapCalc() {
        SensorMeasures ma = null;
        SensorMeasures mg = null;
        SensorMeasures mp = null;
        SensorMeasures md = null;

        if (this.medidasAcelerometro != null)
            ma = new SensorMeasures(this.medidasAcelerometro.medidas);
        if (this.medidasGiroscopio != null)
            mg = new SensorMeasures(this.medidasGiroscopio.medidas);
        if (this.medidasPulsometro != null)
            mp = new SensorMeasures(this.medidasPulsometro.medidas);
        if (this.medidasDetectorPasos != null)
            md = new SensorMeasures(this.medidasDetectorPasos.medidas);


        double sum;

        calc.clear();

        /**Acelerometro**/
        if (ma != null) {
            long acelerometroMediaTiempos = Calculos.media(ma.getTimestamps(),
                    ma.getTimestamps().size());
            ArrayList<Double> acelerometroCoefRegresion = new ArrayList<>();
        
        /*Precision*/
            sum = 0.0;
            for (int ac : ma.getAccuracies())
                sum += ac;
            calc.put(RegulationCalcs.ACCELEROMETER_ACCURACY_MEAN, sum / (double) ma.getAccuracies().size());

            //Estadisticas
            calc.put(RegulationCalcs.ACCELEROMETER_X_MEAN,
                    Calculos.media(ma.getXs()));
            calc.put(RegulationCalcs.ACCELEROMETER_Y_MEAN,
                    Calculos.media(ma.getYs()));
            calc.put(RegulationCalcs.ACCELEROMETER_Z_MEAN,
                    Calculos.media(ma.getZs()));

            calc.put(RegulationCalcs.ACCELEROMETER_X_VAR, Calculos.varianza(ma.getXs(),
                    calc.get(RegulationCalcs.ACCELEROMETER_X_MEAN)));
            calc.put(RegulationCalcs.ACCELEROMETER_Y_VAR, Calculos.varianza(ma.getYs(),
                    calc.get(RegulationCalcs.ACCELEROMETER_Y_MEAN)));
            calc.put(RegulationCalcs.ACCELEROMETER_Z_VAR, Calculos.varianza(ma.getZs(),
                    calc.get(RegulationCalcs.ACCELEROMETER_Z_MEAN)));

            calc.put(RegulationCalcs.ACCELEROMETER_X_StdDvt,
                    Calculos.desviacionEstandar(calc.get(RegulationCalcs.ACCELEROMETER_X_VAR)));
            calc.put(RegulationCalcs.ACCELEROMETER_Y_StdDvt,
                    Calculos.desviacionEstandar(calc.get(RegulationCalcs.ACCELEROMETER_Y_VAR)));
            calc.put(RegulationCalcs.ACCELEROMETER_Z_StdDvt,
                    Calculos.desviacionEstandar(calc.get(RegulationCalcs.ACCELEROMETER_Z_VAR)));

            calc.put(RegulationCalcs.ACCELEROMETER_X_RMS,
                    Calculos.valorCuadraticoMedio(ma.getXs()));
            calc.put(RegulationCalcs.ACCELEROMETER_Y_RMS,
                    Calculos.valorCuadraticoMedio(ma.getYs()));
            calc.put(RegulationCalcs.ACCELEROMETER_Z_RMS,
                    Calculos.valorCuadraticoMedio(ma.getZs()));

            calc.put(RegulationCalcs.ACCELEROMETER_X_MAD,
                    Calculos.desviacionMediaAbsoluta(ma.getXs()));
            calc.put(RegulationCalcs.ACCELEROMETER_Y_MAD,
                    Calculos.desviacionMediaAbsoluta(ma.getYs()));
            calc.put(RegulationCalcs.ACCELEROMETER_Z_MAD,
                    Calculos.desviacionMediaAbsoluta(ma.getZs()));

            calc.put(RegulationCalcs.ACCELEROMETER_X_MEDIAN,
                    Calculos.mediana(ma.getXs()));
            calc.put(RegulationCalcs.ACCELEROMETER_Y_MEDIAN,
                    Calculos.mediana(ma.getYs()));
            calc.put(RegulationCalcs.ACCELEROMETER_Z_MEDIAN,
                    Calculos.mediana(ma.getZs()));

            //Estructurales
            acelerometroCoefRegresion.clear();
            acelerometroCoefRegresion = Calculos.regresionPolinomica(ma.getXs(),
                    ma.getTimestamps(),
                    RegulationCalcs.GRADO, acelerometroMediaTiempos);

            if (acelerometroCoefRegresion.size() >= 4)
                calc.put(RegulationCalcs.ACCELEROMETER_X_PR_G0,
                        acelerometroCoefRegresion.get(3));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_X_PR_G0,
                        0.0);
            if (acelerometroCoefRegresion.size() >= 3)
                calc.put(RegulationCalcs.ACCELEROMETER_X_PR_G1,
                        acelerometroCoefRegresion.get(2));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_X_PR_G1,
                        0.0);
            if (acelerometroCoefRegresion.size() >= 2)
                calc.put(RegulationCalcs.ACCELEROMETER_X_PR_G2,
                        acelerometroCoefRegresion.get(1));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_X_PR_G2,
                        0.0);
            if (acelerometroCoefRegresion.size() >= 1)
                calc.put(RegulationCalcs.ACCELEROMETER_X_PR_G3,
                        acelerometroCoefRegresion.get(0));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_X_PR_G3,
                        0.0);

            acelerometroCoefRegresion.clear();
            acelerometroCoefRegresion = Calculos.regresionPolinomica(ma.getYs(),
                    ma.getTimestamps(),
                    RegulationCalcs.GRADO, acelerometroMediaTiempos);

            if (acelerometroCoefRegresion.size() >= 4)
                calc.put(RegulationCalcs.ACCELEROMETER_Y_PR_G0,
                        acelerometroCoefRegresion.get(3));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_Y_PR_G0,
                        0.0);
            if (acelerometroCoefRegresion.size() >= 3)
                calc.put(RegulationCalcs.ACCELEROMETER_Y_PR_G1,
                        acelerometroCoefRegresion.get(2));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_Y_PR_G1,
                        0.0);
            if (acelerometroCoefRegresion.size() >= 2)
                calc.put(RegulationCalcs.ACCELEROMETER_Y_PR_G2,
                        acelerometroCoefRegresion.get(1));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_Y_PR_G2,
                        0.0);
            if (acelerometroCoefRegresion.size() >= 1)
                calc.put(RegulationCalcs.ACCELEROMETER_Y_PR_G3,
                        acelerometroCoefRegresion.get(0));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_Y_PR_G3,
                        0.0);


            acelerometroCoefRegresion.clear();
            acelerometroCoefRegresion = Calculos.regresionPolinomica(ma.getZs(),
                    ma.getTimestamps(),
                    RegulationCalcs.GRADO, acelerometroMediaTiempos);

            if (acelerometroCoefRegresion.size() >= 4)
                calc.put(RegulationCalcs.ACCELEROMETER_Z_PR_G0,
                        acelerometroCoefRegresion.get(3));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_Z_PR_G0,
                        0.0);
            if (acelerometroCoefRegresion.size() >= 3)
                calc.put(RegulationCalcs.ACCELEROMETER_Z_PR_G1,
                        acelerometroCoefRegresion.get(2));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_Z_PR_G1,
                        0.0);
            if (acelerometroCoefRegresion.size() >= 2)
                calc.put(RegulationCalcs.ACCELEROMETER_Z_PR_G2,
                        acelerometroCoefRegresion.get(1));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_Z_PR_G2,
                        0.0);
            if (acelerometroCoefRegresion.size() >= 1)
                calc.put(RegulationCalcs.ACCELEROMETER_Z_PR_G3,
                        acelerometroCoefRegresion.get(0));
            else
                calc.put(RegulationCalcs.ACCELEROMETER_Z_PR_G3,
                        0.0);

            //Transicionales
            calc.put(RegulationCalcs.ACCELEROMETER_X_TREND,
                    Calculos.tendencia(ma.getXs(),
                            ma.getTimestamps()));
            calc.put(RegulationCalcs.ACCELEROMETER_Y_TREND,
                    Calculos.tendencia(ma.getYs(),
                            ma.getTimestamps()));
            calc.put(RegulationCalcs.ACCELEROMETER_Z_TREND,
                    Calculos.tendencia(ma.getZs(),
                            ma.getTimestamps()));

            calc.put(RegulationCalcs.ACCELEROMETER_X_MOC,
                    Calculos.magnitudDeCambio(ma.getXs(),
                            ma.getTimestamps()));
            calc.put(RegulationCalcs.ACCELEROMETER_Y_MOC,
                    Calculos.magnitudDeCambio(ma.getYs(),
                            ma.getTimestamps()));
            calc.put(RegulationCalcs.ACCELEROMETER_Z_MOC,
                    Calculos.magnitudDeCambio(ma.getZs(),
                            ma.getTimestamps()));

        }


        if (mg != null) {
            /**Giroscopio**/
            long giroscopioMediaTiempos = Calculos.media(mg.getTimestamps(),
                    mg.getTimestamps().size());
            ArrayList<Double> giroscopioCoefRegresion = new ArrayList<>();
        
        /*Precision*/
            sum = 0.0;
            for (int gy : mg.getAccuracies())
                sum += gy;
            calc.put(RegulationCalcs.GYROSCOPE_ACCURACY_MEAN, sum / (double) mg.getAccuracies().size());

            //Estadisticas
            calc.put(RegulationCalcs.GYROSCOPE_X_MEAN,
                    Calculos.media(mg.getXs()));
            calc.put(RegulationCalcs.GYROSCOPE_Y_MEAN,
                    Calculos.media(mg.getYs()));
            calc.put(RegulationCalcs.GYROSCOPE_Z_MEAN,
                    Calculos.media(mg.getZs()));

            calc.put(RegulationCalcs.GYROSCOPE_X_VAR, Calculos.varianza(mg.getXs(),
                    calc.get(RegulationCalcs.GYROSCOPE_X_MEAN)));
            calc.put(RegulationCalcs.GYROSCOPE_Y_VAR, Calculos.varianza(mg.getYs(),
                    calc.get(RegulationCalcs.GYROSCOPE_Y_MEAN)));
            calc.put(RegulationCalcs.GYROSCOPE_Z_VAR, Calculos.varianza(mg.getZs(),
                    calc.get(RegulationCalcs.GYROSCOPE_Z_MEAN)));

            calc.put(RegulationCalcs.GYROSCOPE_X_StdDvt,
                    Calculos.desviacionEstandar(calc.get(RegulationCalcs.GYROSCOPE_X_VAR)));
            calc.put(RegulationCalcs.GYROSCOPE_Y_StdDvt,
                    Calculos.desviacionEstandar(calc.get(RegulationCalcs.GYROSCOPE_Y_VAR)));
            calc.put(RegulationCalcs.GYROSCOPE_Z_StdDvt,
                    Calculos.desviacionEstandar(calc.get(RegulationCalcs.GYROSCOPE_Z_VAR)));

            calc.put(RegulationCalcs.GYROSCOPE_X_RMS,
                    Calculos.valorCuadraticoMedio(mg.getXs()));
            calc.put(RegulationCalcs.GYROSCOPE_Y_RMS,
                    Calculos.valorCuadraticoMedio(mg.getYs()));
            calc.put(RegulationCalcs.GYROSCOPE_Z_RMS,
                    Calculos.valorCuadraticoMedio(mg.getZs()));

            calc.put(RegulationCalcs.GYROSCOPE_X_MAD,
                    Calculos.desviacionMediaAbsoluta(mg.getXs()));
            calc.put(RegulationCalcs.GYROSCOPE_Y_MAD,
                    Calculos.desviacionMediaAbsoluta(mg.getYs()));
            calc.put(RegulationCalcs.GYROSCOPE_Z_MAD,
                    Calculos.desviacionMediaAbsoluta(mg.getZs()));

            calc.put(RegulationCalcs.GYROSCOPE_X_MEDIAN,
                    Calculos.mediana(mg.getXs()));
            calc.put(RegulationCalcs.GYROSCOPE_Y_MEDIAN,
                    Calculos.mediana(mg.getYs()));
            calc.put(RegulationCalcs.GYROSCOPE_Z_MEDIAN,
                    Calculos.mediana(mg.getZs()));

            //Estructurales
            giroscopioCoefRegresion.clear();
            giroscopioCoefRegresion = Calculos.regresionPolinomica(mg.getXs(),
                    mg.getTimestamps(),
                    RegulationCalcs.GRADO, giroscopioMediaTiempos);

            if (giroscopioCoefRegresion.size() >= 4)
                calc.put(RegulationCalcs.GYROSCOPE_X_PR_G0,
                        giroscopioCoefRegresion.get(3));
            else
                calc.put(RegulationCalcs.GYROSCOPE_X_PR_G0,
                        0.0);
            if (giroscopioCoefRegresion.size() >= 3)
                calc.put(RegulationCalcs.GYROSCOPE_X_PR_G1,
                        giroscopioCoefRegresion.get(2));
            else
                calc.put(RegulationCalcs.GYROSCOPE_X_PR_G1,
                        0.0);
            if (giroscopioCoefRegresion.size() >= 2)
                calc.put(RegulationCalcs.GYROSCOPE_X_PR_G2,
                        giroscopioCoefRegresion.get(1));
            else
                calc.put(RegulationCalcs.GYROSCOPE_X_PR_G2,
                        0.0);
            if (giroscopioCoefRegresion.size() >= 1)
                calc.put(RegulationCalcs.GYROSCOPE_X_PR_G3,
                        giroscopioCoefRegresion.get(0));
            else
                calc.put(RegulationCalcs.GYROSCOPE_X_PR_G3,
                        0.0);

            giroscopioCoefRegresion.clear();
            giroscopioCoefRegresion = Calculos.regresionPolinomica(mg.getYs(),
                    mg.getTimestamps(),
                    RegulationCalcs.GRADO, giroscopioMediaTiempos);

            if (giroscopioCoefRegresion.size() >= 4)
                calc.put(RegulationCalcs.GYROSCOPE_Y_PR_G0,
                        giroscopioCoefRegresion.get(3));
            else
                calc.put(RegulationCalcs.GYROSCOPE_Y_PR_G0,
                        0.0);
            if (giroscopioCoefRegresion.size() >= 3)
                calc.put(RegulationCalcs.GYROSCOPE_Y_PR_G1,
                        giroscopioCoefRegresion.get(2));
            else
                calc.put(RegulationCalcs.GYROSCOPE_Y_PR_G1,
                        0.0);
            if (giroscopioCoefRegresion.size() >= 2)
                calc.put(RegulationCalcs.GYROSCOPE_Y_PR_G2,
                        giroscopioCoefRegresion.get(1));
            else
                calc.put(RegulationCalcs.GYROSCOPE_Y_PR_G2,
                        0.0);
            if (giroscopioCoefRegresion.size() >= 1)
                calc.put(RegulationCalcs.GYROSCOPE_Y_PR_G3,
                        giroscopioCoefRegresion.get(0));
            else
                calc.put(RegulationCalcs.GYROSCOPE_Y_PR_G3,
                        0.0);

            giroscopioCoefRegresion.clear();
            giroscopioCoefRegresion = Calculos.regresionPolinomica(mg.getZs(),
                    mg.getTimestamps(),
                    RegulationCalcs.GRADO, giroscopioMediaTiempos);

            if (giroscopioCoefRegresion.size() >= 4)
                calc.put(RegulationCalcs.GYROSCOPE_Z_PR_G0,
                        giroscopioCoefRegresion.get(3));
            else
                calc.put(RegulationCalcs.GYROSCOPE_Z_PR_G0,
                        0.0);
            if (giroscopioCoefRegresion.size() >= 3)
                calc.put(RegulationCalcs.GYROSCOPE_Z_PR_G1,
                        giroscopioCoefRegresion.get(2));
            else
                calc.put(RegulationCalcs.GYROSCOPE_Z_PR_G1,
                        0.0);
            if (giroscopioCoefRegresion.size() >= 2)
                calc.put(RegulationCalcs.GYROSCOPE_Z_PR_G2,
                        giroscopioCoefRegresion.get(1));
            else
                calc.put(RegulationCalcs.GYROSCOPE_Z_PR_G2,
                        0.0);
            if (giroscopioCoefRegresion.size() >= 1)
                calc.put(RegulationCalcs.GYROSCOPE_Z_PR_G3,
                        giroscopioCoefRegresion.get(0));
            else
                calc.put(RegulationCalcs.GYROSCOPE_Z_PR_G3,
                        0.0);


            //Transicionales
            calc.put(RegulationCalcs.GYROSCOPE_X_TREND,
                    Calculos.tendencia(mg.getXs(),
                            mg.getTimestamps()));
            calc.put(RegulationCalcs.GYROSCOPE_Y_TREND,
                    Calculos.tendencia(mg.getYs(),
                            mg.getTimestamps()));
            calc.put(RegulationCalcs.GYROSCOPE_Z_TREND,
                    Calculos.tendencia(mg.getZs(),
                            mg.getTimestamps()));

            calc.put(RegulationCalcs.GYROSCOPE_X_MOC,
                    Calculos.magnitudDeCambio(mg.getXs(),
                            mg.getTimestamps()));
            calc.put(RegulationCalcs.GYROSCOPE_Y_MOC,
                    Calculos.magnitudDeCambio(mg.getYs(),
                            mg.getTimestamps()));
            calc.put(RegulationCalcs.GYROSCOPE_Z_MOC,
                    Calculos.magnitudDeCambio(mg.getZs(),
                            mg.getTimestamps()));
        }

        /**Pulsometro**/
        if (mp != null) {

            long pulsometroMediaTiempos = Calculos.media(mp.getTimestamps(),
                    mp.getTimestamps().size());
            ArrayList<Double> pulsometroCoefRegresion = new ArrayList<>();
        
        /*Precision*/
            sum = 0.0;
            for (int gy : mp.getAccuracies())
                sum += gy;
            calc.put(RegulationCalcs.HEARTRATE_ACCURACY_MEAN, sum / (double) mp.getAccuracies().size());

            //Estadisticas
            calc.put(RegulationCalcs.HEARTRATE_MEAN,
                    Calculos.media(mp.getXs()));

            calc.put(RegulationCalcs.HEARTRATE_VAR, Calculos.varianza(mp.getXs(),
                    calc.get(RegulationCalcs.HEARTRATE_MEAN)));

            calc.put(RegulationCalcs.HEARTRATE_StdDvt,
                    Calculos.desviacionEstandar(calc.get(RegulationCalcs.HEARTRATE_VAR)));

            calc.put(RegulationCalcs.HEARTRATE_RMS,
                    Calculos.valorCuadraticoMedio(mp.getXs()));

            calc.put(RegulationCalcs.HEARTRATE_MAD,
                    Calculos.desviacionMediaAbsoluta(mp.getXs()));

            calc.put(RegulationCalcs.HEARTRATE_MEDIAN,
                    Calculos.mediana(mp.getXs()));

            //Estructurales
            pulsometroCoefRegresion.clear();
            pulsometroCoefRegresion = Calculos.regresionPolinomica(mp.getXs(),
                    mp.getTimestamps(),
                    RegulationCalcs.GRADO, pulsometroMediaTiempos);
            if (pulsometroCoefRegresion.size() >= 4)
                calc.put(RegulationCalcs.HEARTRATE_PR_G0,
                        pulsometroCoefRegresion.get(3));
            else
                calc.put(RegulationCalcs.HEARTRATE_PR_G0,
                        0.0);
            if (pulsometroCoefRegresion.size() >= 3)
                calc.put(RegulationCalcs.HEARTRATE_PR_G1,
                        pulsometroCoefRegresion.get(2));
            else
                calc.put(RegulationCalcs.HEARTRATE_PR_G1,
                        0.0);
            if (pulsometroCoefRegresion.size() >= 2)
                calc.put(RegulationCalcs.HEARTRATE_PR_G2,
                        pulsometroCoefRegresion.get(1));
            else
                calc.put(RegulationCalcs.HEARTRATE_PR_G2,
                        0.0);
            if (pulsometroCoefRegresion.size() >= 1)
                calc.put(RegulationCalcs.HEARTRATE_PR_G3,
                        pulsometroCoefRegresion.get(0));
            else
                calc.put(RegulationCalcs.HEARTRATE_PR_G3,
                        0.0);


            //Transicionales
            calc.put(RegulationCalcs.HEARTRATE_TREND,
                    Calculos.tendencia(mp.getXs(),
                            mp.getTimestamps()));

            calc.put(RegulationCalcs.HEARTRATE_MOC,
                    Calculos.magnitudDeCambio(mp.getXs(),
                            mp.getTimestamps()));
        }


        /**Detector de pasos**/

        if (md != null) {
            long pasosMediaTiemdos = Calculos.media(md.getTimestamps(),
                    md.getTimestamps().size());
            ArrayList<Double> pasosCoefRegresion = new ArrayList<>();

        /*Precision*/
            sum = 0.0;
            for (int gy : md.getAccuracies())
                sum += gy;
            calc.put(RegulationCalcs.STEPDETECTOR_ACCURACY_MEAN, sum / (double) md.getAccuracies().size());

            //Estadisticas
            calc.put(RegulationCalcs.STEPDETECTOR_MEAN,
                    Calculos.media(ma.getXs()));

            calc.put(RegulationCalcs.STEPDETECTOR_VAR, Calculos.varianza(ma.getXs(),
                    calc.get(RegulationCalcs.STEPDETECTOR_MEAN)));

            calc.put(RegulationCalcs.STEPDETECTOR_StdDvt,
                    Calculos.desviacionEstandar(calc.get(RegulationCalcs.STEPDETECTOR_VAR)));

            calc.put(RegulationCalcs.STEPDETECTOR_RMS,
                    Calculos.valorCuadraticoMedio(ma.getXs()));

            calc.put(RegulationCalcs.STEPDETECTOR_MAD,
                    Calculos.desviacionMediaAbsoluta(ma.getXs()));

            calc.put(RegulationCalcs.STEPDETECTOR_MEDIAN,
                    Calculos.mediana(ma.getXs()));

            //Estructurales
            pasosCoefRegresion.clear();
            pasosCoefRegresion = Calculos.regresionPolinomica(ma.getXs(),
                    ma.getTimestamps(),
                    RegulationCalcs.GRADO, pasosMediaTiemdos);

            if (pasosCoefRegresion.size() >= 4)
                calc.put(RegulationCalcs.STEPDETECTOR_PR_G0,
                        pasosCoefRegresion.get(3));
            else
                calc.put(RegulationCalcs.STEPDETECTOR_PR_G0,
                        0.0);
            if (pasosCoefRegresion.size() >= 3)
                calc.put(RegulationCalcs.STEPDETECTOR_PR_G1,
                        pasosCoefRegresion.get(2));
            else
                calc.put(RegulationCalcs.STEPDETECTOR_PR_G1,
                        0.0);
            if (pasosCoefRegresion.size() >= 2)
                calc.put(RegulationCalcs.STEPDETECTOR_PR_G2,
                        pasosCoefRegresion.get(1));
            else
                calc.put(RegulationCalcs.STEPDETECTOR_PR_G2,
                        0.0);
            if (pasosCoefRegresion.size() >= 1)
                calc.put(RegulationCalcs.STEPDETECTOR_PR_G3,
                        pasosCoefRegresion.get(0));
            else
                calc.put(RegulationCalcs.STEPDETECTOR_PR_G3,
                        0.0);


            //Transicionales
            calc.put(RegulationCalcs.STEPDETECTOR_TREND,
                    Calculos.tendencia(ma.getXs(),
                            ma.getTimestamps()));

            calc.put(RegulationCalcs.STEPDETECTOR_MOC,
                    Calculos.magnitudDeCambio(ma.getXs(),
                            ma.getTimestamps()));
        }


        /*Tiempos*/
        //calc.put(RegulationCalcs.TIME_MEAN,  acelerometroMediaTiempos);
    }


    /**
     * Hilo que realiza los calculos necesarios para llamar al clasificador
     */
    public void run() {
        double c = Calculos.media(medidasPulsometro.getXs());

        //this.doHashMapCalc();
        String str = "-------\n";
        for (int i = 0; i < medidasPulsometro.getXs().size(); i++) {
            str += medidasPulsometro.getXs().get(i) + " | ";
            if (i % 3 == 0)
                str += "\n";
        }
        str += "\n--------------\n";
        System.out.println(str);

        //Matrix beta = Calculos.regresionPolinomica(medidasAcelerometro.getXs(), medidasAcelerometro.getTimestamps(), 3);
        //ArrayList<Double> coeficientes = Calculos.regresionPolinomica(medidasAcelerometro.getXs(), medidasAcelerometro.getTimestamps(), 3);
        Log.i("MEDIA", "media: " + c);
        try {
            //Umbral: 95.0
            if (c > 75.0) {
                Preferences.setIsPositiveLastWindow(this.ctx, true);
                EventLog.addRecord(new EventRecord(EventRecord.EVALUACION_POSITIVA + " " + c));
                if (!Preferences.getAssist(this.ctx)) {
                    Preferences.setLaunchApp(this.ctx, true);

                    //Lanzamiento de la actividad de asistencia
                    /*Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    this.ctx.sendBroadcast(intent);*/
                    Intent intent = new Intent();
                    intent.setAction(MainActivity.ACTION_START_ASSIST);
                    this.ctx.sendBroadcast(intent);
                } else if (Preferences.getAssist(this.ctx) && !Preferences.getCharging(this.ctx)) {
                    Preferences.setLaunchApp(this.ctx, true);

                    //Lanzamiento de la actividad de asistencia
                    /*Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    this.ctx.sendBroadcast(intent);*/
                    Intent intent = new Intent();
                    intent.setAction(MainActivity.ACTION_START_ASSIST);
                    this.ctx.sendBroadcast(intent);
                }
            } else {
                Preferences.setIsPositiveLastWindow(this.ctx, false);
                EventLog.addRecord(new EventRecord(EventRecord.EVALUACION_NEGATIVA + " " + c));
            }
        } catch (Exception e) {
            Log.e("HiloCalcClasif", "Error : " + e.getMessage());
        } finally {
            wakeLock.release();
        }
    }
}
