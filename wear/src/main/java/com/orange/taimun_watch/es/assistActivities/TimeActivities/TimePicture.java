package com.orange.taimun_watch.es.assistActivities.TimeActivities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.orange.taimun_watch.es.MainActivity;
import com.orange.taimun_watch.es.ProgressUtils.CircleProgress;
import com.orange.taimun_watch.es.ProgressUtils.DonutProgress;
import com.orange.taimun_watch.es.ProgressUtils.SquareProgress;
import com.orange.taimun_watch.es.ProgressUtils.SquareProgressBar;
import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.Regulacion.Regulation;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.ConfirmationActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.ContentPictureActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.GifActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.MultipleSelectorActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.PositiveReinforcementActivity;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.Preferences;
import com.orange.taimun_watch.es.utils.ShapeWear;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;

/**
 * Clase que maneja la logica de las imagenes con contador
 *
 * @author Javier Cuadrado Manso
 */
public class TimePicture extends AssistActivity {

    private ImageView source = null;

    private DonutProgress donutProgress;
    private CircleProgress circleProgress;
    private SquareProgressBar squareProgressBar;
    private SquareProgress squareProgress;

    private int progress = 0;
    private Handler progressUpdater;

    private int timerType;

    private CountDownTimer cdt;

    private int timeIterator = 1;

    private int shape;

    /**
     * Funcion que carga la interfaz de la actividad
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("TimerPicture", "onCreate");

        //Vibracion de inicio.
        Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(Regulation.VIBRATION_STEP);

        /*Obtencion de la forma del reloj.*/
        ShapeWear.setOnShapeChangeListener(new ShapeWear.OnShapeChangeListener() {
            @Override
            public void shapeDetected(ShapeWear.ScreenShape screenShape) {
                //Log.d("MSA", "shapeDetected isRound:" + screenShape);
                //Log.d("MSA", "shapeDetected getShape:" + ShapeWear.getShape());
                if (screenShape == ShapeWear.ScreenShape.ROUND ||
                        screenShape == ShapeWear.ScreenShape.MOTO_ROUND)
                    shape = 0;
                else
                    shape = 1;
            }
        });
        ShapeWear.setOnSizeChangeListener(new ShapeWear.OnSizeChangeListener() {
            @Override
            public void sizeDetected(int widthPx, int heightPx) {
                //Log.d("MSA", "sizeDetected w:" + widthPx + ", h:" + heightPx);
            }
        });
        ShapeWear.initShapeWear(this);

        /*Obtencion del tipo de contador.*/
        timerType = this.regulation.getActualStrategy().getTimerType();

        /*Cargar la interfaz correspondiente.*/
        WatchViewStub stub = null;
        if (timerType == 0) {
            setContentView(R.layout.activity_picture_progressbar);
            stub = (WatchViewStub) findViewById(R.id.watch_view_stub_time);
        } else {
            setContentView(R.layout.activity_picture_fill);
            stub = (WatchViewStub) findViewById(R.id.watch_view_stub_time_fill);
            if (timerType == 1)
                progress = 100;
        }

        final WatchViewStub finalstub = stub;

        //Preparacion de la imagen
        final String src = this.regulation.getActualStrategy().getActualStep();

        /*
        Bitmap tmp = null;
        if (Preferences.DEBUG)
            tmp = super.getBitmapFromAsset(src);
        else {


        }

        if(tmp == null)
            this.source.setImageBitmap(super.getBitmapFromAsset("error.png"));

        final Bitmap bm = tmp;
        */

        //Preparacion de los contadores
        finalstub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {

                /*Obtencion del elemento de progreso*/
                if (shape == 0) {
                    if (timerType == 0)
                        donutProgress = (DonutProgress) findViewById(R.id.donut_progress);
                    else
                        circleProgress = (CircleProgress) findViewById(R.id.circle_progress);
                } else {
                    if (timerType == 0) {
                        squareProgressBar = (SquareProgressBar) findViewById(R.id.sprogressbar);
                        squareProgressBar.setWidth(10);
                        squareProgressBar.setColor("#009dff");
                    } else {
                        squareProgress = (SquareProgress) findViewById(R.id.square_progress);
                    }
                }

                /*Carga de la imagen*/
                source = (ImageView) findViewById(R.id.MainContentCPA);

                /*
                if (bm == null)
                    source.setImageResource(R.mipmap.ic_launcher);
                else
                    source.setImageBitmap(bm);
                */

                File fileRecurso = new File(getApplicationContext().getFilesDir() + "/regulation_files/" + src);

                Glide.with(getApplicationContext()).load(fileRecurso)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(source);

                /*Proceso para la actualizacion del progreso*/
                progressUpdater = new Handler();
                progressUpdater.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            final int p = progress;

                            @Override
                            public void run() {
                                if (shape == 0) {
                                    if (timerType == 0) {
                                        donutProgress.setProgress(p);
                                    } else {
                                        circleProgress.setProgress(p);
                                    }
                                } else {
                                    if (timerType == 0)
                                        squareProgressBar.setProgress(p);
                                    else {
                                        squareProgress.setProgress(p);
                                    }
                                }
                            }
                        });
                        progressUpdater.postDelayed(this, 100);
                    }
                }, 100);
            }
        });

        postExecute = new Handler();
    }

    /**
     * Funcion que inicia los contadores
     */
    @Override
    public void onResume() {
        super.onResume();

        //Vibracion de inicio.
        Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(Regulation.VIBRATION_STEP);

        /*Obtencion de la duracion del timer*/
        final long millis = this.regulation.getActualStrategy().getTime() * 1000;

        /*Inicio del timer*/
        cdt = new CountDownTimer(millis, 100) {
            public void onTick(long millisUntilFinished) {
                if (timerType == 1)
                    progress = 100 - (100 - (int) (((double) millisUntilFinished / (double) millis) * 100));
                else
                    progress = 100 - (int) (((double) millisUntilFinished / (double) millis) * 100);
                int size = regulation.getActualStrategy().getSteps().size();
                if (regulation.getActualStrategy().isGlobalTime())
                    if (timerType == 1) {
                        if (progress <= 100 - ((100 / size) * timeIterator)
                                && timeIterator < size) {
                            timeIterator++;
                            new NextStepTask().execute();
                        }
                    } else {
                        if (progress >= (100 / size) * timeIterator
                                && timeIterator < size) {
                            timeIterator++;
                            new NextStepTask().execute();
                        }
                    }
            }

            public void onFinish() {
                if (timerType == 1)
                    progress = 0;
                else
                    progress = 100;

                new NextStepTask().execute();
            }
        }.start();

    }

    /**
     * Funcion que restaura la actividad si se ha salido de ella
     */
    @Override
    public void onPause() {
        if (!Preferences.getNewActivity(getApplicationContext())) {
            EventLog.addRecord(new EventRecord(EventRecord.SALIDA_REGULACION));
            postExecute.postDelayed(new RestoreActivity(), 1000);
        }
        cdt.cancel();
        super.onPause();
    }

    /**
     * Clase privada encargada de restaurar al actividad
     */
    private class RestoreActivity implements Runnable {

        /**
         * Funcion que restaura la actividad
         */
        @Override
        public void run() {
            //Si el reloj no se esta cargando, se volvera a abrir la actividad.
            if (!Preferences.getCharging(getApplicationContext())) {
                Log.i("TimerPicture", "restarting app");
                Preferences.setLaunchApp(getApplicationContext(), false);

                Intent i = new Intent(TimePicture.this, TimePicture.class);
                //Para las cuentras atras, se reinicia la estrategia para volver a empezar la cuenta
                if (regulation.getActualStrategy().isGlobalTime()) {
                    regulation.getActualStrategy().restartStrategy();
                    regulation.getActualStrategy().nextStep();
                }

                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                //Thread.currentThread().interrupt();
                return;
            }
        }
    }

    /**
     * Funcion para anadir un evento de toque al log
     *
     * @param v View v
     */
    public void touch(View v) {
        EventLog.addRecord(new EventRecord(EventRecord.TOQUE));
    }

    /**
     * Clase privada que avanza al siguiente paso
     */
    private class NextStepTask extends AsyncTask {
        private boolean isNextStrategy = false;

        /**
         * Funcion que avanza al siguiente paso
         *
         * @param params null
         * @return null
         */
        @Override
        protected Object doInBackground(Object[] params) {
            Log.i("TimerPicture", "NextStepTask");

            touched = false;
            Intent i = null;

            //Si no tiene paso...
            if (!regulation.getActualStrategy().nextStep()) {
                isNextStrategy = true;
                //Reinicio del iterador por si la siguiente estrategia es del mismo tipo que esta
                timeIterator = 1;
                //La estrategia ha terminado y se comprueba si tiene una pregunta final
                if (regulation.getActualStrategy().getQuestion() != false) {
                    EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Pregunta"));
                    i = new Intent(TimePicture.this, ConfirmationActivity.class);
                } else if (regulation.getActualStrategy().hasReinforcement()) {
                    EventLog.addRecord(new EventRecord(EventRecord.REFUERZO_POSITIVO));
                    i = new Intent(TimePicture.this, PositiveReinforcementActivity.class);
                } else {
                    //Si era una estrategia escogida desde un selector, se pone la opcion a -1
                    //para indicar que se vuelve al array de estrategias
                    regulation.setOption(-1);
                    //Si no tiene pregunta, se pasa a la siguiente estrategia
                    //Si no tiene siguiente estrategia, la regulacion ha terminado
                    if (!regulation.nextStrategy()) {
                        EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
                        EventLog.writeEventLog(getApplicationContext(),
                                Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                        Settings.Secure.ANDROID_ID));
                        Preferences.setEndRegulation(getApplicationContext(), true);
                        i = new Intent(TimePicture.this, MainActivity.class);
                    }
                    //En caso contrario, se comprueba si esta estrategia es simple o un selector
                    else {
                        EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getActualStrategy().getName()));
                        if (regulation.isSelector()) {
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Selector"));
                            i = new Intent(TimePicture.this, MultipleSelectorActivity.class);
                        } else {
                            //Si es simple, se reinicia y se comprueba que tiene pasos
                            regulation.getActualStrategy().restartStrategy();
                            if (regulation.getActualStrategy().isCarrusel()) {
                                EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Carrusel"));
                                i = new Intent(TimePicture.this, CarruselActivity.class);
                            } else if (!regulation.getActualStrategy().nextStep()) {
                                Log.e("TimePicture", "Error: La estrategia " +
                                        regulation.getActualStrategy().getId() + " no tiene pasos");
                                i = new Intent(TimePicture.this, MainActivity.class);
                            } else {
                                //Si tiene pasos, al coger el primero se clasifica
                                i = getIntentStep();
                                if (i == null) return null;
                            }
                        }
                    }
                }
            } else {
                i = getIntentStep();
                if (i == null) return null;
            }

            Preferences.setNewActivity(getApplicationContext(), true);
            i.putExtra("regulation", regulation);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

            startActivity(i);
            finish();
            Thread.currentThread().interrupt();
            return null;
        }

        /**
         * Funcion que obtiene la actividad del siguiente paso
         *
         * @return Intent de la siguiente actividad
         */
        private Intent getIntentStep() {
            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + regulation.getActualStrategy().getActualStep()));
            String s = regulation.getActualStrategy().getActualStep();
            int time = regulation.getActualStrategy().getTime();
            switch (AssistActivity.getStepType(s, time)) {
                case AssistActivity.STATIC_GIF:
                    return new Intent(TimePicture.this, GifActivity.class);
                case AssistActivity.STATIC_PICTURE:
                    return new Intent(TimePicture.this, ContentPictureActivity.class);
                case AssistActivity.TIME_PICTURE:
                    if (isNextStrategy)
                        return new Intent(TimePicture.this, TimePicture.class);

                    //Si es un paso del mismo tipo que el actual,
                    //se cargan las fuentes y se continua sin
                    // cargar la nueva actividad

                    final Bitmap bm = BitmapFactory.decodeFile(getApplicationContext().getFilesDir() + "/regulation_files/" + s);

                    //Si no tiene un tiempo global, hay que reiniciar el contador
                    //Si lo tiene, se continua el contador en el punto en el el que se encontraba
                    final boolean isGlobalTimer = regulation.getActualStrategy().isGlobalTime();
                    long tmpMillis = 0;
                    if (!isGlobalTimer) {
                        /*tmpMillis = (regulation.getActualStrategy().getTime() * 1000)
                                / regulation.getActualStrategy().getSteps().size();*/
                        tmpMillis = regulation.getActualStrategy().getTime() * 1000;
                    }
                    final long millis = tmpMillis;


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (bm == null)
                                source.setImageResource(R.mipmap.ic_launcher);
                            else
                                source.setImageBitmap(bm);

                            if (!isNextStrategy) {
                                if (!isGlobalTimer)
                                    cdt = new CountDownTimer(millis, 100) {
                                        public void onTick(long millisUntilFinished) {
                                            if (timerType == 1)
                                                progress = 100 - (100 - (int) (((double) millisUntilFinished / (double) millis) * 100));
                                            else
                                                progress = 100 - (int) (((double) millisUntilFinished / (double) millis) * 100);
                                        }

                                        public void onFinish() {
                                            if (timerType == 1)
                                                progress = 0;
                                            else
                                                progress = 100;
                                            /*En caso de que el tiempo sea del paso, al terminar se pasa al siguiente paso*/
                                            new NextStepTask().execute();
                                        }
                                    }.start();
                            }
                        }
                    });
                    isNextStrategy = false;
                    return null;
                case AssistActivity.TIME_GIF:
                    return new Intent(TimePicture.this, TimeGif.class);
                default:
                    Log.e("TimePicture", "Error: La estretagia esta mal formada");
                    return new Intent(TimePicture.this, MainActivity.class);
            }
        }
    }
}
