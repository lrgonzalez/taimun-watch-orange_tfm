package com.orange.taimun_watch.es.SensoresYMedidas;

import android.content.Context;
import android.hardware.Sensor;

import java.util.Calendar;

/**
 * Clase que administra el detector de pasos.
 *
 * @author Javier Cuadrado Manso
 */
public class StepDetectorSensor extends ObservableSensor {
    Context context; //PARA DEBUG

    /**
     * Constructor
     *
     * @param sd  Sensor que detcta los pasos
     * @param ctx Contexto
     */
    public StepDetectorSensor(Sensor sd, Context ctx) {
        super(sd);
        this.context = ctx;
    }

    /**
     * Actualiza las medidas tomadas
     *
     * @param accuracy Precision
     * @param pasos    Numero de pasos dados
     */
    public void setNewMeasures(int accuracy, float pasos) {
        if (accuracy < 0)
            return;
        Calendar cal = Calendar.getInstance();
        this.medidasSensor.medidas.add(new Measure(cal.getTimeInMillis(), accuracy, pasos));
        setChanged();
        notifyObservers();
    }
}
