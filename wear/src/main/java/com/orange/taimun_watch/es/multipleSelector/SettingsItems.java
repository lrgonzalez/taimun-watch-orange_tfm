package com.orange.taimun_watch.es.multipleSelector;

import android.graphics.drawable.Drawable;

/**
 *
 * Clase auxiliar de los items de la lista utilizada en el selector multiple.
 *
 * Basada en el ejemplo de "Creating a Custom Wearable List View" del libro
 * "Step by Step Android Wear" de Alex HO.
 *
 * @author Javier Cuadrado Manso
 *
 */
public class SettingsItems {

    public SettingsItems(Drawable iconRes, String title) {
        this.iconRes = iconRes;
        this.title = title;
    }
    public Drawable iconRes;
    public String title;

}
