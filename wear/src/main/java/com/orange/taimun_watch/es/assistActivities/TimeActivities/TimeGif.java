package com.orange.taimun_watch.es.assistActivities.TimeActivities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;

import java.io.FileInputStream;
import java.io.IOException;

import com.orange.taimun_watch.es.MainActivity;
import com.orange.taimun_watch.es.ProgressUtils.CircleProgress;
import com.orange.taimun_watch.es.ProgressUtils.DonutProgress;
import com.orange.taimun_watch.es.ProgressUtils.SquareProgress;
import com.orange.taimun_watch.es.ProgressUtils.SquareProgressBar;
import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.Regulacion.Regulation;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.ConfirmationActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.ContentPictureActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.GifActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.MultipleSelectorActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.PositiveReinforcementActivity;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.Preferences;
import com.orange.taimun_watch.es.utils.ShapeWear;
import com.orange.taimun_watch.es.widget.GifMovieView;

/**
 * Clase que maneja la logica de la actividad de gif con tiempo
 *
 * @author Javier Cuadrado Manso
 */
public class TimeGif extends AssistActivity {

    private DonutProgress donutProgress;
    private CircleProgress circleProgress;
    private SquareProgressBar squareProgressBar;
    private SquareProgress squareProgress;

    private int progress = 0;
    private Handler progressUpdater;

    private int timerType;

    private CountDownTimer cdt;

    private int timeIterator = 1;

    private int shape;

    private FileInputStream fis;

    /**
     * Funcion que carga la interfaz de la actividad
     *
     * @param savedInstanceState Instancia del estado guardado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("TimeGif", "onCreate");

        //Obtencion de la forma del reloj
        ShapeWear.setOnShapeChangeListener(new ShapeWear.OnShapeChangeListener() {
            @Override
            public void shapeDetected(ShapeWear.ScreenShape screenShape) {
                //Log.d("MSA", "shapeDetected isRound:" + screenShape);
                //Log.d("MSA", "shapeDetected getShape:" + ShapeWear.getShape());
                if (screenShape == ShapeWear.ScreenShape.ROUND ||
                        screenShape == ShapeWear.ScreenShape.MOTO_ROUND)
                    shape = 0;
                else
                    shape = 1;
            }
        });
        ShapeWear.setOnSizeChangeListener(new ShapeWear.OnSizeChangeListener() {
            @Override
            public void sizeDetected(int widthPx, int heightPx) {
                //Log.d("MSA", "sizeDetected w:" + widthPx + ", h:" + heightPx);
            }
        });
        ShapeWear.initShapeWear(this);


        /*Obtencion del tipo de contador*/
        timerType = this.regulation.getActualStrategy().getTimerType();

        /*Cargar la interfaz correspondiente*/
        WatchViewStub stub = null;
        if (timerType == 0) {
            setContentView(R.layout.activity_gif_progressbar);
            stub = (WatchViewStub) findViewById(R.id.watch_view_stub_gif_time_pb);
        } else {
            setContentView(R.layout.activity_gif_fill);
            stub = (WatchViewStub) findViewById(R.id.watch_view_stub_gif_time_fill);
        }
        final WatchViewStub finalstub = stub;

        //Carga del gif
        String sourceName = this.regulation.getActualStrategy().getActualStep();

        Movie mv = null;
        try {
            if (Preferences.DEBUG) {
                mv = Movie.decodeStream(getApplicationContext().getAssets().open(sourceName));
            } else {
                fis = new FileInputStream(getApplicationContext().getApplicationContext().getFilesDir() + "/" + sourceName);
                mv = Movie.decodeStream(fis);
            }
        } catch (Exception e) {
            Log.e("TimeGif", "Error: El gif no se ha podido decodificar");
        }

        if(mv == null)
            try {
                mv = Movie.decodeStream(getApplicationContext().getAssets().open("error.gif"));
            } catch (IOException e) {
                e.printStackTrace();
            }

        final Movie movie = mv;

        //Preparacion de los contadores
        finalstub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {

                /*Obtencion del elemento de progreso*/
                if (shape == 0) {
                    if (timerType == 0)
                        donutProgress = (DonutProgress) findViewById(R.id.donut_progress);
                    else
                        circleProgress = (CircleProgress) findViewById(R.id.circle_progress);
                } else {
                    if (timerType == 0) {
                        squareProgressBar = (SquareProgressBar) findViewById(R.id.sprogressbar);
                        squareProgressBar.setWidth(10);
                        squareProgressBar.setColor("#009dff");
                    } else {
                        squareProgress = (SquareProgress) findViewById(R.id.square_progress);
                    }
                }

                /*Asignacion del gif al view del gif*/
                final GifMovieView gif1 = (GifMovieView) findViewById(R.id.gif1);
                gif1.setMovie(movie);


                /*Proceso para la actualizacion del progreso*/
                progressUpdater = new Handler();
                progressUpdater.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            final int p = progress;

                            @Override
                            public void run() {
                                if (shape == 0) {
                                    if (timerType == 0)
                                        donutProgress.setProgress(p);
                                    else
                                        circleProgress.setProgress(p);
                                } else {
                                    if (timerType == 0)
                                        squareProgressBar.setProgress(p);
                                    else {
                                        squareProgress.setProgress(p);
                                    }
                                }
                            }
                        });
                        progressUpdater.postDelayed(this, 100);
                    }
                }, 100);
            }
        });

        postExecute = new Handler();
    }

    /**
     * Funcion que inicia los contadores
     */
    @Override
    public void onResume() {
        super.onResume();
        /*Obtencion de la duracion del timer*/
        final long millis = this.regulation.getActualStrategy().getTime() * 1000;

        /*Inicio del timer*/
        cdt = new CountDownTimer(millis, 100) {
            public void onTick(long millisUntilFinished) {
                if (timerType == 2)
                    progress = 100 - (100 - (int) (((double) millisUntilFinished / (double) millis) * 100));
                else
                    progress = 100 - (int) (((double) millisUntilFinished / (double) millis) * 100);
                int size = regulation.getActualStrategy().getSteps().size();
                if (regulation.getActualStrategy().isGlobalTime())
                    if (progress >= (100 / size) * timeIterator
                            && timeIterator < size) {
                        timeIterator++;
                        new NextStepTask().execute();
                    }
            }

            public void onFinish() {
                if (timerType == 2)
                    progress = 0;
                else
                    progress = 100;

                new NextStepTask().execute();
            }
        }.start();
    }

    /**
     * Funcion que restaura la actividad en caso de que se salga de esta
     */
    @Override
    public void onPause() {
        if (!Preferences.getNewActivity(getApplicationContext())) {
            EventLog.addRecord(new EventRecord(EventRecord.SALIDA_REGULACION));
            postExecute.postDelayed(new RestoreActivity(), 1000);
        }
        if (!Preferences.DEBUG)
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        cdt.cancel();
        super.onPause();
    }

    /**
     * Clase privada que restaura la actividad
     */
    private class RestoreActivity implements Runnable {

        @Override
        public void run() {
            //Si el reloj no se esta cargando volvera a abrir la actividad.
            if (!Preferences.getCharging(getApplicationContext())) {
                Log.i("TimeGif", "restarting app");
                Preferences.setLaunchApp(getApplicationContext(), false);

                Intent i = new Intent(TimeGif.this, TimeGif.class);
                //Para las cuentras atras, se reinicia la estrategia para volver a empezar la cuenta
                if (regulation.getActualStrategy().isGlobalTime()) {
                    regulation.getActualStrategy().restartStrategy();
                    regulation.getActualStrategy().nextStep();
                }

                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                //Thread.currentThread().interrupt();
                return;
            }
        }
    }

    public void touch(View v) {
        EventLog.addRecord(new EventRecord(EventRecord.TOQUE));
    }

    /**
     * Clase privada encargada de avnzar al paso siguiente
     */
    private class NextStepTask extends AsyncTask {
        private boolean isNextStrategy = false;

        /**
         * Funcion que avanza al paso siguiente
         *
         * @param params null
         * @return null
         */
        @Override
        protected Object doInBackground(Object[] params) {
            Log.i("TimeGif", "NextStepTask");

            touched = false;
            Intent i = null;

            //Si no tiene paso...
            if (!regulation.getActualStrategy().nextStep()) {
                isNextStrategy = true;
                //Reinicio del iterador por si la siguiente estrategia es del mismo tipo que esta
                timeIterator = 1;
                //...La estrategia ha terminado y se comprueba si tiene una pregunta final
                if (regulation.getActualStrategy().getQuestion() != false) {
                    EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Pregunta"));
                    i = new Intent(TimeGif.this, ConfirmationActivity.class);
                } else if (regulation.getActualStrategy().hasReinforcement()) {
                    EventLog.addRecord(new EventRecord(EventRecord.REFUERZO_POSITIVO));
                    i = new Intent(TimeGif.this, PositiveReinforcementActivity.class);
                } else {
                    //Si era una estrategia escogida desde un selector, se pone la opcion a -1
                    //para indicar que se vuelve al array de estrategias
                    regulation.setOption(-1);
                    //Si no tiene pregunta, se pasa a la siguiente estrategia
                    //Si no tiene siguiente estrategia, la regulacion ha terminado
                    if (!regulation.nextStrategy()) {
                        EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
                        EventLog.writeEventLog(getApplicationContext(),
                                Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                        Settings.Secure.ANDROID_ID));
                        Preferences.setEndRegulation(getApplicationContext(), true);
                        i = new Intent(TimeGif.this, MainActivity.class);
                    }
                    //En caso contrario, se comprueba si esta estrategia es simple o un selector
                    else {
                        EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getActualStrategy().getName()));
                        if (regulation.isSelector()) {
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Selector"));
                            i = new Intent(TimeGif.this, MultipleSelectorActivity.class);
                        } else {
                            //Si es simple, se reinicia y se comprueba que tiene pasos
                            regulation.getActualStrategy().restartStrategy();
                            if (regulation.getActualStrategy().isCarrusel()) {
                                EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Carrusel"));
                                i = new Intent(TimeGif.this, CarruselActivity.class);
                            } else if (!regulation.getActualStrategy().nextStep()) {
                                Log.e("TimeGif", "Error: La estrategia " +
                                        regulation.getActualStrategy().getId() + " no tiene pasos");
                                i = new Intent(TimeGif.this, MainActivity.class);
                            } else {
                                //Si tiene pasos, al coger el primero se clasifica
                                i = getIntentStep();
                                if (i == null) {
                                    if (!Preferences.DEBUG)
                                        try {
                                            fis.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    return null;
                                }
                            }
                        }
                    }
                }
            } else {
                i = getIntentStep();
                if (i == null) {
                    if (!Preferences.DEBUG)
                        try {
                            fis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    return null;
                }
            }
            if (!Preferences.DEBUG)
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            Preferences.setNewActivity(getApplicationContext(), true);
            i.putExtra("regulation", regulation);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //Vibra al terminar.
            Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(Regulation.VIBRATION_STEP);
            startActivity(i);
            finish();
            Thread.currentThread().interrupt();
            return null;
        }

        /**
         * Funcion que devuelve la actividad a la que se quiere avanzar
         *
         * @return Intent de la siguiente actividad
         */
        private Intent getIntentStep() {
            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + regulation.getActualStrategy().getActualStep()));
            String s = regulation.getActualStrategy().getActualStep();
            int time = regulation.getActualStrategy().getTime();
            switch (AssistActivity.getStepType(s, time)) {
                case AssistActivity.STATIC_GIF:
                    return new Intent(TimeGif.this, GifActivity.class);
                case AssistActivity.STATIC_PICTURE:
                    return new Intent(TimeGif.this, ContentPictureActivity.class);
                case AssistActivity.TIME_PICTURE:
                    return new Intent(TimeGif.this, TimePicture.class);
                case AssistActivity.TIME_GIF:
                    if (isNextStrategy)
                        return new Intent(TimeGif.this, TimeGif.class);

                    //Si es un paso del mismo tipo que el actual,
                    //se cargan las fuentes y se continua sin
                    // caragr nueva actividad
                    final boolean isGlobalTimer = regulation.getActualStrategy().isGlobalTime();
                    long tmpMillis = 0;
                    if (!isGlobalTimer) {
                    /*tmpMillis = (regulation.getActualStrategy().getTime() * 1000)
                            / regulation.getActualStrategy().getSteps().size();*/
                        tmpMillis = regulation.getActualStrategy().getTime() * 1000;
                    }
                    final long millis = tmpMillis;

                    final String sourceName = s;
                    final GifMovieView gif1 = (GifMovieView) findViewById(R.id.gif1);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                final Movie mv = Movie.decodeStream(getApplicationContext().getAssets().open(sourceName));
                                gif1.setMovie(mv);
                            } catch (IOException e) {
                                //e.printStackTrace();
                                Log.e("TimeGif", "Error: El gif no se ha podido decodificar");
                            }
                            if (!isNextStrategy) {
                                if (!isGlobalTimer)
                                    cdt = new CountDownTimer(millis, 100) {
                                        public void onTick(long millisUntilFinished) {
                                            if (timerType == 2)
                                                progress = 100 - (100 - (int) (((double) millisUntilFinished / (double) millis) * 100));
                                            else
                                                progress = 100 - (int) (((double) millisUntilFinished / (double) millis) * 100);
                                        }

                                        public void onFinish() {
                                            if (timerType == 2)
                                                progress = 0;
                                            else
                                                progress = 100;
                                            /*En caso de que el tiempo sea del paso, al terminar se pasa al siguiente paso*/
                                            //if (!regulation.getActualStrategy().isGlobalTime())
                                            new NextStepTask().execute();
                                        }
                                    }.start();
                            }
                        }
                    });
                    isNextStrategy = false;
                    return null;
                default:
                    Log.e("TimeGif", "Error: La estretagia esta mal formada");
                    return new Intent(TimeGif.this, MainActivity.class);
            }
        }
    }
}
