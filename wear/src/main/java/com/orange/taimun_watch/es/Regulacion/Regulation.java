package com.orange.taimun_watch.es.Regulacion;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Clase que maneja la funcionalidad de una regulacion
 *
 * @author Javier Cuadrado Manso
 */
public class Regulation implements Parcelable {
    public static final long VIBRATION_TIME = 2000;
    public static final long VIBRATION_STEP = 500;

    private final int id;
    private String name;
    private boolean vibration = false;
    private String audio = "";
    private ArrayList<ArrayList<Integer>> schedule;
    private HashMap<Integer, Strategy> strategies;

    private int actualStrategy = -1;
    private int option = -1;

    /**
     * Constructor de la regulacion
     *
     * @param identificador Id de la regulacion
     */
    public Regulation(int identificador) {
        this.id = identificador;
        this.schedule = new ArrayList<>();
        ArrayList<Integer> arr = new ArrayList<>();
        this.schedule.add(arr);
        this.strategies = new HashMap<>();
        restartRegulation();
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String n) {
        this.name = n;
    }

    public boolean getVibration() {
        return vibration;
    }

    public void setVibration(boolean vibration) {
        this.vibration = vibration;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public void addNewStrategyId(int id) {
        ArrayList<Integer> lst = new ArrayList<>();
        lst.add(id);
        this.schedule.add(lst);
    }

    public void addNewSelector(ArrayList<Integer> selector) {
        this.schedule.add(selector);
    }

    public ArrayList<ArrayList<Integer>> getSchedule() {
        return schedule;
    }

    public void setSchedule(ArrayList<ArrayList<Integer>> schedule) {
        this.schedule = schedule;
    }

    /**
     * Funcion que inserta una nueva estrategia si esta no se ha insertado antes
     *
     * @param s
     * @return
     */
    public boolean addStrategy(Strategy s) {
        if (this.strategies.containsKey(s.getId()))
            return false;
        this.strategies.put(s.getId(), s);
        return true;
    }

    public Strategy getStrategy(int id) {
        return this.strategies.get(id);
    }

    public HashMap<Integer, Strategy> getStrategies() {
        return strategies;
    }

    public void setStrategies(HashMap<Integer, Strategy> strategies) {
        this.strategies = strategies;
    }

    public void setActualStrategy(int actualStrategy) {
        this.actualStrategy = actualStrategy;
    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }

    public void restartRegulation() {
        this.actualStrategy = -1;
    }

    /**
     * Funcion que indica si la estrategia actual es un selector
     *
     * @return true si es selector, false si no lo es
     */
    public boolean isSelector() {
        if (this.actualStrategy >= this.schedule.size())
            return false;
        if (this.schedule.get(this.actualStrategy).size() > 1)
            return true;
        return false;
    }


    /**
     * Funcion que avanza en el array de estrategias
     *
     * @return true si hay una estrategia siguiente o false si termino la regulacion
     */
    public boolean nextStrategy() {
        this.actualStrategy++;
        return this.actualStrategy < this.schedule.size();
    }

    /**
     * Funcion que devuelve la estrategia actual. Si es un selector se devuelve la estartegia
     * escogida en el selector
     *
     * @return La estrategia actual
     */
    public Strategy  getActualStrategy() {
        if (option == -1)
            return this.strategies.get(this.schedule.get(this.actualStrategy).get(0));
        else
            return this.strategies.get(option);
    }

    public ArrayList<Integer> getSelectorStrategies() {
        return this.schedule.get(this.actualStrategy);
    }

    @Override
    public String toString() {
        String regulationString = new String();
        regulationString += "{\nid: " + id + "\n";
        regulationString += "name: " + name + "\n";
        regulationString += "vibration: " + vibration + "\n";
        regulationString += "audio: " + audio + "\n";
        regulationString += "schedule: " + schedule + "\n";
        regulationString += "strategies: [";
        for (int id : strategies.keySet())
            regulationString += "\t" + this.getStrategy(id) + "\n";
        regulationString += "]" + "\n";
        regulationString += "ActualStrategy: " + actualStrategy + "\n";
        regulationString += "}";
        return regulationString;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Regulation)) return false;
        Regulation r = (Regulation) object;
        return this.id == r.id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeByte((byte) (vibration ? 1 : 0));
        dest.writeString(audio);
        dest.writeSerializable(schedule);
        dest.writeInt(strategies.keySet().size());
        for (int key : strategies.keySet())
            dest.writeParcelable(strategies.get(key), 0);
        dest.writeInt(actualStrategy);
        dest.writeInt(option);
    }

    public static final Parcelable.Creator<Regulation> CREATOR = new Creator<Regulation>() {
        public Regulation createFromParcel(Parcel source) {
            Regulation r = new Regulation(source.readInt());
            r.setName(source.readString());
            r.setVibration(source.readByte() != 0);
            r.setAudio(source.readString());
            r.setSchedule((ArrayList<ArrayList<Integer>>) source.readSerializable());
            int size = source.readInt();
            HashMap<Integer, Strategy> strs = new HashMap<>();
            for (int i = 0; i < size; i++) {
                Strategy s = source.readParcelable(Strategy.class.getClassLoader());
                strs.put(s.getId(), s);
            }
            r.setStrategies(strs);
            r.setActualStrategy(source.readInt());
            r.setOption(source.readInt());
            return r;
        }

        public Regulation[] newArray(int size) {
            return new Regulation[size];
        }
    };
}
