package com.orange.taimun_watch.es;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

import com.orange.taimun_watch.es.R;

import com.orange.taimun_watch.es.Regulacion.Regulation;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.ContentPictureActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.GifActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.MultipleSelectorActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.CarruselActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimeGif;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimePicture;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.JSONToRegulation;
import com.orange.taimun_watch.es.utils.Preferences;
import com.orange.taimun_watch.es.utils.SensorLog;

/**
 * Clase principal que se encarga de iniciar el servicio de toma de datos
 * y las actividades de asistencia si se detecta estress
 *
 * @author Javier Cuadrado Manso
 */
public class MainActivity extends Activity implements View.OnTouchListener, Runnable {
    private Handler handler = new Handler();

    private Runnable run;
    private boolean enableButton;
    private SensorManager mSensorManager;

    private boolean imprimirJSON = false;

    protected static Regulation regulation;

    private static String watchId;

    static final String ACTION_START_ASSIST = "com.orange.taimun_watch.es.START_ASSIST";

    public static MainActivity ma;

    /**
     * Funcion que inicializa las variablesy carga la interfaz de la actividad.
     *
     * @param savedInstanceState estado actual de la app
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity", "onCreate");
        SensorLog.checkDirectory(getApplicationContext());
        EventLog.checkDirectory(getApplicationContext());
        EventLog.initRecord();

        File folder = getApplicationContext().getFilesDir();
        boolean check = new File(folder, "regulation_files").exists();
        if (!check)
            new File(folder.getAbsolutePath() + "/regulation_files").mkdir();

        //Carpeta regulation files.
        File reg = new File(folder.getAbsolutePath() + "/regulation_files");

        Log.d("WFILES", "-Carpeta " + reg.getAbsolutePath() + "-");
        File[] fl = reg.listFiles();

        if(fl.length == 0) {
            Log.d("WFILES", "La carpeta regulation_files esta vacía.");
        } else {
            for(File f : fl) {
                Log.d("WFILES", f.getName());
            }
        }

        new EventLog();
        this.watchId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);


        //Lectura del JSON recibido, para cargar la regulacion en memoria
        //Si no hay json porque se acaba de instalar la app y no esta en modo depuración,
        //fallará y esperará a que se pulse el boton start
        Log.i("WEAR", "Leemos el JSON recibido.");
        new JSONTask().execute();

        //Obtencion del boton para asignar su evento onTouch
        Button btn = (Button) findViewById(R.id.Button02);
        btn.setOnTouchListener(this);

        //Activacion del proceso retardado y del boton
        this.run = this;
        this.enableButton = true;

        //Si la aplicacion ya estaba corriendo, se cambia el boton a Stop
        if (Preferences.getRunning(this.getApplicationContext())) {
            btn.setText("STOP");
            btn.setBackgroundResource(R.drawable.rounded_button_red);
            SensorAdmin.resetTimeWindow();
        }

        mSensorManager = (SensorManager) getApplicationContext().getSystemService(Context.SENSOR_SERVICE);

        //Obtencion del texto en el que se mostrara el identificador del reloj
        /*TextView tv = (TextView) findViewById(R.id.identificador);
        tv.setText(Secure.getString(this.getApplicationContext().getContentResolver(),
                Secure.ANDROID_ID));*/

    }

    /**
     * Funcion encargada de recibir los eventos causados por los toques sobre la pantalla
     * Si el boton muestra Start, con un toque iniciara el servicio de mediciones
     * Si el boton muestra Stop, con un toque de 10 segundos se detendra el servicio de mediciones
     *
     * @param v View del boton
     * @param m Evento de movimiento
     * @return false
     */
    @Override
    public boolean onTouch(View v, MotionEvent m) {

        Button btn = (Button) findViewById(R.id.Button02);
        ComponentName component;
        //Caso en el que el boton muestre Stop
        if (btn.getText().toString().compareTo("STOP") == 0) {
            switch (m.getAction()) {
                //En caso de que se pulse el boton, se inicia ua cuenta hasta 10 segundos
                //Al terminar la cuenta se inicia un hilo que detendra las mediciones
                case MotionEvent.ACTION_DOWN:
                    Log.i("Boton", "down on stop");

                    //DEBUG
                    //Log.i("ASISTENCIA", "Iniciamos la asistencia.");
                    //Intent intent = new Intent();
                    //intent.setAction(MainActivity.ACTION_START_ASSIST);
                    //sendBroadcast(intent);

                    EventLog.addRecord(new EventRecord(EventRecord.TOQUE));
                    //if (!this.enableButton) break;
                    this.enableButton = false;
                    handler.post(run);
                    component = new ComponentName(MainActivity.this, StartAssistReceiver.class);
                    getPackageManager().setComponentEnabledSetting(component,
                            PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                    break;

                //Si se mueve la pantalla para salir de la app, se cancela la cuenta
                case MotionEvent.ACTION_CANCEL:
                    this.enableButton = true;
                    handler.removeCallbacks(run);
                    component = new ComponentName(MainActivity.this, StartAssistReceiver.class);
                    getPackageManager().setComponentEnabledSetting(component,
                            PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                    break;

                //Si se levanta el dedo de la pantalla, se cancela la cuenta
                case MotionEvent.ACTION_UP:
                    this.enableButton = true;
                    handler.removeCallbacks(run);
                    component = new ComponentName(MainActivity.this, StartAssistReceiver.class);
                    getPackageManager().setComponentEnabledSetting(component,
                            PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                    break;
            }
            //Caso en el que se muestra el boton Start
        } else {
            switch (m.getAction()) {
                //Al levantar el dedo de la pantalla, se inicia el servicio de mediciones
                //Se inicia el servicio al levantar el dedo y no al pulsar el boton para evitar
                //que se inicie el servicio nada mas cancelarlo
                case MotionEvent.ACTION_UP:
                    //if (regulation == null) {
                    String str = JSONToRegulation.readFile(getApplicationContext());
                    if (str == null) {
                        Toast.makeText(getApplicationContext(), getString(R.string.noJSON), Toast.LENGTH_SHORT).show();
                        break;
                    }

                    //Si el Smartwatch no tiene sensor de ritmo cardiaco no se inicia el servicio.
                    if(mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE) == null) {
                        Toast.makeText(getApplicationContext(), R.string.noHeartSensor, Toast.LENGTH_SHORT).show();
                        break;
                    }

                    regulation = JSONToRegulation.parseJSON(str);
                    Log.d("MainActivity", regulation.toString());

                    if (!this.enableButton) break;
                    this.enableButton = true;
                    btn.setText("STOP");

                    btn.setBackgroundResource(R.drawable.rounded_button_red);
                    Preferences.setIsPositiveLastWindow(getApplicationContext(), false);
                    Intent i = new Intent(this, SensorService.class);
                    startService(i);
                    Preferences.setRunning(this.getApplicationContext(), true);
                    break;

                case MotionEvent.ACTION_DOWN:
                    EventLog.addRecord(new EventRecord(EventRecord.TOQUE));
                    this.enableButton = true;
                    component = new ComponentName(MainActivity.this, StartAssistReceiver.class);
                    getPackageManager().setComponentEnabledSetting(component,
                            PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                    break;

                case MotionEvent.ACTION_CANCEL:
                    this.enableButton = true;
                    break;
            }

        }
        return false;
    }

    /**
     * Proceso secundario que termina el hilo de mediciones
     */
    @Override
    public void run() {
        //Vibracion del dispositivo para indicar que se han detenido las mediciones
        Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(750);

        //Cambio del boton a Start
        Button btn = (Button) findViewById(R.id.Button02);
        btn.setText("START");
        btn.setBackgroundResource(R.drawable.rounded_button_green);

        //Detencion del servicio de mediciones
        Intent i = new Intent(this, SensorService.class);
        stopService(i);

        //Modificacion de las preferencias para indicar que el proceso ya no esta corriendo
        Preferences.setRunning(this.getApplicationContext(), false);
    }


    /**
     * Funcion que inicia la apariencia del boton en funcion de el estado de la asistencia
     */
    @Override
    public void onResume() {
        super.onResume();
        Log.i("MainActivity", "onResume");

        //Reinicio de los flags
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Preferences.setCharging(getApplicationContext(), false);
        Preferences.setAssist(getApplicationContext(), false);


        Button btn = (Button) findViewById(R.id.Button02);

        if (Preferences.getRunning(getApplicationContext())) {
            //this.enableButton = false;
            //Cambio del boton a Stop
            btn.setText("STOP");
            btn.setBackgroundResource(R.drawable.rounded_button_red);
        } else {
            this.enableButton = true;
            //Cambio del boton a Start
            btn.setText("START");
            btn.setBackgroundResource(R.drawable.rounded_button_green);
        }

        new EventLog();

        if (Preferences.getEndRegulation(getApplicationContext())) {
            Preferences.setEndRegulation(getApplicationContext(), false);
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        } else {
            ma = this;
        }
    }


    @Override
    public void onStop() {
        if (!Preferences.getNewActivity(getApplicationContext())) {
            EventLog.addRecord(new EventRecord(EventRecord.APLICACION_SUSPENDIDA));
            EventLog.writeEventLog(getApplicationContext(), watchId);
        }
        super.onStop();
    }


    /**
     * Clase estatica que se encarga de iniciar la asistencia
     */
    public static class StartAssistReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("SAR", "onReceive");
            new StartAssist().execute(context);
        }
    }


    /**
     * Clase privada que inicia la asistecia
     */
    private static class StartAssist extends AsyncTask {
        /**
         * @param params Context
         * @return null
         */
        @Override
        protected Object doInBackground(Object[] params) {
            Context ctx = (Context) params[0];
            if (Preferences.getAssist(ctx)) {
                Log.e("MAIN", "La regulacion se esta ejecutando");
                return null;
            }

            EventLog.addRecord(new EventRecord(EventRecord.INICIO_REGULACION + " " + regulation.getName() + " " + regulation.getId()));
            EventLog.writeEventLog(ctx, watchId);

            Preferences.setNewActivity(ctx, true);
            Preferences.setLaunchApp(ctx, false);

            //Vibracion inicial del dispositivo
            if (regulation.getVibration())
                try {
                    Vibrator v = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
                    v.vibrate(Regulation.VIBRATION_TIME);
                } catch (Exception e) {
                    Log.e("WEAR", "No hay vibrador");
                }

            //Inicio de la asistencia
            Intent i = null;

            //Avance a la primera estrategia
            regulation.restartRegulation();

            //Si no tiene una primera estrategia, esta mal formada
            if (!regulation.nextStrategy()) {
                Log.e("MainActivity", "Error: La regulacion no tiene estrategias");
            } else {


                //Si es un selector, se carga la actividad del selector
                if (regulation.isSelector()) {
                    EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + "Selector"));
                    i = new Intent(ma, MultipleSelectorActivity.class);
                }
                //Si no lo es, se carga el primer paso de la estrategia o la estartegia entera si es carrusel
                else {
                    regulation.getActualStrategy().restartStrategy();
                    EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getActualStrategy().getName()));
                    if (regulation.getActualStrategy().isCarrusel()) {
                        EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Carrusel"));
                        i = new Intent(ma, CarruselActivity.class);
                    } else if (!regulation.getActualStrategy().nextStep()) {
                        Log.e("MainActivity", "Error: La estrategia " +
                                regulation.getActualStrategy().getId() +
                                " no tiene pasos");
                        ma.finish();
                        Thread.currentThread().interrupt();
                    } else {
                        i = getIntentStep();
                        if (i == null) return null;
                    }

                }
                Preferences.setAssist(ctx, true);
                i.putExtra("regulation", regulation);
                i.putExtra("option", -1);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ctx.startActivity(i);
                ma.finish();
                Thread.currentThread().interrupt();
            }

            return null;
        }

        /**
         * funcion que devuelve el tipo de actividad
         *
         * @return Intent de la siguiente actividad
         */
        private Intent getIntentStep() {
            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + regulation.getActualStrategy().getActualStep()));
            String s = regulation.getActualStrategy().getActualStep();
            Log.d("ASSIST", "Actual step: " + s);
            int time = regulation.getActualStrategy().getTime();
            switch (AssistActivity.getStepType(s, time)) {
                case AssistActivity.STATIC_GIF:
                    return new Intent(ma, GifActivity.class);
                case AssistActivity.STATIC_PICTURE:
                    return new Intent(ma, ContentPictureActivity.class);
                case AssistActivity.TIME_PICTURE:
                    return new Intent(ma, TimePicture.class);
                case AssistActivity.TIME_GIF:
                    return new Intent(ma, TimeGif.class);
                default:
                    Log.e("MainActivity", "Error: La estretagia esta mal formada");
                    return null;
            }
        }
    }

    /**
     * Clase privada que carga la regulacion del JSON en memoria
     */
    public class JSONTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            String str = JSONToRegulation.readFile(getApplicationContext());

            if (str == null) {
                return null;
            }

            Log.i("WEAR", "Imprimimos la regulacion recibida.");
            Log.d("WEAR", str);


            Log.i("WEAR", "Establecemos la regulacion leida.");
            regulation = JSONToRegulation.parseJSON(str);

           /*Locale myLocale = new Locale("ca");
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);*/

            Log.i("MainActivity", "Regulation: " + regulation.toString());
            //System.out.println(regulation);
            return null;
        }
    }

}
