package com.orange.taimun_watch.es.SensoresYMedidas;

import android.content.Context;
import android.hardware.Sensor;

import java.util.Calendar;

/**
 * Clase que gestiona lo relacionado con el sensor cardiaco
 *
 * @author Javier Cuadrado Manso
 */
public class HeartRateSensor extends ObservableSensor {
    Context context; //PARA DEBUG

    /**
     * Constructor de la clase
     *
     * @param heartRateSensor Sensor de pulso
     * @param ctx Contexto
     */
    public HeartRateSensor(Sensor heartRateSensor, Context ctx) {
        super(heartRateSensor);
        this.context = ctx; //PARA DEBUG
    }

    /**
     * Actualiza las medidas tomadas
     *
     * @param accuracy precision
     * @param rate     medida de las pulsaciones
     */
    public void setNewMeasures(int accuracy, float rate) {
        if(accuracy < 0)
            return;
        Calendar cal = Calendar.getInstance();
        this.medidasSensor.medidas.add(new Measure(cal.getTimeInMillis(), accuracy, rate));
        setChanged();
        notifyObservers();
    }

}
