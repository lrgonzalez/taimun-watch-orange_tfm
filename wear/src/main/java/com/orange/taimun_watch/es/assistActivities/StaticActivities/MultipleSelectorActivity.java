package com.orange.taimun_watch.es.assistActivities.StaticActivities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.orange.taimun_watch.es.MainActivity;
import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.CarruselActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimeGif;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimePicture;
import com.orange.taimun_watch.es.multipleSelector.SettingsAdapter;
import com.orange.taimun_watch.es.multipleSelector.SettingsItems;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.Preferences;


/**
 * Clase que administra la logica de la actividad de selector multiple
 *
 * @author Javier Cuadrado Manso
 */
public class MultipleSelectorActivity extends AssistActivity implements WearableListView.ClickListener {

    private WearableListView listView;
    private TextView mHeader;

    //private float mDefaultCircleRadius;
    //private float mSelectedCircleRadius;

    private int option = -1;

    private CountDownTimer reVibrate = null;

    /**
     * Funcion que carga la interfaz de un selector multiple segun la regulacion
     *
     * @param savedInstanceState Instancia del estado guardado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multipleselector);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //mDefaultCircleRadius = getResources().getDimension(R.dimen.default_settings_circle_radius);
        ///mSelectedCircleRadius = getResources().getDimension(R.dimen.selected_settings_circle_radius);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                listView = (WearableListView) stub.findViewById(R.id.sample_list_view);
                listView.addOnScrollListener(mOnScrollListener);
                loadAdapter();
                mHeader = (TextView) findViewById(R.id.header);
                mHeader.setText(getResources().getString(R.string.textoSelector));
            }
        });

        postExecute = new Handler();

        this.reVibrate = new CountDownTimer(millisToVibrate, millisToVibrate) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(4000);
                this.start();
            }
        }.start();


    }

    /**
     * Carga del selector multiple
     */
    private void loadAdapter() {
        List<SettingsItems> items = new ArrayList<>();

        ArrayList<Integer> strategies = this.regulation.getSelectorStrategies();
        int numSources = strategies.size();

        //Carga de imagenes de las estrategias
        for (int i = 0; i < numSources; i++) {
            int strategy = strategies.get(i);
            Bitmap bm = null;
            if (strategy == -1) {
                bm = super.getBitmapFromAsset("terminar.png");
            } else {
                String icon = this.regulation.getStrategies().get(strategy).getIcon();
                Log.d("DEBUG", "ICONO: " + icon);
                if(!icon.equals("null"))
                    bm = BitmapFactory.decodeFile(getApplicationContext().getFilesDir() + "/regulation_files/" + icon + ".png");
            }

            Drawable drawable;
            if (bm == null)
                drawable = new BitmapDrawable(getResources(), super.getBitmapFromAsset("icon_estrategia.png"));
            else
                drawable = new BitmapDrawable(getResources(), bm);
            String text;
            if (strategy == -1)
                text = getResources().getString(R.string.terminarSelector);
            else
                text = this.regulation.getStrategies().get(strategy).getName();
            items.add(new SettingsItems(drawable, text));
        }

        SettingsAdapter mAdapter = new SettingsAdapter(this, items);

        listView.setAdapter(mAdapter);
        listView.setClickListener(this);
    }

    /**
     * Funcion que obtiene la posicion pulsada y ejecuta la tarea para avanzar a la siguiente estrategia
     *
     * @param viewHolder
     */
    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {
        EventLog.addRecord(new EventRecord(EventRecord.TOQUE));
        if (!this.allowTouch)
            return;
        touched = true;
        option = viewHolder.getPosition();
        this.allowTouch = false;
        this.reVibrate.cancel();
        new NextStepTask().execute();

    }

    /**
     * Funcion que muestra un toast cuando se pulsa en la cabecera superior
     */
    @Override
    public void onTopEmptyRegionClick() {
        EventLog.addRecord(new EventRecord(EventRecord.TOQUE));
        //Toast.makeText(this, "Elije una opción", Toast.LENGTH_SHORT).show();
    }

    /**
     * Listener del scroll, para mover la cabecera y que no se quede flotando encima de la lista
     */
    private WearableListView.OnScrollListener mOnScrollListener =
            new WearableListView.OnScrollListener() {
                @Override
                public void onAbsoluteScrollChange(int i) {
                    if (i > 0) {
                        mHeader.setY(-i);
                    }
                }

                @Override
                public void onScroll(int i) {
                }

                @Override
                public void onScrollStateChanged(int i) {
                    if (i == RecyclerView.SCROLL_STATE_DRAGGING)
                        EventLog.addRecord(new EventRecord(EventRecord.SCROLL));
                }

                @Override
                public void onCentralPositionChanged(int i) {
                    EventLog.addRecord(new EventRecord(EventRecord.SELECTOR_CAMBIO));
                }
            };

    /**
     * Funcion encargada de restaurar la actividad en caso de que se salga de ella
     */
    @Override
    public void onPause() {
        if (!Preferences.getNewActivity(getApplicationContext())) {
            this.allowTouch = false;
            postExecute = new Handler();
            postExecute.postDelayed(new RestoreActivity(), 1000);
            EventLog.addRecord(new EventRecord(EventRecord.SALIDA_REGULACION));
        }
        this.reVibrate.cancel();
        /*else
            PreferenciasTTT.setNewActivity(getApplicationContext(), false);*/
        super.onPause();
    }

    /**
     * Clase privada encargada de restaurar la actividad
     */
    private class RestoreActivity implements Runnable {

        /**
         * Funcion que restaura la actividad
         */
        @Override
        public void run() {
            //Si el reloj no se esta cargando
            if (!Preferences.getCharging(getApplicationContext())) {
                Log.i("MSActivity", "restarting app");
                Preferences.setLaunchApp(getApplicationContext(), false);

                Intent i = new Intent(MultipleSelectorActivity.this, MultipleSelectorActivity.class);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                //Thread.currentThread().interrupt();
                return;
            }
        }
    }

    /**
     * Clase privada que avanza al siguiente paso
     */
    private class NextStepTask extends AsyncTask {

        /**
         * Funcion que avanza al siguiente paso
         *
         * @param params null
         * @return null
         */
        @Override
        protected Object doInBackground(Object[] params) {
            Log.i("MSActivity", "NextStepTask");
            //Condicion de paso siguiente
            //1- Que este en pantalla
            //2- que el tiempo halla pasado y/o que se halla realizado el gesto
            if (touched) {
                touched = false;
                Intent i = null;
                int strategyId = regulation.getSelectorStrategies().get(option);
                regulation.setOption(strategyId);

                //Si el id es -1, significa que se ha seleccionado la opcion de terminar la regulacion
                if (strategyId == -1) {
                    EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
                    EventLog.writeEventLog(getApplicationContext(),
                            Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                    Settings.Secure.ANDROID_ID));
                    Preferences.setEndRegulation(getApplicationContext(), true);
                    i = new Intent(MultipleSelectorActivity.this, MainActivity.class);
                    //Si la opcion tiene un id inexistente, se trata de un error
                } else if (regulation.getStrategy(strategyId) == null) {
                    Log.e("MultipleSelector", "Error: No existe la estrategia seleccionada en el selector");
                    i = new Intent(MultipleSelectorActivity.this, MainActivity.class);
                    //Carga de la estrategia
                } else {
                    regulation.getStrategy(strategyId).restartStrategy();
                    EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getStrategy(strategyId).getName()));
                    if (regulation.getStrategy(strategyId).isCarrusel()) {
                        EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Carrusel"));
                        i = new Intent(MultipleSelectorActivity.this, CarruselActivity.class);
                    } else if (!regulation.getStrategy(strategyId).nextStep()) {
                        Log.e("MultipleSelector", "Error: La estretagia no tiene pasos");
                    } else {
                        i = getIntentStep();
                        if (i == null) {
                            allowTouch = true;
                            return null;
                        }
                    }
                }

                Preferences.setNewActivity(getApplicationContext(), true);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                //Thread.currentThread().interrupt();
                allowTouch = true;
                return null;
            }
            touched = false;
            allowTouch = true;
            Preferences.setNewActivity(getApplicationContext(), false);
            return null;
        }

        /**
         * Funcion que devuelve la actividad a cargar
         *
         * @return Intent de la siguiente actividad
         */
        private Intent getIntentStep() {
            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + regulation.getActualStrategy().getActualStep()));
            String s = regulation.getActualStrategy().getActualStep();
            int time = regulation.getActualStrategy().getTime();
            switch (AssistActivity.getStepType(s, time)) {
                case AssistActivity.STATIC_GIF:
                    return new Intent(MultipleSelectorActivity.this, GifActivity.class);
                case AssistActivity.STATIC_PICTURE:
                    return new Intent(MultipleSelectorActivity.this, ContentPictureActivity.class);
                case AssistActivity.TIME_PICTURE:
                    return new Intent(MultipleSelectorActivity.this, TimePicture.class);
                case AssistActivity.TIME_GIF:
                    return new Intent(MultipleSelectorActivity.this, TimeGif.class);
                default:
                    Log.e("Selector", "Error: La estretagia esta mal formada");
                    return new Intent(MultipleSelectorActivity.this, MainActivity.class);
            }
        }
    }


}


/*********************************
 * ShapeWear.setOnShapeChangeListener(new ShapeWear.OnShapeChangeListener() {
 *
 * @Override public void shapeDetected(ShapeWear.ScreenShape screenShape) {
 * Log.d("MSA", "shapeDetected isRound:" + screenShape);
 * Log.d("MSA", "shapeDetected getShape:" + ShapeWear.getShape());
 * }
 * });
 * ShapeWear.setOnSizeChangeListener(new ShapeWear.OnSizeChangeListener() {
 * @Override public void sizeDetected(int widthPx, int heightPx) {
 * Log.d("MSA", "sizeDetected w:" + widthPx + ", h:" + heightPx);
 * }
 * });
 * ShapeWear.initShapeWear(this);
 *********************************/