package com.orange.taimun_watch.es.utils;

import java.util.ArrayList;
import java.util.Collections;

import Jama.Matrix;
import Jama.QRDecomposition;

/**
 * Clase que realiza calculos sobre una serie de medidas
 *
 * @author Javier Cuadrado Manso
 */
public final class Calculos {

    private static final double SLOPE_THRESHOLD = 0.5;
    private static final double MOC_SERIES_PERCENTAGE = 0.2; //[0.0, 1.0]

    /*Estadisticas*/

    /**
     * Calcula la media
     *
     * @param medidas medidas
     * @return La media
     */
    public static double media(ArrayList<Double> medidas) {
        double s = 0.0;
        for (double d : medidas) {
            s += d;
        }
        return s / (double) medidas.size();
    }

    /**
     * Calcula la media
     *
     * @param medidas medidas
     * @return La media
     */
    public static long media(ArrayList<Long> medidas, int size) {
        long s = 0;
        if(size == 0) return 0;
        for (long d : medidas) {
            s += d;
        }
        return s / (long) size;
    }

    /**
     * Calculo de la mediana
     *
     * @param medidas
     * @return
     */
    public static double mediana(ArrayList<Double> medidas) {
        Collections.sort(medidas);

        if(medidas.size() == 0)
            return 0.0;
        else if (medidas.size() % 2 == 1)
            return medidas.get((medidas.size() - 1) / 2);
        else {
            double medianaInferior = medidas.get(((medidas.size() - 1) / 2) - 1);
            double medianaSuperior = medidas.get((medidas.size() - 1) / 2);
            return (medianaInferior + medianaSuperior) / 2;
        }
    }

    /**
     * Calcula la varianza
     *
     * @param medidas medidas
     * @return La varianza
     */
    public static double varianza(ArrayList<Double> medidas) {
        if(medidas.size() == 0)
            return 0.0;
        double media = Calculos.media(medidas);
        return Calculos.varianza(medidas, media);
    }

    /**
     * Calcula la varianza
     *
     * @param medidas medidas
     * @param media   media
     * @return La varianza
     */
    public static double varianza(ArrayList<Double> medidas, double media) {
        if(medidas.size() == 0)
            return 0.0;
        double sum = 0.0;
        for (double d : medidas)
            sum += (media - d) * (media - d);
        return sum / (double) medidas.size();
    }

    /**
     * Calcula la desviacion estandar
     *
     * @param medidas medidas
     * @return La varianza
     */
    public static double desviacionEstandar(ArrayList<Double> medidas) {
        double media = Calculos.media(medidas);
        return Calculos.desviacionEstandar(medidas, media);
    }

    /**
     * Calcula la desviacion estandar
     *
     * @param medidas medidas
     * @param media   La media
     * @return La desviacion estavdar
     */
    public static double desviacionEstandar(ArrayList<Double> medidas, double media) {
        double varianza = Calculos.varianza(medidas);
        return Calculos.desviacionEstandar(varianza);
    }

    /**
     * Calcula la desviacion estandar
     *
     * @param varianza Varianza
     * @return La desviacion estandar
     */
    public static double desviacionEstandar(double varianza) {
        return Math.sqrt(varianza);
    }

    /**
     * Calcula el rango intercuartilico
     *
     * @param medidas
     * @return
     */
    public static double rangoIntercuartilico(ArrayList<Double> medidas) {
        int size = medidas.size();
        if (size < 2) //En el caso de que haya menos de dos elementos
            return 0;
        else if (size == 2) //Caso de dos elementos
            return medidas.get(1) - medidas.get(0);
        else { //Caso de mas de dos elementos
            ArrayList<Double> medidasOrdenadas = (ArrayList<Double>) medidas.clone();
            Collections.sort(medidasOrdenadas);

            int i1 = size + 1 / 4;
            int i3 = 3 * i1;
            return medidasOrdenadas.get(i3 - 1) - medidasOrdenadas.get(i1 - 1);
        }
    }

    /**
     * Devuelve la suma al cuadrado de las medidas
     *
     * @param medidas
     * @return
     */
    public static double sumaCuadradaMedidas (ArrayList<Double> medidas){
        double sum = 0.0;
        for (double medida : medidas)
            sum += medida * medida;
        return sum;
    }

    /**
     * Calcula el valor cuadratico medio (RMS)
     *
     * @param medidas
     * @return
     */
    public static double valorCuadraticoMedio(ArrayList<Double> medidas) {
        if(medidas.size() == 0)
            return 0.0;
        return Calculos.valorCuadraticoMedio(Calculos.sumaCuadradaMedidas(medidas), medidas.size());
    }

    /**
     * Calcula el valor cuadratico medio (RMS)
     *
     * @param sumaCuadradaMedidas
     * @param N
     * @return
     */
    public static double valorCuadraticoMedio(double sumaCuadradaMedidas, int N) {
        return Math.sqrt(sumaCuadradaMedidas / (double)N);
    }

    /**
     * Calcula la desviacion media absoluta (MAD)
     *
     * @param medidas
     * @return
     */
    public static double desviacionMediaAbsoluta(ArrayList<Double> medidas) {
        if(medidas.size() == 0)
            return 0.0;
        return desviacionMediaAbsoluta(medidas, Calculos.media(medidas));
    }

    /**
     * Calcula la desviacion media absoluta (MAD)
     *
     * @param medidas
     * @param media
     * @return
     */
    public static double desviacionMediaAbsoluta(ArrayList<Double> medidas, double media) {
        if(medidas.size() == 0)
            return 0.0;
        double sum = 0.0;
        for (double medida : medidas)
            sum += Math.abs(medida - media);
        return sum / medidas.size();
    }

    /**Estructurales**/

    /**
     * Calculo de los coeficientes al aplicar regresion polinomica
     *
     * @param medidas
     * @param tiempos
     * @param grado
     * @return
     */
    public static ArrayList<Double> regresionPolinomica(ArrayList<Double> medidas, ArrayList<Long> tiempos, int grado){
        return Calculos.regresionPolinomica(medidas, tiempos, grado, Calculos.media(tiempos, tiempos.size()));
    }

    /**
     * Calculo de los coeficientes al aplicar regresion polinomica
     *
     * @param medidas
     * @param tiempos
     * @param grado
     * @param mediaTiempos
     * @return
     */
    public static ArrayList<Double> regresionPolinomica(ArrayList<Double> medidas, ArrayList<Long> tiempos, int grado, long mediaTiempos) {
        if(medidas.size() == 0)
            return null;

        // mean of y[] values
        long mean = mediaTiempos;

        int N = medidas.size();
        Matrix beta = null;
        double SSE = 0.0;                 // sum of squares due to error
        double SST = 0.0;                 // total sum of squares

        // create matrix from vector
        double[] tmp = new double[tiempos.size()];
        for (int i = 0; i < tiempos.size(); i++)
            tmp[i] = tiempos.get(i);

        Matrix Y = new Matrix(tmp, N);


        // in case Vandermonde matrix does not have full rank, reduce degree until it does
        boolean done = false;
        while (!done) {

            // build Vandermonde matrix
            double[][] vandermonde = new double[N][grado + 1];
            for (int i = 0; i < N; i++) {
                for (int j = 0; j <= grado; j++) {
                    vandermonde[i][j] = Math.pow(medidas.get(i), j);
                }
            }
            Matrix X = new Matrix(vandermonde);

            // find least squares solution
            QRDecomposition qr = new QRDecomposition(X);

            // decrease degree and try again
            if (!qr.isFullRank()) {
                grado--;
                continue;
            }

            // linear regression coefficients
            beta = qr.solve(Y);

            // total variation to be accounted for
            for (int i = 0; i < N; i++) {
                double dev = tiempos.get(i) - mean;
                SST += dev * dev;
            }

            // variation not accounted for
            Matrix residuals = X.times(beta).minus(Y);
            SSE = residuals.norm2() * residuals.norm2();
            break;
        }
        ArrayList<Double> coeficientes = new ArrayList<>();
        for (int i = 0; i < beta.getRowDimension(); i++)
            coeficientes.add(beta.get(i, 0));
        return coeficientes;
    }

    /**Transicionales**/

    /**
     * Calculo de la inclinacion (Slope)
     *
     * @param medidas
     * @param tiempos
     * @return
     */
    public static double inclinacion(ArrayList<Double> medidas, ArrayList<Long> tiempos) {
        if(medidas.size() == 0)
            return 0.0;
        double mediaT = Calculos.media(medidas);
        double mediaV = Calculos.media(tiempos, tiempos.size());

        double w = 0, z = 0;

        for (int i = 0; i < medidas.size(); i++) {
            w += (tiempos.get(i) - mediaT) * (tiempos.get(i) - mediaT);
            z += (tiempos.get(i) - mediaT) * (medidas.get(i) - mediaV);
        }
        return z / w;
    }

    /**
     * Calculo de la tendencia en funcion de la inclinacion
     *
     * @param inclinacion
     * @return
     */
    public static double tendencia (double inclinacion){
        if(inclinacion > Calculos.SLOPE_THRESHOLD)
            return 1;
        else if (inclinacion > Calculos.SLOPE_THRESHOLD)
            return -1;
        else
            return 0;
    }

    /**
     * Calculo de la tendencia en funcion de las medidas y las marcas de tiempos
     *
     * @param medidas
     * @param tiempos
     * @return
     */
    public static double tendencia (ArrayList<Double> medidas, ArrayList<Long> tiempos){
        return Calculos.tendencia(Calculos.inclinacion(medidas, tiempos));
    }

    /**
     * Calculo de la magnitud de cambio (MOC)
     *
     * @param medidas
     * @param tiempos
     * @return
     */
    public static double magnitudDeCambio(ArrayList<Double> medidas, ArrayList<Long> tiempos){
        if(medidas.size() <= 1)
            return 0.0;
        double start = tiempos.get(0);
        double end = tiempos.get(tiempos.size() -1);

        double spMinus = start + (end-start) * Calculos.MOC_SERIES_PERCENTAGE;
        double spPlus = start + (end-start) * (1 - Calculos.MOC_SERIES_PERCENTAGE);

        double maxPlus = medidas.get(0);
        double minPlus = maxPlus;
        double maxMinus = medidas.get(medidas.size() - 1);
        double minMinus = maxMinus;

        for(int i = 0; tiempos.get(i) <= spMinus; i++){
            double value = medidas.get(i);
            maxMinus = Math.max(maxMinus, value);
            minMinus = Math.min(minMinus, value);
        }

        for(int i = medidas.size() -1; tiempos.get(i) >= spPlus; i--){
            double value = medidas.get(i);
            maxMinus = Math.max(maxPlus, value);
            minMinus = Math.min(minPlus, value);
        }
        return Math.max(Math.abs(maxPlus - minMinus), Math.abs(maxMinus - minPlus));
    }

}
