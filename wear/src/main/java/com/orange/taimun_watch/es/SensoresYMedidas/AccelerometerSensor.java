package com.orange.taimun_watch.es.SensoresYMedidas;

import android.content.Context;
import android.hardware.Sensor;

import java.util.Calendar;

/**
 * Clase que administra el sensor del acelerometro
 *
 * @author Javier Cuadrado Manso
 */
public class AccelerometerSensor extends ObservableSensor {
    Context context; //PARA DEBUG

    /**
     * Constructor de la clase
     *
     * @param acc El sensor del acelerometoro
     * @param ctx Contexto
     */
    public AccelerometerSensor(Sensor acc, Context ctx) {
        super(acc);
        this.context = ctx;
    }

    /**
     * Actualiza las medidas tomadas
     *
     * @param accuracy precision
     * @param x        medida x
     * @param y        medida y
     * @param z        medida z
     */
    public void setNewMeasures(int accuracy, float x, float y, float z) {
        if(accuracy < 0)
            return;
        Calendar cal = Calendar.getInstance();
        this.medidasSensor.medidas.add(new Measure(cal.getTimeInMillis(), accuracy, x, y, z));
        setChanged();
        notifyObservers();
    }

}
