package com.orange.taimun_watch.es.assistActivities.StaticActivities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orange.taimun_watch.es.MainActivity;
import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.CarruselActivity;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimeGif;
import com.orange.taimun_watch.es.assistActivities.TimeActivities.TimePicture;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.Preferences;

import java.io.File;

/**
 * Clase que contiene la logica de la actividad de imagen sin temporizador
 *
 * @author Javier Cuadrado Manso
 */
public class ContentPictureActivity extends AssistActivity {

    private ImageView source = null;
    private CountDownTimer reVibrate = null;

    /**
     * Funcion que carga la imagen del paso actual indicada por la regulacion
     *
     * @param savedInstanceState Instancia del estado guardado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_picture);

        Log.i("ContentPictureActivity", "onCreate");

        source = (ImageView) findViewById(R.id.MainContentCPA);

        String src = this.regulation.getActualStrategy().getActualStep();
        final File fileRecurso = new File(getApplicationContext().getFilesDir() + "/regulation_files/" + src);

        Glide.with(getApplicationContext()).load(fileRecurso)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(source);

        /*
        Bitmap bm = null;
        if (Preferences.DEBUG)
            bm = super.getBitmapFromAsset(src);
        else
            bm = BitmapFactory.decodeFile(getApplicationContext().getFilesDir() + "/regulation_files/" + src);

        if (bm == null)
            this.source.setImageBitmap(super.getBitmapFromAsset("error.png"));
        else
            this.source.setImageBitmap(bm);
        */

        this.reVibrate = new CountDownTimer(millisToVibrate, millisToVibrate) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(4000);
                Toast.makeText(getApplicationContext(), R.string.toca, Toast.LENGTH_SHORT).show();
                this.start();
            }
        }.start();
    }

    /**
     * Funcion que recoje el toque en la pantalla y comienza la tarea de avanzar al siguiente paso
     *
     * @param v View de la pantalla
     */
    public void touch(View v) {
        EventLog.addRecord(new EventRecord(EventRecord.TOQUE));
        if (!this.allowTouch)
            return;
        this.touched = true;
        this.allowTouch = false;
        this.reVibrate.cancel();
        new NextStepTask().execute();

    }

    /**
     * Funcion que reactiva el paso en caso de que el usuario salga de la actividad
     */
    @Override
    public void onPause() {
        if (!Preferences.getNewActivity(getApplicationContext())) {
            this.allowTouch = false;
            postExecute = new Handler();
            postExecute.postDelayed(new RestoreActivity(), 1000);
            EventLog.addRecord(new EventRecord(EventRecord.SALIDA_REGULACION));
        }
        reVibrate.cancel();
        super.onPause();
    }

    /**
     * Clase privada encargada de restaurar la actividad en caso de salirse de ella
     */
    private class RestoreActivity implements Runnable {

        /**
         * Funcion que restaura la actividad
         */
        @Override
        public void run() {
            //Si el reloj no se esta cargando, se restaura la actividad
            if (!Preferences.getCharging(getApplicationContext())) {
                Log.i("ContentPictureActivity", "restarting app");
                Preferences.setLaunchApp(getApplicationContext(), false);

                Intent i = new Intent(ContentPictureActivity.this, ContentPictureActivity.class);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();

                return;
            }
        }
    }

    /**
     * Clase privada encargada de avanzar al siguiente paso
     */
    private class NextStepTask extends AsyncTask {
        /**
         * Funcion que avanza al siguiente paso
         *
         * @param params null
         * @return null
         */
        @Override
        protected Object doInBackground(Object[] params) {
            Log.i("ContentPicture", "NextStepTask");
            //Condicion de paso siguiente
            //Que se halla realizado el toque
            if (touched) {
                //postExecute.removeCallbacksAndMessages(null);
                touched = false;
                Intent i = null;

                //Si no tiene paso...
                if (!regulation.getActualStrategy().nextStep()) {
                    //...La estrategia ha terminado y se comprueba si tiene una pregunta final
                    if (regulation.getActualStrategy().getQuestion() != false) {
                        EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Pregunta"));
                        i = new Intent(ContentPictureActivity.this, ConfirmationActivity.class);
                        //SI no la tiene, se comprueba si tiene que saltar el refuerzo positivio si existe
                    } else if (regulation.getActualStrategy().hasReinforcement()) {
                        //& !Preferences.getIsPositiveLastWindow(getApplicationContext()//
                        Log.d("REFUERZO", "Salta el refuerzo positivo.");
                        EventLog.addRecord(new EventRecord(EventRecord.REFUERZO_POSITIVO));
                        i = new Intent(ContentPictureActivity.this, PositiveReinforcementActivity.class);
                    } else {
                        //Si era una estrategia escogida desde un selector, se pone la opcion a -1
                        //para indicar que se vuelve al array de estrategias
                        regulation.setOption(-1);
                        //Si no tiene pregunta, se pasa a la siguiente estrategia
                        //Si no tiene siguiente estrategia, la regulacion ha terminado
                        if (!regulation.nextStrategy()) {
                            EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
                            EventLog.writeEventLog(getApplicationContext(),
                                    Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                            Settings.Secure.ANDROID_ID));
                            Preferences.setEndRegulation(getApplicationContext(), true);
                            i = new Intent(ContentPictureActivity.this, MainActivity.class);
                        }
                        //En caso contrario, se comprueba si esta estrategia es simple o un selector
                        else {
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getActualStrategy().getName()));
                            if (regulation.isSelector()) {
                                EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Selector"));
                                i = new Intent(ContentPictureActivity.this, MultipleSelectorActivity.class);
                            } else {
                                //Si es simple, se reinicia y se comprueba que tiene pasos
                                regulation.getActualStrategy().restartStrategy();
                                if (regulation.getActualStrategy().isCarrusel()) {
                                    EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Carrusel"));
                                    i = new Intent(ContentPictureActivity.this, CarruselActivity.class);
                                } else if (!regulation.getActualStrategy().nextStep()) {
                                    Log.e("Picture", "Error: La estrategia " +
                                            regulation.getActualStrategy().getId() + " no tiene pasos");
                                    i = new Intent(ContentPictureActivity.this, MainActivity.class);
                                } else {
                                    //SI tiene pasos, al coger el primero se clasifica
                                    i = getIntentStep();
                                    if (i == null) {
                                        allowTouch = true;
                                        return null;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    i = getIntentStep();
                    if (i == null) {
                        allowTouch = true;
                        return null;
                    }
                }
                Preferences.setNewActivity(getApplicationContext(), true);
                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                allowTouch = true;
                Thread.currentThread().interrupt();
                return null;
            }
            touched = false;
            allowTouch = true;
            Preferences.setNewActivity(getApplicationContext(), false);
            return null;
        }

        /**
         * Funcion que obtiene la actividad siguiente
         *
         * @return Intent de la siguiente actividad
         */
        private Intent getIntentStep() {
            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + regulation.getActualStrategy().getActualStep()));
            String s = regulation.getActualStrategy().getActualStep();
            int time = regulation.getActualStrategy().getTime();
            switch (AssistActivity.getStepType(s, time)) {
                case AssistActivity.STATIC_GIF:
                    return new Intent(ContentPictureActivity.this, GifActivity.class);
                case AssistActivity.STATIC_PICTURE:
                    //Si es un paso del mismo tipo que el actual,
                    //se cargan las fuentes y se continua sin
                    // cargar la nueva actividad

                    String actualStep = regulation.getActualStrategy().
                            getActualStep();

                    Log.d("ASSIST", "getIntentStep() - actualStep: " + actualStep);

                    final File fileRecurso = new File(getApplicationContext().getFilesDir() + "/regulation_files/" + actualStep);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Glide.with(getApplicationContext()).load(fileRecurso)
                                    .centerCrop()
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(source);
                        }
                    });

                    reVibrate.start();
                    return null;
                case AssistActivity.TIME_PICTURE:
                    return new Intent(ContentPictureActivity.this, TimePicture.class);
                case AssistActivity.TIME_GIF:
                    return new Intent(ContentPictureActivity.this, TimeGif.class);
                default:
                    Log.e("StaticPicture", "Error: La estretagia esta mal formada");
                    return new Intent(ContentPictureActivity.this, MainActivity.class);
            }
        }
    }


}
