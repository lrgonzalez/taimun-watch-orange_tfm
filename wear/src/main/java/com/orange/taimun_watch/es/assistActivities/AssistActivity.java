package com.orange.taimun_watch.es.assistActivities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;

import java.io.IOException;
import java.io.InputStream;

import com.orange.taimun_watch.es.Regulacion.Regulation;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.JSONToRegulation;
import com.orange.taimun_watch.es.utils.Preferences;

/**
 * Clase generica que contiene funciones comunes para todas las actividades de asistencia
 *
 * @author Javier Cuadrado Manso
 */
public class AssistActivity extends Activity {

    protected Regulation regulation;
    protected boolean touched = false;

    protected BroadcastReceiver mybr = null;
    protected IntentFilter myif = null;

    protected Handler postExecute;

    public final static String ERROR_TYPE = "ERROR";
    public final static String STATIC_PICTURE = "STATIC_PICTURE";
    public final static String STATIC_GIF = "STATIC_GIF";
    public final static String CARRUSEL = "CARRUSEL";
    public final static String TIME_PICTURE = "TIME_PICTURE";
    public final static String TIME_GIF = "GIF_PICTURE";

    protected boolean allowTouch = false;


    protected long millisToVibrate = 1 * 15 * 1000;

    /**
     * Carga de elementos comunes entre todas las actividades
     *
     * @param savedInstanceState Instancia del estado guardado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("AssistActivity", "onCreate");
        if (getIntent().hasExtra("regulation")) {
            this.regulation = getIntent().getParcelableExtra("regulation");
        } else {
            String str = JSONToRegulation.readFile(getApplicationContext());
            regulation = JSONToRegulation.parseJSON(str);
        }

        //setContentView(R.layout.activity_assist);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //PreferenciasTTT.setInitAssistKey(getApplicationContext(), false);

        //Escuchador de evento de carga del reloj o baja bateria
        myif = new IntentFilter();
        myif.addAction(Intent.ACTION_POWER_CONNECTED);
        myif.addAction(Intent.ACTION_BATTERY_LOW);

        mybr = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i("AA", "running Receiver: charging");

                Preferences.setCharging(context, true);
                Preferences.setAssist(context, false);
                EventLog.writeEventLog(getApplicationContext(),
                        Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                Settings.Secure.ANDROID_ID));

                finish();
            }
        };

        registerReceiver(mybr, myif);
    }

    /**
     * Reinicio de las preferencias
     */
    @Override
    public void onResume() {
        super.onResume();
        Log.i("AssistActivity", "onResume");
        Preferences.setAssist(getApplicationContext(), true);
        Preferences.setNewActivity(getApplicationContext(), false);
        this.allowTouch = true;
    }

    /**
     * Eliminacion del registro de escuchador de eventos de carga
     */
    @Override
    public void onStop() {
        Log.i("AssistActivity", "onStop");

        Log.i("AA", "stopping Receiver");
        try {
            unregisterReceiver(mybr);
        } catch (Exception e) {

        }

        super.onStop();
    }

    /**
     * Funcion que indica si la actividad tiene que escuchar al evento de toque
     *
     * @return true si el tiempo es 0, false si no
     */
    public boolean hasGesture() {
        return this.regulation.getActualStrategy().getTime() == 0;
    }

    /**
     * Funcion que devuelve un bitmap en funcion del nombre de un recurso comprimido con la app
     *
     * @param strName Nombre del recurso
     * @return Bitmap
     */
    public Bitmap getBitmapFromAsset(String strName) {
        try {
            AssetManager assetManager = getAssets();
            InputStream iStr = assetManager.open(strName);
            return BitmapFactory.decodeStream(iStr);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Funcion que devuelve el tipo de la actividad en funcion de la extension del archivo del paso
     *
     * @param step Archivo
     * @param time ENtero que indica si tiene o no tiempo
     * @return Tipo
     */
    public static String getStepType(String step, int time) {
        if (time == 0) {
            if (step.endsWith(".png") || step.endsWith(".jpg"))
                return STATIC_PICTURE;
            else if (step.endsWith(".gif"))
                return STATIC_GIF;
            else
                return ERROR_TYPE;
        } else {
            if (step.endsWith(".png") || step.endsWith(".jpg"))
                return TIME_PICTURE;
            else if (step.endsWith(".gif"))
                return TIME_GIF;
            else
                return ERROR_TYPE;
        }

    }
}
