package com.orange.taimun_watch.es.SensoresYMedidas;

import android.hardware.Sensor;

import java.util.Observable;

/**
 * Clase que administra un sensor
 * Es Observable para que se detecten cambios en las medidas tomadas
 *
 * @author Javier Cuadrado Manso
 */
public class ObservableSensor extends Observable {

    protected SensorMeasures medidasSensor;
    protected Sensor sensor;

    public ObservableSensor(Sensor s) {
        super();
        sensor = s;
        this.medidasSensor = new SensorMeasures();
    }

    public Sensor getSensor() {
        return this.sensor;
    }

    /**
     * Devuelve la medida tomada
     *
     * @return Las medidas tomas en el paquete Measure
     */
    public SensorMeasures getMeasures() {
        return this.medidasSensor;
    }

    public String getSensorInfo() {
        return this.sensor.toString();
    }

    /**
     * Elimina las medidas tomadas
     */
    public void clearMeasures() {
        this.medidasSensor.medidas.clear();
    }

    @Override
    public String toString() {
        String str = this.getSensorInfo() + "\n";
        for (Measure m : this.medidasSensor.medidas)
            str += m + "\n";
        return str;
    }
}
