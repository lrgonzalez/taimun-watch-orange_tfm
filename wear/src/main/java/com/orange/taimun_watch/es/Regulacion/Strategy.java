package com.orange.taimun_watch.es.Regulacion;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Clase que contiene la logica de una estrategia
 *
 * @author Javier Cuadrado Manso
 */
public class Strategy implements Parcelable {
    private final int id;
    private String name;
    private String icon;
    private int time = 0;
    private boolean isGlobalTime = false;
    private boolean question = false;
    private int timerType = 0;
    private boolean isCarrusel = false;
    private boolean reinforcement = false;
    private ArrayList<String> steps;


    private int actualStep = -1;

    public Strategy(int identification) {
        this.id = identification;
        this.steps = new ArrayList<>();
        this.restartStrategy();
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean isGlobalTime() {
        return isGlobalTime;
    }

    public void setIsGlobalTime(boolean isGlobalTime) {
        this.isGlobalTime = isGlobalTime;
    }

    public boolean getQuestion() {
        return question;
    }

    public void setQuestion(boolean question) {
        this.question = question;
    }

    public int getTimerType() {
        return timerType;
    }

    public void setTimerType(int timerType) {
        this.timerType = timerType;
    }

    public boolean isCarrusel() {
        return isCarrusel;
    }

    public void setIsCarrusel(boolean isCarrusel) {
        this.isCarrusel = isCarrusel;
    }


    public boolean hasReinforcement(){
        return this.reinforcement;
    }

    public void setReinforcement(boolean reinforcement) {
        this.reinforcement = reinforcement;
    }

    public boolean addStep(String s) {
        return this.steps.add(s);
    }

    public String getStep(int index) {
        return this.steps.get(index);
    }

    public void restartStrategy() {
        this.actualStep = -1;
    }

    public boolean hasNextStep() {
        return this.actualStep + 1 < this.steps.size();
    }

    /**
     * Funcion que avanza al paso siguiente de la estrategia
     *
     * @return true si hay un paso siguiente, false si no lo hay
     */
    public boolean nextStep() {
        this.actualStep++;
        return this.actualStep < this.steps.size();
    }

    public String getActualStep() {
        return this.getStep(this.actualStep);
    }

    public void setActualStep(int actualStep) {
        this.actualStep = actualStep;
    }

    public ArrayList<String> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<String> steps) {
        this.steps = steps;
    }

    public void removeAllSteps() {
        this.steps.clear();
    }

    @Override
    public String toString() {
        String strategyString = new String();
        strategyString += "{\n";
        strategyString += "id: " + id + "\n";
        strategyString += "name: " + name + "\n";
        strategyString += "icon: " + icon + "\n";
        strategyString += "time: " + time + "\n";
        strategyString += "isGlobalTime: " + isGlobalTime + "\n";
        strategyString += "Question: " + question + "\n";
        strategyString += "TimerType: " + timerType + "\n";
        strategyString += "isCarrusel: " + isCarrusel + "\n";
        strategyString += "Reinforcement: " + reinforcement + "\n";
        strategyString += "actualStep: " + actualStep + "\n";
        strategyString += "steps: [\n";
        for (String s : steps)
            strategyString += "\t" + s + "\n";
        strategyString += "]\n";
        strategyString += "}\n";
        return strategyString;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Strategy)) return false;
        Strategy s = (Strategy) object;
        return this.id == s.id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(icon);
        dest.writeInt(time);
        dest.writeByte((byte) (isGlobalTime ? 1 : 0));
        dest.writeByte((byte) (question ? 1 : 0));
        dest.writeInt(timerType);
        dest.writeByte((byte) (isCarrusel ? 1 : 0));
        dest.writeByte((byte) (reinforcement ? 1 : 0));
        dest.writeSerializable(steps);
        dest.writeInt(actualStep);
    }

    public static final Parcelable.Creator<Strategy> CREATOR = new Creator<Strategy>() {
        public Strategy createFromParcel(Parcel source) {
            Strategy s = new Strategy(source.readInt());
            s.setName(source.readString());
            s.setIcon(source.readString());
            s.setTime(source.readInt());
            s.setIsGlobalTime(source.readByte() != 0);
            s.setQuestion(source.readByte() != 0);
            s.setTimerType(source.readInt());
            s.setIsCarrusel(source.readByte() != 0);
            s.setReinforcement(source.readByte() != 0);
            s.setSteps((ArrayList<String>) source.readSerializable());
            s.setActualStep(source.readInt());
            return s;
        }

        public Strategy[] newArray(int size) {
            return new Strategy[size];
        }
    };
}
