package com.orange.taimun_watch.es.multipleSelector;


import android.content.Context;
import android.support.wearable.view.CircledImageView;
import android.support.wearable.view.WearableListView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.orange.taimun_watch.es.R;

/**
 * Clase auxiliar de la vista de los items utilizados en las listas utilizadas en el selector multiple.
 *
 * Basada en el ejemplo de "Creating a Custom Wearable List View" del libro
 * "Step by Step Android Wear" de Alex HO.
 *
 * @author Javier Cuadrado Manso
 */
public final class SettingsItemView extends FrameLayout implements WearableListView.OnCenterProximityListener {

    final CircledImageView image;
    final TextView text;

    public SettingsItemView(Context context) {
        super(context);
        View.inflate(context, R.layout.wearablelistview_item, this);
        image = (CircledImageView) findViewById(R.id.image);
        text = (TextView) findViewById(R.id.text);
    }


    @Override
    public void onCenterPosition(boolean b) {

        //Animation example to be ran when the view becomes the centered one
        text.setScaleX(1f);
        text.setScaleY(1f);
        //text.setAlpha(1f);
        image.setScaleX(1f);
        image.setScaleY(1f);
        //image.setAlpha(1f);
        //image.animate().scaleX(1f).scaleY(1f).alpha(1);
        //text.animate().scaleX(1f).scaleY(1f).alpha(1);

    }

    @Override
    public void onNonCenterPosition(boolean b) {

        //Animation example to be ran when the view is not the centered one anymore
        text.setScaleX(0.7f);
        text.setScaleY(0.7f);
        //text.setAlpha(0.6f);
        image.setScaleX(0.7f);
        image.setScaleY(0.7f);
        //image.setAlpha(0.6f);
        //image.animate().scaleX(0.8f).scaleY(0.8f).alpha(0.6f);
        //text.animate().scaleX(0.8f).scaleY(0.8f).alpha(0.6f);

    }
}