package com.orange.taimun_watch.es.utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Clase de los eventos de interaccion con el reloj
 *
 * @author Javier Cuadrado Manso
 */
public class EventRecord implements Parcelable {
    private long timestamp;
    private String tipo;

    public static final String EVALUACION_POSITIVA = "EVALUACION_POSITIVA";
    public static final String EVALUACION_NEGATIVA = "EVALUACION_NEGATIVA";
    public static final String INICIO_REGULACION = "INICIO_REGULACION";
    public static final String TOQUE = "TOQUE";
    public static final String SWIPE = "SWIPE";
    public static final String CARRUSEL_CAMBIO = "CARRUSEL_CAMBIO";
    public static final String APLICACION_SUSPENDIDA = "APLICACION_SUSPENDIDA";
    public static final String SALIDA_REGULACION = "SALIDA_REGULACION";
    public static final String FIN_REGULACION = "FIN_REGULACION";
    public static final String PREGUNTA_SI = "RESPUESTA_SI";
    public static final String PREGUNTA_NO = "RESPUESTA_NO";
    public static final String INICIO_ESTRATEGIA = "INICIO_ESTRATEGIA_";
    public static final String INICIO_PASO = "INICIO_PASO_";
    public static final String SCROLL = "SCROLL";
    public static final String SELECTOR_CAMBIO = "SELECTOR_CAMBIO";
    public static final String REFUERZO_POSITIVO = "REFUERZO_POSITIVO";

    private EventRecord(long time, String tp) {
        this.timestamp = time;
        this.tipo = tp;
    }

    public EventRecord(String tp) {
        this.timestamp = System.currentTimeMillis();
        this.tipo = tp;
    }

    @Override
    public String toString() {
        return timestamp + "\t" + tipo;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.timestamp);
        dest.writeString(this.tipo);
    }

    public static final Parcelable.Creator<EventRecord> CREATOR = new Creator<EventRecord>() {
        public EventRecord createFromParcel(Parcel source) {
            long l = source.readLong();
            String s = source.readString();
            return new EventRecord(l, s);
        }

        public EventRecord[] newArray(int size) {
            return new EventRecord[size];
        }
    };
}
