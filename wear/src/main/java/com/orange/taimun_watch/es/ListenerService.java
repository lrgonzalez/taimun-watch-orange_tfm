package com.orange.taimun_watch.es;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.util.TimingLogger;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.wearable.Channel;
import com.google.android.gms.wearable.ChannelApi;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.orange.taimun_watch.es.utils.JSONToRegulation;
import com.orange.taimun_watch.es.utils.Preferences;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Clase que escucha las peticiones de la aplicacion del movil y las responde
 * con mensajes o con bloques de datos
 *
 * @author Javier Cuadrado Manso
 */
public class ListenerService extends WearableListenerService implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String MOB_TO_WEAR = "/mob_to_wear";
    private static final String WEAR_TO_MOB = "/wear_to_mob";

    private static final String START_SYNC_LOG_FILES = "/start_sync_log_files";
    private static final String SEND_LOG_FILE = "/send_log_file";
    private static final String SEND_LOG_FILE_AGAIN = "/send_log_file_again";
    private static final String SEND_NEXT_LOG_FILE = "/send_next_log_file";
    private static final String LOG_FILE = "/log_file";
    private static final String NO_MORE_LOGS = "/no_more_logs";
    private static final String SEND_LOG = "send_log";

    private static final String TIMESTAMP = "timestamp";
    private static final String SYNC_TYPE = "sync_type";
    private static final String REGULATION_SYNC_KEY = "sync_reg";
    private static final String LOG_SYNC_KEY = "sync_log";
    private static final String READY_TO_SYNC_LOG = "ready_log";
    private static final String READY_TO_SYNC_REGULATION = "ready_reg";
    private static final String NOT_READY_TO_SYNC = "not_ready";
    private static final String PIN_SYNC = "pin_sync";

    private static final String JSON_FILE = "/json_file";
    private static final String PNG_FILE = "/png_file";
    private static final String JPG_FILE = "/jpg_file";
    private static final String GIF_FILE = "/gif_file";
    private static final String FILE = "/file";
    private static final String NO_MORE_FILES = "/no_more_files";
    private static final String SEND_IMAGE_FILE_AGAIN = "/send_image_file_again";
    private static final String SEND_NEXT_IMAGE_FILE = "/send_next_image_file";


    private static final int MAX_PAYLOAD = 95000; //El maximo es 100KB, pero dejamo espacio para mas datos
    private static final String TIMESTAMP_KEY = "timestamp";
    private static final String FILE_NAME = "filename";
    private static final String BUFFER_INDEX_KEY = "buffer_index";
    private static final String BUFFER_KEY = "buffer";
    private static final String IS_FINAL_PART_KEY = "isFinalPart";
    private static final String FILE_LENGTH = "file_length";

    private GoogleApiClient googleClient;

    private File recivedFile;
    private boolean channelOK = true;

    private static byte[] buffer;
    private static int bufferIndex;

    private FirebaseStorage storage = FirebaseStorage.getInstance();

    /**
     * Funcion que recoge los paquetes de datos
     *
     * @param dataEvents Evento que cotiene el paquete de datos
     */
    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        super.onDataChanged(dataEvents);
        if (googleClient == null) {
            googleClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }

        if (!googleClient.isConnected()) {
            googleClient.connect();
        }

        for (DataEvent event : dataEvents) {
            Log.i("WEAR", "Path recibido: " + event.getDataItem().getUri().getPath());
            Log.i("WEAR", "Mensaje recibido: " + event.getDataItem().getData());

            // Comprobacion del tipo de datos
            if (event.getType() == DataEvent.TYPE_CHANGED) {

                // Comprobacion del path
                String path = event.getDataItem().getUri().getPath();

                switch (path) {
                    //case X + id del nodo local.
                    case MOB_TO_WEAR: {
                        //Wearable.NodeApi.getLocalNode(googleClient).await();
                        Log.i("WEAR", MOB_TO_WEAR);
                        DataItem item = event.getDataItem();
                        DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                        String typeSync = dataMap.getString(SYNC_TYPE);

                        if (Preferences.getRunning(this.getApplicationContext())
                                || Preferences.getAssist(this.getApplicationContext())) {
                            new SendMessageThread(WEAR_TO_MOB, NOT_READY_TO_SYNC).start();
                        } else if (typeSync.equals(LOG_SYNC_KEY)) { // TODO: sobraria?
                            com.orange.taimun_watch.es.utils.EventLog.writeEventLog(getApplicationContext(),
                                    Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                            Settings.Secure.ANDROID_ID));
                            new SendMessageThread(WEAR_TO_MOB, String.valueOf(countFiles() + 1)).start();
                        } else if (typeSync.equals(REGULATION_SYNC_KEY)) {
                            com.orange.taimun_watch.es.utils.EventLog.writeEventLog(getApplicationContext(),
                                    Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                            Settings.Secure.ANDROID_ID));

                            this.deletePreviousRegulation();

                            new SendMessageThread(WEAR_TO_MOB, READY_TO_SYNC_REGULATION).start();
                        //} else if (typeSync.equals(PIN_SYNC)) {
                            /* El pin del telefono ha sido modificado. Se necesita un nuevo fichero KS */
                            // Se guarda el nuevo PIN y nombre del grupo recibidos en las preferencias
                            /*final String pin = dataMap.getString("PIN_CODE");
                            String grupo = dataMap.getString("GROUP_CODE");
                            Log.i("WEAR", "Pin obtenido: " + dataMap.getString("PIN_CODE") + "grupo: " + dataMap.getString("GROUP_CODE"));
                            Preferences.setPinCode(getApplicationContext(), dataMap.getString("PIN_CODE"));
                            Preferences.setGroupCode(getApplicationContext(), dataMap.getString("GROUP_CODE"));
                            // Descargar ks de firebase
                            StorageReference storageRef = storage.getReference();
                            // El nombre de la carpeta es el del grupo de 6 digitos
                            StorageReference fileRef = storageRef.child(grupo + "/ksFile");
                            final File ksRelojSave = new File(getFilesDir(), "keyStoreWatch");
                            fileRef.getFile(ksRelojSave).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                    // Local temp file has been created
                                    Log.i("LISTENER ", "KS descargado en " + ksRelojSave.getAbsolutePath());
                                    */
                                    /*try {  // No se carga, cuando lo necesita busca el fichero keyStoreWatch
                                        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
                                        InputStream ksRelojStream = new FileInputStream(ksRelojSave);
                                        ks.load(ksRelojStream, pin.toCharArray());
                                    } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
                                        e.printStackTrace();
                                    }*/
                                /*}
                            });
//                            ksRelojSave.delete(); // Una vez esta cargado en el KeyStore del reloj, no necesitamos el fichero
                        */
                        } else {
                            Log.e("WEAR", "Tipo de sincronizacion no reconocida.");
                        }

                        break;
                    }
                    case START_SYNC_LOG_FILES: {
                        Log.i("WEAR", START_SYNC_LOG_FILES);
                        ///this.moveLastFileSended();
                        mergeLogs();

//                        sendLogFile();
                        moveLastFileSended(); // borrar archivos enviados
                        // Avisamos que ha terminado la subida de ficheros
                        new SendMessageThread(NO_MORE_FILES, NO_MORE_FILES).start();
                        break;
                    }
                    /*case SEND_LOG_FILE: { // TODO sobra
                        Log.i("WEAR", SEND_LOG_FILE);
                        ListenerService.bufferIndex += ListenerService.MAX_PAYLOAD;
                        //Si el indice del buffer supera su tamaño, significa que se ha termiando de enviar el fichero
                        if (ListenerService.bufferIndex >= ListenerService.buffer.length) {
                            ListenerService.bufferIndex = 0;
                            ListenerService.buffer = null;
                            moveLastFileSended();
                        }
                        //Envio del nuevo fichero o de la siguiente parte del fichero
                        //sendLogFile();
                        break;
                    }*/
                    /*case SEND_LOG_FILE_AGAIN: { // TODO sobra
                        Log.i("WEAR", SEND_LOG_FILE_AGAIN);
                        //sendLogFile();
                        break;
                    }*/
                    case FILE: {
                        Log.i("WEAR", FILE);
                        DataItem item = event.getDataItem();
                        DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();

                        try {
                            getReceivedFile(dataMap);
                            new SendMessageThread(SEND_NEXT_IMAGE_FILE, SEND_NEXT_IMAGE_FILE).start();
                        } catch (Exception e) {
                            Log.e("WEAR", "Error, excepcion al recibir fichero: " + e.getMessage());
                            new SendMessageThread(SEND_IMAGE_FILE_AGAIN, SEND_IMAGE_FILE_AGAIN).start();
                        }

                        break;
                    }
                    case NO_MORE_FILES: {
                        break;
                    }
                    case SEND_NEXT_IMAGE_FILE:
                        break;
                    case SEND_IMAGE_FILE_AGAIN:
                        break;
                    default:
                        Log.e("WEAR", "Path no reconocido. ¿De quien es este comando?: " + path);
                        break;
                }
            }
        }
    }

    /**
     * @param connectionHint
     */
    @Override
    public void onConnected(Bundle connectionHint) {
    }

    /**
     * @param cause
     */
    @Override
    public void onConnectionSuspended(int cause) {
    }

    /**
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    /**
     * FUncion para copiar un fichero en otro
     *
     * @param src Origen
     * @param dst Destini
     * @throws IOException
     * @deprecated En desuso por no usarse ya
     */
    @Deprecated
    private void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    /**
     * Funcion que cuenta el numero de ficheros a enviar
     *
     * @return Numero de ficheros
     */
    private int countFiles() {
        int count = 0;
        File folder = new File(getApplicationContext().getFilesDir(), "sensor");
        File[] files = folder.listFiles();
        count = files.length;
        folder = new File(getApplicationContext().getFilesDir(), "events");
        files = folder.listFiles();
        return count + files.length;
    }

    /**
     * Funcion que mezcla los logs por dia y sensor o evento con los de Firebase y actualiza
     * los archivos de Firebase.
     */
    private void mergeLogs() {
        Context ctx = getApplicationContext();
        //Obtencion de los ficheros de la carpeta sensor y events
        File sensorFolder = new File(ctx.getFilesDir(), "sensor");
        File eventsFolder = new File(ctx.getFilesDir(), "events");

        // Listas de los ficheros agrupados en uno por cada uno
        ArrayList<File> sensorFiles = mergeAux(sensorFolder, false);
        ArrayList<File> eventsFiles = mergeAux(eventsFolder, false);

        Map<String, Boolean> resultadosDescargasSensors;
        Map<String, Boolean> resultadosDescargasEvents;
        // Descargar en sensorFolder los archivos de Firebase en las listas
        resultadosDescargasSensors = firebaseDownload(sensorFiles, "sensors", sensorFolder, ctx);
        resultadosDescargasEvents = firebaseDownload(eventsFiles, "events", eventsFolder, ctx);

        // DESENCRINPTAR los archivos bajados
        // Solo se pueden desencriptar los que se hayan descargado bien
        for (File sensFile : sensorFiles) {
            File encryptedFile = new File(sensFile.getParent(), "enc-" + sensFile.getName());
            Boolean exists = resultadosDescargasSensors.get(encryptedFile.getName());
            if (exists) {
                if (decryptAES(ctx, encryptedFile, sensFile, "sensor") != -1)
                    Log.i("WEAR mergeLogs sensor", "Fichero " + encryptedFile + " desencriptado");
                else {
                    Log.e("WEAR mergeLogs sensor", "Fichero " + encryptedFile + " no desencriptado");
                }
            } else {
                Log.e("WEAR mergeLogs sensor", "No existe " + sensFile.getName());
            }
        }
        Log.i("WEAR mergeLogs", "Ficheros desencriptados sensors");

        for (File eventsFile : eventsFiles) {
            File encryptedFile = new File(eventsFile.getParent(), "enc-" + eventsFile.getName());
            Boolean exists = resultadosDescargasEvents.get(encryptedFile.getName());
            if (exists) {
                if (decryptAES(ctx, encryptedFile, eventsFile, "events") != -1)
                    Log.i("WEAR mergeLogs events", "Fichero " + encryptedFile + " desencriptado");
                else {
                    Log.e("WEAR mergeLogs events", "Fichero " + encryptedFile + " no desencriptado");
                }
            } else {
                Log.e("WEAR mergeLogs events", "No existe " + eventsFile.getName());
            }
        }
        Log.i("WEAR mergeLogs", "Ficheros desencriptados events");

        // Juntar los bajados y los nuevos
        mergeAux(sensorFolder, true);
        mergeAux(eventsFolder, true);

        // ENCRIPTAR los archivos antes de subirlos
        ArrayList<File> sensorFilesEncrypted = new ArrayList<>();
        ArrayList<File> eventsFilesEncrypted = new ArrayList<>();

        for (File plainFile : sensorFiles) {
            File newEncFile = new File(plainFile.getParent(), "enc-" + plainFile.getName());
            File encryptedFile = encryptAES(ctx, plainFile, newEncFile);
            if (encryptedFile != null)
                sensorFilesEncrypted.add(encryptedFile);
            else {
                Log.e("WEAR ENCRYPT_AES sensor", "Fallo al encriptar el fichero. No se subira a Firebase: " + plainFile);
//                return; //continuar con los demás
            }
        }

        for (File plainFile : eventsFiles) {
            File newEncFile = new File(plainFile.getParent(), "enc-" + plainFile.getName());
            File encryptedFile = encryptAES(ctx, plainFile, newEncFile);
            if (encryptedFile != null)
                eventsFilesEncrypted.add(encryptedFile);
            else {
                Log.e("WEAR ENCRYPT_AES event", "Fallo al encriptar el fichero. No se subira a Firebase: " + plainFile);
//                return;
            }
        }
        Log.i("WEAR mergeLogs", "Subiendo ficheros a Firebase...");

        //Subir a Firebase los ficheros completos
        firebaseUpdate(sensorFilesEncrypted, "sensors", ctx);
        firebaseUpdate(eventsFilesEncrypted, "events", ctx);

        //Actualizar lista de ficheros de Firebase
        firebaseUpdateFilesList(sensorFiles, "sensors", ctx);
        firebaseUpdateFilesList(eventsFiles, "events", ctx);
    }

    public void firebaseUpdateFilesList(ArrayList<File> ficheros, String folder, Context ctx) {
        // Descargar lista actual (si existe)
        // La carpeta tiene el nombre del grupo
        String grupo = Preferences.getGroupCode(ctx);
        StorageReference folderRef = storage.getReference().child(grupo + "/" + folder + "/" + "lista_ficheros.txt"); //folder = middlePath
        // Donde se guarda en el reloj en la raiz
        final File tempName = new File(ctx.getFilesDir(), "lista_ficheros.txt");

        // Guardar en carpeta sensor o en events. Se descargan en segundo plano
        StorageTask status = folderRef.getFile(tempName).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // Local temp file has been created
                Log.i("RELOJ UPDATE", "Ficheros descargados. taskSnapshot: " + taskSnapshot);
                tempName.setWritable(true);
                tempName.setReadable(true);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                Log.e("RELOJ UPDATE", "Error al descargar ficheros: " + exception);
            }
        });
        Log.i("RELOJ UPDATE", "Lista ficheros en descarga esperando...");
        while (status.isInProgress()); /*{
            Log.i("RELOJ UPDATE", "Ficheros en descarga esperando...");
        }*/

        // Leer y comprobar que los nuevos nombres no estaban ya
        // TODO comprobar caso carpeta vacia (primera vez sincro)
        Set<String> ficherosLista = new HashSet<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(tempName));
            String ficheroLeido;
            while ((ficheroLeido = br.readLine()) != null) {
                Log.i("RELOJ UPDATE", "Ficheros en lista: " + ficheroLeido);
                ficherosLista.add(ficheroLeido);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Añadir nuevos nombres
        for (File sensor : ficheros) {
            ficherosLista.add(sensor.getName());
        }
        FileWriter fichero = null;
        PrintWriter pw;
        try {
            fichero = new FileWriter(tempName);
            pw = new PrintWriter(fichero);

            for (String nombreFich : ficherosLista)
                pw.println(nombreFich);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero)
                    fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        // Subir lista de nombres actualizada
        Uri uriFile = Uri.fromFile(tempName);
        // Ruta en firebase (igual que la que cargamos al descargar = folderRef)
        // iniciar subida
        UploadTask uploadTask = folderRef.putFile(uriFile);
        status = uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                //Obtener URL de descarga para mostrarla
                Uri url = taskSnapshot.getDownloadUrl();
                Log.i("WEAR UPDATE", "Exito firebase: " + url);
            }
        });
        Log.i("RELOJ", "Lista ficheros en carga esperando..."); // Esperar a que se suban
        while (status.isInProgress());/*{
            Log.i("RELOJ", "Lista ficheros en carga esperando...");
        }*/
        Log.i("WEAR UPDATE", "Fin carga lista ficheros.");
        //Una vez actualizado el fichero, se elimina el temporal
        tempName.delete();
    }

    /**
     * Función que desencripta un fichero y guarda el resultado en otro
     *
     * @param ctx        contexto de la aplicación
     * @param encFile    fichero a desencriptar
     * @param decFile    fichero resultante desencriptado
     * @param middlePath puede ser sensor o events
     * @return 0 si ok -1 si hubo algún error
     */
    public int decryptAES(Context ctx, File encFile, File decFile, String middlePath) {
        File decryptedFile = new File(getFilesDir(), middlePath + "/" + decFile.getName());
        FileInputStream fis;
        FileOutputStream fos;
        try {
            fis = new FileInputStream(encFile);
            fos = new FileOutputStream(decryptedFile, false);
        } catch (FileNotFoundException e) {
            Log.e("decryptAES", "No hay fichero \'" + decryptedFile + "\' para desencriptar.");
            return -1;
            //e.printStackTrace();
        }
        // Recuperar sks de KeyStore con las preferencias del reloj con el PIN y el nombre del grupo
        String pin = Preferences.getPinCode(ctx);
        SecretKeySpec sks = getFromKeyStore(Preferences.getGroupCode(ctx), pin.toCharArray());
        if (sks == null) {
            Log.e("WEAR DECRYPT_AES", "sks es null ");
            return -1;
        }

        // Desencriptar en buffer
        try {
            int ptLength = 0;
            // Obtener IV del principio del encriptado
            byte[] iv = new byte[16];
            fis.read(iv);

            // Inicializar cipher con IV y usar la clave recuperada del KS
            Cipher c = Cipher.getInstance("AES/GCM/NoPadding");
            c.init(Cipher.DECRYPT_MODE, sks, new IvParameterSpec(iv));

            byte[] inputBufferAux = new byte[32 * 128];

            // Leer por trozos
            while ((ptLength = fis.read(inputBufferAux)) != -1){
                byte[] dec= c.update(inputBufferAux, 0, ptLength);
                fos.write(dec);
            }
            // Parte final
            byte[] dec = c.doFinal();
            fos.write(dec);

            fos.close();
            fis.close();
            encFile.delete();
            return 0;
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException e) { //| ShortBufferException
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Función que encripta un fichero y lo guarda en otro
     *
     * @param ctx           contexto
     * @param plainFile     fichero a encriptar
     * @param encryptedFile fichero resultante encriptado
     * @return encryptedFile
     */
    public File encryptAES(Context ctx, File plainFile, File encryptedFile) {
        FileOutputStream fos = null;
        FileInputStream fis = null;

        try {
            fos = new FileOutputStream(encryptedFile);
            fis = new FileInputStream(plainFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // A partir de las preferencias se obtienen el grupo (nombre clave KS) y el pin para abrir el KS
        Log.i("WEAR", "PIN preferences: " + Preferences.getPinCode(ctx) + " PIN GROUP CODE: " + Preferences.getGroupCode(ctx));
        String pin = Preferences.getPinCode(ctx);
        SecretKeySpec sks = getFromKeyStore(Preferences.getGroupCode(ctx), pin.toCharArray());
        if (sks == null) {
            Log.e("WEAR ENCRYPT_AES", "sks es null ");
            return null;
        }
        // Generar aleatorio para el IV
        SecureRandom rng = new SecureRandom();
        try {
            int readLen;
            Cipher c = Cipher.getInstance("AES/GCM/NoPadding");
            byte[] ivData = new byte[c.getBlockSize()];
            rng.nextBytes(ivData);  // IV aleatorio
            Log.i("WEAR ENCRYPT_AES", "LEN ivData: Fich: " + ivData.length + plainFile.getName());
            c.init(Cipher.ENCRYPT_MODE, sks, new IvParameterSpec(ivData));

            // El IV no está considerado como un secreto, así que está bien escribirlo al principio del fichero.
            fos.write(ivData);

            // Buffer de lectura temporal por bloques
            byte[] inputBufferAux = new byte[32 * 128]; // TODO - encontrar tam optimo bloque
            // Leer por trozos
            while ((readLen = fis.read(inputBufferAux)) != -1){
                byte[] enc= c.update(inputBufferAux, 0, readLen);
                fos.write(enc);
            }
            // Parte final
            byte[] enc = c.doFinal();
            fos.write(enc);
            fos.close();
            fis.close();
            plainFile.delete(); // Ya no necesitamos el fichero en texto plano

            return encryptedFile;

        } catch (InvalidKeyException e) {
            Log.e("WEAR ENCRYPT_AES", "InvalidKeyException");
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("WEAR ENCRYPT_AES", "File read failed: " + e.toString());
        } catch (Exception e) {
            Log.e("WEAR ENCRYPT_AES", "Otro fallo: " + e.toString());
        }
        return null;
    }

    /**
     * Función para recuperar un SecretKeySpec del KeyStore
     *
     * @param alias    nombre de la clave a recuperar
     * @param password contraseña para abrir el KeyStore
     * @return SecretKeySpec solicitado
     */
    public SecretKeySpec getFromKeyStore(String alias, char[] password) {
        try {
            // El KS se recupera del fichero encriptado descargado de Firebase usando la contraseña
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            File file = new File(getFilesDir(), "keyStoreWatch");
            FileInputStream fis = new FileInputStream(file);
            //Cargar keystore
            ks.load(fis, password);

            // Obtener clave
            Key key = ks.getKey(alias, password);
            if (key instanceof SecretKeySpec) {
                return (SecretKeySpec) key;
            }
        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Descarga una lista de archivos del servidor de Firebase indicado y los guarda en una carpeta.
     *
     * @param files      archivos que se quieren descargar
     * @param middlePath sensor o events
     * @param folder     carpeta donde se situará el fichero descargado
     * @param ctx        contexto de la aplicación
     * @return Map con el nombre del fichero y True o False según se haya descargado bien o mal
     */
    private Map<String, Boolean> firebaseDownload(ArrayList<File> files, String middlePath, File folder, Context ctx) {
        Map<String, Boolean> resultadosDescargas = new HashMap<>();
        for (File auxFile : files) {
            /*String watchId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            StorageReference folderRef = storage.getReference().child(watchId + "/" + middlePath + "/"
                    + auxFile.getName());*/

            // La carpeta tiene el nombre del grupo
            String grupo = Preferences.getGroupCode(ctx);
            StorageReference folderRef = storage.getReference().child(grupo + "/" + middlePath + "/" + auxFile.getName());

            final File tempName = new File(folder + "/" + "enc-" + auxFile.getName());

            // Guardar en carpeta sensor o en events. Se descargan en segundo plano
            StorageTask status = folderRef.getFile(tempName).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    // Local temp file has been created
                    Log.i("RELOJ", "Ficheros descargados. taskSnapshot: " + taskSnapshot);
                    tempName.setWritable(true);
                    tempName.setReadable(true);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                    Log.e("RELOJ", "Error al descargar ficheros: " + exception);
                }
            });
            Log.i("RELOJ", "Ficheros en descarga esperando...");
            while (status.isInProgress()); /*{
                Log.i("RELOJ", "Ficheros en descarga esperando...");
            }*/
            // Guardamos los resultados de la descarga para no hacer nada con los ficheros que hayan fallado
            resultadosDescargas.put("enc-" + auxFile.getName(), status.isSuccessful());
        }
        return resultadosDescargas;
    }

    /**
     * Funcion que sube una lista de ficheros a la ruta de Firebase especificada
     *
     * @param files      lista de ficheros a subir
     * @param middlePath sensor o events
     * @param ctx        contexto de la aplicación
     */
    private void firebaseUpdate(ArrayList<File> files, String middlePath, Context ctx) {
        for (File updFile : files) {
            String[] parts = updFile.getName().split("-", 2); // Para eliminar el prefijo enc del nombre
            File auxFile = new File(updFile.getParent(), parts[1]);
            if (!updFile.renameTo(auxFile))
                Log.e("WEAR firebaseUpdate", "Fallo al cambiar el nombre de " + updFile);

            Uri uriFile = Uri.fromFile(auxFile);
            // Ruta en firebase
            StorageReference storageReference = storage.getReference();
            // El nombre de la carpeta es el nombre del grupo
            String grupo = Preferences.getGroupCode(ctx);
            StorageReference fullStoRef = storageReference.child(grupo + "/" + middlePath + "/" + uriFile.getLastPathSegment());

            // iniciar subida
            UploadTask uploadTask = fullStoRef.putFile(uriFile);
            StorageTask status = uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //Obtener URL de descarga para mostrarla
                    Uri url = taskSnapshot.getDownloadUrl();
                    Log.i("WEAR", "Exito firebase: " + url);
                }
            });
            while (status.isInProgress()); {
                Log.i("RELOJ", "Ficheros en carga esperando...");
            }
            //Una vez mezclados los ficheros, se eliminan los temporales
            Log.i("RELOJ UPDATE", "Borrando " + auxFile + ". updFile = " + updFile);
            auxFile.delete();
        }
    }

    /**
     * Funcion que devuelve la lista de ficheros a sincronizar.
     * Si merge es true también junta los ficheros del mismo dia.
     */
    private ArrayList<File> mergeAux(File folder, Boolean merge) {
        File[] files = folder.listFiles();
        ArrayList<File> filesList = new ArrayList<>();

        Arrays.sort(files, new Comparator<File>() {
            public int compare(File f1, File f2) {
                return f1.getName().compareTo(f2.getName());
            }
        });
        //Por cada fichero de sensores o eventos // que tenga 4 partes en el nombre
        for (File rcvFile : files) {
            String[] parts = rcvFile.getName().split("-");
            String dstFilename;
            if (parts.length < 4) { //acelerometro-1_4_2018.txt
                continue; // Ya esta hecho
            } else {
                if (rcvFile.getName().contains("events")) {
                    dstFilename = "events-" + loadDay(rcvFile.getName());
                } else {
                    if (rcvFile.getName().contains("acelerometro"))
                        dstFilename = "acelerometro-" + loadDay(rcvFile.getName());
                    else if (rcvFile.getName().contains("giroscopio"))
                        dstFilename = "giroscopio-" + loadDay(rcvFile.getName());
                    else if (rcvFile.getName().contains("pulsometro"))
                        dstFilename = "pulsometro-" + loadDay(rcvFile.getName());
                    else if (rcvFile.getName().contains("detectorPasos"))
                        dstFilename = "detectorPasos-" + loadDay(rcvFile.getName());
                    else if (rcvFile.getName().contains("contadorPasos"))
                        dstFilename = "contadorPasos-" + loadDay(rcvFile.getName());
                    else
                        continue;
                }
            }
            File dstFile = new File(folder, dstFilename);
            // Si solo queremos la lista de ficheros sin merge
            if (!merge) {
                if (!filesList.contains(dstFile)) {
                    filesList.add(dstFile);
                }
                continue;
            }
            // Meter datos en los ficheros
            try {
                FileInputStream fis = new FileInputStream(rcvFile);
                byte[] tmpBuf = new byte[(int) rcvFile.length()];
                fis.read(tmpBuf);
                fis.close();

                FileOutputStream fos;
                dstFile.setWritable(true);

                //Si el fichero existe, se concatena su contenido al final de este
                if (dstFile.exists())
                    fos = new FileOutputStream(dstFile, true);
                    //Si no existe, se escribe una cabecera
                else {
                    //Añadir a una lista los ficheros de sensores
                    filesList.add(dstFile);
                    fos = new FileOutputStream(dstFile, false);
                    if (dstFilename.contains("pulsometro") || dstFilename.contains("detectorPasos"))
                        fos.write("timestamp\tAccuracy\tmeasure\n".getBytes());
                    else if (dstFilename.contains("acelerometro") || dstFilename.contains("giroscopio"))
                        fos.write("timestamp\tAccuracy\tx\ty\tz\n".getBytes());
                    else
                        fos.write("timestamp\tevent\n".getBytes());
                }

                //Escritura de todos los datos recibidos
                fos.write(tmpBuf);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.e("MOVIL", "Error mezclando los ficheros. No se pudo encontrar el fichero");
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("MOVIL", "Error mezclando los ficheros. Error I/O");
                e.printStackTrace();
            }
        }
        return filesList;
    }

    /**
     * Funcion que obtiene la fecha del nombre del fichero para saber si existe o no
     *
     * @param filename Nombre del fichero
     * @return Fecha del fichero
     */
    private static String loadDay(String filename) {
        String[] parts = filename.split("-");
        if (parts.length < 4)
            return makeDay();
        return parts[3];
    }

    /**
     * Funcion que crea el String identificativo de la fecha actual
     *
     * @return String del dia
     */
    private static String makeDay() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH) + "_" + c.get(Calendar.MONTH) + "_"
                + c.get(Calendar.YEAR);
    }

    /**
     * Funcion que obtiene el primer fichero listo para enviar
     *
     * @return Primer fichero para enviar o null si no hay
     */
    private File getFirstFile() {
        //Obtencion de los ficheros de la carpeta sensor
        File folder = new File(getApplicationContext().getFilesDir(), "sensor");
        File[] files = folder.listFiles();
        //Si tiene ficheros, se devuelve el primero
        if (files.length > 0) {
            /*Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });*/
            return files[0];
            //Si no tiene ficheros, se obtienen los ficheros de los eventos
        } else {
            folder = new File(getApplicationContext().getFilesDir(), "events");
            files = folder.listFiles();
            //Si esta carpeta no tiene ficheros, el envio de logs ha terminado
            if (files.length == 0) {
                return null;
            }
            /*Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });*/
            return files[0];
        }
    }

    /**
     * Funcion que elimina los logs enviados
     */
    private void moveLastFileSended() {
        File folder = new File(getApplicationContext().getFilesDir(), "sensor");
        File[] files = folder.listFiles();
        for (File file : files){
            if (file != null){
                // Nos aseguramos de borrar solo los trozos
                String[] fileNameParts = file.getName().split("-");
                if (fileNameParts.length == 4) {
                    file.delete();
                }
            }
        }
        folder = new File(getApplicationContext().getFilesDir(), "events");
        files = folder.listFiles();
        for (File file : files){
            if (file != null){
                // Nos aseguramos de borrar solo los trozos
                String[] fileNameParts = file.getName().split("-");
                if (fileNameParts.length == 4) {
                    file.delete();
                }
            }
        }
    }

    /**
     * Funcion que obtiene los bytes del paquete de datos enviado y crea el fichero si era
     * el ultimo paquete de datos del fichero
     *
     * @param dataMap Paquete de datos
     */
    private void getReceivedFile(DataMap dataMap) {
        String fileName = dataMap.getString(FILE_NAME);
        byte[] partial_buffer = dataMap.getByteArray(BUFFER_KEY);
        int buf_index = dataMap.getInt(BUFFER_INDEX_KEY);
        boolean isFinal = dataMap.getBoolean(IS_FINAL_PART_KEY);
        long fileLength = dataMap.getLong(FILE_LENGTH);

        if (buffer == null)
            buffer = new byte[(int) fileLength];

        //Concatenar el buffer recibido al anterior
        System.arraycopy(partial_buffer, 0, buffer, buf_index, partial_buffer.length);

        //Si es el ultimo paquete de datos del fichero, este se crea directamente con los bytes
        if (isFinal) {
            try {
                Log.d("RECEIVE", "Recibido:" + fileName);
                FileOutputStream fos = new FileOutputStream(
                        getApplicationContext().getFilesDir() + "/regulation_files/" + fileName);

                fos.write(buffer);
                fos.close();
            } catch (FileNotFoundException ex) {
                Log.e("WEAR", "Error en getReceiveFile: FileNotFoundException : " + ex);
            } catch (IOException ioe) {
                Log.e("WEAR", "Error en getReceiveFile: IOException : " + ioe);
            }

            //Liberar el bufer
            buffer = null;
            return;
        }

    }

    /**
     * Funcion que borra la regulacion porque se va a recibir una nueva
     */
    private void deletePreviousRegulation() {
        File folder = new File(getApplicationContext().getFilesDir(), "regulation_files");
        File[] files = folder.listFiles();
        for (File tmp : files)
            if (!tmp.isDirectory())
                tmp.delete();
    }

    /**
     * Funcion que envia un fichero o trozo de fichero
     */
    private void sendLogFile() {
        File file = getFirstFile();
        //Si no hay fichero, se envia el mensaje de que no hay ficheros
        if (file == null) {
            Log.i("WEAR", "No hay mas logs");
            new SendMessageThread(NO_MORE_LOGS, NO_MORE_LOGS).start();
        } else {
            String fileName = file.getName();
            //SI el fichero es mayor que el tamano de cola, se enviara en fragmentos
            if (file.length() >= MAX_PAYLOAD) {
                DataMap dataMap = new DataMap();
                //SI el indice es 0, se crea un buffer con el tamano del fichero
                if (ListenerService.bufferIndex == 0) {

                    ListenerService.buffer = new byte[(int) file.length()];
                    try {
                        FileInputStream fileInputStream = new FileInputStream(file);
                        fileInputStream.read(ListenerService.buffer);
                        fileInputStream.close();
                    } catch (FileNotFoundException e) {
                        Log.e("WEAR", "File Not Found.");
                        return;
                    } catch (IOException e1) {
                        Log.e("WEAR", "Error Reading The File.");
                        return;
                    }

                }

                byte[] buffer_tmp;
                //Si los bytes restantes son menores que la cola, se envia solo esos bytes
                if (bufferIndex + MAX_PAYLOAD > buffer.length) {
                    buffer_tmp = Arrays.copyOfRange(buffer,
                            bufferIndex, buffer.length);
                    dataMap.putBoolean(IS_FINAL_PART_KEY, true);
                    //En caso contrario, se envia un paquete con el tamano permitido por la cola
                } else {
                    buffer_tmp = Arrays.copyOfRange(buffer,
                            bufferIndex, bufferIndex + MAX_PAYLOAD);
                    dataMap.putBoolean(IS_FINAL_PART_KEY, false);
                }

                //Compeltar los datos del paquete
                dataMap.putLong(TIMESTAMP_KEY, System.currentTimeMillis());
                dataMap.putString(FILE_NAME, fileName);
                dataMap.putInt(BUFFER_INDEX_KEY, bufferIndex);
                dataMap.putByteArray(BUFFER_KEY, buffer_tmp);
                dataMap.putLong(FILE_LENGTH, file.length());

                Log.i("WEAR", "Mensaje enviado: FILE. Fichero: " + fileName + ". "
                        + bufferIndex + " de " + buffer.length);
                new SendToDataLayerThread(LOG_FILE, dataMap).start();
            } else {
                //Caso en el que el fichero es mas pequeno que el tamano de cola
                ListenerService.buffer = new byte[(int) file.length()];
                try {
                    FileInputStream fileInputStream = new FileInputStream(file);
                    fileInputStream.read(ListenerService.buffer);
                    fileInputStream.close();
                } catch (FileNotFoundException e) {
                    Log.e("WEAR", "File Not Found.");
                } catch (IOException e1) {
                    Log.e("WEAR", "Error Reading The File.");
                }

                DataMap dataMap = new DataMap();
                dataMap.putLong(TIMESTAMP_KEY, System.currentTimeMillis());
                dataMap.putString(FILE_NAME, fileName);
                dataMap.putInt(BUFFER_INDEX_KEY, bufferIndex);
                dataMap.putByteArray(BUFFER_KEY, buffer);
                dataMap.putBoolean(IS_FINAL_PART_KEY, true);
                dataMap.putLong(FILE_LENGTH, file.length());

                Log.i("WEAR", "Mensaje enviado: FILE. Fichero: " + fileName + ". "
                        + bufferIndex + " de " + buffer.length);
                new SendToDataLayerThread(LOG_FILE, dataMap).start();
            }
        }
    }


    /**
     * Clase privada que se ejecuta en otro hilo para enviar un paquete de datos al reloj
     */
    class SendToDataLayerThread extends Thread {
        String path;
        DataMap dataMap;

        /**
         * Constructor de la clase
         *
         * @param p    Path
         * @param data Paquete de datos
         */
        SendToDataLayerThread(String p, DataMap data) {
            path = p;
            dataMap = data;
        }

        /**
         * Funcion que envia el paquete de datos
         */
        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(googleClient).await();
            for (Node node : nodes.getNodes()) {
                // Construct a DataRequest and send over the data layer
                PutDataMapRequest putDMR = PutDataMapRequest.create(path);
                putDMR.getDataMap().putAll(dataMap);
                PutDataRequest request = putDMR.asPutDataRequest();
                request.setUrgent();
                DataApi.DataItemResult result = Wearable.DataApi.putDataItem(googleClient, request).await();
                if (result.getStatus().isSuccess()) {
                    Log.v("MOVIL", "DataMap sent to: " + node.getDisplayName());
                } else {
                    // Log an error
                    Log.v("MOVIL", "ERROR: failed to send DataMap to " + node.getDisplayName());
                }

            }
        }
    }

    /**
     * Clase privada que envia en un nuevo hilo un mensaje al movil
     */
    class SendMessageThread extends Thread {
        String path;
        String message;

        /**
         * Constructor que guarda el path y el mensaje
         *
         * @param p   Path
         * @param msg Mensaje
         */
        SendMessageThread(String p, String msg) {
            path = p;
            message = msg;
        }

        /**
         * Funcion que envia el mensaje
         */
        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi
                    .getConnectedNodes(googleClient).await();
            for (Node node : nodes.getNodes()) {
                MessageApi.SendMessageResult result = Wearable.MessageApi
                        .sendMessage(googleClient, node.getId(),
                                path, message.getBytes()).await();
                if (result.getStatus().isSuccess()) {
                    Log.v("RELOJ", "Mensaje enviado: {" + message + "} sent to: " +
                            node.getDisplayName());
                } else {
                    // Log an error
                    Log.v("RELOJ", "ERROR: failed to send Message to " + node.getDisplayName());
                }
            }
        }
    }

    protected class JSONTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            String str = JSONToRegulation.readFile(getApplicationContext());
            MainActivity.regulation = JSONToRegulation.parseJSON(str);

            return null;
        }
    }

    /**************************************************************************************************/
    /**************************************************************************************************/
    /************************************FUNCIONES OBSOLETAS*******************************************/
    /**************************************************************************************************/
    /**************************************************************************************************/

    /**
     * Clase privada que envia en un nuevo hilo un fichero de medidas a traves de channel
     *
     * @deprecated En desuso por la limitacion de tamano de los ficheros
     */
    @Deprecated
    class SendFile extends Thread {
        String path;
        File file;

        /**
         * Constructor que guarda el path y el fichero
         *
         * @param p Path
         * @param f Fichero
         */
        SendFile(String p, File f) {
            path = p;
            file = f;
        }

        /**
         * Funcion que envia el fichero
         */
        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi
                    .getConnectedNodes(googleClient).await();
            Node node = nodes.getNodes().get(0);

            ChannelApi.OpenChannelResult result = Wearable.ChannelApi.openChannel(googleClient,
                    node.getId(), this.path).await();
            if (result == null) {
                Log.e("WEAR", "Error: result null");
                return;
            }
            if (!result.getStatus().isSuccess()) {
                Log.e("WEAR", "Error: result not success");
            }
            Channel channel = result.getChannel();
            if (channel == null) {
                Log.e("WEAR", "Error: channel es null | " + result.getStatus().getStatusMessage());

            }

            Status st = channel.sendFile(googleClient, Uri.fromFile(file)).await();
            if (st.getStatus().isSuccess()) {
                Log.v("Reloj", "Archivo " + file.getName() + " enviado con exito a " + node.getDisplayName());
                channel.close(googleClient);
            } else {
                Log.e("Reloj", "Archivo " + file.getName() + " no enviado a " + node.getDisplayName());
                channel.close(googleClient);
            }

        }
    }

    /**
     * Funcion que prepara el archivo temporal que contendra el fichero de medidas
     * que esta enviando el reloj.
     *
     * @param channel Canal
     * @Deprecated En desuso por la limitacion del tamano de los ficheros
     */
    @Deprecated
    @Override
    public void onChannelOpened(Channel channel) {
        Log.i("ESTADO CONEXION WEAR", "OnChannelOpened");
        boolean result;
        String[] completePath = channel.getPath().split("@");
        if (completePath.length != 2) {
            Log.e("WEAR", "Path mal formado: " + channel.getPath());
            super.onChannelOpened(channel);
            return;
        }
        String path = completePath[0];
        String fileName = completePath[1];

        if (path.equals(JSON_FILE) ||
                path.equals(JPG_FILE) ||
                path.equals(PNG_FILE) ||
                path.equals(GIF_FILE)) {
            Log.i("WEAR", "Fichero recibido por channel");


            if (this.recivedFile != null)
                this.recivedFile.delete();

            //Creacion del fichero temporal
            this.recivedFile = new File(this.getApplicationContext().getFilesDir().getPath() + "/" + fileName);

            try {
                result = this.recivedFile.createNewFile();
            } catch (IOException e) {
                Log.e("WEAR", "Error creando el fichero temporal");
                return;
            }
            //Inicio de la recepcion del fichero
            channel.receiveFile(googleClient, Uri.fromFile(this.recivedFile), false).await();
        } else {
            Log.e("WEAR", "Fichero recibido por canal no reconocido");
            Log.e("WEAR", "Path: " + channel.getPath());
        }
        super.onChannelOpened(channel);
    }

    /**
     * Funcion llamada al finalizar la recepcion del fichero.
     * Se encarga de procesar el fichero recivido y, de estar todo en orden,
     * reponder indicando que fichero se ha recivido.
     *
     * @param channel              Canal
     * @param closeReason          Razon de cierre
     * @param appSpecificErrorCode Codigo de error
     * @deprecated En desuso por no utilizar ya canales
     */
    @Deprecated
    @Override
    public void onInputClosed(Channel channel, int closeReason, int appSpecificErrorCode) {
        Log.i("WEAR", "onInputClosed: " + closeReason);
        //Si existe algun error en la recepcion
        if (closeReason != ChannelApi.ChannelListener.CLOSE_REASON_NORMAL) {
            if (!this.channelOK) {
                //Dos errores seguidos, enviamos un mensaje para intentar recivir mas ficheros

                this.channelOK = true;
                this.recivedFile.delete();
                super.onInputClosed(channel, closeReason, appSpecificErrorCode);

                new SendMessageThread(SEND_IMAGE_FILE_AGAIN, SEND_IMAGE_FILE_AGAIN).start();
                return;
            } else {

                this.channelOK = false;
                this.recivedFile.delete();
                super.onInputClosed(channel, closeReason, appSpecificErrorCode);

                new SendMessageThread(SEND_IMAGE_FILE_AGAIN, SEND_IMAGE_FILE_AGAIN).start();
                return;
            }
        }
        //En caso de que la recepcion del fichero sea correcta
        this.channelOK = true;

        Log.i("WEAR", "Fichero recibido");

        //Prepacion del fichero para su procesado
        if (this.recivedFile != null) {
            Log.i("WEAR", "Nombre: " + this.recivedFile.getName());
            File definitive = new File(this.getApplicationContext().getFilesDir().getPath()
                    + "/regulation_files/" + this.recivedFile.getName());


            try {
                definitive.createNewFile();
                this.copy(this.recivedFile, definitive);
            } catch (IOException e) {
                Log.e("WEAR", "Error: no se ha podido copiar el fichero");
            }

            this.recivedFile.delete();
        } else {
            Log.e("FILE_ERROR_WEAR", "Fichero nulo");
            super.onInputClosed(channel, closeReason, appSpecificErrorCode);
            return;
        }

        //Enviar nuevo mensaje
        super.onInputClosed(channel, closeReason, appSpecificErrorCode);
        new SendMessageThread(SEND_NEXT_IMAGE_FILE, SEND_NEXT_IMAGE_FILE).start();
    }
}

