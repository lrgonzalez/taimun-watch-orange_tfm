package com.orange.taimun_watch.es.SensoresYMedidas;

/**
 * Clase para las medidas de los sensores
 *
 * @author Javier Cuadrado Manso
 */
public class Measure {
    public long timestamp;
    public int accuracy;
    private boolean discret;
    public float x;
    public float y;
    public float z;

    /**
     * Constructor de la medida del pulsometro y detector de pasos
     *
     * @param t  tiempo en milisegundos
     * @param a  precision
     * @param xm medida x (pulso o pasos)
     */
    public Measure(long t, int a, float xm) {
        this.timestamp = t;
        this.accuracy = a;
        this.x = xm;
        this.discret = true;
    }

    /**
     * Constructor de la medida de los sensores del acelerometro y giroscopio
     *
     * @param t  tiempo en milisegundos
     * @param a  precision
     * @param xm medida x
     * @param ym medida y
     * @param zm medida z
     */
    public Measure(long t, int a, float xm, float ym, float zm) {
        this.timestamp = t;
        this.accuracy = a;
        this.x = xm;
        this.y = ym;
        this.z = zm;
        this.discret = false;
    }

    @Override
    public String toString() {
        if(discret == false) {
            return timestamp + "\t" + this.accuracy + "\t" + this.x + "\t" + this.y + "\t" + this.z;
        } else {
            return timestamp + "\t" + this.accuracy + "\t" + this.x;
        }
    }
}
