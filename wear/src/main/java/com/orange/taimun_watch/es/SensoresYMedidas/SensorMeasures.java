package com.orange.taimun_watch.es.SensoresYMedidas;

import java.util.ArrayList;

/**
 * Clase que contiene un historico de las medidas tomadas por el sensor
 *
 * @author Javier Cuadrado Manso
 */
public class SensorMeasures {
    public ArrayList<Measure> medidas;

    public SensorMeasures() {
        this.medidas = new ArrayList<>();
    }

    public SensorMeasures (ArrayList<Measure> m){
        this.medidas = (ArrayList<Measure>) m.clone();
    }

    public ArrayList<Long> getTimestamps() {
        ArrayList<Long> tmp = new ArrayList<>();
        for (Measure m : this.medidas)
            tmp.add(m.timestamp);
        return tmp;
    }

    public ArrayList<Integer> getAccuracies() {
        ArrayList<Integer> tmp = new ArrayList<>();
        for (Measure m : this.medidas)
            tmp.add(m.accuracy);
        return tmp;
    }

    public ArrayList<Double> getXs() {
        ArrayList<Double> tmp = new ArrayList<>();
        for (Measure m : this.medidas)
            tmp.add((double) m.x);
        return tmp;
    }

    public ArrayList<Double> getYs() {
        ArrayList<Double> tmp = new ArrayList<>();
        for (Measure m : this.medidas)
            tmp.add((double) m.y);
        return tmp;
    }

    public ArrayList<Double> getZs() {
        ArrayList<Double> tmp = new ArrayList<>();
        for (Measure m : this.medidas)
            tmp.add((double) m.z);
        return tmp;
    }
}
