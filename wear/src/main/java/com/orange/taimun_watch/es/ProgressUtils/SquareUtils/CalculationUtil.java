package com.orange.taimun_watch.es.ProgressUtils.SquareUtils;

import android.content.Context;
import android.util.TypedValue;

/**
 * Clase auxiliar que convierte la medida en dps a pixeles
 *
 * @author Javier Cuadrado Manso
 */
public class CalculationUtil {

	/**
	 * Funcion que convierte dp en pixeles
	 * @param dp
	 * @param context
     * @return
     */
	public static int convertDpToPx(float dp, Context context) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				context.getResources().getDisplayMetrics());
	}
}
