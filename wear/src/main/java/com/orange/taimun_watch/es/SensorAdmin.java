package com.orange.taimun_watch.es;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;

import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import com.orange.taimun_watch.es.SensoresYMedidas.AccelerometerSensor;
import com.orange.taimun_watch.es.SensoresYMedidas.GyroscopeSensor;
import com.orange.taimun_watch.es.SensoresYMedidas.HeartRateSensor;
import com.orange.taimun_watch.es.SensoresYMedidas.SensorMeasures;
import com.orange.taimun_watch.es.SensoresYMedidas.StepDetectorSensor;
import com.orange.taimun_watch.es.SensoresYMedidas.StepCounterSensor;
import com.orange.taimun_watch.es.utils.SensorLog;

/**
 * Clase que administra todos los sensores.
 *
 * Implementa Observer para saber cuando se han producido cambios en los sensores
 *
 * @author Javier Cuadrado Manso
 */
public class SensorAdmin implements Observer {
    private static SensorAdmin instance = null;

    private SensorManager mSensorManager;
    private HeartRateSensor heartSensor = null;
    private AccelerometerSensor acSensor = null;
    private GyroscopeSensor girSensor = null;
    private StepDetectorSensor stepDetector = null;
    private StepCounterSensor stepCounter = null;
    private Context ctx;

    private static Calendar inicioVentanaTiempo;
    //VENTANA 30s.
    private static int SEGUNDOS_VENTANA_TIEMPO = 30;

    private PowerManager.WakeLock wakeLock;

    /**
     * Funcion que evita que haya varias instancias de la misma clase
     *
     * @param context
     * @return
     */
    public static SensorAdmin getInstance(Context context) {
        if (instance == null) {
            instance = new SensorAdmin(context);
        }
        return instance;
    }


    /**
     * Constructor de la clase. Inicializa todos los sensores
     *
     * @param context Context
     */
    private SensorAdmin(Context context) {
        this.ctx = context;
        SensorAdmin.resetTimeWindow();
        this.mSensorManager = ((SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE));

        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "SensorAdmin");
        wakeLock.acquire();
    }

    /**
     * Funcion que inicializa los sensores si existen en el reloj
     */
    private void createSensors() {
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE) != null)
            this.heartSensor = new HeartRateSensor(mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE), this.ctx);
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
            this.acSensor = new AccelerometerSensor(mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), this.ctx);
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null)
            this.girSensor = new GyroscopeSensor(mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), this.ctx);
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null)
            this.stepDetector = new StepDetectorSensor(mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR), this.ctx);
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) != null) {
            this.stepCounter = new StepCounterSensor(mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER), this.ctx);
        }
    }

    /**
     * @return
     */
    public HeartRateSensor getHeartSensor() {
        return heartSensor;
    }

    /**
     * @return
     */
    public AccelerometerSensor getAcSensor() {
        return acSensor;
    }

    /**
     * @return
     */
    public GyroscopeSensor getGirSensor() {
        return girSensor;
    }

    /**
     * @return
     */
    public StepDetectorSensor getStepDetector() {
        return stepDetector;
    }

    public StepCounterSensor getStepCounter() { return stepCounter; }
    /**
     * resetea la ventana de tiempo
     */
    public static void resetTimeWindow() {
        inicioVentanaTiempo = Calendar.getInstance();
    }

    /**
     * Registra los listeners de todos los sensores existentes
     *
     * @param sel
     */
    public void registerListeners(SensorEventListener sel) {
        this.createSensors();
        //if(this.heartSensor == null || this.acSensor == null || this.girSensor == null || this.getStepDetector() == null)
        //Log.e("SensorAdmin", "ERROR: LOS SENSORES BASE NO FUNCIONAN");//TEMPORAL!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if (this.heartSensor != null)
            mSensorManager.registerListener(sel, this.heartSensor.getSensor(), 3);
        if (this.acSensor != null)
            mSensorManager.registerListener(sel, this.acSensor.getSensor(), 3);
        if (this.girSensor != null)
            mSensorManager.registerListener(sel, this.girSensor.getSensor(), 3);
        if (this.stepDetector != null)
            mSensorManager.registerListener(sel, this.stepDetector.getSensor(), 3);
        if (this.stepCounter != null)
            mSensorManager.registerListener(sel, this.stepCounter.getSensor(), 2);
    }

    /**
     * Elimina los listeners de todos los sensores
     *
     * @param sel
     */
    public void unregisterListeners(SensorEventListener sel) {
        this.mSensorManager.unregisterListener(sel);

        try {
            wakeLock.release();
        } catch (Exception e) {

        }
    }

    /*Sensor Listener*/
    /**
     * Funcion que lee las medidas de los sensores y las imprime por pantalla.
     * Tambien se guarda esta medida junto al timestamp en un fichero de texto.
     * En esta funcion no hace falta comprovar la existencia de los sensores puesto
     * que nunca va a llegar un evento de un sensor que no posea el reloj
     *
     * @param sensorEvent
     */
    public void onSensorChanged(SensorEvent sensorEvent) {

        try {
            //latch.await();
            if (sensorEvent.sensor.getType() == Sensor.TYPE_HEART_RATE) {
                if (sensorEvent.values[0] > 0) {
                    this.getHeartSensor().setNewMeasures(sensorEvent.accuracy, sensorEvent.values[0]);
                }
            } else if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                this.getAcSensor().setNewMeasures(sensorEvent.accuracy, sensorEvent.values[0],
                        sensorEvent.values[1], sensorEvent.values[2]);
            } else if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
                this.getGirSensor().setNewMeasures(sensorEvent.accuracy, sensorEvent.values[0],
                        sensorEvent.values[1], sensorEvent.values[2]);
            } else if (sensorEvent.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
                this.getStepDetector().setNewMeasures(sensorEvent.accuracy, sensorEvent.values[0]);
            } else if (sensorEvent.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
                this.getStepCounter().setNewMeasures(sensorEvent.accuracy, sensorEvent.values[0]);
                Log.d("PASOS", "Pasos: " + sensorEvent.values[0]);
            }


        } catch (Exception e) {
            Log.e("ERROR", e.getMessage(), e);
        } /*catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public void clearMeasures() {
        if (this.getAcSensor() != null)
            this.getAcSensor().clearMeasures();
        if (this.getGirSensor() != null)
            this.getGirSensor().clearMeasures();
        if (this.getHeartSensor() != null)
            this.getHeartSensor().clearMeasures();
        if (this.getStepDetector() != null)
            this.getStepDetector().clearMeasures();
        if(this.getStepCounter() != null)
            this.getStepCounter().clearMeasures();
    }

    /*Observer*/

    /**
     * @param observable
     * @param data
     */
    @Override
    public void update(Observable observable, Object data) {
        if (Calendar.getInstance().getTimeInMillis() - inicioVentanaTiempo.getTimeInMillis()
                >= SEGUNDOS_VENTANA_TIEMPO * 1000) {

            SensorMeasures acMeasures = null;
            SensorMeasures gyrMeasures = null;
            SensorMeasures hrMeasures = null;
            SensorMeasures stMeasures = null;
            SensorMeasures scMeasures = null;

            final String watchId = Settings.Secure.getString(ctx.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            if (this.getAcSensor() != null) {
                acMeasures = this.getAcSensor().getMeasures();
                final SensorMeasures acm = new SensorMeasures(acMeasures.medidas);
                acMeasures = acm;
                Log.d("Sensors", "Acelerometro " + acMeasures.toString());
                new Thread(new Runnable() {
                    public void run() {
                        SensorLog.writeSensorLog(ctx, "acelerometro", watchId, acm);
                    }
                }).start();
            }

            if (this.getGirSensor() != null) {
                gyrMeasures = this.getGirSensor().getMeasures();
                final SensorMeasures gym = new SensorMeasures(gyrMeasures.medidas);
                gyrMeasures = gym;
                Log.d("Sensors", "Giroscopio " + gyrMeasures.toString());
                new Thread(new Runnable() {
                    public void run() {
                        SensorLog.writeSensorLog(ctx, "giroscopio", watchId, gym);
                    }
                }).start();
            }

            if (this.getHeartSensor() != null) {
                hrMeasures = this.getHeartSensor().getMeasures();
                final SensorMeasures hrm = new SensorMeasures(hrMeasures.medidas);
                hrMeasures = hrm;
                Log.d("Sensors", "Corazon " + hrMeasures.toString());
                new Thread(new Runnable() {
                    public void run() {
                        SensorLog.writeSensorLog(ctx, "pulsometro", watchId, hrm);
                    }
                }).start();
            }

            if (this.getStepDetector() != null) {
                stMeasures = this.getStepDetector().getMeasures();
                final SensorMeasures stm = new SensorMeasures(stMeasures.medidas);
                stMeasures = stm;
                Log.d("Sensors", "Pasos (detector) " + stMeasures.toString());
                new Thread(new Runnable() {
                    public void run() {
                        SensorLog.writeSensorLog(ctx, "detectorPasos", watchId, stm);
                    }
                }).start();
            }

            /*if (this.getStepCounter() != null) {
                scMeasures = this.getStepCounter().getMeasures();
                final SensorMeasures scm = new SensorMeasures(scMeasures.medidas);
                scMeasures = scm;
                Log.d("Sensor", "Pasos (counter) " + scMeasures.toString());
                new Thread(new Runnable() {
                    public void run() {
                        SensorLog.writeSensorLog(ctx, "contadorPasos", watchId, scm);
                    }
                }).start();

            }*/

            HiloCalculadorClasificador hcc = new HiloCalculadorClasificador(
                    acMeasures, gyrMeasures, hrMeasures, stMeasures, scMeasures, this.ctx);
            hcc.run();

            //Limpieza de las medidas para la nueva ventana de tiempo
            this.clearMeasures();

            //Inicio de la nueva ventana de tiempo
            resetTimeWindow();
        }
    }
}
