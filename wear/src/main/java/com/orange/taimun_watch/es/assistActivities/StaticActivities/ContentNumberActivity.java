package com.orange.taimun_watch.es.assistActivities.StaticActivities;

import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;

@Deprecated
public class ContentNumberActivity extends AssistActivity {
    //protected Strategy strategy;
    //protected Step step;


    private String source = null;
    private String text;
    private TextView tv;
    private RelativeLayout rl;

    /**
     * Funcion que crea la actividad
     *
     * @param savedInstanceState Instancia del estado guardado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_number);

        /*Log.i("ContentNumberActivity", "onCreate");

        while (source == null)
            try {
                this.source = this.regulation.getActualStrategy().getActualStep().getSource(0).getSource();
            } catch (NullPointerException e) {

            }

        tv = (TextView) findViewById(R.id.MainContentCNA);
        tv.setText(source);

        rl = (RelativeLayout) findViewById(R.id.numberLayout);
        rl.setBackgroundColor(Color.parseColor(this.regulation.getActualStrategy().getActualStep().getBackgroudColor()));
        if (Color.parseColor(this.regulation.getActualStrategy().getActualStep().getBackgroudColor()) == Color.WHITE)
            tv.setTextColor(Color.BLACK);
        else
            tv.setTextColor(Color.WHITE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        PreferenciasTTT.setInitAssistKey(getApplicationContext(), false);
        */

        Log.e("Number", "Error, se ha cargado un numero");
    }

/*
    *//**
     * En caso de que el reloj fuese puesto a cargar, se volvera a la actividad principal
     *//*
    @Override
    public void onResume() {
        super.onResume();
        Log.i("ContentNumberActivity", "onResume");
        PreferenciasTTT.setAssistOnScreenKey(getApplicationContext(), true);
        timer = System.currentTimeMillis();
        if (!PreferenciasTTT.getAssist(getApplicationContext())) {
            new BackgroundTasks().execute();
        }
        PreferenciasTTT.setAssist(getApplicationContext(), true);
        //PreferenciasTTT.setNewActivity(getApplicationContext(), false);
    }

    private class BackgroundTasks extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Log.i("ContentNumber", "Launch background");
            while (PreferenciasTTT.getAssist(getApplicationContext())) {
                Log.i("ContentNumber", "running background");
                *//*Log.i("ContentNumberActivity", "backgroundtask "
                        + PreferenciasTTT.getAssist(getApplicationContext()) + " "
                        + PreferenciasTTT.getInitAssistKey(getApplicationContext()) + " "
                        + PreferenciasTTT.getAssistOnScreen(getApplicationContext()) + " "
                        + PreferenciasTTT.getLaunchApp(getApplicationContext()));*//*

                //En caso de que se ponga a cargar
                if (PreferenciasTTT.getCharging(getApplicationContext())) {
                    Log.i("ContentNumberActivity", "CHARGING!!!!!!!!!!!!!!!!!!!");
                    PreferenciasTTT.setCharging(getApplicationContext(), false);
                    PreferenciasTTT.setAssist(getApplicationContext(), false);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Intent i = new Intent(AssistActivity.this, MainActivity.class);
                            //i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            //startActivity(i);
                            finish();
                            //Thread.currentThread().interrupt();
                        }
                    });
                    return null;
                }

                //Si la aplicacion no esta en pantalla y se detecta que
                //se sigue en crisis, intentará vover a abrir la actividad.
                //HABRA QUE CAMBIAR ESTA CONDICION PARA QUE SE ABRA SI O SI!!!!!
                //HABRA QUE REINICIAR EL TIEMPO DEL CONTADOR SI LO HAY
                if (!PreferenciasTTT.getAssistOnScreen(getApplicationContext())
                *//**&& PreferenciasTTT.getLaunchApp(getApplicationContext())**//*) {
                    Log.i("ContentNumberActivity", "restarting app");
                    PreferenciasTTT.setLaunchApp(getApplicationContext(), false);
                    PreferenciasTTT.setAssist(getApplicationContext(), false);

                    Intent i = new Intent(ContentNumberActivity.this, ContentNumberActivity.class);
                    i.putExtra("regulation", regulation);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    cancel(true);
                    finish();

                    //Thread.currentThread().interrupt();
                    return null;
                }

                if (!PreferenciasTTT.getInitAssistKey(getApplicationContext())
                        && PreferenciasTTT.getAssistOnScreen(getApplicationContext())) {
                    //Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    //v.vibrate(2000);
                }


                //Condicion de paso siguiente
                //1- Que este en pantalla
                //2- que el tiempo halla pasado y/o que se halla realizado el gesto
                if (PreferenciasTTT.getAssistOnScreen(getApplicationContext())
                        && (getMillisecondsCounter() != -1
                        && (timer + getMillisecondsCounter()) <= System.currentTimeMillis())
                        || (hasGesture() && touched)) {

                    //loadNextStep();
                    //AQUI HABRA QUE VER SI HAY O NO PASO
//Log.i("KKKKKK", "cond: " + timer + " " + getMillisecondsCounter() + " " + System.currentTimeMillis());

                    touched = false;

                    Intent i = null;
                    Step s = regulation.getNextStep();
                    if (s == null) {
                        PreferenciasTTT.setNewActivity(getApplicationContext(), true);
                        i = new Intent(ContentNumberActivity.this, MainActivity.class);
                        PreferenciasTTT.setAssist(getApplicationContext(), false);

                        i.putExtra("regulation", regulation);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                        Thread.currentThread().interrupt();
                        return null;
                    } else if (s.getStepType().equals("ContenidoNumero")) {
                        timer = System.currentTimeMillis();
                        final String newText = s.getSource(0).getSource();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setText(newText);
                            }
                        });
                    } else if (s.getStepType().equals("ContenidoImagen")) {
                        PreferenciasTTT.setNewActivity(getApplicationContext(), true);
                        PreferenciasTTT.setAssist(getApplicationContext(), false);
                        i = new Intent(ContentNumberActivity.this, ContentPictureActivity.class);

                        i.putExtra("regulation", regulation);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        finish();
                        Thread.currentThread().interrupt();
                        return null;
                    } else if (s.getStepType().equals("SelectorM")) {
                        PreferenciasTTT.setNewActivity(getApplicationContext(), true);
                        PreferenciasTTT.setAssist(getApplicationContext(), false);
                        i = new Intent(ContentNumberActivity.this, MultipleSelectorActivity.class);

                        i.putExtra("regulation", regulation);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        finish();
                        Thread.currentThread().interrupt();
                        return null;
                    } else if (s.getStepType().equals("ContenidoGif")) {
                        PreferenciasTTT.setNewActivity(getApplicationContext(), true);
                        PreferenciasTTT.setAssist(getApplicationContext(), false);
                        i = new Intent(ContentNumberActivity.this, GifActivity.class);

                        i.putExtra("regulation", regulation);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        finish();
                        Thread.currentThread().interrupt();
                        return null;
                    } else if (s.getStepType().equals("Carrusel")) {
                        PreferenciasTTT.setNewActivity(getApplicationContext(), true);
                        PreferenciasTTT.setAssist(getApplicationContext(), false);
                        i = new Intent(ContentNumberActivity.this, CarruselActivity.class);

                        i.putExtra("regulation", regulation);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        finish();
                        Thread.currentThread().interrupt();
                        return null;
                    } else if (s.getStepType().equals("Selector2")) {
                        PreferenciasTTT.setNewActivity(getApplicationContext(), true);
                        PreferenciasTTT.setAssist(getApplicationContext(), false);
                        i = new Intent(ContentNumberActivity.this, ConfirmationActivity.class);

                        i.putExtra("regulation", regulation);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        finish();
                        Thread.currentThread().interrupt();
                        return null;
                    }
                }

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                PreferenciasTTT.setNewActivity(getApplicationContext(), false);
            }

            return null;
        }

        protected void onPostExecute(Long result) {
            Log.i("ContentNumberActivitybc", "onPostExcute");
        }

    }*/
}
