package com.orange.taimun_watch.es.assistActivities.TimeActivities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.support.wearable.view.DotsPageIndicator;
import android.support.wearable.view.GridPagerAdapter;
import android.support.wearable.view.GridViewPager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import com.orange.taimun_watch.es.MainActivity;
import com.orange.taimun_watch.es.ProgressUtils.CircleProgress;
import com.orange.taimun_watch.es.ProgressUtils.DonutProgress;
import com.orange.taimun_watch.es.ProgressUtils.SquareProgress;
import com.orange.taimun_watch.es.ProgressUtils.SquareProgressBar;
import com.orange.taimun_watch.es.R;
import com.orange.taimun_watch.es.assistActivities.AssistActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.ConfirmationActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.ContentPictureActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.GifActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.MultipleSelectorActivity;
import com.orange.taimun_watch.es.assistActivities.StaticActivities.PositiveReinforcementActivity;
import com.orange.taimun_watch.es.utils.EventLog;
import com.orange.taimun_watch.es.utils.EventRecord;
import com.orange.taimun_watch.es.utils.Preferences;
import com.orange.taimun_watch.es.utils.ShapeWear;

/**
 * Clase que administra la logica del carrusel
 *
 * @author Javier Cuadrado Manso
 */
public class CarruselActivity extends AssistActivity {

    private ArrayList<Bitmap> imagesCarr = new ArrayList<>();

    private DonutProgress donutProgress;
    private CircleProgress circleProgress;
    private SquareProgressBar squareProgressBar;
    private SquareProgress squareProgress;

    private int progress = 0;
    private Handler progressUpdater;

    private int timerType;

    private CountDownTimer cdt;

    private int shape;

    /**
     * Funcion que carga la interfaz del carrusel
     *
     * @param savedInstanceState Instancia del estado guardado
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("CarruselActivity", "onCreate");

        //Carga de la forma del reloj, para modificar la forma del contador de tiempo
        ShapeWear.setOnShapeChangeListener(new ShapeWear.OnShapeChangeListener() {
            @Override
            public void shapeDetected(ShapeWear.ScreenShape screenShape) {
                //Log.d("MSA", "shapeDetected isRound:" + screenShape);
                //Log.d("MSA", "shapeDetected getShape:" + ShapeWear.getShape());
                if (screenShape == ShapeWear.ScreenShape.ROUND ||
                        screenShape == ShapeWear.ScreenShape.MOTO_ROUND)
                    shape = 0;
                else
                    shape = 1;
            }
        });
        ShapeWear.setOnSizeChangeListener(new ShapeWear.OnSizeChangeListener() {
            @Override
            public void sizeDetected(int widthPx, int heightPx) {
                //Log.d("MSA", "sizeDetected w:" + widthPx + ", h:" + heightPx);
            }
        });
        ShapeWear.initShapeWear(this);

        /*Obtencion del tipo de contador*/
        timerType = this.regulation.getActualStrategy().getTimerType();

        /*Cargar la interfaz correspondiente*/
        WatchViewStub stub = null;
        if (timerType == 1) {
            setContentView(R.layout.activity_carrusel_progressbar);
            stub = (WatchViewStub) findViewById(R.id.watch_view_stub_time);
        } else {
            setContentView(R.layout.activity_carrusel_fill);
            stub = (WatchViewStub) findViewById(R.id.watch_view_stub_time_fill);
            if (timerType == 3)
                progress = 100;
        }
        final WatchViewStub finalstub = stub;

        //Cargar imagenes de carrusel
        loadCarrusel();

        final Context ctx = getApplicationContext();

        //Preparacion del contador
        finalstub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {

                //Obtencion del elemento de progreso
                if (shape == 0) {
                    if (timerType == 1)
                        donutProgress = (DonutProgress) findViewById(R.id.donut_progress);
                    else
                        circleProgress = (CircleProgress) findViewById(R.id.circle_progress);
                } else {
                    if (timerType == 1) {
                        squareProgressBar = (SquareProgressBar) findViewById(R.id.sprogressbar);
                        squareProgressBar.setWidth(10);
                        squareProgressBar.setColor("#009dff");
                    } else {
                        squareProgress = (SquareProgress) findViewById(R.id.square_progress);
                    }
                }

                //final Resources res = getResources();
                final GridViewPager pager = (GridViewPager) findViewById(R.id.pager);

                //Asignacion del adaptador al marcador de paginas
                pager.setAdapter(new ImageAdapter(ctx));

                DotsPageIndicator dotsPageIndicator = (DotsPageIndicator) findViewById(R.id.page_indicator);
                dotsPageIndicator.setPager(pager);
                dotsPageIndicator.setOnPageChangeListener(new GridViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, int i1, float v, float v1, int i2, int i3) {
                        //Log.i("KKKKKKK", "onPageScrolled");
                    }

                    @Override
                    public void onPageSelected(int i, int i1) {
                        EventLog.addRecord(new EventRecord(EventRecord.CARRUSEL_CAMBIO));
                        //Log.i("KKKKKKK", "onPageSelected");
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {
                        if (i == GridViewPager.SCROLL_STATE_DRAGGING)
                            EventLog.addRecord(new EventRecord(EventRecord.SWIPE));
                    }
                });


                //Proceso para la actualizacion del progreso
                progressUpdater = new Handler();
                progressUpdater.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            final int p = progress;

                            @Override
                            public void run() {
                                if (shape == 0) {
                                    if (timerType == 1)
                                        donutProgress.setProgress(p);
                                    else
                                        circleProgress.setProgress(p);
                                } else {
                                    if (timerType == 1)
                                        squareProgressBar.setProgress(p);
                                    else {
                                        squareProgress.setProgress(p);
                                    }
                                }
                            }
                        });
                        progressUpdater.postDelayed(this, 100);
                    }
                }, 100);
            }
        });

        postExecute = new Handler();
    }

    /**
     * Funcion que obtiene las imagenes del carrusel
     */
    private void loadCarrusel() {
        int numSources = this.regulation.getActualStrategy().getSteps().size();
        for (int i = 0; i < numSources; i++) {
            String src = this.regulation.getActualStrategy().getStep(i);
            Bitmap bm = null;
            if (Preferences.DEBUG)
                bm = super.getBitmapFromAsset(src);
            else
                bm = BitmapFactory.decodeFile(getApplicationContext().getFilesDir() + "/" + src);
            if(bm == null)
                bm = super.getBitmapFromAsset("error.png");
            imagesCarr.add(bm);
        }
    }

    /**
     * Clase con parte de la logica del carrusel
     */
    public class ImageAdapter extends GridPagerAdapter {
        final Context mContext;

        public ImageAdapter(final Context context) {
            mContext = context;
        }

        @Override
        public int getRowCount() {
            return 1;
        }

        @Override
        public int getColumnCount(int i) {
            return imagesCarr.size();
        }

        //---Go to current column when scrolling up or down (instead of default column 0)---
        @Override
        public int getCurrentColumnForRow(int row, int currentColumn) {
            return currentColumn;
        }

        //---Return our car image based on the provided row and column---
        @Override
        public Object instantiateItem(ViewGroup viewGroup, int row, int col) {
            ImageView imageView;
            imageView = new ImageView(mContext);
            imageView.setImageBitmap(imagesCarr.get(col));
            imageView.setBackgroundColor(Color.rgb(236, 238, 242));
            viewGroup.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup viewGroup, int i, int i2, Object o) {
            viewGroup.removeView((View) o);
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view.equals(o);
        }
    }

    /**
     * Funcion que inicia el contador
     */
    @Override
    public void onResume() {
        super.onResume();
        /*Obtencion de la duracion del timer*/
        final long millis = this.regulation.getActualStrategy().getTime() * 1000;

        /*Inicio del timer*/
        cdt = new CountDownTimer(millis, 100) {
            public void onTick(long millisUntilFinished) {
                if (timerType == 3)
                    progress = 100 - (100 - (int) (((double) millisUntilFinished / (double) millis) * 100));
                else
                    progress = 100 - (int) (((double) millisUntilFinished / (double) millis) * 100);
            }

            public void onFinish() {
                if (timerType == 3)
                    progress = 0;
                else
                    progress = 100;
                /*En caso de que el tiempo sea del paso, al terminar se pasa al siguiente paso*/
                //if (!regulation.getActualStrategy().isGlobalTime())
                new NextStepTask().execute();
            }
        }.start();
    }

    /**
     * Funcion que restaura la actividad si se ha salido de esta
     */
    @Override
    public void onPause() {
        if (!Preferences.getNewActivity(getApplicationContext())) {
            postExecute.postDelayed(new RestoreActivity(), 1000);
            EventLog.addRecord(new EventRecord(EventRecord.SALIDA_REGULACION));
        }
        cdt.cancel();
        super.onPause();
    }

    /**
     * Clase privada que restaura la actividad
     */
    private class RestoreActivity implements Runnable {

        /**
         * Funcion que restaura la actividad
         */
        @Override
        public void run() {
            //Log.i("ContentPictureActivity", "RestoreActivity");

            //Si el reloj no se esta cargando, se volvera a abrir la actividad.
            if (!Preferences.getCharging(getApplicationContext())) {
                Log.i("CarruselActivity", "restarting app");
                Preferences.setLaunchApp(getApplicationContext(), false);

                Intent i = new Intent(CarruselActivity.this, CarruselActivity.class);
                //Para las cuentras atras, se reinicia la estrategia para volver a empezar la cuenta
                if (regulation.getActualStrategy().isGlobalTime()) {
                    regulation.getActualStrategy().restartStrategy();
                    regulation.getActualStrategy().nextStep();
                }

                i.putExtra("regulation", regulation);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                //Thread.currentThread().interrupt();
                return;
            }
        }
    }

    /**
     * Clase privada que avanza al paso siguiente
     */
    private class NextStepTask extends AsyncTask {

        /**
         * Funcion encargada de avanzar al paso siguiente
         *
         * @param params null
         * @return null
         */
        @Override
        protected Object doInBackground(Object[] params) {
            Log.i("CarruselActivity", "NextStepTask");

            touched = false;
            Intent i = null;

            //La estrategia ha terminado y se comprueba si tiene una pregunta final
            if (regulation.getActualStrategy().getQuestion() != false) {
                EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Pregunta"));
                i = new Intent(CarruselActivity.this, ConfirmationActivity.class);
            } else if (regulation.getActualStrategy().hasReinforcement()
                    && !Preferences.getIsPositiveLastWindow(getApplicationContext())) {
                EventLog.addRecord(new EventRecord(EventRecord.REFUERZO_POSITIVO));
                i = new Intent(CarruselActivity.this, PositiveReinforcementActivity.class);
            } else {
                //Si era una estrategia escogida desde un selector, se pone la opcion a -1
                //para indicar que se vuelve al array de estrategias
                regulation.setOption(-1);
                //Si no tiene pregunta, se pasa a la siguiente estrategia
                //Si no tiene siguiente estrategia, la regulacion ha terminado
                if (!regulation.nextStrategy()) {
                    EventLog.addRecord(new EventRecord(EventRecord.FIN_REGULACION));
                    EventLog.writeEventLog(getApplicationContext(),
                            Settings.Secure.getString(getApplicationContext().getContentResolver(),
                                    Settings.Secure.ANDROID_ID));
                    Preferences.setEndRegulation(getApplicationContext(), true);
                    i = new Intent(CarruselActivity.this, MainActivity.class);
                }
                //En caso contrario, se comprueba si esta estrategia es simple o un selector
                else {
                    EventLog.addRecord(new EventRecord(EventRecord.INICIO_ESTRATEGIA + regulation.getActualStrategy().getName()));
                    if (regulation.isSelector()) {
                        EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Selector"));
                        i = new Intent(CarruselActivity.this, MultipleSelectorActivity.class);
                    } else {
                        //Si es simple, se reinicia y se comprueba que tiene pasos
                        regulation.getActualStrategy().restartStrategy();
                        if (regulation.getActualStrategy().isCarrusel()) {
                            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + "Carrusel"));
                            i = new Intent(CarruselActivity.this, CarruselActivity.class);
                        } else if (!regulation.getActualStrategy().nextStep()) {
                            Log.e("CarruselActivity", "Error: La estrategia " +
                                    regulation.getActualStrategy().getId() + " no tiene pasos");
                            i = new Intent(CarruselActivity.this, MainActivity.class);
                        } else {
                            //Si tiene pasos, al coger el primero se clasifica
                            i = getIntentStep();
                            if (i == null) return null;
                        }
                    }
                }
            }

            Preferences.setNewActivity(getApplicationContext(), true);
            i.putExtra("regulation", regulation);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
            Thread.currentThread().interrupt();
            return null;
        }

        /**
         * Funcion que devuelve la siguiente actividad
         *
         * @return Intent de la siguiente actividad
         */
        private Intent getIntentStep() {
            EventLog.addRecord(new EventRecord(EventRecord.INICIO_PASO + regulation.getActualStrategy().getActualStep()));
            String s = regulation.getActualStrategy().getActualStep();
            int time = regulation.getActualStrategy().getTime();
            switch (AssistActivity.getStepType(s, time)) {
                case AssistActivity.STATIC_GIF:
                    return new Intent(CarruselActivity.this, GifActivity.class);
                case AssistActivity.STATIC_PICTURE:
                    return new Intent(CarruselActivity.this, ContentPictureActivity.class);
                case AssistActivity.TIME_PICTURE:
                    return new Intent(CarruselActivity.this, TimePicture.class);
                case AssistActivity.TIME_GIF:
                    return new Intent(CarruselActivity.this, TimeGif.class);
                default:
                    Log.e("TimePicture", "Error: La estretagia esta mal formada");
                    return new Intent(CarruselActivity.this, MainActivity.class);
            }
        }
    }
}
