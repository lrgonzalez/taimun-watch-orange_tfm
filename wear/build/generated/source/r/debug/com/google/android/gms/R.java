/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.android.gms;

public final class R {
    public static final class attr {
        public static final int allowShortcuts = 0x7f040001;
        public static final int buttonSize = 0x7f04001a;
        public static final int circleCrop = 0x7f04001b;
        public static final int colorScheme = 0x7f04002f;
        public static final int contentProviderUri = 0x7f040031;
        public static final int corpusId = 0x7f040032;
        public static final int corpusVersion = 0x7f040033;
        public static final int defaultIntentAction = 0x7f040034;
        public static final int defaultIntentActivity = 0x7f040035;
        public static final int defaultIntentData = 0x7f040036;
        public static final int documentMaxAgeSecs = 0x7f04003a;
        public static final int featureType = 0x7f040060;
        public static final int imageAspectRatio = 0x7f04006e;
        public static final int imageAspectRatioAdjust = 0x7f04006f;
        public static final int indexPrefixes = 0x7f040074;
        public static final int inputEnabled = 0x7f040075;
        public static final int noIndex = 0x7f040090;
        public static final int paramName = 0x7f04009e;
        public static final int paramValue = 0x7f04009f;
        public static final int perAccountTemplate = 0x7f0400a2;
        public static final int schemaOrgProperty = 0x7f0400ac;
        public static final int schemaOrgType = 0x7f0400ad;
        public static final int scopeUris = 0x7f0400ae;
        public static final int searchEnabled = 0x7f0400b0;
        public static final int searchLabel = 0x7f0400b1;
        public static final int sectionContent = 0x7f0400b2;
        public static final int sectionFormat = 0x7f0400b3;
        public static final int sectionId = 0x7f0400b4;
        public static final int sectionType = 0x7f0400b5;
        public static final int sectionWeight = 0x7f0400b6;
        public static final int semanticallySearchable = 0x7f0400b7;
        public static final int settingsDescription = 0x7f0400b8;
        public static final int sourceClass = 0x7f0400bf;
        public static final int subsectionSeparator = 0x7f0400c3;
        public static final int toAddressesSection = 0x7f0400ca;
        public static final int trimmable = 0x7f0400cb;
        public static final int userInputSection = 0x7f0400cd;
        public static final int userInputTag = 0x7f0400ce;
        public static final int userInputValue = 0x7f0400cf;
    }
    public static final class color {
        public static final int common_google_signin_btn_text_dark = 0x7f060012;
        public static final int common_google_signin_btn_text_dark_default = 0x7f060013;
        public static final int common_google_signin_btn_text_dark_disabled = 0x7f060014;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f060015;
        public static final int common_google_signin_btn_text_dark_pressed = 0x7f060016;
        public static final int common_google_signin_btn_text_light = 0x7f060017;
        public static final int common_google_signin_btn_text_light_default = 0x7f060018;
        public static final int common_google_signin_btn_text_light_disabled = 0x7f060019;
        public static final int common_google_signin_btn_text_light_focused = 0x7f06001a;
        public static final int common_google_signin_btn_text_light_pressed = 0x7f06001b;
        public static final int common_google_signin_btn_tint = 0x7f06001c;
    }
    public static final class drawable {
        public static final int common_full_open_on_phone = 0x7f08000a;
        public static final int common_google_signin_btn_icon_dark = 0x7f08000b;
        public static final int common_google_signin_btn_icon_dark_focused = 0x7f08000c;
        public static final int common_google_signin_btn_icon_dark_normal = 0x7f08000d;
        public static final int common_google_signin_btn_icon_dark_normal_background = 0x7f08000e;
        public static final int common_google_signin_btn_icon_disabled = 0x7f08000f;
        public static final int common_google_signin_btn_icon_light = 0x7f080010;
        public static final int common_google_signin_btn_icon_light_focused = 0x7f080011;
        public static final int common_google_signin_btn_icon_light_normal = 0x7f080012;
        public static final int common_google_signin_btn_icon_light_normal_background = 0x7f080013;
        public static final int common_google_signin_btn_text_dark = 0x7f080014;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f080015;
        public static final int common_google_signin_btn_text_dark_normal = 0x7f080016;
        public static final int common_google_signin_btn_text_dark_normal_background = 0x7f080017;
        public static final int common_google_signin_btn_text_disabled = 0x7f080018;
        public static final int common_google_signin_btn_text_light = 0x7f080019;
        public static final int common_google_signin_btn_text_light_focused = 0x7f08001a;
        public static final int common_google_signin_btn_text_light_normal = 0x7f08001b;
        public static final int common_google_signin_btn_text_light_normal_background = 0x7f08001c;
        public static final int googleg_disabled_color_18 = 0x7f08001f;
        public static final int googleg_standard_color_18 = 0x7f080020;
    }
    public static final class id {
        public static final int adjust_height = 0x7f0a0009;
        public static final int adjust_width = 0x7f0a000a;
        public static final int auto = 0x7f0a000f;
        public static final int center = 0x7f0a0017;
        public static final int contact = 0x7f0a0020;
        public static final int dark = 0x7f0a0021;
        public static final int date = 0x7f0a0023;
        public static final int demote_common_words = 0x7f0a0024;
        public static final int demote_rfc822_hostnames = 0x7f0a0025;
        public static final int email = 0x7f0a0029;
        public static final int html = 0x7f0a0036;
        public static final int icon_only = 0x7f0a0039;
        public static final int icon_uri = 0x7f0a003a;
        public static final int index_entity_types = 0x7f0a003e;
        public static final int instant_message = 0x7f0a0040;
        public static final int intent_action = 0x7f0a0041;
        public static final int intent_activity = 0x7f0a0042;
        public static final int intent_data = 0x7f0a0043;
        public static final int intent_data_id = 0x7f0a0044;
        public static final int intent_extra_data = 0x7f0a0045;
        public static final int large_icon_uri = 0x7f0a0048;
        public static final int light = 0x7f0a004a;
        public static final int match_global_nicknames = 0x7f0a004d;
        public static final int none = 0x7f0a0051;
        public static final int normal = 0x7f0a0052;
        public static final int omnibox_title_section = 0x7f0a0057;
        public static final int omnibox_url_section = 0x7f0a0058;
        public static final int plain = 0x7f0a005d;
        public static final int rfc822 = 0x7f0a005e;
        public static final int standard = 0x7f0a006c;
        public static final int text = 0x7f0a006f;
        public static final int text1 = 0x7f0a0070;
        public static final int text2 = 0x7f0a0071;
        public static final int thing_proto = 0x7f0a0072;
        public static final int url = 0x7f0a0076;
        public static final int wide = 0x7f0a0093;
    }
    public static final class integer {
        public static final int google_play_services_version = 0x7f0b0007;
    }
    public static final class string {
        public static final int common_google_play_services_enable_button = 0x7f100020;
        public static final int common_google_play_services_enable_text = 0x7f100021;
        public static final int common_google_play_services_enable_title = 0x7f100022;
        public static final int common_google_play_services_install_button = 0x7f100023;
        public static final int common_google_play_services_install_text = 0x7f100024;
        public static final int common_google_play_services_install_title = 0x7f100025;
        public static final int common_google_play_services_notification_channel_name = 0x7f100026;
        public static final int common_google_play_services_notification_ticker = 0x7f100027;
        public static final int common_google_play_services_unknown_issue = 0x7f100028;
        public static final int common_google_play_services_unsupported_text = 0x7f100029;
        public static final int common_google_play_services_update_button = 0x7f10002a;
        public static final int common_google_play_services_update_text = 0x7f10002b;
        public static final int common_google_play_services_update_title = 0x7f10002c;
        public static final int common_google_play_services_updating_text = 0x7f10002d;
        public static final int common_google_play_services_wear_update_text = 0x7f10002e;
        public static final int common_open_on_phone = 0x7f10002f;
        public static final int common_signin_button_text = 0x7f100030;
        public static final int common_signin_button_text_long = 0x7f100031;
    }
    public static final class styleable {
        public static final int[] AppDataSearch = { };
        public static final int[] Corpus = { 0x7f040031, 0x7f040032, 0x7f040033, 0x7f04003a, 0x7f0400a2, 0x7f0400ad, 0x7f0400b7, 0x7f0400cb };
        public static final int Corpus_contentProviderUri = 0;
        public static final int Corpus_corpusId = 1;
        public static final int Corpus_corpusVersion = 2;
        public static final int Corpus_documentMaxAgeSecs = 3;
        public static final int Corpus_perAccountTemplate = 4;
        public static final int Corpus_schemaOrgType = 5;
        public static final int Corpus_semanticallySearchable = 6;
        public static final int Corpus_trimmable = 7;
        public static final int[] FeatureParam = { 0x7f04009e, 0x7f04009f };
        public static final int FeatureParam_paramName = 0;
        public static final int FeatureParam_paramValue = 1;
        public static final int[] GlobalSearch = { 0x7f040034, 0x7f040035, 0x7f040036, 0x7f0400b0, 0x7f0400b1, 0x7f0400b8 };
        public static final int GlobalSearch_defaultIntentAction = 0;
        public static final int GlobalSearch_defaultIntentActivity = 1;
        public static final int GlobalSearch_defaultIntentData = 2;
        public static final int GlobalSearch_searchEnabled = 3;
        public static final int GlobalSearch_searchLabel = 4;
        public static final int GlobalSearch_settingsDescription = 5;
        public static final int[] GlobalSearchCorpus = { 0x7f040001 };
        public static final int GlobalSearchCorpus_allowShortcuts = 0;
        public static final int[] GlobalSearchSection = { 0x7f0400b2, 0x7f0400b5 };
        public static final int GlobalSearchSection_sectionContent = 0;
        public static final int GlobalSearchSection_sectionType = 1;
        public static final int[] IMECorpus = { 0x7f040075, 0x7f0400bf, 0x7f0400ca, 0x7f0400cd, 0x7f0400ce, 0x7f0400cf };
        public static final int IMECorpus_inputEnabled = 0;
        public static final int IMECorpus_sourceClass = 1;
        public static final int IMECorpus_toAddressesSection = 2;
        public static final int IMECorpus_userInputSection = 3;
        public static final int IMECorpus_userInputTag = 4;
        public static final int IMECorpus_userInputValue = 5;
        public static final int[] LoadingImageView = { 0x7f04001b, 0x7f04006e, 0x7f04006f };
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] Section = { 0x7f040074, 0x7f040090, 0x7f0400ac, 0x7f0400b3, 0x7f0400b4, 0x7f0400b6, 0x7f0400c3 };
        public static final int Section_indexPrefixes = 0;
        public static final int Section_noIndex = 1;
        public static final int Section_schemaOrgProperty = 2;
        public static final int Section_sectionFormat = 3;
        public static final int Section_sectionId = 4;
        public static final int Section_sectionWeight = 5;
        public static final int Section_subsectionSeparator = 6;
        public static final int[] SectionFeature = { 0x7f040060 };
        public static final int SectionFeature_featureType = 0;
        public static final int[] SignInButton = { 0x7f04001a, 0x7f04002f, 0x7f0400ae };
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
